
      module mod_com_starkl

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer ,parameter  :: MDMJCI = 3*MXSYM

      integer                    ,save  :: ICLRO = 0
      integer ,dimension(MDMJCI) ,save  :: JDIM  = 0
!
      real(kind=dppr) ,pointer ,dimension(:,:,:,:) ,save  :: DIP                                   ! (MXDIMS,MXDIMI,3,MXOPT)
      real(kind=dppr) ,pointer ,dimension(:)       ,save  :: H                                     ! (MXELMT)
      real(kind=dppr) ,pointer ,dimension(:)       ,save  :: HDK                                   ! (MXDIMS)
      real(kind=dppr) ,pointer ,dimension(:)       ,save  :: HDL                                   ! (MXDIMI)
      real(kind=dppr) ,pointer ,dimension(:,:)     ,save  :: HDLJ                                  ! (MXDIMI,MDMJCI)
      real(kind=dppr) ,pointer ,dimension(:,:)     ,save  :: TK                                    ! (MXDIMS,MXDIMS)
      real(kind=dppr) ,pointer ,dimension(:,:)     ,save  :: TL                                    ! (MXDIMI,MXDIMI)
      real(kind=dppr) ,pointer ,dimension(:,:,:)   ,save  :: TLJ                                   ! (MXDIMI,MXDIMI,MDMJCI)

      integer         ,pointer ,dimension(:)       ,save  :: KO,LI                                 ! (MXELMT)
      integer                                      ,save  :: ICKRO


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_STARKL
      IMPLICIT NONE

      integer  :: ierr

      allocate(DIP(MXDIMS,MXDIMI,3,MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_STARKL_DIP')
      DIP = 0.d0
      allocate(H(MXELMT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_STARKL_H')
      H = 0.d0
      allocate(HDK(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_STARKL_HDK')
      HDK = 0.d0
      allocate(HDL(MXDIMI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_STARKL_HDL')
      HDL = 0.d0
      allocate(HDLJ(MXDIMI,MDMJCI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_STARKL_HDLJ')
      HDLJ = 0.d0
      allocate(TK(MXDIMS,MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_STARKL_TK')
      TK = 0.d0
      allocate(TL(MXDIMI,MXDIMI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_STARKL_TL')
      TL = 0.d0
      allocate(TLJ(MXDIMI,MXDIMI,MDMJCI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_STARKL_TLJ')
      TLJ = 0.d0

      allocate(KO(MXELMT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_STARKL_KO')
      KO = 0
      allocate(LI(MXELMT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_STARKL_LI')
      LI = 0
!
      return
      end subroutine ALLOC_STARKL

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXDIMI

      subroutine RESIZE_MXDIMI_STARKL(C_DIMI)
      IMPLICIT NONE
      integer :: C_DIMI

      integer :: i,ierr
      integer :: j
      integer :: k

! DIP
      allocate(rpd4(MXDIMS,C_DIMI,3,MXOPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMI_STARKL_DIP')
      rpd4 = 0.d0
      do k=1,MXOPT
        do j=1,3
          do i=1,MXDIMS
            rpd4(i,1:MXDIMI,j,k) = DIP(i,:,j,k)
          enddo
        enddo
      enddo
      deallocate(DIP)
      DIP => rpd4
! HDL
      allocate(rpd1(C_DIMI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMI_STARKL_HDL')
      rpd1 = 0.d0
      rpd1(1:MXDIMI) = HDL(:)
      deallocate(HDL)
      HDL => rpd1
! HDLJ
      allocate(rpd2(C_DIMI,MDMJCI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMI_STARKL_HDLJ')
      rpd2 = 0.d0
      do i=1,MDMJCI
        rpd2(1:MXDIMI,i) = HDLJ(:,i)
      enddo
      deallocate(HDLJ)
      HDLJ => rpd2
! TL
      allocate(rpd2(C_DIMI,C_DIMI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMI_STARKL_TL')
      rpd2 = 0.d0
      do i=1,MXDIMI
        rpd2(1:MXDIMI,i) = TL(:,i)
      enddo
      deallocate(TL)
      TL => rpd2
! TLJ
      allocate(rpd3(C_DIMI,C_DIMI,MDMJCI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMI_STARKL_TLJ')
      rpd3 = 0.d0
      do j=1,MDMJCI
        do i=1,MXDIMI
          rpd3(1:MXDIMI,i,j) = TLJ(:,i,j)
        enddo
      enddo
      deallocate(TLJ)
      TLJ => rpd3
!
      return
      end subroutine RESIZE_MXDIMI_STARKL

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXDIMS

      subroutine RESIZE_MXDIMS_STARKL(C_DIMS)
      IMPLICIT NONE
      integer :: C_DIMS

      integer :: i,ierr
      integer :: j
      integer :: k

! DIP
      allocate(rpd4(C_DIMS,MXDIMI,3,MXOPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_STARKL_DIP')
      rpd4 = 0.d0
      do k=1,MXOPT
        do j=1,3
          do i=1,MXDIMI
            rpd4(1:MXDIMS,i,j,k) = DIP(:,i,j,k)
          enddo
        enddo
      enddo
      deallocate(DIP)
      DIP => rpd4
! HDK
      allocate(rpd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_STARKL_HDK')
      rpd1 = 0.d0
      rpd1(1:MXDIMS) = HDK(:)
      deallocate(HDK)
      HDK => rpd1
! TK
      allocate(rpd2(C_DIMS,C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_STARKL_TK')
      rpd2 = 0.d0
      do i=1,MXDIMS
        rpd2(1:MXDIMS,i) = TK(:,i)
      enddo
      deallocate(TK)
      TK => rpd2
!
      return
      end subroutine RESIZE_MXDIMS_STARKL

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXELMT

      subroutine RESIZE_MXELMT_STARKL(C_ELMT)
      IMPLICIT NONE
      integer :: C_ELMT

      integer :: ierr

! H
      allocate(rpd1(C_ELMT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXELMT_STARKL_H')
      rpd1 = 0.d0
      rpd1(1:MXELMT) = H(:)
      deallocate(H)
      H => rpd1
! KO
      allocate(ipd1(C_ELMT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXELMT_STARKL_KO')
      ipd1 = 0
      ipd1(1:MXELMT) = KO(:)
      deallocate(KO)
      KO => ipd1
! LI
      allocate(ipd1(C_ELMT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXELMT_STARKL_LI')
      ipd1 = 0
      ipd1(1:MXELMT) = LI(:)
      deallocate(LI)
      LI => ipd1
!
      return
      end subroutine RESIZE_MXELMT_STARKL

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPT

      subroutine RESIZE_MXOPT_STARKL(C_OPT)
      IMPLICIT NONE
      integer :: C_OPT

      integer :: i,ierr
      integer :: j
      integer :: k

! DIP
      allocate(rpd4(MXDIMS,MXDIMI,3,C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_STARKL_DIP')
      rpd4 = 0.d0
      do k=1,3
        do j=1,MXDIMI
          do i=1,MXDIMS
            rpd4(i,j,k,1:MXOPT) = DIP(i,j,k,:)
          enddo
        enddo
      enddo
      deallocate(DIP)
      DIP => rpd4
!
      return
      end subroutine RESIZE_MXOPT_STARKL

      end module mod_com_starkl
