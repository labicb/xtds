
      module mod_com_trmomt

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,pointer ,dimension(:) ,save  :: DERITR                                      ! (MXOPT)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_TRMOMT
      IMPLICIT NONE

      integer  :: ierr

      allocate(DERITR(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_TRMOMT_DERITR')
      DERITR = 0.d0
!
      return
      end subroutine ALLOC_TRMOMT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPT

      subroutine RESIZE_MXOPT_TRMOMT(C_OPT)
      IMPLICIT NONE
      integer :: C_OPT

      integer :: ierr

! DERITR
      allocate(rpd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_TRMOMT_DERITR')
      rpd1 = 0.d0
      rpd1(1:MXOPT) = DERITR(:)
      deallocate(DERITR)
      DERITR => rpd1
!
      return
      end subroutine RESIZE_MXOPT_TRMOMT

      end module mod_com_trmomt
