
      module mod_com_predlx

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr)      ,pointer ,dimension(:) ,save  :: DIFF,DIFS                              ! (MXOBS)
      real(kind=dppr)      ,pointer ,dimension(:) ,save  :: FCAO,FEXP                              ! (MXOBS)
      real(kind=dppr)      ,pointer ,dimension(:) ,save  :: SCAL,SEXP                              ! (MXOBS)

      integer              ,pointer ,dimension(:) ,save  :: IND                                    ! (MXOBS)
      integer              ,pointer ,dimension(:) ,save  :: JI,JS                                  ! (MXOBS)
      integer              ,pointer ,dimension(:) ,save  :: NINF,NSUP                              ! (MXOBS)

      character(len =   1) ,pointer ,dimension(:) ,save  :: PAINF,PASUP                            ! (MXOBS)
      character(len =   2) ,pointer ,dimension(:) ,save  :: SYINF,SYSUP                            ! (MXOBS)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_PREDLX
      IMPLICIT NONE

      integer  :: ierr

      allocate(DIFF(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PREDLX_DIFF')
      DIFF = 0.d0
      allocate(DIFS(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PREDLX_DIFS')
      DIFS = 0.d0
      allocate(FCAO(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PREDLX_FCAO')
      FCAO = 0.d0
      allocate(FEXP(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PREDLX_FEXP')
      FEXP = 0.d0
      allocate(SCAL(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PREDLX_SCAL')
      SCAL = 0.d0
      allocate(SEXP(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PREDLX_SEXP')
      SEXP = 0.d0

      allocate(IND(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PREDLX_IND')
      IND = 0
      allocate(JI(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PREDLX_JI')
      JI = 0
      allocate(JS(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PREDLX_JS')
      JS = 0
      allocate(NINF(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PREDLX_NINF')
      NINF = 0
      allocate(NSUP(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PREDLX_NSUP')
      NSUP = 0

      allocate(PAINF(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PREDLX_PAINF')
      PAINF = ''
      allocate(PASUP(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PREDLX_PASUP')
      PASUP = ''
      allocate(SYINF(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PREDLX_SYINF')
      SYINF = ''
      allocate(SYSUP(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PREDLX_SYSUP')
      SYSUP = ''
!
      return
      end subroutine ALLOC_PREDLX

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOBS

      subroutine RESIZE_MXOBS_PREDLX(C_OBS)
      IMPLICIT NONE
      integer :: C_OBS

      integer :: ierr

      character(len =   1) ,pointer ,dimension(:)  :: cpd1_1
      character(len =   2) ,pointer ,dimension(:)  :: cpd1_2

! DIFF
      allocate(rpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PREDLX_DIFF')
      rpd1 = 0.d0
      rpd1(1:MXOBS) = DIFF(:)
      deallocate(DIFF)
      DIFF => rpd1
! DIFS
      allocate(rpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PREDLX_DIFS')
      rpd1 = 0.d0
      rpd1(1:MXOBS) = DIFS(:)
      deallocate(DIFS)
      DIFS => rpd1
! FCAO
      allocate(rpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PREDLX_FCAO')
      rpd1 = 0.d0
      rpd1(1:MXOBS) = FCAO(:)
      deallocate(FCAO)
      FCAO => rpd1
! FEXP
      allocate(rpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PREDLX_FEXP')
      rpd1 = 0.d0
      rpd1(1:MXOBS) = FEXP(:)
      deallocate(FEXP)
      FEXP => rpd1
! SCAL
      allocate(rpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PREDLX_SCAL')
      rpd1 = 0.d0
      rpd1(1:MXOBS) = SCAL(:)
      deallocate(SCAL)
      SCAL => rpd1
! SEXP
      allocate(rpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PREDLX_SEXP')
      rpd1 = 0.d0
      rpd1(1:MXOBS) = SEXP(:)
      deallocate(SEXP)
      SEXP => rpd1

! IND
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PREDLX_IND')
      ipd1 = 0
      ipd1(1:MXOBS) = IND(:)
      deallocate(IND)
      IND => ipd1
! JI
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PREDLX_JI')
      ipd1 = 0
      ipd1(1:MXOBS) = JI(:)
      deallocate(JI)
      JI => ipd1
! JS
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PREDLX_JS')
      ipd1 = 0
      ipd1(1:MXOBS) = JS(:)
      deallocate(JS)
      JS => ipd1
! NINF
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PREDLX_NINF')
      ipd1 = 0
      ipd1(1:MXOBS) = NINF(:)
      deallocate(NINF)
      NINF => ipd1
! NSUP
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PREDLX_NSUP')
      ipd1 = 0
      ipd1(1:MXOBS) = NSUP(:)
      deallocate(NSUP)
      NSUP => ipd1

! PAINF
      allocate(cpd1_1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PREDLX_PAINF')
      cpd1_1 = ''
      cpd1_1(1:MXOBS) = PAINF(:)
      deallocate(PAINF)
      PAINF => cpd1_1
! PASUP
      allocate(cpd1_1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PREDLX_PASUP')
      cpd1_1 = ''
      cpd1_1(1:MXOBS) = PASUP(:)
      deallocate(PASUP)
      PASUP => cpd1_1
! SYINF
      allocate(cpd1_2(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PREDLX_SYINF')
      cpd1_2 = ''
      cpd1_2(1:MXOBS) = SYINF(:)
      deallocate(SYINF)
      SYINF => cpd1_2
! SYSUP
      allocate(cpd1_2(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PREDLX_SYSUP')
      cpd1_2 = ''
      cpd1_2(1:MXOBS) = SYSUP(:)
      deallocate(SYSUP)
      SYSUP => cpd1_2
!
      return
      end subroutine RESIZE_MXOBS_PREDLX

      end module mod_com_predlx
