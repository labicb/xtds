
      module mod_main_starkl

      use mod_dppr
      use mod_par_tds
      use mod_com_pa
      use mod_com_starkl


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_pa
      call alloc_starkl
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXDIMI
      IMPLICIT NONE

      integer  :: M_DIMI

      M_DIMI = MXRES(MXDIMI)
      call RESIZE_MXDIMI_STARKL(M_DIMI)
      MXDIMI = M_DIMI
!
      return
      end subroutine RESIZE_MXDIMI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXDIMS
      IMPLICIT NONE

      integer  :: M_DIMS

      M_DIMS = MXRES(MXDIMS)
      call RESIZE_MXDIMS_STARKL(M_DIMS)
      MXDIMS = M_DIMS
!
      return
      end subroutine RESIZE_MXDIMS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXELMT
      IMPLICIT NONE

      integer  :: M_ELMT

      M_ELMT = MXRES(MXELMT)
      call RESIZE_MXELMT_STARKL(M_ELMT)
      MXELMT = M_ELMT
!
      return
      end subroutine RESIZE_MXELMT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPT
      IMPLICIT NONE

      integer  :: M_OPT

      M_OPT = MXRES(MXOPT)
      call RESIZE_MXOPT_PA(M_OPT)
      call RESIZE_MXOPT_STARKL(M_OPT)
      MXOPT = M_OPT
!
      return
      end subroutine RESIZE_MXOPT

      end module mod_main_starkl
