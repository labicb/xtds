
      module mod_main_xtri

      use mod_dppr
      use mod_par_tds
      use mod_com_xtri


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_xtri
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_NBLMAX
      IMPLICIT NONE

      integer  :: M_LMAX

      M_LMAX = MXRES(NBLMAX)
      call RESIZE_NBLMAX_XTRI(M_LMAX)
      NBLMAX = M_LMAX
!
      return
      end subroutine RESIZE_NBLMAX

      end module mod_main_xtri
