
      module mod_main_hdi

      use mod_dppr
      use mod_par_tds
      use mod_com_hdic
      use mod_com_pgdh


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_hdic
      call alloc_pgdh
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXDIMS
      IMPLICIT NONE

      integer  :: M_DIMS

      M_DIMS = MXRES(MXDIMS)
      call RESIZE_MXDIMS_HDIC(M_DIMS)
      call RESIZE_MXDIMS_PGDH(M_DIMS)
      MXDIMS = M_DIMS
!
      return
      end subroutine RESIZE_MXDIMS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXELMD
      IMPLICIT NONE

      integer  :: M_ELMD

      M_ELMD = MXRES(MXELMD)
      call RESIZE_MXELMD_PGDH(M_ELMD)
      MXELMD = M_ELMD
!
      return
      end subroutine RESIZE_MXELMD

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPH
      IMPLICIT NONE

      integer  :: M_OPH

      M_OPH = MXRES(MXOPH)
      call RESIZE_MXOPH_HDIC(M_OPH)
      call RESIZE_MXOPH_PGDH(M_OPH)
      MXOPH = M_OPH
!
      return
      end subroutine RESIZE_MXOPH

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXSNV
      IMPLICIT NONE

      integer  :: M_SNV

      M_SNV = MXRES(MXSNV)
      call RESIZE_MXSNV_HDIC(M_SNV)
      MXSNV = M_SNV
!
      return
      end subroutine RESIZE_MXSNV

      end module mod_main_hdi
