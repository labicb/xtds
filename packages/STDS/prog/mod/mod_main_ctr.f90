
      module mod_main_ctr

      use mod_dppr
      use mod_par_tds
      use mod_com_ctr


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_ctr
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXNIV
      IMPLICIT NONE

      integer  :: M_NIV

      M_NIV = MXRES(MXNIV)
      call RESIZE_MXNIV_CTR(M_NIV)
      MXNIV = M_NIV
!
      return
      end subroutine RESIZE_MXNIV

      end module mod_main_ctr
