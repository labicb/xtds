#!/bin/sh
# 1 June 2001
#
# STDS generates prediction ASCII files in the working directory from
# any available parameter file.
#
# The command arguments to be specified are (in this order) :
#
# 'MOLECULE/UpperState_LowerState' : parameter file name.
#     or   /State_Stark
# 'Jmax' : Maximum J Value for the Upper State.
# 'Fmin' : Lower Frequency Limit (cm-1).
# 'Fmax' : Upper Frequency Limit (cm-1).
# 'Tvib' : Vibrational Temperature (K).
# 'Trot' : Rotational Temperature (K).
# 'Smin' : Intensity Lower Limit (cm-2.atm-1 or arbitrary units).
# 'Psta' : Polarization state (for Raman spectra only : R111|R110|R001).
# 'FPVIB': Vibrational partion function.
# 'ABUND': Isotopic abundance.
#
# SYNTAX :
# STDS [MOLECULE/UpperState_...  Jmax [Fmin Fmax Tvib Trot Smin [Psta] [fpvib FPVIB] [abund ABUND]]]
#
# EXAMPLES :
#
# STDS
# (lists available molecules and band systems)
#
# STDS     12CH4/Pa_DYADmGS       8
# (calculates upper levels of the Dyad band system of 12CH4
#  and generates two ASCII files ./PREDICTION and ./jener.xy)
#
# STDS     13CD4/Pa_DYAD_stark    8
# (calculates Stark Coefficients for the Dyad band system of 13CD4
#  and generates the ASCII file ./PREDICTION)
#
# STDS     12CH4/Pa_DYADmGS       8   1000  2000   296   296  1.d-6
# (calculates transitions between Dyad and Ground States of 12CH4
#  and generates two ASCII files ./PREDICTION and ./spectr.xy)
#
# ./PREDICTION contains fully explicit headings and data
#
# ./*.xy are two-column files directly usable for graphical display
#
# ./jener.xy contains data for plotting energy diagrams :
#  The first  column (x) is the J quantum number
#  The second column (y) is the reduced energy
#                        = E(J) - Bo J(J+1) + Do (J(J+1))**2
#
# ./spectr.xy contains data for plotting stick spectra :
#  The first  column (x) is the transition wavenumber (cm-1)
#  The second column (y) is the transition intensity (cm-2.atm-1 or a.u)
#
######################################################################
#
BASD=will be set by *tds_INSTALL
PARD=$BASD"/para"
#
SCRD=$BASD/prog/exe
BIND=$BASD"/bin"
#
if [ $# = 0 ] ; then
  echo ""
  head -19 $SCRD/STDS | more
  echo ""
  echo "TO GET THE LIST OF AVAILABLE PARAMETER FILES (first argument), ENTER"
  echo "file_name [default is screen] :"
  read REP
  echo ""
  echo "                             Please Wait ..."
  echo ""
#
  SCRATCH="/tmp/STDS_"$$
  if [ -f $SCRATCH ] ; then
    rm $SCRATCH
  fi
  CURD=`pwd`
  cd $PARD
  LISTE=`ls`
  for FICH in $LISTE
    do
    echo "" >> $SCRATCH
    LISTEC=`ls $FICH`
    for FICHC in $LISTEC
      do
      echo $FICH/$FICHC >> $SCRATCH
    done
  done
  cd $CURD
  if [ x$REP = x ] ; then
    more $SCRATCH
    echo ""
    echo "Press <RETURN>"
    read REP
  else
    cp -i $SCRATCH $REP
  fi
  rm $SCRATCH
#
  echo ""
  head -53 $SCRD/STDS | more
  exit
fi
#
#  transfert des arguments de la ligne de commande
#  dans le fichier 'control_argu'.
#
NBARG=$#
CONTROL_FILE=control_argu
PARA_FILE=para_file
if [ -f $CONTROL_FILE ] ; then rm $CONTROL_FILE ; fi
date > $CONTROL_FILE
PARA_FILE_ORG=`echo $PARD/$1`
shift
echo $PARA_FILE_ORG  >>$CONTROL_FILE
echo $PARA_FILE      >>$CONTROL_FILE
for ARG do echo $ARG >>$CONTROL_FILE ; done
#
HRED_FOUT='hred_temp'
#
#  fabrication des fichiers de controle de toute la chaine de programmes
#
 cp $PARA_FILE_ORG $PARA_FILE
 echo ""
#
#  calcul complet
#
 date
 $BIND/ctrl.x
 REPONSE=`cat $CONTROL_FILE`
 for REP in $REPONSE ; do
   case $REP in
       intra | inter    ) INTERA=$REP
                          ;;
       dip | pol | str  ) DPS=$REP
                          ;;
   esac
 done
 rm $CONTROL_FILE
 if [ $NBARG = 2 ] && [ $DPS != str ] ; then
   NIV=oui
 else
   NIV=non
 fi
#
#  calculs relatifs a la polyade superieure
#  si differente de polyade inferieure
#  ou niveaux seulement.
#
 if [ $INTERA = inter ] || [ $NIV = oui ] ; then
   mv control_hmodel_sup control
   date
   $BIND/hmodel.x
   mv control_parchk_h control
   date
   $BIND/parchk.x
   mv control_hred_sup control
   date
   $BIND/hred.x
   REPONSE=`cat $HRED_FOUT`
   for REP in $REPONSE ; do
     mv $REP"_hred_temp" $REP
   done
   rm $HRED_FOUT
   mv control_rovbas_sup control
   date
   $BIND/rovbas.x
   mv control_hmatri_sup control
   date
   $BIND/hmatri.x
   mv control_hdiag_sup control
   date
   $BIND/hdi.x
   rm HA* ME_*
   cp $PARA_FILE_ORG $PARA_FILE
   if [ $NIV = oui ] ; then
     rm MH*
     date
     $BIND/jener.x
     mv jener.t PREDICTION
     rm EN* FN* VP*
     rm $PARA_FILE
     rm control
     exit
   fi
 fi
#
#  calculs relatifs a la polyade inferieure
#
 mv control_hmodel_inf control
 date
 $BIND/hmodel.x
 if [ $INTERA = intra ]  ; then
   mv control_parchk_h control
   date
   $BIND/parchk.x
 fi
 mv control_hred_inf control
 date
 $BIND/hred.x
 REPONSE=`cat $HRED_FOUT`
 for REP in $REPONSE ; do
   mv $REP"_hred_temp" $REP
 done
 rm $HRED_FOUT
 mv control_rovbas_inf control
 date
 $BIND/rovbas.x
 mv control_hmatri_inf control
 date
 $BIND/hmatri.x
 mv control_hdiag_inf control
 date
 $BIND/hdi.x
 rm HA* ME_*
#
#  calculs relatifs aux options
#
 case $DPS in
  dip ) mv control_dipmod control
        date
        $BIND/dipmod.x
        mv control_parchk_tm control
        date
        $BIND/parchk.x
        rm MH*
        mv control_dipmat control
        date
        $BIND/dipmat.x
        mv control_trmomt control
        date
        $BIND/trm.x
        rm MD* DI*
        mv control_trans control
        date
        $BIND/tra.x
        rm TR*
        mv control_spect control
        date
        $BIND/spect.x
        mv spectr.t PREDICTION
        rm trans.t
        ;;
  pol ) mv control_polmod control
        date
        $BIND/polmod.x
        mv control_parchk_tm control
        date
        $BIND/parchk.x
        rm MH*
        mv control_polmat control
        date
        $BIND/polmat.x
        mv control_trmomt control
        date
        $BIND/trm.x
        rm MP* PO*
        mv control_trans control
        date
        $BIND/tra.x
        rm TP*
        mv control_spect control
        date
        $BIND/spect.x
        mv spectr.t PREDICTION
        rm trans.t
        ;;
  str ) mv control_dipmod control
        date
        $BIND/dipmod.x
        mv control_parchk_tm control
        date
        $BIND/parchk.x
        rm MH*
        mv control_dipmat control
        date
        $BIND/dipmat.x
        mv control_starkl control
        date
        $BIND/starkl.x
        mv starkl.t PREDICTION
        rm MD* DI*
        ;;
 esac
 date
##
 rm EN* FN* VP*
 rm $PARA_FILE
 rm control
