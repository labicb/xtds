!
! ***  TENSEURS DE DEGRE OMEGA DANS TD.
!
!      CALCULE LES INDICES IT DES NB TENSEURS D'UN OSCILLATEUR I
!      DES MOLECULES XY4 DE DEGRE IV.
!        IT(N),N=1,NB
!        IT(N)=1000*IV+100*IK+10*IN+IC
!        IK=  IV, IV-2, ..... 1 OU 0
!        IN= INDICE CARACTERISANT LES DIFFERENTES VALEURS DE IC.
!        IC= SYMETRIE DANS TD.
!
      SUBROUTINE TCUBE(I,IV,NB,IT,MDMIT)                                                           ! SMIL G.PIERRE MAI 82
      use mod_par_tds
      IMPLICIT NONE
      integer ,dimension(MDMIT)  :: IT
      integer          :: I,IV,NB,MDMIT

! functions
      integer          :: NSYM1

      integer          :: I1,IC,ICMA,ICMI,IK,IKM,IN,IUG
      integer          :: NN
!
      NB = 0
      IF( I .NE. 1 ) GOTO 1
      NB    = 1
      IT(1) = 1000*IV+1
      RETURN
!
1     IF( I .NE. 2 ) GOTO 2
      DO IKM=IV,0,-2
        IK   = 2*IV-IKM-2*(IV/2)
        ICMI = 1
        ICMA = 2
        IF( IK .EQ. 0        ) ICMA = 1
        IF( IK .EQ. 3*(IK/3) ) GOTO 3
        ICMI = 3
        ICMA = 3
!
3       DO IC=ICMI,ICMA
          NB     = NB+1
          IT(NB) = 1000*IV+100*IK+IC
        ENDDO
      ENDDO
      RETURN
!
2     IF( I .NE. 3 .AND.           &
          I .NE. 4       ) RETURN
      DO IKM=IV,0,-2
        IK  = 2*IV-IKM-2*(IV/2)
        IUG = MOD(IK,2)+1
E6:     DO IC=1,MXSYM
          NN = NSYM1(IK,IUG,IC)
          IF( NN .EQ. 0 ) CYCLE E6
          DO I1=1,NN
            IN     = I1-1
            NB     = NB+1
            IT(NB) = 1000*IV+100*IK+10*IN+IC
          ENDDO
        ENDDO E6
      ENDDO
!
      RETURN
      END SUBROUTINE TCUBE
