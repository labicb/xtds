!
!  E.M.R.V. COUPLAGE EXTERNE
!
!
! SMIL G.PIERRE JUIN 82
! MOD. T.GABARD DEC 92
!
!     E123=<(IB12*IB3)//(IA12*IA3)//(IK12*IK3)>
!     IPM=+1 POUR UN OPERATEUR CREATION
!     IPM=-1 POUR UN OPERATEUR ANNIHILATION
!
      FUNCTION E123(IPM,IB,IA,IK)
      use mod_dppr
      use mod_par_tds
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: E123
      integer ,dimension(NBVQN+1)  :: IB,IA,IK
      integer          :: IPM

! functions
      real(kind=dppr)  :: E12,EMRVA,EUFC

      real(kind=dppr)  :: EUF
      real(kind=dppr)  :: R12,R3,RC

      integer          :: IA1,IA2,IA23,IA234
      integer          :: IAC3,IAL3,IAN3,IAV3
      integer          :: IB1,IB2,IB23,IB234
      integer          :: IBC3,IBL3,IBN3,IBV3
      integer          :: IK1,IK2,IK23,IK234
      integer          :: IKC3,IKL3,IKN3,IKV3
!
      E123 = 0.D0
      CALL VLNC(IB(3),IBV3,IBL3,IBN3,IBC3)
      CALL VLNC(IA(3),IAV3,IAL3,IAN3,IAC3)
      CALL VLNC(IK(3),IKV3,IKL3,IKN3,IKC3)
      IF( IAN3 .NE. 0 ) RETURN
      CALL VLNC(IB(5),IB1,IB2,IB23,IB234)
      CALL VLNC(IA(5),IA1,IA2,IA23,IA234)
      CALL VLNC(IK(5),IK1,IK2,IK23,IK234)
      RC   = SQRT(DC(IK23)*DC(IA23)*DC(IB23))
      EUF  = EUFC(IB2,IBC3,IB23,IA2,IAC3,IA23,IK2,IKC3,IK23)
      R3   = EMRVA(3,IBV3,IBL3,IBN3,IBC3,IPM,IAV3,IAL3,IAC3,IKV3,IKL3,IKN3,IKC3)
      R12  = E12(IPM,IB,IA,IK)
      E123 = RC*EUF*R12*R3
!
      RETURN
      END FUNCTION E123
