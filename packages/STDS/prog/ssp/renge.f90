!
! *** PERMUTTE LES LIGNES/COLONNES D'UN TABLEAU ENTIER SUIVANT L'ORDRE
!     CROISSANT DES ELEMENTS DE LA PREMIERE LIGNE/COLONNE DU TABLEAU
!
! SMIL C. MILAN MAI 75
!
      SUBROUTINE RENGE(L,N,M,IPER)
      IMPLICIT NONE
      integer ,dimension(N,M)  :: L
      integer          :: N,M,IPER

      integer          :: I,IP1
      integer          :: J
      integer          :: K
      integer          :: LI,LL
      integer          :: NM1
!
      IPER = 1
      NM1  = N-1
E1:   DO I=1,NM1
        K   = 0
        LI  = L(I,1)
        IP1 = I+1
E2:     DO J=IP1,N
          IF( L(J,1) .GE. LI ) CYCLE E2
          K  = J
          LI = L(K,1)
        ENDDO E2
        IF( K .EQ. 0 ) CYCLE E1
        DO J=1,M
          LL     = L(I,J)
          L(I,J) = L(K,J)
          L(K,J) = LL
        ENDDO
        IPER = -IPER
      ENDDO E1
!
      RETURN
      END SUBROUTINE RENGE
