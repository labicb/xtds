!
!  POSITIONNEMENT DANS LE FICHIER DE FONCTIONS
!
      SUBROUTINE IPAR(LUI,NNIV,NSV)
      IMPLICIT NONE
      integer          :: LUI,NNIV,NSV

      integer          :: I
!
      DO I=1,8+NNIV
        READ(LUI)
      ENDDO
      READ(LUI) NSV
      READ(LUI)
      DO I=1,NSV
        READ(LUI)
      ENDDO
!
      RETURN
      END SUBROUTINE IPAR
