!
! CREATION DES CODES V1234
!
      SUBROUTINE CV1234(N,V1M,V2M,V3M,V4M,NC,M)
      use mod_dppr
      use mod_par_tds
      use mod_main_ctr
      IMPLICIT NONE
      character(len =   2)  :: V1M,V2M,V3M,V4M
      character(len =   3)  :: NC
      integer               :: N,M

      integer  :: NV1,NV1M,NV2,NV2M,NV3,NV3M,NV4,NV4M
!
1001  FORMAT(I1)
1002  FORMAT('N',I1)
1003  FORMAT('N',I2)
1004  FORMAT(I2)
8000  FORMAT(  ' !!! STOP ON ERROR')
8001  FORMAT(  ' !!! POLYAD NUMBER ERROR'           ,/,   &
               ' !!! MXPOL  EXCEEDED : ',I8,' > ',I8   )
!
      IF( N .GT. MXPOL ) THEN
        PRINT 8001, N,MXPOL
        PRINT 8000
        PRINT *
        STOP
      ENDIF
      READ(V1M,1004) NV1M
      READ(V2M,1004) NV2M
      READ(V3M,1004) NV3M
      READ(V4M,1004) NV4M
      M = 0
      DO NV1=MIN(NV1M,N/2),0,-1
        DO NV3=MIN(NV3M,N/2),0,-1
          DO NV2=MIN(NV2M,N),0,-1
            DO NV4=MIN(NV4M,N),0,-1
              IF( 2*NV1+NV2+2*NV3+NV4 .EQ. N ) THEN
                M = M+1
                IF( M .GT. MXNIV ) CALL RESIZE_MXNIV
                WRITE(V1234(1,M),1001) NV1
                WRITE(V1234(2,M),1001) NV2
                WRITE(V1234(3,M),1001) NV3
                WRITE(V1234(4,M),1001) NV4
              ENDIF
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      IF( M .LT. 10 ) THEN
        WRITE(NC,1002) M
      ELSE
        WRITE(NC,1003) M
      ENDIF
!
      RETURN
      END SUBROUTINE CV1234
