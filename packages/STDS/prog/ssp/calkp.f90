!
!  CALCUL DES K, EXTRAITS PAR LES FONCTIONS DKP, ODKP
!  D COMME DIAG, OD COMME OFF DIAG.
!  POUR UTILISATION DANS LE PROGRAMME POLMAT.F
!
! REV    JAN 1995 JPC,CW (PARAMETER)
!
      SUBROUTINE CALKP(JM)
      use mod_dppr
      use mod_par_tds
      use mod_com_sy
      use mod_main_polmat
      IMPLICIT NONE
      integer          :: JM

! functions
      integer          :: CTR,NSYM1

      real(kind=dppr)  :: VODKX

      integer          :: IDK_SUP,IODK_SUP
      integer          :: I,IC1,IC2,IC3,INC,ISYM
      integer          :: J1,J2,J3,JDIFF
      integer          :: N1,N2,N3,NS2,NS3
!
!  OFF DIAG : J1 = 2, C1 = E OU F2
!
      IODK_SUP = -1
      J1       =  2
      DO ISYM=1,2
        IF( ISYM .EQ. 1 ) THEN
          IC1 = 3
        ELSE
          IC1 = 5
        ENDIF
        DO JDIFF=1,2
          INC = 1
          N1  = 0
          DO J2=0,JM-JDIFF
            J3                    = J2+JDIFF
            INJODK(JDIFF,ISYM,J3) = INC
E2:         DO IC3=1,MXSYM
              NS3 = NSYM1(J3,1,IC3)
              IF( NS3 .EQ. 0 ) CYCLE E2
E3:           DO IC2=1,MXSYM
                IF( CTR(IC1,IC2,IC3) .EQ. 0 ) CYCLE E3
                NS2 = NSYM1(J2,1,IC2)
                IF( NS2 .EQ. 0 ) CYCLE E3
                DO N3=0,NS3-1
                  DO N2=0,NS2-1
                    IODKX(JDIFF,ISYM,INC) = 100000*IC3+10000*IC2+100*N3+N2
                    CALL KCUBU(J1,J2,J3,1,1,1,N1,N2,N3,IC1,IC2,IC3,VODKX,I)
                    VODK(JDIFF,ISYM,INC) = VODKX
                    INC                  = INC+1
                    IF( INC .GT. MXODK ) CALL RESIZE_MXODK
                  ENDDO
                ENDDO
              ENDDO E3
            ENDDO E2
          ENDDO
          IF( INC-1 .GT. IODK_SUP ) IODK_SUP = INC-1
          INJODK(JDIFF,ISYM,JM+1) = INC
        ENDDO
      ENDDO
!
!  DIAG
!
      IDK_SUP = -1
      INC     =  1
      DO J2=0,JM
        J3        = J2
        INJDK(J2) = INC
! LIMITATION A L'ORDRE 3
        DO J1=0,3
E103:     DO IC1=1,MXSYM
            IF( NSYM1(J1,1,IC1) .EQ. 0 ) CYCLE E103
            DO IC3=1,MXSYM
E105:         DO IC2=IC3,MXSYM
                IF( CTR(IC1,IC2,IC3) .EQ. 0 ) CYCLE E105
                DO N3=0,NSYM1(J3,1,IC3)-1
E107:             DO N2=0,NSYM1(J2,1,IC2)-1
                    IF( J1 .EQ. 0 ) THEN
                      IF( N2  .NE. N3  .OR.               &
                          IC2 .NE. IC3      ) CYCLE E107
                      VDK(INC) = ( (-1)**J2 )*SQRT(DC(IC2)/(2*J2+1))
                    ELSE
                      CALL KCUBU(J1,J2,J3,1,1,1,N1,N2,N3,IC1,IC2,IC3,VDK(INC),I)
                    ENDIF
                    IDKX(INC) = 10000000*J1+1000000*IC1+100000*IC3+10000*IC2+100*N3+N2
                    INC       = INC+1
                    IF( INC .GT. MXDK ) CALL RESIZE_MXDK
                  ENDDO E107
                ENDDO
              ENDDO E105
            ENDDO
          ENDDO E103
        ENDDO
      ENDDO
      IF( INC-1 .GT. IDK_SUP ) IDK_SUP = INC-1
      CALL DEBUG( 'CALKP  => J=',JM)
      CALL DEBUG( 'CALKP  => MXDK=',IDK_SUP)
      CALL DEBUG( 'CALKP  => MXODK=',IODK_SUP)
      INJDK(JM+1) = INC
!
      RETURN
      END SUBROUTINE CALKP
