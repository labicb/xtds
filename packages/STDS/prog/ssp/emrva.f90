!
! SMIL CHAMPION MAI 82
!
!     ELEMENTS MATRICIELS REDUITS DES OPERATEURS VIBRATIONNELS
!     ELEMENTAIRES, LES OPERATEURS SONT CARACTERISES PAR :
!                 IDEG    = DEGENERESCENCE DE L'OSCILLATEUR ASSOCIE
!                 IPM     = +1 POUR UN OPERATEUR CREATION
!                         = -1 POUR UN OPERATEUR ANNIHILATION
!                 OMEGA   = DEGRE VIBRATIONNEL < OU = 4
!                 K       = OMEGA , OMEGA-2 , ... , 1 OU 0
!                 N       = MULTIPLICITE (TRIPLEMENT DEGENERE SEULEMENT)
!                 C       = SYMETRIE DANS TD (A1,A2,E,F1,F2)
!
      FUNCTION EMRVA(IDEG,VP,LP,NP,CP,IPM,OMEGA,K,GAMA,V,L,N,C)
      use mod_dppr
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: EMRVA
      integer          :: IDEG,VP,LP,NP,CP,IPM,OMEGA,K,GAMA,V,L,N,C

! functions
      real(kind=dppr)  :: A1N,A21,A22,A23,A24,A31,A32,A33,A34
      integer          :: IPSI

!
      EMRVA = 0.D0
      IF( IPSI(IDEG,VP,LP,NP,CP)    .EQ. 0     ) RETURN
      IF( IPSI(IDEG,V,L,N,C)        .EQ. 0     ) RETURN
      IF( IPSI(IDEG,OMEGA,K,0,GAMA) .EQ. 0     ) RETURN
      IF( IPM*(VP-V)                .NE. OMEGA ) RETURN
      IF( OMEGA                     .NE. 0     ) GOTO 10
      IF( L-LP                      .NE. 0     ) RETURN
      IF( N-NP                      .NE. 0     ) RETURN
      IF( C-CP                      .NE. 0     ) RETURN
      EMRVA = SQRT(DC(C))
      RETURN
!
10    IF( OMEGA .LT. 0 ) RETURN
      SELECT CASE( IDEG )
        CASE( 1 )
          EMRVA = A1N(IPM,VP,V,OMEGA)
        CASE( 2 )
          SELECT CASE( OMEGA )
            CASE( 1 )
              EMRVA = A21(IPM,VP,LP,CP,V,L,C)
            CASE( 2 )
              EMRVA = A22(IPM,VP,LP,CP,GAMA,V,L,C)
            CASE( 3 )
              EMRVA = A23(IPM,VP,LP,CP,K,GAMA,V,L,C)
            CASE( 4 )
              EMRVA = A24(IPM,VP,LP,CP,K,GAMA,V,L,C)
          END SELECT
        CASE( 3 )
          SELECT CASE( OMEGA )
            CASE( 1 )
              EMRVA = A31(IPM,VP,LP,NP,CP,V,L,N,C,0)
            CASE( 2 )
              EMRVA = A32(IPM,VP,LP,NP,CP,K,GAMA,V,L,N,C,0)
            CASE( 3 )
              EMRVA = A33(IPM,VP,LP,NP,CP,K,GAMA,V,L,N,C,0)
            CASE( 4 )
              EMRVA = A34(IPM,VP,LP,NP,CP,K,GAMA,V,L,N,C,0)
          END SELECT
      END SELECT
!
      RETURN
      END FUNCTION EMRVA
