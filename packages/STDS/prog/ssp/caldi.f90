!
! ***  CALCULE LES ELEM. MAT. DES OP. DU M. DIP.
!
! SMIL J.P.C., J.M.J. , G.P.    DEC.88
! MOD. T.GABARD MAR. 93
! REV    JAN 1995 JPC,CW (PARAMETER)
!
      SUBROUTINE CALDI(JS,ICS,NFBS,JI,ICI,NFBI,ICOR,IGV)
      use mod_dppr
      use mod_par_tds
      use mod_com_sy
      use mod_main_dipmat
      IMPLICIT NONE
      integer          :: JS,ICS,NFBS,JI,ICI,NFBI,ICOR,IGV

! functions
      real(kind=dppr)  :: DCA21,DCA22,DKUB,DZCA2,EMRRO,ODKUB
      integer          :: NSYM1

      real(kind=dppr)  :: CACOS,CALAB,CAMOL,COEF
      real(kind=dppr)  :: ELM
      real(kind=dppr)  :: PHASE1,PHASE2
      real(kind=dppr)  :: SUMC,SUMN
      real(kind=dppr)  :: TERM1,TERM2
      real(kind=dppr)  :: XC

      integer          :: IB,IBRA,ICRB,ICRK,ICSUM,IG
      integer          :: IK,IKET,IO,IVB,IVK
      integer          :: JIS
      integer          :: KR
      integer          :: NMB,NRB,NRCB,NRCK,NRK
      integer          :: NSSUM,NVB,NVK
!
! OMEGA
!
      IO = ICOR/1000
!
! KR
!
      KR = (ICOR-IO*1000)/100
!
! GAMMA R
!
      IG = (ICOR-IO*1000-KR*100)/10
!
! N R
!
      NBELM = 0
!
! POLYADE INFERIEURE
!
E3:   DO IBRA=1,NFBI
        NVB = NVCODI(IBRA)
!
! NUMERO DU SOUS-NIVEAU VIBRATIONNEL INF.
!
        IB = NVB/10
!
! C'V
!
        IVB  = NVB-10*IB
        NRCB = NRCODI(IBRA)
!
! N'
!
        NRB = NRCB/10
!
! C'R
!
        ICRB = NRCB-10*NRB
!
!   POLYADE SUPERIEURE
!
E4:     DO IKET=1,NFBS
          NVK = NVCODS(IKET)
!
! NUMERO DU SOUS-NIVEAU VIBRATIONNEL SUP.
!
          IK = NVK/10
          IF( EMRD(IK,IB) .EQ. 0.D0 ) CYCLE E4
!
! CV
!
          IVK  = NVK-10*IK
          NRCK = NRCODS(IKET)
!
! N
!
          NRK = NRCK/10
!
! CR
!
          ICRK = NRCK-10*NRK
!
!    PREMIERE SOMMATION
!
          TERM1 = 0.D0
E5:       DO ICSUM=1,MXSYM
            NSSUM = NSYM1(JI,1,ICSUM)
            IF( NSSUM .EQ. 0 ) CYCLE E5
            IF( IVB .EQ. 1 )THEN
              XC = DCA21(IG,IGV,ICRB,ICI,ICRK,ICSUM,IVK,ICS)
              IF( XC .EQ. 0.D0 ) CYCLE E5
            ELSE
              XC = DZCA2(IG,IGV,ICRB,IVB,ICI,ICRK,ICSUM,IVK,ICS)
              IF( XC .EQ. 0.D0 ) CYCLE E5
            ENDIF
            SUMN = 0.D0
            DO NMB=0,NSSUM-1
              IF( JS .EQ. JI ) THEN
                CACOS = DKUB(1,JI,NRK,NMB,4,ICRK,ICSUM)
              ELSE
                CACOS = ODKUB(JS,JI,NRK,NMB,ICRK,ICSUM)
              ENDIF
              IF(  KR    .EQ. 0          .AND.         &
                  (ICSUM .NE. ICRB .OR.                &
                   NMB   .NE. NRB      )       ) THEN
                CAMOL = 0.D0
              ELSE
                CAMOL = DKUB(KR,JI,NMB,NRB,IG,ICSUM,ICRB)
              ENDIF
              CALAB = CACOS*CAMOL
              SUMN  = SUMN+CALAB
            ENDDO
            SUMC  = SUMN*XC
            TERM1 = TERM1+SUMC
          ENDDO E5
          PHASE1 = PC(IVB)*PC(IVK)*PC(ICRB)*PC(ICRK)*PC(ICS)
          PHASE1 = PHASE1*PC(ICI)*PC(IG)*PC(IGV)
          TERM1  = TERM1*PHASE1*EMRRO(IO,KR,JI)
!
!    DEUXIEME SOMMATION
!
          TERM2 = 0.D0
E6:       DO ICSUM=1,MXSYM
            NSSUM = NSYM1(JS,1,ICSUM)
            IF( NSSUM .EQ. 0 ) CYCLE E6
            IF( IVB   .EQ. 1 )THEN
              XC = DCA22(IG,IGV,ICRK,IVK,ICS,ICRB,ICSUM,ICI)
              IF( XC .EQ. 0.D0 ) CYCLE E6
            ELSE
              XC = DZCA2(IG,IGV,ICRK,IVK,ICS,ICRB,ICSUM,IVB,ICI)
              IF( XC .EQ. 0.D0 ) CYCLE E6
            ENDIF
            SUMN = 0.D0
            DO NMB=0,NSSUM-1
              IF( JS .EQ. JI ) THEN
                CACOS = DKUB(1,JI,NMB,NRB,4,ICSUM,ICRB)
              ELSE
                CACOS = ODKUB(JS,JI,NMB,NRB,ICSUM,ICRB)
              ENDIF
              IF(  KR    .EQ. 0          .AND.         &
                  (ICSUM .NE. ICRK .OR.                &
                   NMB   .NE. NRK      )       ) THEN
                CAMOL = 0.D0
              ELSE
                CAMOL = DKUB(KR,JS,NRK,NMB,IG,ICRK,ICSUM)
              ENDIF
              CALAB = CACOS*CAMOL
              SUMN  = SUMN+CALAB
            ENDDO
            SUMC  = SUMN*XC
            TERM2 = TERM2+SUMC
          ENDDO E6
          JIS    = JI+JS
          PHASE2 = (-1)**JIS
          TERM2  = TERM2*PHASE2*EMRRO(IO,KR,JS)
!
!    SOMME DES 2 TERMES
!
          COEF  = SQRT(3.D0)*EMRD(IK,IB)/2.D0
          COEF  = COEF*SQRT(DC(ICI))
          ELM   = (TERM1+TERM2)*COEF
          ELM   = ELM*SQRT(DBLE((2*JI+1)*(2*JS+1)))
          NBELM = NBELM+1
          IF( NBELM .GT. MXELMT ) CALL RESIZE_MXELMT
          LI(NBELM) = IBRA
          KO(NBELM) = IKET
          H(NBELM)  = ELM
        ENDDO E4
      ENDDO E3
!
      RETURN
      END SUBROUTINE CALDI
