!
      SUBROUTINE DERIP(IFB,NBOPH)
      use mod_dppr
      use mod_par_tds
      use mod_com_pgde
      use mod_com_pgdh
      IMPLICIT NONE
      integer          :: IFB,NBOPH

      real(kind=dppr)  :: VV

      integer          :: I,IC,IL,IOPH
!
      DO IOPH=1,NBOPH
        DERI(IOPH) = 0.D0
      ENDDO
E2:   DO IOPH=1,NBOPH
        IF( NUMI(IOPH) .EQ. NUMI(IOPH+1) ) CYCLE E2
        DO I=NUMI(IOPH),NUMI(IOPH+1)-1
          IC = KO(I)
          IL = LI(I)
          VV = T(IL,IFB)*T(IC,IFB)
          IF( IL .NE. IC ) VV = VV*2.D0
          DERI(IOPH) = DERI(IOPH)+VV*EL(I)
        ENDDO
      ENDDO E2
!
      RETURN
      END SUBROUTINE DERIP
