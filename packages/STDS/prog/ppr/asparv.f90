      PROGRAM ASPARV
!
! REV    SEPT 2006  CW
!
!  lire un fichier binaire (ex. INFILE) de type para_variance.t
!  et l'ecrire en ASCII dans INFILE'_ASC'
!
! APPEL : asparv
!
      use mod_dppr
      use mod_par_tds
      use mod_main_paradj
      IMPLICIT NONE

      integer          :: IPSTD
      integer          :: J
      integer          :: K
      integer          :: NBOP

      character(len = NBCTIT)  :: TITRE
      character(len = 150)     :: INFILE
      character(len = 160)     :: OUTFILE
!
1000  FORMAT(A)
1020  FORMAT(/,            &
             'ASPARV : ')
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
7000  FORMAT('ENTER para_variance.t TYPE FILE NAME [para_variance.t] :')
8000  FORMAT(' !!! ASPARV : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF FOR ',A)
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      PRINT 1020
!
!  FICHIER D'ENTREE
!
      PRINT 7000
      READ(*,1000) INFILE
      IF( LEN_TRIM(INFILE) .EQ. 0 ) THEN
        INFILE = 'para_variance.t'
      ENDIF
      PRINT 2000,  TRIM(INFILE)
      OPEN(12,FILE=TRIM(INFILE),FORM='UNFORMATTED',STATUS='OLD')
!
!  FICHIER DE SORTIE
!
      OUTFILE = TRIM(INFILE)//'_ASC'
      PRINT 2001,  TRIM(OUTFILE)
      OPEN(10,FILE=TRIM(OUTFILE))
      WRITE(10,*) '>>> NFIT,JMAX,SQRT(SIGMA2),SQRT(SIGMAN)'
      READ (12,END=9003) TITRE
      WRITE(10,*)        TITRE
      WRITE(10,*) '>>> TITRAJ(J,NOAJM)'
      DO J=1,3
        READ (12,END=9003) TITRE
        WRITE(10,*)        TITRE
      ENDDO
      WRITE(10,*) '>>> NBOP,TITRAJ(4,NOAJM)'
      READ (12,END=9003) NBOP,TITRE
      WRITE(10,*)        NBOP,TITRE
      DO WHILE( NBOP .GT. MXOPH )
        CALL RESIZE_MXOPH
      ENDDO
      WRITE(10,*) '>>> TITRAJ(5,NOAJM)'
      READ (12,END=9003) TITRE
      WRITE(10,*)        TITRE
      WRITE(10,*) '>>> FDATE'
      READ (12,END=9003) TITRE
      WRITE(10,*)        TITRE
      WRITE(10,*) '>>> COPAJS(IPSTD),PSTDAJ(IPSTD),ETPSTD(IPSTD)'
      DO IPSTD=1,NBOP
        READ (12,END=9003) COPAJS(1),PSTDAJ(1),ETPSTD(1)
        WRITE(10,*)        COPAJS(1),PSTDAJ(1),ETPSTD(1)
      ENDDO
      WRITE(10,*) '>>> (SIGMA2*VSTDS(K,IPSTD),K=IPSTD,NBOP)'
      DO IPSTD=1,NBOP
        READ (12,END=9003) (VSTDS(K,1),K=IPSTD,NBOP)
        WRITE(10,*)        (VSTDS(K,1),K=IPSTD,NBOP)
      ENDDO
      GOTO 9000
!
9003  PRINT 8003, TRIM(INFILE)
      PRINT 8000
!
9000  CLOSE(10)
      PRINT *
      END PROGRAM ASPARV
