      PROGRAM STARKL
!
!  02.03.89 FORTRAN 77 pour SUN4 MODIFIE LE 26.09.89
!
!     M.P. COQUARD
!
!     15 AVR 1995 JPC,CW
!
! ##################################################################################
! ##                                                                              ##
! ##  CALCUL DES ELEMENTS MATRICIELS DU MOMENT DE TRANSITION DANS LA BASE PROPRE  ##
! ##                                                                              ##
! ##################################################################################
!
! APPEL : starkl Pn Nm Pn' Nm' Dp Jmax para_file_name
!
! DIMENSION MAXIMUM D'UN BLOC SUPERIEUR.
!     MXDIMS               ! HDK:TK:DIP
!
! DIMENSION MAXIMUM D'UN BLOC INFERIEUR.
!     MXDIMI               ! HDL:TL:DIP:HDLJ:TLJ
!
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_main_starkl
      IMPLICIT NONE

      real(kind=dppr)  :: A
      real(kind=dppr)  :: DERI,DJ
      real(kind=dppr)  :: EMPRO
      real(kind=dppr)  :: TM

      integer          :: I,ICK,ICK1,ICL,ICL1,ICLUK,ICLUL,IDJ,II,IK
      integer          :: IK1,IL,IL1,IN,IOP,IOP1,IS,ISV
      integer          :: J,JCL,JK,JK1,JL1,JLUK,JLUL,JL,JMAX
      integer          :: KK,KL
      integer          :: NBELM,NFBK,NFBL,NICK,NICL,NL1,NK1,NNIVI
      integer          :: NNIVS,NS,NSV,NSVI,NSVS

      character(len = NBCTIT)  :: TITRE
      character(len =  11) ,dimension(5)  :: CARG
      character(len =   8)  :: FSORT = 'starkl.t'
      character(len = 120)  :: FMTR,FNINF,FNSUP,FPARA,NOM

      logical          :: INTRA = .FALSE.
!
1000  FORMAT(A)
1002  FORMAT(/,     &
             I4,A)
1004  FORMAT(/,                                                          &
             'Transition Moment Parameters in Tetrahedral Formalism',/)
1010  FORMAT(/,              &
             'STARKL : ',A)
1200  FORMAT(/,                                                &                                   ! cf. FORMAT 1200 de jener.f
             '   Energy      J  C     n       Stark Coef.',/)
1201  FORMAT(F15.6,1X,I3,'  3',3X,I3,3X,E13.4)                                                     ! cf. FORMAT 1201 de jener.f
1202  FORMAT(I2)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
8000  FORMAT(' !!! STARKL : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8100  FORMAT(' !!! J,C ERROR')
8101  FORMAT(' !!! DIPOLE MOMENT FILE => UNEXPECTED EOF')
8102  FORMAT(' !!! UPPER LEVEL FILE => UNEXPECTED EOF')
8103  FORMAT(' !!! LOWER LEVEL FILE => UNEXPECTED EOF')
8104  FORMAT(' !!! ERROR OPENING DIPOLE MOMENT FILE: ',A)
8105  FORMAT(' !!! ERROR OPENING EXPERIMENTAL FILE: ',A)
8106  FORMAT(' !!! ERROR OPENING UPPER EIGENVAL. AND VECT. FILE: ',A)
8107  FORMAT(' !!! ERROR OPENING LOWER EIGENVAL. AND VECT. FILE: ',A)
8108  FORMAT(' !!! ERROR OPENING FILE: ',A)
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1000,END=9997) FDATE
      PRINT 1010,            FDATE
      DO I=1,5
        READ(10,1000,END=9997) CARG(I)
      ENDDO
      READ(CARG(2)(2:3),1202) NNIVS
      READ(CARG(4)(2:3),1202) NNIVI
      READ(10,*,ERR=9997) JMAX
      READ(10,1000,ERR=9997) FPARA
      CLOSE(10)
      FNINF = 'VP_'//TRIM(CARG(3))//'_'
      FNSUP = 'VP_'//TRIM(CARG(1))//'_'
      IF( FNINF .EQ. FNSUP ) INTRA = .TRUE.
      FMTR = 'DI_'//TRIM(CARG(1))//'m'//TRIM(CARG(3))//'_'//TRIM(CARG(5))//'_'
      PRINT 2000, TRIM(FNINF)
      PRINT 2000, TRIM(FNSUP)
      PRINT 2000, TRIM(FPARA)
      PRINT 2000, TRIM(FMTR)
      PRINT 2001, TRIM(FSORT)
!
!  APPLICATION DES DIRECTIVES
!
      OPEN(81,ERR=4081,FILE=FSORT,STATUS='UNKNOWN')                                                ! FICHIER DE SORTIE
      IF( INTRA ) THEN
        OPEN(60,STATUS='SCRATCH',FORM='UNFORMATTED')
      ELSE
        OPEN(60,ERR=4060,FILE=FNINF,STATUS='OLD',FORM='UNFORMATTED')                               ! FICHIER DE NIV INF
      ENDIF
      OPEN(61,ERR=4061,FILE=FNSUP,STATUS='OLD',FORM='UNFORMATTED')                                 ! FICHIER DE NIV SUP
      OPEN(80,ERR=4080,FILE=FPARA,STATUS='OLD')                                                    ! FICHIER DE PARAMETRES DU MOMENT DE TRANSITIONS
      OPEN(40,ERR=4040,FILE=FMTR,STATUS='OLD',FORM='UNFORMATTED')                                  ! FICHIER DU MOMENT DE TRANS
!
! *** LECTURE DES PARAMETRES DE H
!     DANS LE FICHIER VECTEURS PROPRES / VALEURS PROPRES
!     DE POLYADE SUP ET RECOPIE DANS LE FICHIER DE SORTIE
!
      CALL IOPBA0(61,81)
      READ(61,END=4161) NSVS
      DO ISV=1,NSVS
        READ(61,END=4161)
      ENDDO
      READ(61,END=4161)
      IF( INTRA ) THEN
!
5000    READ(61,END=5002) JLUL,ICLUL,NFBL
        IF( JLUL .GT. JMAX+2 ) GOTO 5002
        WRITE(60)         JLUL,ICLUL,NFBL
        DO WHILE( NFBL .GT. MXDIMI )
          CALL RESIZE_MXDIMI
        ENDDO
        IF( NFBL .EQ. 0 ) GOTO 5000
        READ (61) (HDL(I),I=1,NFBL)
        WRITE(60) (HDL(I),I=1,NFBL)
        DO IL=1,NFBL
          READ (61) NICL,ICLRO,(TL(IL,I),I=1,NFBL)
          WRITE(60) NICL,ICLRO,(TL(IL,I),I=1,NFBL)
        ENDDO
        GOTO 5000
!
5002    REWIND(61)
        CALL IOPBA0(61,0)
        READ(61) NSVS
        DO ISV=1,NSVS
          READ(61)
        ENDDO
        READ(61)
      ENDIF
!
! *** LECTURE DES PARAMETRES DU MOMENT DE TRANSITION
!     DANS LE FICHIER DE PARAMETRES H+D
!     STOCKAGE EN MEMOIRE ET RECOPIE DANS LE FICHIER DE SORTIE
!
      CALL IOPAB(80,0)                                                                             ! LECTURE DES PARAMETRES DE H
      WRITE(81,1004)
      CALL IOPAA(80,81)                                                                            ! LECTURE DES PARAMETRES DU M.D. ET STOCKAGE EN MEMOIRE
!
! *** LECTURE DU DEBUT DU FICHIER DE MOMENT DIPOLAIRE
!
      DO I=1,3
        READ(40)
      ENDDO
      READ(40)
      DO I=1,4+NNIVI+NNIVS
        READ(40)
      ENDDO
      NS = 0
      DO IN=1,2                                                                                    ! BOUCLE SUR LES DEUX NIVEAUX
        READ (40)      NSV,TITRE
        WRITE(81,1002) NSV,TRIM(TITRE)
        DO ISV=1,NSV
          READ (40)      TITRE(:58)
          WRITE(81,1000) TITRE(:58)
        ENDDO
        NS = NS+NSV
      ENDDO
      DO I=1,3
        READ(40)
      ENDDO
      READ(40) TITRE(:67)
      READ(40) NBOPT,NOM(:24)
      DO WHILE( NBOPT .GT. MXOPT )
        CALL RESIZE_MXOPT
      ENDDO
      WRITE(81,1200)
!
!
! *** BOUCLE SUR LES NIVEAUX SUP (K=SUP)
! *** BOUCLE SUR JSUP
!
E3:   DO JK=0,JMAX
        DJ = JK
!
! *** REPOSITIONNEMENT SUR LE FICHIER DE V.PROPRES DE LA POLYADE INF
!
        REWIND(60)                                                                                 ! RELECTURE DE L'ENTETE DU FICHIER INF
        IF( .NOT. INTRA ) THEN
          CALL IOPBA0(60,0)
          READ(60,END=4160) NSVI
          DO ISV=1,NSVI
            READ(60,END=4160)
          ENDDO
          READ(60,END=4160)
        ENDIF
!
! *** STOCKAGE DES DIMENSIONS ET DES V.PROPRES ET VA.PROPRES DE LA POLYADE INF
!     POUR LES 3 BRANCHES * MXSYM VALEURS DE ICINF
!
        DO I=1,3*MXSYM                                                                             ! ATTENTION : 5*MXSYM POUR RAMAN
          JDIM(I) = 0
        ENDDO
        DO JL=MAX(JK-1,0),JK+1
          DO ICL=1,MXSYM
!
6666        READ(60,END=67) JLUL,ICLUL,NFBL
            DO WHILE( NFBL .GT. MXDIMI )
              CALL RESIZE_MXDIMI
            ENDDO
            IF( NFBL .EQ. 0 ) GOTO 6666
            READ(60,END=4160) (HDL(I),I=1,NFBL)
            DO IL=1,NFBL
              READ(60,END=4160) NICL,ICLRO,(TL(IL,I),I=1,NFBL)
            ENDDO
            IF( JLUL .GT. JK+1 ) GOTO 67
            IF( JLUL .LT. JL   ) GOTO 6666
            JCL       = (JLUL-JK+1)*MXSYM+ICLUL
            JDIM(JCL) = NFBL
            DO I=1,NFBL
              HDLJ(I,JCL) = HDL(I)
              DO J=1,NFBL
                TLJ(J,I,JCL) = TL(J,I)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
!
! *** BOUCLE SUR ICSUP
!
67      CONTINUE
E103:   DO ICK=1,MXSYM
!
! *** STOCKAGE DE LA DIMENSION ET DES V./VA. PROPRES DE LA POLYADE SUP
!     POUR LE BLOC JSUP,ICSUP
!
          READ(61,END=4161) JLUK,ICLUK,NFBK
          DO WHILE( NFBK .GT. MXDIMS )
            CALL RESIZE_MXDIMS
          ENDDO
          READ(40,END=4140) JK1,ICK1,NK1
          IF( JK   .NE. JK1   .OR.              &
              ICK  .NE. ICK1       ) GOTO 4151
          IF( NFBK .EQ. 0          ) CYCLE E103
          IF( JLUK .NE. JK    .OR.                  &
              ICK  .NE. ICLUK      ) BACKSPACE(61)
          IF( JLUK .NE. JK    .OR.               &
              ICK  .NE. ICLUK      ) CYCLE E103
          READ(61,END=4161) (HDK(I),I=1,NFBK)
          DO IK=1,NFBK
            READ(61,END=4161) NICK,ICKRO,(TK(IK,I),I=1,NFBK)
          ENDDO
!
! ******************************************************
! *** MATRICE DE TRANSITION DANS LES FONCTIONS DE BASE
! ******************************************************
!
! *** STOCKAGE DES E.M. DANS LES FONCTIONS DE BASE
!
E6:       DO JL=MAX(JK-1,0),JK+1
            READ(40,END=4140) JK1,ICK1,NK1,  &
                              JL1,ICL1,NL1
            ICL = ICL1
            JCL = (JL-JK+1)*MXSYM+ICL
            IF( JL  .NE. JL1 ) GOTO 4151
            IF( NL1 .EQ. 0   ) CYCLE E6
            IDJ = JL-JK+2
!
! *** INITIALISATION DE DIP(IK1,IL1,IDG,IOP)
!
            DO IOP=1,NBOPT
              DO IS=1,MXDIMS
                DO II=1,MXDIMI
                  DIP(IS,II,IDJ,IOP) = 0.D0
                ENDDO
              ENDDO
            ENDDO
!
! *** BOUCLE SUR LES OPERATEURS
!
E301:       DO IOP=1,NBOPT
146           READ(40,END=4140) JK1,ICK1,NK1,JL1,ICL1,NL1,IOP1,NBELM
              DO WHILE( NBELM .GT. MXELMT )
                CALL RESIZE_MXELMT
              ENDDO
              IF( JL    .EQ. JL1  .AND.             &
                  JK    .EQ. JK1  .AND.             &
                  ICK   .EQ. ICK1 .AND.             &
                  IOP   .EQ. IOP1       ) GOTO 150
              IF( NBELM .NE. 0          ) READ(40,END=4140)
              GOTO 146
!
150           IF( NBELM .EQ. 0 ) CYCLE E301
              READ(40,END=4140) (LI(I),KO(I),H(I),I=1,NBELM)
              DO I=1,NBELM
                IK1                  = KO(I)
                IL1                  = LI(I)
                DIP(IK1,IL1,IDJ,IOP) = H(I)
              ENDDO
            ENDDO E301
!
! ******************************************************
! *** MATRICE DE TRANSITION DANS LES FONCTIONS PROPRES
! ******************************************************
!
! *** BOUCLE P Q R SUR LES NIV INF (L=INF)
!
            NFBL = JDIM(JCL)
            IF( NFBL .EQ. 0 ) CYCLE E6
            DO I=1,NFBL
              HDL(I) = HDLJ(I,JCL)
              DO J=1,NFBL
                TL(J,I) = TLJ(J,I,JCL)
              ENDDO
            ENDDO
            IDJ = JL-JK+2
            IF( JK  .NE. JL  .OR.             &
                ICK .NE. ICL      ) CYCLE E6
            IF( ICK .NE. 3        ) CYCLE E6
!
! *** BOUCLE SUR LE NIVEAU SUPERIEUR (H=SUP)
!
            DO IK=1,NFBK
              IF( DJ .NE. 0.D0 ) THEN
!
! *** BOUCLE SUR LE NIVEAU INFERIEUR.
!
                IL = IK
                TM = 0.D0
!
! *** BOUCLE SUR LES OPERATEURS
!
                DO IOP=1,NBOPT
                  EMPRO = 0.D0
                  DO KL=1,NFBL
                    A = 0.D0
                    DO KK=1,NFBK
                      A = A+DIP(KK,KL,IDJ,IOP)*TK(KK,IK)
                    ENDDO
                    EMPRO = EMPRO+A*TL(KL,IL)
                  ENDDO
                  DERI = -EMPRO/SQRT((2.D0*DJ+1.D0)*(DJ+1.D0)*DJ)*SQRT(3.D0)
                  TM   = TM+DERI*PARAT(IOP)                                                        ! --> ELT. MATR. DU M.T.
                ENDDO                                                                              ! BOUCLE SUR LES OPERATEURS
                WRITE(81,1201) HDK(IK),JK,IK,TM
              ENDIF
            ENDDO                                                                                  ! BOUCLE SUR NIV. SUP. (NFB)
          ENDDO E6                                                                                 ! BOUCLE PQR SUR NIV. INF.
        ENDDO E103                                                                                 ! BOUCLE SUR NIV. SUP. (C)
      ENDDO E3                                                                                     ! BOUCLE SUR NIV. SUP. (J)
      CLOSE(80)
      CLOSE(81)
      GOTO 9000
!
4081  PRINT 8108, FSORT
      GOTO  9999
4060  PRINT 8107, FNINF
      GOTO  9999
4061  PRINT 8106, FNSUP
      GOTO  9999
4080  PRINT 8105, FPARA
      GOTO  9999
4040  PRINT 8104, FMTR
      GOTO  9999
4160  PRINT 8103
      GOTO  9999
4161  PRINT 8102
      GOTO  9999
4140  PRINT 8101
      GOTO  9999
4151  PRINT 8100
      GOTO  9999
9997  PRINT 8002
      CLOSE(10)
      GOTO 9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  CLOSE(40)
      CLOSE(60)
      CLOSE(61)
      PRINT *
      END PROGRAM STARKL
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!     LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOPAB(LUI,LUO)
      use mod_dppr
      use mod_par_tds
      use mod_com_starkl
      IMPLICIT NONE
      integer          :: LUI,LUO

      real(kind=dppr)  :: PARA

      integer          :: I,IP
      integer          :: MBOPT

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,2E15.8)
8000  FORMAT(' !!! STARKL : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPAB')
!
      DO I=1,4
        READ(LUI,1000,END=2000)     TITRE
        IF( LUO .NE. 0 ) WRITE(LUO) TITRE
      ENDDO
      READ(LUI,1001,END=2000)     MBOPT,TITRE
      IF( LUO .NE. 0 ) WRITE(LUO) MBOPT
      READ(LUI,1000,END=2000)     TITRE
      IF( LUO .NE. 0 ) WRITE(LUO) TITRE
      READ(LUI,1000,END=2000)     CHAINE
      IF( LUO .NE. 0 ) WRITE(LUO) CHAINE
      DO IP=1,MBOPT
        READ(LUI,1002,END=2000)     CHAINE,PARA
        IF( LUO .NE. 0 ) WRITE(LUO) CHAINE,PARA
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPAB
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!     LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOPBA0(LUI,LUO)
      use mod_dppr
      use mod_par_tds
      use mod_com_starkl
      IMPLICIT NONE
      integer          :: LUI,LUO

      real(kind=dppr)  :: PARA

      integer          :: I,IP
      integer          :: NBOPH

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,2E15.8)
8000  FORMAT(' !!! STARKL : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPBA0')
!
      DO I=1,4
        READ(LUI,END=2000)               TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TRIM(TITRE)
      ENDDO
      READ(LUI,END=2000)               NBOPH,CHAINE
      IF( LUO .NE. 0 ) WRITE(LUO,1001) NBOPH,TRIM(CHAINE)
      READ(LUI,END=2000)               TITRE
      IF( LUO .NE. 0 ) WRITE(LUO,1000) TRIM(TITRE)
      READ(LUI,END=2000)               TITRE
      IF( LUO .NE. 0 ) WRITE(LUO,1000) TRIM(TITRE)
      DO IP=1,NBOPH
        READ(LUI,END=2000)               CHAINE,PARA
        IF( LUO .NE. 0 ) WRITE(LUO,1002) CHAINE,PARA
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPBA0
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!     LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOPAA(LUI,LUO)
      use mod_dppr
      use mod_par_tds
      use mod_main_starkl
      IMPLICIT NONE
      integer          :: LUI,LUO

      real(kind=dppr)  :: PARA

      integer          :: I,IP
      integer          :: MBOPT

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,2E15.8)
8000  FORMAT(' !!! STARKL : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPAA')
!
      DO I=1,4
        READ(LUI,1000,END=2000)          TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TRIM(TITRE)
      ENDDO
      READ(LUI,1001,END=2000)          MBOPT,CHAINE
      DO WHILE( MBOPT .GT. MXOPT )
        CALL RESIZE_MXOPT
      ENDDO
      IF( LUO .NE. 0 ) WRITE(LUO,1001) MBOPT,TRIM(CHAINE)
      READ(LUI,1000,END=2000)          TITRE
      IF( LUO .NE. 0 ) WRITE(LUO,1000) TRIM(TITRE)
      READ(LUI,1000,END=2000)          TITRE
      IF( LUO .NE. 0 ) WRITE(LUO,1000) TRIM(TITRE)
      DO IP=1,MBOPT
        READ(LUI,1002,END=2000)          CHAINE,PARA
        PARAT(IP) = PARA
        IF( LUO .NE. 0 ) WRITE(LUO,1002) CHAINE,PARA
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPAA
