      PROGRAM PREDLX
!
!  depuis un fichier PredFile (de type prediction.t)
!  extraire des predictions (+ pour frequence ou + pour intensite si presente)
!  et les formater en LaTeX dans un fichier PredFile.tex
!
!  APPEL : predlx PredFile [MHz/GHz] [iau] [lpp n] [best]
!
!  iau  : intensity arbitrary unit (default: cm-2.atm-1)
!  lpp  : nb de lignes par pages dans le fichier .tex
!  best : ne garder que la meilleure prediction (si attributions multiples)
!
! NOV 2008
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_main_predlx
      IMPLICIT NONE

      real(kind=dppr)  :: DIFFC,DIFSC
      real(kind=dppr)  :: FCAOC,FEXPC
      real(kind=dppr)  :: SCALC,SEXPC

      integer          :: I
      integer          :: J,JIC,JSC
      integer          :: NBLC,NBLPP,NBOPC,NBR,NBREST,NINFC,NSUPC,NSVC

      character(len =   1)  :: FASC
      character(len =   1)  :: PAINFC,PASUPC
      character(len =   1)  :: SASC,SUBINF,SUBSUP
      character(len =   2)  :: SYINFC,SYSUPC
      character(len =   4)  :: CARG
      character(len =  20)  :: UNITDF,UNITF,UNITI
      character(len =  30)  :: FTSCAL,FTSEXP
      character(len = 120)  :: FPRED
      character(len = 124)  :: FPREDX
      character(len = 500)  :: LINE

      logical          :: LBEST,LFREQ
!
1000  FORMAT(A)
1001  FORMAT(I4)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
2010  FORMAT(/,              &
             'PREDLX : ',A)
2020  FORMAT(F13.6,2X,A,20X,F11.3,F13.6,46X,2(I3,1X,2A,I3,1X))                                     ! selon FORMAT 2020 de eq_tds et eq_int
2021  FORMAT(E13.4,2X,A,21X,F10.3,1X,E12.4)                                                        ! selon FORMAT 2021 de eq_int
2023  FORMAT(48X,E12.4)                                                                            ! selon FORMAT 2021 de eq_int
3020  FORMAT(F13.6   ,' & ',   &
             F13.6   ,' & ',   &
         '$',F11.3  ,'$ & ',   &
             I3      ,' & ',   &
         '$',3A,'$',A,' & ',   &
             I3      ,' & ',   &
             I3      ,' & ',   &
         '$',3A,'$',A,' & ',   &
             I3            ,   &
             '\\'           )
4020  FORMAT(F13.6   ,' & ',   &
             F13.6   ,' & ',   &
         '$',F11.3  ,'$ & ',   &
             A       ,' & ',   &
             A       ,' & ',   &
         '$',F10.3  ,'$ & ',   &
             I3      ,' & ',   &
         '$',3A,'$',A,' & ',   &
             I3      ,' & ',   &
             I3      ,' & ',   &
         '$',3A,'$',A,' & ',   &
             I3            ,   &
             '\\'           )
4021  FORMAT(13X     ,' & ',   &
             F13.6   ,' & ',   &
             11X     ,' & ',   &
             A       ,' & ',   &
             A       ,' & ',   &
         '$',F10.3  ,'$ & ',   &
             I3      ,' & ',   &
         '$',3A,'$',A,' & ',   &
             I3      ,' & ',   &
             I3      ,' & ',   &
         '$',3A,'$',A,' & ',   &
             I3            ,   &
             '\\'           )
4022  FORMAT(F13.6   ,' & ',   &
             F13.6   ,' & ',   &
         '$',F11.3  ,'$ & ',   &
             13X     ,' & ',   &
             A       ,' & ',   &
             10X     ,' & ',   &
             I3      ,' & ',   &
         '$',3A,'$',A,' & ',   &
             I3      ,' & ',   &
             I3      ,' & ',   &
         '$',3A,'$',A,' & ',   &
             I3            ,   &
             '\\'           )
8000  FORMAT(' !!! PREDLX : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8004  FORMAT(' !!! BAD OPTION IN CONTROL FILE')
8100  FORMAT(' !!! ERROR OPENING PREDICTION FILE : ',A)
8101  FORMAT(' !!! ERROR READING PREDICTION FILE : ',A)
8102  FORMAT(' !!! ERROR OPENING LaTeX FILE : ',A)
8103  FORMAT(' !!! ERROR WRITING LaTeX FILE : ',A)
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1000,END=9997) FDATE
      PRINT 2010,            FDATE
      READ(10,1000,END=9997) FPRED
      UNITF  = 'cm$^{-1}$'
      UNITDF = '10$^{-3}$'//TRIM(UNITF)
      UNITI  = 'cm$^{-2}$atm$^{-1}$'
      NBLPP  = 50                                                                                  ! 50 lignes/page
      LBEST  = .FALSE.                                                                             ! pas de reduction des attributions multiples
!
11    READ(10,1000,END=10) CARG
      IF    ( CARG .EQ. 'MHz'  ) THEN
        UNITF  = 'MHz'
        UNITDF = 'kHz'
      ELSEIF( CARG .EQ. 'GHz'  ) THEN
        UNITF  = 'GHz'
        UNITDF = 'MHz'
      ELSEIF( CARG .EQ. 'iau'  ) THEN
        UNITI = 'a.u.'
      ELSEIF( CARG .EQ. 'lpp'  ) THEN
        READ(10,1001,ERR=9997) NBLPP
      ELSEIF( CARG .EQ. 'best' ) THEN
        LBEST = .TRUE.                                                                             ! reduire les attributions multiples
      ELSE
        GOTO 9996
      ENDIF
      GOTO 11
!
10    CLOSE(10)
      PRINT 2000,           TRIM(FPRED)
      OPEN(91,ERR=4910,FILE=TRIM(FPRED),STATUS='UNKNOWN')                                          ! prediction.t
      FPREDX = TRIM(FPRED)//'.tex'
      PRINT 2001,           TRIM(FPREDX)
      OPEN(96,ERR=4960,FILE=TRIM(FPREDX),STATUS='UNKNOWN')                                         ! prediction.t.tex
! lire l entete
      DO I=1,5
        READ(91,1000,ERR=4911)
      ENDDO
      READ(91,1001,ERR=4911) NBOPC
      DO I=1,NBOPC+7
        READ(91,1000,ERR=4911)
      ENDDO
      READ(91,1001,ERR=4911) NBOPC
      DO I=1,NBOPC+3
        READ(91,1000,ERR=4911)
      ENDDO
      READ(91,1001,ERR=4911) NSVC
      DO I=1,NSVC+2
        READ(91,1000,ERR=4911)
      ENDDO
      READ(91,1000,ERR=4911) LINE
      IF( LINE .NE. '' ) THEN
        READ(LINE,1001,ERR=4911) NSVC
        DO I=1,NSVC+1
          READ(91,1000,ERR=4911)
        ENDDO
      ENDIF
      READ(91,1000,ERR=4911)
      READ(91,1000,ERR=4911) LINE
      IF( LINE(22:42) .EQ. ' TRANSITION OPERATORS' ) THEN                                          ! FORMAT 3008 de eq_int
        LFREQ  = .FALSE.                                                                           ! intensites
        NBREST = 9
      ELSE
        LFREQ  = .TRUE.                                                                            ! frequences
        NBREST = 4
      ENDIF
      DO I=1,NBREST                                                                                ! lire le reste
        READ(91,1000,ERR=4911)
      ENDDO
! lire les donnees
      NBR = 0                                                                                      ! nb de raies
!
7     READ(91,1000,ERR=4911,END=8) LINE                                                            ! stocker
      READ(LINE,2020,ERR=4911) FEXPC,FASC
      IF( FASC .NE. '+' ) THEN                                                                     ! pas en frequence; en intensite ?
        IF( LFREQ ) GOTO 7                                                                         ! pas d intensite a tester
        READ(91,2021,ERR=4911) SEXPC,SASC
        IF( SASC .NE. '+' ) GOTO 7                                                                 ! pas en intensite
        BACKSPACE(91)                                                                              ! repositionner
      ENDIF
      READ(LINE,2020,ERR=4911) FEXPC,FASC,DIFFC,FCAOC,   &
                               JIC,SYINFC,PAINFC,NINFC,  &
                               JSC,SYSUPC,PASUPC,NSUPC
      IF( .NOT. LFREQ ) THEN                                                                       ! intensites
        READ(91,1000,ERR=4911) LINE
        READ(LINE,2021,ERR=4911) SEXPC,SASC
        IF( SASC .EQ. '+' ) THEN
          READ(LINE,2021,ERR=4911) SEXPC,SASC,DIFSC,SCALC
        ELSE
          SEXPC = 0.D0
          READ(LINE,2023,ERR=4911) SCALC
        ENDIF
      ENDIF
! tester les attributions multiples
      IF( NBR .EQ. 0 ) GOTO 9                                                                      ! rien a comparer
      IF( LBEST      ) THEN
        IF( JIC    .EQ. JI(NBR)    .AND.         &
            SYINFC .EQ. SYINF(NBR) .AND.         &
            PAINFC .EQ. PAINF(NBR) .AND.         &
            NINFC  .EQ. NINF(NBR)  .AND.         &
            JSC    .EQ. JS(NBR)    .AND.         &
            SYSUPC .EQ. SYSUP(NBR) .AND.         &
            PASUPC .EQ. PASUP(NBR) .AND.         &
            NSUPC  .EQ. NSUP(NBR)        ) THEN
          IF( ABS(DIFFC) .LE. ABS(DIFF(NBR)) ) THEN                                                ! meilleur ?
            GOTO 13                                                                                ! remplacer le precedent
          ELSE
            GOTO 7                                                                                 ! ignorer, lire la ligne suivante
          ENDIF
        ENDIF
      ENDIF
!
9     NBR = NBR+1                                                                                  ! creer une nouvelle donnee
      IF( NBR .GT. MXOBS ) CALL RESIZE_MXOBS
!
13    FEXP(NBR)  = FEXPC                                                                           ! stocker
      DIFF(NBR)  = DIFFC
      FCAO(NBR)  = FCAOC
      JI(NBR)    = JIC
      SYINF(NBR) = SYINFC
      PAINF(NBR) = PAINFC
      NINF(NBR)  = NINFC
      JS(NBR)    = JSC
      SYSUP(NBR) = SYSUPC
      PASUP(NBR) = PASUPC
      NSUP(NBR)  = NSUPC
      SEXP(NBR)  = SEXPC
      DIFS(NBR)  = DIFSC
      SCAL(NBR)  = SCALC
      GOTO 7                                                                                       ! lire la ligne suivante
!
8     CONTINUE                                                                                     ! tout est lu
      CALL ORDER(NBR,FCAO,IND,MXOBS)                                                               ! trier
! ecrire les donnees
      NBLC = 0                                                                                     ! debut de page
E12:  DO I=1,NBR
        J = IND(I)
        IF( NBLC .EQ. 0 ) THEN
          IF( I .NE. 1 ) THEN                                                                      ! saut de page sauf debut
            LINE = '\newpage'
            WRITE(96,1000,ERR=4961) TRIM(LINE)
          ENDIF
! entete LaTeX
          LINE = '\begin{tabular}{'
          IF( LFREQ ) THEN
            LINE = TRIM(LINE)//'ccrrlrrlr'
          ELSE
            LINE = TRIM(LINE)//'ccrllrrlrrlr'
          ENDIF
          LINE = TRIM(LINE)//'}'
          WRITE(96,1000,ERR=4961) TRIM(LINE)
          LINE = '\hline'
          WRITE(96,1000,ERR=4961) TRIM(LINE)
          LINE = '$\tilde{\nu}_e$/'//TRIM(UNITF)//' &'
          WRITE(96,1000,ERR=4961) TRIM(LINE)
          LINE = '$\tilde{\nu}_c$/'//TRIM(UNITF)//' &'
          WRITE(96,1000,ERR=4961) TRIM(LINE)
          LINE = '$\tilde{\nu}_e-\tilde{\nu}_c$/'
          LINE = TRIM(LINE)//TRIM(UNITDF)//' &'
          WRITE(96,1000,ERR=4961) TRIM(LINE)
          IF( .NOT. LFREQ ) THEN
            LINE = '$I_e/$'//TRIM(UNITI)//' &'
            WRITE(96,1000,ERR=4961) TRIM(LINE)
            LINE = '$I_c/$'//TRIM(UNITI)//' &'
            WRITE(96,1000,ERR=4961) TRIM(LINE)
            LINE = '$I_e-I_c/\%$ &'
            WRITE(96,1000,ERR=4961) TRIM(LINE)
          ENDIF
          LINE = '$J''''$        &'
          WRITE(96,1000,ERR=4961) TRIM(LINE)
          LINE = '$C''''$        &'
          WRITE(96,1000,ERR=4961) TRIM(LINE)
          LINE = '$\alpha''''$   &'
          WRITE(96,1000,ERR=4961) TRIM(LINE)
          LINE = '$J''$         &'
          WRITE(96,1000,ERR=4961) TRIM(LINE)
          LINE = '$C''$         &'
          WRITE(96,1000,ERR=4961) TRIM(LINE)
          LINE = '$\alpha''$     \\'
          WRITE(96,1000,ERR=4961) TRIM(LINE)
          LINE = '\hline'
          WRITE(96,1000,ERR=4961) TRIM(LINE)
        ENDIF
        IF( SYINF(J)(2:2) .EQ. '' ) THEN
          SUBINF = ''
        ELSE
          SUBINF = '_'
        ENDIF
        IF( SYSUP(J)(2:2) .EQ. '' ) THEN
          SUBSUP = ''
        ELSE
          SUBSUP = '_'
        ENDIF
        IF( LFREQ ) THEN
          WRITE(96,3020,ERR=4961) FEXP(J),FCAO(J),DIFF(J),                                    &
                                  JI(J),SYINF(J)(1:1),SUBINF,SYINF(J)(2:2),PAINF(J),NINF(J),  &
                                  JS(J),SYSUP(J)(1:1),SUBSUP,SYSUP(J)(2:2),PASUP(J),NSUP(J)
        ELSE
          IF( FEXP(J) .NE. 0.D0 ) THEN
            IF( SEXP(J) .NE. 0.D0 ) THEN
              CALL FTDATA(SEXP(J),FTSEXP)
              CALL FTDATA(SCAL(J),FTSCAL)
              WRITE(96,4020,ERR=4961) FEXP(J),FCAO(J),DIFF(J),                                    &
                                      TRIM(FTSEXP),TRIM(FTSCAL),DIFS(J),                          &
                                      JI(J),SYINF(J)(1:1),SUBINF,SYINF(J)(2:2),PAINF(J),NINF(J),  &
                                      JS(J),SYSUP(J)(1:1),SUBSUP,SYSUP(J)(2:2),PASUP(J),NSUP(J)
            ELSE
              CALL FTDATA(SCAL(J),FTSCAL)
              WRITE(96,4022,ERR=4961) FEXP(J),FCAO(J),DIFF(J),                                    &
                                      TRIM(FTSCAL),                                               &
                                      JI(J),SYINF(J)(1:1),SUBINF,SYINF(J)(2:2),PAINF(J),NINF(J),  &
                                      JS(J),SYSUP(J)(1:1),SUBSUP,SYSUP(J)(2:2),PASUP(J),NSUP(J)
            ENDIF
          ELSE
            IF( SEXP(J) .NE. 0.D0 ) THEN
              CALL FTDATA(SEXP(J),FTSEXP)
              CALL FTDATA(SCAL(J),FTSCAL)
              WRITE(96,4021,ERR=4961) FCAO(J),                                                    &
                                      TRIM(FTSEXP),TRIM(FTSCAL),DIFS(J),                          &
                                      JI(J),SYINF(J)(1:1),SUBINF,SYINF(J)(2:2),PAINF(J),NINF(J),  &
                                      JS(J),SYSUP(J)(1:1),SUBSUP,SYSUP(J)(2:2),PASUP(J),NSUP(J)
            ELSE
              CYCLE E12
            ENDIF
          ENDIF
        ENDIF
        NBLC = NBLC+1
        IF( NBLC .NE. NBLPP ) CYCLE E12                                                            ! prochaine ligne
        LINE = '\hline'                                                                            ! fin de tableau
        WRITE(96,1000,ERR=4961) TRIM(LINE)
        LINE = '\end{tabular}'
        WRITE(96,1000,ERR=4961) TRIM(LINE)
        NBLC = 0                                                                                   ! prochaine page
      ENDDO E12
      IF( NBLC .NE. 0 ) THEN                                                                       ! fin de tableau
        LINE = '\hline'                                                                            ! fin
        WRITE(96,1000,ERR=4961) TRIM(LINE)
        LINE = '\end{tabular}'
        WRITE(96,1000,ERR=4961) TRIM(LINE)
      ENDIF
      CLOSE(96)
      CLOSE(91)
      GOTO 9000
!
4961  PRINT 8103, FPREDX
      GOTO  9999
4960  PRINT 8102, FPREDX
      GOTO  9999
4911  PRINT 8101, FPRED
      GOTO  9999
4910  PRINT 8100, FPRED
      GOTO  9999
9996  PRINT 8004
      GOTO  9999
9997  PRINT 8002
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM PREDLX
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!  formater en LaTex un reel.
!
      SUBROUTINE FTDATA(DATA,WSTR)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: DATA
      character(len =  *)  :: WSTR

      integer          :: I
      integer          :: J

      character(len =  30) :: STR
!
1000  FORMAT(E13.4)
1001  FORMAT(I4)
!
      WRITE(STR,1000) DATA
      I    = INDEX(STR,'E')
      WSTR = STR(:I-1)
      READ(STR(I+1:),*) J
      IF( J .NE. 0 ) THEN
        WRITE(STR,1001) J
        WSTR = TRIM(WSTR)//'$\times 10^{'//TRIM(STR)//'}$'
      ENDIF
!
      RETURN
      END SUBROUTINE FTDATA
