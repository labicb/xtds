#! /bin/sh
 set -v
##
## Example of intensity parameter fitting :
## Pentad band of 12CH4.
##
BASD=will be set by *tds_INSTALL
##
 SCRD=$BASD/prog/exe
 PARD=$BASD/para/12CH4
 EXPD=$BASD/exp/12CH4
 CTRD=$BASD/ctrp/12CH4
##
## Jmax values.
##
 JPlow=22
 JPupp=21
 JPupp_low=21
##
## Parameter file.
##
 SPARA=Pa_PENTADmGS
 WPARA=$SPARA"_work"
##
## Assignment files.
##
 ASG1=$EXPD/dip/ASG_Brown
 ASG2=$EXPD/dip/ASG_PR_JQSRT97_sca
 ASG3=$EXPD/dip/ASG_Q_JCP92_sca
 ASG4=$EXPD/dip/ASG_JOSA76_sca
 ASG5=$EXPD/dip/ASG_JMS79
 ASG6=$EXPD/dip/ASG_calib
##
## Parameter constraint file.
##
 CLF=$CTRD/Pentad
##
 PARA=$SPARA"_nulpar"
 $SCRD/passx nulpar T $CLF $WPARA $PARA
#########################################################
##
## Hamiltonian diagonalization and transition moments.
##
## GS and Pentad levels.
##
 $SCRD/passx hdiag  P0 N1 D6       $JPlow     $PARA
 $SCRD/passx hdiag  P2 N5 D665     $JPupp     $PARA
 $SCRD/passx trmomt P2 N5 P0 N1 D3 $JPupp_low $PARA dip
##
## Pentad fit.
##
 \cp $CLF CL_T_P2mP0
##
 $SCRD/exasg 'P2mP0' $ASG1
 \cp ASG_EXP ASG_Brown.t
 \mv ASG_EXP assignments.t
##
 $SCRD/passx eq_int P2 P0 D3 296 1.D-4 $JPupp_low $PARA CL_T_P2mP0 prec
 \mv normal_eq.t       NQ_T_P2mP0_1
 \mv prediction.t      Pred_P2mP0_1
 \mv prediction_mix.t  Pred_mix_P2mP0_1
 \mv statistics.t      Stat_P2mP0_1
##
 $SCRD/exasg 'P2mP0' $ASG2
 \cp ASG_EXP ASG_PR_JQSRT97.t
 \mv ASG_EXP assignments.t
##
 $SCRD/passx eq_int P2 P0 D3 296 1.D-4 $JPupp_low $PARA CL_T_P2mP0 prec
 \mv normal_eq.t       NQ_T_P2mP0_2
 \mv prediction.t      Pred_P2mP0_2
 \mv prediction_mix.t  Pred_mix_P2mP0_2
 \mv statistics.t      Stat_P2mP0_2
##
 $SCRD/exasg 'P2mP0' $ASG3
 \cp ASG_EXP ASG_Q_JCP92_sca.t
 \mv ASG_EXP assignments.t
##
 $SCRD/passx eq_int P2 P0 D3 296 1.D-4 $JPupp_low $PARA CL_T_P2mP0 prec
 \mv normal_eq.t       NQ_T_P2mP0_3
 \mv prediction.t      Pred_P2mP0_3
 \mv prediction_mix.t  Pred_mix_P2mP0_3
 \mv statistics.t      Stat_P2mP0_3
##
 $SCRD/exasg 'P2mP0' $ASG4
 \cp ASG_EXP ASG_JOSA76_sca.t
 \mv ASG_EXP assignments.t
##
 $SCRD/passx eq_int P2 P0 D3 295 1.D-4 $JPupp_low $PARA CL_T_P2mP0 prec
 \mv normal_eq.t       NQ_T_P2mP0_4
 \mv prediction.t      Pred_P2mP0_4
 \mv prediction_mix.t  Pred_mix_P2mP0_4
 \mv statistics.t      Stat_P2mP0_4
##
 $SCRD/exasg 'P2mP0' $ASG5
 \cp ASG_EXP ASG_JMS79.t
 \mv ASG_EXP assignments.t
##
 $SCRD/passx eq_int P2 P0 D3 294 1.D-4 $JPupp_low $PARA CL_T_P2mP0 prec
 \mv normal_eq.t       NQ_T_P2mP0_5
 \mv prediction.t      Pred_P2mP0_5
 \mv prediction_mix.t  Pred_mix_P2mP0_5
 \mv statistics.t      Stat_P2mP0_5
##
 $SCRD/exasg 'P2mP0' $ASG6
 \cp ASG_EXP ASG_calib.t
 \mv ASG_EXP assignments.t
##
 $SCRD/passx eq_int P2 P0 D3 296 1.D-4 $JPupp_low $PARA CL_T_P2mP0 prec
 \mv normal_eq.t       NQ_T_P2mP0_6
 \mv prediction.t      Pred_P2mP0_6
 \mv prediction_mix.t  Pred_mix_P2mP0_6
 \mv statistics.t      Stat_P2mP0_6
##
## New intensity parameter estimates.
##
 $SCRD/passx paradj T NQ_T_P2mP0_1 $PARA  \
                      NQ_T_P2mP0_2 $PARA  \
                      NQ_T_P2mP0_3 $PARA  \
                      NQ_T_P2mP0_4 $PARA  \
                      NQ_T_P2mP0_5 $PARA  \
                      NQ_T_P2mP0_6 $PARA
 \rm NQ_T_P2mP0_1 NQ_T_P2mP0_2 NQ_T_P2mP0_3 NQ_T_P2mP0_4 NQ_T_P2mP0_5 NQ_T_P2mP0_6
##
 \rm assignments.t
 \rm     TD*
 \rm CL* ED*     VP*
###
### The new parameter files are :
###
###                 NQ_T_P2mP0_*_adj
###

