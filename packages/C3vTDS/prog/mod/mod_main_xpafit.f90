
      module mod_main_xpafit

      use mod_dppr
      use mod_par_tds
      use mod_par_x
      use mod_com_cas
      use mod_com_der
      use mod_com_derv
      use mod_com_en
      use mod_com_pgde
      use mod_com_pgdh
      use mod_com_statf
      use mod_com_stats
      use mod_com_trm
      use mod_com_trmomt
      use mod_com_xassi
      use mod_com_xfjac
      use mod_com_xfonc
      use mod_com_xhdic
      use mod_com_xpafit
      use mod_com_xpara
      use mod_com_xtran
      use mod_com_xwf
      use mod_com_xwfa
      use mod_com_xws

private  :: RESIZE_LWA, RESIZE_MXOBZ, RESIZE_MXOP, RESIZE_MXTRAD


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_cas
      call alloc_der
      call alloc_derv
      call alloc_en
      call alloc_pgde
      call alloc_pgdh
      call alloc_statf
      call alloc_stats
      call alloc_trm
      call alloc_trmomt
      call alloc_xassi
      call alloc_xfjac
      call alloc_xfonc
      call alloc_xhdic
      call alloc_xpafit
      call alloc_xpara
      call alloc_xtran
      call alloc_xwf
      call alloc_xwfa
      call alloc_xws
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_LWA
      IMPLICIT NONE

      integer  :: M_LWA

      M_LWA = 5*MXOP+MXOBZ
      CALL RESIZE_LWA_XPAFIT(M_LWA)
      LWA = M_LWA
!
      return
      end subroutine RESIZE_LWA

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXATR
      IMPLICIT NONE

      integer  :: M_ATR

      M_ATR = MXRES(MXATR)
      call RESIZE_MXATR_XPARA(M_ATR)
      call RESIZE_MXATR_XTRAN(M_ATR)
      call RESIZE_MXATR_XPAFIT(M_ATR)
      call RESIZE_MXATR_XWS(M_ATR)
      MXATR = M_ATR
! dependencies
      CALL RESIZE_MXTRAD
!
      return
      end subroutine RESIZE_MXATR

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXATRT
      IMPLICIT NONE

      integer  :: M_ATRT

      M_ATRT = MXRES(MXATRT)
      call RESIZE_MXATRT_XPARA(M_ATRT)
      call RESIZE_MXATRT_XTRAN(M_ATRT)
      MXATRT = M_ATRT
! dependencies
      CALL RESIZE_MXOP
!
      return
      end subroutine RESIZE_MXATRT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXDIMI
      IMPLICIT NONE

      integer  :: M_DIMI

      M_DIMI = MXRES(MXDIMI)
      call RESIZE_MXDIMI_TRM(M_DIMI)
      MXDIMI = M_DIMI
!
      return
      end subroutine RESIZE_MXDIMI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXDIMS
      IMPLICIT NONE

      integer  :: M_DIMS

      M_DIMS = MXRES(MXDIMS)
      call RESIZE_MXDIMS_PGDH(M_DIMS)
      call RESIZE_MXDIMS_TRM(M_DIMS)
      call RESIZE_MXDIMS_XHDIC(M_DIMS)
      MXDIMS = M_DIMS
!
      return
      end subroutine RESIZE_MXDIMS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXELMD
      IMPLICIT NONE

      integer  :: M_ELMD

      M_ELMD = MXRES(MXELMD)
      call RESIZE_MXELMD_PGDH(M_ELMD)
      MXELMD = M_ELMD
!
      return
      end subroutine RESIZE_MXELMD

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXELMT
      IMPLICIT NONE

      integer  :: M_ELMT

      M_ELMT = MXRES(MXELMT)
      call RESIZE_MXELMT_TRM(M_ELMT)
      MXELMT = M_ELMT
!
      return
      end subroutine RESIZE_MXELMT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXENI
      IMPLICIT NONE

      integer  :: M_ENI

      M_ENI = MXRES(MXENI)
      call RESIZE_MXENI_XWFA(M_ENI)
      call RESIZE_MXENI_XWS(M_ENI)
      MXENI = M_ENI
!
      return
      end subroutine RESIZE_MXENI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXNIV
      IMPLICIT NONE

      integer  :: M_NIV

      M_NIV = MXRES(MXNIV)
      call RESIZE_MXNIV_STATF(M_NIV)
      call RESIZE_MXNIV_XWF(M_NIV)
      MXNIV = M_NIV
!
      return
      end subroutine RESIZE_MXNIV

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOBS
      IMPLICIT NONE

      integer  :: M_OBS

      M_OBS = MXRES(MXOBS)
      call RESIZE_MXOBS_CAS(M_OBS)
      call RESIZE_MXOBS_DER(M_OBS)
      call RESIZE_MXOBS_EN(M_OBS)
      call RESIZE_MXOBS_XASSI(M_OBS)
      call RESIZE_MXOBS_XWF(M_OBS)
      MXOBS = M_OBS
! dependencies
      CALL RESIZE_MXOBZ
!
      return
      end subroutine RESIZE_MXOBS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOBZ
      IMPLICIT NONE

      integer  :: M_OBZ

      M_OBZ = 2*MXOBS+MXOP
      CALL RESIZE_MXOBZ_XFJAC(M_OBZ)
      CALL RESIZE_MXOBZ_XFONC(M_OBZ)
      CALL RESIZE_MXOBZ_XPAFIT(M_OBZ)
      MXOBZ = M_OBZ
! dependencies
      CALL RESIZE_LWA
!
      return
      end subroutine RESIZE_MXOBZ

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOP
      IMPLICIT NONE

      integer  :: M_OP

      M_OP = MXOPH+MXOPT*MXATRT
      CALL RESIZE_MXOP_XPARA(M_OP)
      CALL RESIZE_MXOP_XPAFIT(M_OP)
      MXOP = M_OP
! dependencies
      CALL RESIZE_MXOBZ
!
      return
      end subroutine RESIZE_MXOP

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPH
      IMPLICIT NONE

      integer  :: M_OPH

      M_OPH = MXRES(MXOPH)
      call RESIZE_MXOPH_DER(M_OPH)
      call RESIZE_MXOPH_DERV(M_OPH)
      call RESIZE_MXOPH_PGDE(M_OPH)
      call RESIZE_MXOPH_PGDH(M_OPH)
      call RESIZE_MXOPH_XPARA(M_OPH)
      call RESIZE_MXOPH_XWFA(M_OPH)
      MXOPH = M_OPH
! dependencies
      CALL RESIZE_MXOP
!
      return
      end subroutine RESIZE_MXOPH

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPT
      IMPLICIT NONE

      integer  :: M_OPT

      M_OPT = MXRES(MXOPT)
      call RESIZE_MXOPT_TRM(M_OPT)
      call RESIZE_MXOPT_TRMOMT(M_OPT)
      call RESIZE_MXOPT_XPARA(M_OPT)
      call RESIZE_MXOPT_XWS(M_OPT)
      MXOPT = M_OPT
! dependencies
      CALL RESIZE_MXOP
!
      return
      end subroutine RESIZE_MXOPT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXSNV
      IMPLICIT NONE

      integer  :: M_SNV

      M_SNV = MXRES(MXSNV)
      call RESIZE_MXSNV_STATS(M_SNV)
      call RESIZE_MXSNV_XHDIC(M_SNV)
      call RESIZE_MXSNV_XWF(M_SNV)
      call RESIZE_MXSNV_XWFA(M_SNV)
      call RESIZE_MXSNV_XWS(M_SNV)
      MXSNV = M_SNV
!
      return
      end subroutine RESIZE_MXSNV

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXTRAD
      IMPLICIT NONE

      integer  :: M_TRAD

      M_TRAD = 2*MXATR
      CALL RESIZE_MXTRAD_XTRAN(M_TRAD)
      MXTRAD = M_TRAD
!
      return
      end subroutine RESIZE_MXTRAD

      end module mod_main_xpafit
