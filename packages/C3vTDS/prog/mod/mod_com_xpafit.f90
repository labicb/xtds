
      module mod_com_xpafit

      use mod_dppr
      use mod_par_tds
      use mod_par_x
      use mod_alloc
      IMPLICIT NONE

      integer ,save  :: NFEV = 0
      integer ,save  :: NJEV = 0
!
      real(kind=dppr) ,pointer ,dimension(:,:) ,save  :: AIY                                       ! (MXOP,2*MXOP+1)
      real(kind=dppr) ,pointer ,dimension(:,:) ,save  :: FJAC                                      ! (MXOBZ,MXOP)
      real(kind=dppr) ,pointer ,dimension(:)   ,save  :: WA                                        ! (LWA)

      integer         ,pointer ,dimension(:)   ,save  :: IPVT                                      ! (MXOP)
      integer         ,pointer ,dimension(:)   ,save  :: KTRT                                      ! (MXATR)
      integer         ,pointer ,dimension(:)   ,save  :: ISEL                                      ! (MXOBZ)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_XPAFIT
      IMPLICIT NONE

      integer  :: ierr

      allocate(AIY(MXOP,2*MXOP+1),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPAFIT_AIY')
      AIY = 0.d0
      allocate(FJAC(MXOBZ,MXOP),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPAFIT_FJAC')
      FJAC = 0.d0
      allocate(WA(LWA),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPAFIT_WA')
      WA = 0.d0

      allocate(IPVT(MXOP),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPAFIT_IPVT')
      IPVT = 0
      allocate(KTRT(MXATR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPAFIT_KTRT')
      KTRT = 0
      allocate(ISEL(MXOBZ),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPAFIT_ISEL')
      ISEL = 0
!
      return
      end subroutine ALLOC_XPAFIT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! LWA

      subroutine RESIZE_LWA_XPAFIT(C_LWA)
      IMPLICIT NONE
      integer :: C_LWA

      integer :: ierr

! WA
      allocate(rpd1(C_LWA),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_LWA_XPAFIT_WA')
      rpd1 = 0.d0
      rpd1(1:LWA) = WA(:)
      deallocate(WA)
      WA => rpd1
!
      return
      end subroutine RESIZE_LWA_XPAFIT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXATR

      subroutine RESIZE_MXATR_XPAFIT(C_ATR)
      IMPLICIT NONE
      integer :: C_ATR

      integer :: ierr

! KTRT
      allocate(ipd1(C_ATR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXATR_XPAFIT_KTRT')
      ipd1 = 0
      ipd1(1:MXATR) = KTRT(:)
      deallocate(KTRT)
      KTRT => ipd1
!
      return
      end subroutine RESIZE_MXATR_XPAFIT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOBZ

      subroutine RESIZE_MXOBZ_XPAFIT(C_OBZ)
      IMPLICIT NONE
      integer :: C_OBZ

      integer :: i,ierr

! FJAC
      allocate(rpd2(C_OBZ,MXOP),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBZ_XPAFIT_FJAC')
      rpd2 = 0.d0
      do i=1,MXOP
        rpd2(1:MXOBZ,i) = FJAC(:,i)
      enddo
      deallocate(FJAC)
      FJAC => rpd2

! ISEL
      allocate(ipd1(C_OBZ),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBZ_XPAFIT_ISEL')
      ipd1 = 0
      ipd1(1:MXOBZ) = ISEL(:)
      deallocate(ISEL)
      ISEL => ipd1
!
      return
      end subroutine RESIZE_MXOBZ_XPAFIT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOP

      subroutine RESIZE_MXOP_XPAFIT(C_OP)
      IMPLICIT NONE
      integer :: C_OP

      integer :: i,ierr

! AIY
      allocate(rpd2(C_OP,2*C_OP+1),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOP_XPAFIT_AIY')
      rpd2 = 0.d0
      do i=1,2*MXOP+1
        rpd2(1:MXOP,i) = AIY(:,i)
      enddo
      deallocate(AIY)
      AIY => rpd2
! FJAC
      allocate(rpd2(MXOBZ,C_OP),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOP_XPAFIT_FJAC')
      rpd2 = 0.d0
      do i=1,MXOP
        rpd2(:,i) = FJAC(:,i)
      enddo
      deallocate(FJAC)
      FJAC => rpd2

! IPVT
      allocate(ipd1(C_OP),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOP_XPAFIT_IPVT')
      ipd1 = 0
      ipd1(1:MXOP) = IPVT(:)
      deallocate(IPVT)
      IPVT => ipd1
!
      return
      end subroutine RESIZE_MXOP_XPAFIT

      end module mod_com_xpafit
