
      module mod_com_fp

      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE

      real(kind=dppr) ,dimension(NBVQN) ,save  :: VIBNU
      real(kind=dppr)                   ,save  :: ABUND
      real(kind=dppr)                   ,save  :: SPINY
      real(kind=dppr)                   ,save  :: FPVIB,B0,A0,BB0,DD0

      contains

      function FPART(TROT,TVIB)
      use mod_dppr
      use mod_par_tds
      use mod_com_const
      IMPLICIT NONE
      real(kind=dppr)  :: FPART
      real(kind=dppr)  :: TROT,TVIB

      real(kind=dppr)  :: FRMD
      real(kind=dppr)  :: A
      real(kind=dppr)  :: B
      real(kind=dppr)  :: AA,ALPHA
      real(kind=dppr)  :: BETA
      real(kind=dppr)  :: GAMA
      real(kind=dppr)  :: ZETA
      real(kind=dppr)  :: SIGMAS
      real(kind=dppr)  :: M
      real(kind=dppr)  :: DELTA
      real(kind=dppr)  :: P
      real(kind=dppr)  :: FTEMP
      real(kind=dppr)  :: SIF

      integer ,dimension(NBVQN)  :: IDEG = (/ 1, 1, 1, 2, 2, 2 /)
      integer          :: I
!
      A = PLANK*CLUM/BOLT
      IF( FPVIB .EQ. 0.D0 ) THEN
        FPVIB = 1.D0
        DO I=1,NBVQN
          B     = 1.D0-EXP(-VIBNU(I)*A/TVIB)
          B     = B**IDEG(I)
          FPVIB = FPVIB/B
        ENDDO
      ENDIF
!
! *** PARTITION FUNCTION FROM MCDOWELL JCP 93  2810 (1990)
! WARNING: SIF = SPIN INDEPENDENT FACTOR = 2*SPINZ+1
! WARNING: NEEDS TO IMPLEMENT SPINZ
! WARNING: PRESENTVALUE (4) IS FOR Z = Cl (SPINZ = 3/2)
!
      BETA = PLANK*CLUM*B0/BOLT/TROT
      SIGMAS = DBLE((2*SPINY+1)**3/3)
      M = B0/A0
      P = DBLE(2/(2*SPINY+1)**2)
      SIF = 4;
      DELTA = P*DEXP(PI**2*M*(BETA*M-6)/54/BETA)
      FTEMP = SIGMAS*DSQRT(PI*M)*DEXP(BETA*(4-M)/12)/DSQRT(BETA**3)
      FRMD = FTEMP*(1+BETA**2*(1-M)**2/90)*(1+DELTA)*SIF
      FPART = FRMD*FPVIB
!
      RETURN
      end function FPART

      end module mod_com_fp
