
      module mod_com_xwfa

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,pointer ,dimension(:,:) ,save  :: DERINFA                                   ! (MXOPH,MXENI)
      real(kind=dppr) ,pointer ,dimension(:)   ,save  :: DERSUPA                                   ! (MXOPH)
      real(kind=dppr) ,pointer ,dimension(:)   ,save  :: ENINFA                                    ! (MXENI)

      integer         ,pointer ,dimension(:)   ,save  :: IXCL                                      ! (MXOPH)
      integer         ,pointer ,dimension(:)   ,save  :: JIC                                       ! (MXENI)
      integer         ,pointer ,dimension(:)   ,save  :: IPCSNA,NUSVSUA                            ! (MXSNV)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_XWFA
      IMPLICIT NONE

      integer  :: ierr

      allocate(DERINFA(MXOPH,MXENI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWFA_DERINFA')
      DERINFA = 0.d0
      allocate(DERSUPA(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWFA_DERSUPA')
      DERSUPA = 0.d0
      allocate(ENINFA(MXENI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWFA_ENINFA')
      ENINFA = 0.d0

      allocate(IXCL(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWFA_IXCL')
      IXCL = 0
      allocate(JIC(MXENI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWFA_JIC')
      JIC = 0
      allocate(IPCSNA(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWFA_IPCSNA')
      IPCSNA = 0
      allocate(NUSVSUA(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWFA_NUSVSUA')
      NUSVSUA = 0
!
      return
      end subroutine ALLOC_XWFA

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXENI

      subroutine RESIZE_MXENI_XWFA(C_ENI)
      IMPLICIT NONE
      integer :: C_ENI

      integer :: i,ierr

! DERINFA
      allocate(rpd2(MXOPH,C_ENI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXENI_XWFA_DERINFA')
      rpd2 = 0.d0
      do i=1,MXENI
        rpd2(:,i) = DERINFA(:,i)
      enddo
      deallocate(DERINFA)
      DERINFA => rpd2
! ENINFA
      allocate(rpd1(C_ENI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXENI_XWFA_ENINFA')
      rpd1 = 0.d0
      rpd1(1:MXENI) = ENINFA(:)
      deallocate(ENINFA)
      ENINFA => rpd1

! JIC
      allocate(ipd1(C_ENI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXENI_XWFA_JIC')
      ipd1 = 0
      ipd1(1:MXENI) = JIC(:)
      deallocate(JIC)
      JIC => ipd1
!
      return
      end subroutine RESIZE_MXENI_XWFA

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPH

      subroutine RESIZE_MXOPH_XWFA(C_OPH)
      IMPLICIT NONE
      integer :: C_OPH

      integer :: i,ierr

! DERINFA
      allocate(rpd2(C_OPH,MXENI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_XWFA_DERINFA')
      rpd2 = 0.d0
      do i=1,MXENI
        rpd2(1:MXOPH,i) = DERINFA(:,i)
      enddo
      deallocate(DERINFA)
      DERINFA => rpd2
! DERSUPA
      allocate(rpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_XWFA_DERSUPA')
      rpd1 = 0.d0
      rpd1(1:MXOPH) = DERSUPA(:)
      deallocate(DERSUPA)
      DERSUPA => rpd1

! IXCL
      allocate(ipd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_XWFA_IXCL')
      ipd1 = 0
      ipd1(1:MXOPH) = IXCL(:)
      deallocate(IXCL)
      IXCL => ipd1
!
      return
      end subroutine RESIZE_MXOPH_XWFA

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNV

      subroutine RESIZE_MXSNV_XWFA(C_SNV)
      IMPLICIT NONE
      integer :: C_SNV

      integer :: ierr

! IPCSNA
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_XWFA_IPCSNA')
      ipd1 = 0
      ipd1(1:MXSNV) = IPCSNA(:)
      deallocate(IPCSNA)
      IPCSNA => ipd1
! NUSVSUA
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_XWFA_NUSVSUA')
      ipd1 = 0
      ipd1(1:MXSNV) = NUSVSUA(:)
      deallocate(NUSVSUA)
      NUSVSUA => ipd1
!
      return
      end subroutine RESIZE_MXSNV_XWFA

      end module mod_com_xwfa
