!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
! CE PROGRAMME CALCULE LES TERMES N INTERVENANT DANS
! LE CALCUL DES 3C-IL
!
      FUNCTION NKC(AK1,IC1,AK2,IC2,AK3,IC3)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: NKC
      real(kind=dppr)  :: AK1,AK2,AK3
      integer          :: IC1,IC2,IC3

      real(kind=dppr)  :: AN
      real(kind=dppr)  :: Y

      integer          :: IL1,ILI1,ILP1,ILS1
      integer          :: IL2,ILI2,ILP2,ILS2
      integer          :: IL3,ILI3,ILP3,ILS3
      integer          :: IFF
!
      AN = 0.D0
      CALL LIMC(IC1,ILI1,ILS1,ILP1)
      DO IL1=ILI1,ILS1,ILP1
        CALL LIMC(IC2,ILI2,ILS2,ILP2)
        DO IL2=ILI2,ILS2,ILP2
          CALL LIMC(IC3,ILI3,ILS3,ILP3)
          DO IL3=ILI3,ILS3,ILP3
            CALL FKCL(AK1,AK2,AK3,IC1,IC2,IC3,IL1,IL2,IL3,Y,IFF)
            AN = AN+(ABS(Y))**2
          ENDDO
        ENDDO
      ENDDO
      NKC = AN
!
      RETURN
      END FUNCTION NKC
