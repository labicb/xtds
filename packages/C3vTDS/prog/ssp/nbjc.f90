!
! *** MULTIPLICITES DE A1,A2,E,F1 ET F2 DANS D**J(U OU G)
!
! SMIL CHAMPION DEC 78
!
      SUBROUTINE NBJC(IR,IUG,NA1,NA2,NE,NF1,NF2)
      IMPLICIT NONE
      integer          :: IR,IUG,NA1,NA2,NE,NF1,NF2

      integer ,dimension(12)  :: N = (/ 10000,    10,   101, 1011,     &
                                        10111,   121, 11112, 1122,     &
                                        10222, 11132, 11223, 1233  /)
      integer          :: IP,IR1
      integer          :: NB,NM
!
      IP  = IR/12
      IR1 = IR-12*(IR/12)+1
      NB  = N(IR1)
      NA1 = NB/10000
      NA2 = (NB-NA1*10000)/1000
      NE  = (NB-NA1*10000-NA2*1000)/100
      NF1 = (NB-NA1*10000-NA2*1000-NE*100)/10
      NF2 = NB-NA1*10000-NA2*1000-NE*100-NF1*10
      NA1 = NA1+IP
      NA2 = NA2+IP
      NE  = NE+2*IP
      NF1 = NF1+3*IP
      NF2 = NF2+3*IP
      IF( IUG .LE. 1 ) RETURN
      NM  = NA2
      NA2 = NA1
      NA1 = NM
      NM  = NF2
      NF2 = NF1
      NF1 = NM
!
      RETURN
      END SUBROUTINE NBJC
