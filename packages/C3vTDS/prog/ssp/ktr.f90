!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
!       CE PROGRAMME VERIFIE LES REGLES DE PRODUITS DES IRREPS DANS CIV
!       (TRIANGULAR DES COEFFICIENTS K)
!
      FUNCTION KTR(AK1,AK2,AK3)
      use mod_dppr
      IMPLICIT NONE
      integer          :: KTR
      real(kind=dppr)  :: AK1,AK2,AK3

!
      KTR = 0
      IF( AK1 .EQ. AK2 ) THEN
        IF    ( AK1 .LE.  0.D0          ) THEN
          IF( AK3 .EQ. 0.D0 ) THEN
            KTR = 1
            RETURN
          ENDIF
        ELSEIF( AK3 .EQ. -1.D0     .OR.         &
                AK3 .EQ.  0.D0     .OR.         &
                AK3 .EQ.  2.D0*AK1      ) THEN
          KTR = 1
          RETURN
        ENDIF
      ENDIF
      IF( AK1 .LE. 0.D0 .AND.         &
          AK2 .LE. 0.D0 .AND.         &
          AK1 .NE. AK2        ) THEN
        IF( AK3 .EQ. -1.D0 ) THEN
          KTR = 1
          RETURN
        ENDIF
      ENDIF
      IF( AK1 .NE. AK2  .AND.         &
          AK1 .GE. 0.D0 .AND.         &
          AK2 .GE. 0.D0       ) THEN
        IF( AK3 .EQ. ABS(AK1-AK2) .OR.         &
            AK3 .EQ. AK2+AK1           ) THEN
          KTR = 1
          RETURN
        ENDIF
      ENDIF
      IF( AK1 .EQ. -1.D0 .AND.         &
          AK2 .GT.  0.D0       ) THEN
        IF( AK3 .EQ. AK2 ) THEN
          KTR = 1
          RETURN
        ENDIF
      ENDIF
      IF( AK1 .GT.  0.D0 .AND.         &
          AK2 .EQ. -1.D0       ) THEN
        IF( AK3 .EQ. AK1 ) THEN
          KTR = 1
          RETURN
        ENDIF
      ENDIF
!
      RETURN
      END FUNCTION KTR
