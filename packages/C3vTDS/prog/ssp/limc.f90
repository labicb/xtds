!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
      SUBROUTINE LIMC(IC,ILI,ILS,ILP)
      IMPLICIT NONE
      integer          :: IC,ILI,ILS,ILP

!
      ILI = 1
      ILS = 2
      ILP = 1
      IF( IC .EQ. 2 .OR.         &
          IC .EQ. 5      ) THEN
        ILI = 2
        ILS = 2
        ILP = 1
      ENDIF
      IF( IC .EQ. 1 .OR.         &
          IC .EQ. 4      ) THEN
        ILI = 1
        ILS = 1
        ILP = 1
      ENDIF
!
      RETURN
      END SUBROUTINE LIMC
