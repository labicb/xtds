!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
! ***  CALCULE LES ELEM. MAT. DES OP. DU M. DIP.
!
      SUBROUTINE CALDI(JS,ICS,NFBS,JI,ICI,NFBI,ICOR,IGV,IGRV,KCC,IGRVC)
      use mod_dppr
      use mod_par_tds
      use mod_com_sy
      use mod_main_dipmat
      IMPLICIT NONE
      integer          :: JS,ICS,NFBS,JI,ICI,NFBI,ICOR,IGV,IGRV,KCC,IGRVC

! functions
      real(kind=dppr)  :: EMRRO
      integer          :: IDIMK

      real(kind=dppr) , dimension(MDMIGR)  ::  AKRVI,BKRVI
      real(kind=dppr)  :: A1,A2,A3,A4,A5,A6,A7,A8,AKRV
      real(kind=dppr)  :: B1,B2,B3,B4,B5,B6,B7,B8,BKRV
      real(kind=dppr)  :: COEF
      real(kind=dppr)  :: ELM,ELM1,ELM2
      real(kind=dppr)  :: PH,PHA,PHASE,PHASE1,PHASE2,PHI,PHS,PH1,PH12,PH2,P1,P2
      real(kind=dppr)  :: SIX,SIXK
      real(kind=dppr)  :: TERM1,TERM2
      real(kind=dppr)  :: XC,XCK,XK,XR,XRK,XS,XSC,XSK

      integer          :: IAV1,IAV2,IBRA,IFK,IG,IJ,IKET,IKI,IKKI,IKKS,IKR1,IKR2,IKRV1,IKRV2,IKS
      integer          :: IL,IM,IMK,IN,INK,IO,IP,IP2,IR,IR2,ISIX,ISIXK,IZ,I1,I2
      integer          :: KR
      integer          :: NJ,NN,NV1,NV2
!
! OMEGA
!
! IO EST LE DEGRE PAR RAPPORT AUX COMPOSANTES J_{i} (IO=OMEGA)
!
      IO = ICOR/100
!
! KR EST LE RANG DE L'OPERATEUR ROTATIONNEL DANS O(3) (KR=L)
!
      KR = ICOR/10-(ICOR/100)*10
!
! GAMMA R
!
      IG    = ICOR-(ICOR/10)*10
      NBELM = 0
!
! POLYADE INFERIEURE
!
E3:   DO IBRA=1,NFBI
        NV1   = NVCODI(IBRA)
        I1    = NV1/10
        IAV1  = NV1-10*I1
        IKR1  = NRCODI(IBRA)
        IKRV1 = NRVCDI(IBRA)
!
! POLYADE SUPERIEURE
!
E4:     DO IKET=1,NFBS
          NV2 = NVCODS(IKET)
          I2  = NV2/10
          IF( EMRD(I2,I1) .EQ. 0.D0 ) CYCLE E4
          IAV2  = NV2-10*I2
          IKR2  = NRCODS(IKET)
          IKRV2 = NRVCDS(IKET)
          CALL KIV3C(DBLE(IGRVC-2),DBLE(IKRV2-2),DBLE(IKRV1-2),2,ICS,ICI,XK,IFK)
          IR    = KCC+IGRV+IKRV1+IKRV2
          PH    = (-1)**IR
          P1    = IDIMK(DBLE(IGRVC-2))
          P2    = IDIMK(DBLE(IKRV1-2))
          PH12  = SQRT(P1/P2)
          PHASE = PH*PH12*XK
!
! PREMIERE SOMMATION
!
          TERM1 = 0.D0
          PHS   = 1.D0
          DO IKKS=0,JS
            IKS = IKKS
            IF( IKKS        .EQ. 0 .AND.             &
                JS-(JS/2)*2 .NE. 0       ) IKS = -1
            CALL MULCIV(DBLE(IGRV-2),DBLE(IKRV2-2),NJ,AKRVI)
            DO IJ=1,NJ
              AKRV = AKRVI(IJ)
              CALL SIX6K(DBLE(KCC-2)  ,DBLE(IGRV-2) ,DBLE(IGRVC-2),             &
                         DBLE(IKRV2-2),DBLE(IKRV1-2),AKRV         ,SIXK,ISIXK)
              CALL NF9K(DBLE(IKS)   ,DBLE(IAV1-2) ,AKRV,DBLE(IG-2),      &
                        DBLE(IGV-2) ,DBLE(IGRV-2) ,DBLE(IKR2-2)   ,      &
                        DBLE(IAV2-2),DBLE(IKRV2-2),XRK            ,INK)
              CALL NF9K(DBLE(IKR1-2),DBLE(IAV1-2),DBLE(IKRV1-2),             &
                        DBLE(KCC-2) ,0.D0        ,DBLE(KCC-2)  ,DBLE(IKS),   &
                        DBLE(IAV1-2),AKRV        ,XCK          ,IMK       )
              CALL KSU2CV(1.D0        ,DBLE(JS),DBLE(JI),DBLE(KCC-2),DBLE(IKS),   &
                          DBLE(IKR1-2),XR      ,IR                             )
              CALL KSU2CV(DBLE(KR)    ,DBLE(JS) ,DBLE(JS),DBLE(IG-2),   &
                          DBLE(IKR2-2),DBLE(IKS),XC      ,IP         )
              PH1    = SQRT(DBLE((2*JI+1)*(2*JS+1)))
              A1     = IDIMK(DBLE(IKRV1-2))
              A2     = IDIMK(AKRV)
              A3     = IDIMK(DBLE(KCC-2))
              A4     = IDIMK(DBLE(IGRV-2))
              A5     = IDIMK(DBLE(IKRV2-2))
              A6     = IDIMK(DBLE(IAV1-2))
              A7     = IDIMK(DBLE(IKR1-2))
              A8     = IDIMK(DBLE(IKS))
              PHASE1 = A2*SQRT(A1*A3*A4*A5*A6*A7*A8)
              ELM1   = EMRRO(IO,KR,JS)
              IF( IR+IP .EQ. 2 ) PHS = -PHS
              TERM1 = TERM1+PHS*SIXK*XRK*XCK*XR*XC*PHASE1*ELM1/PH1
            ENDDO
          ENDDO
!
! DEUXIEME SOMMATION
!
          PHI   = 1.D0
          TERM2 = 0.D0
          DO IKKI=0,JI
            IKI = IKKI
            IF( IKKI        .EQ. 0 .AND.             &
                JI-(JI/2)*2 .NE. 0       ) IKI = -1
            CALL MULCIV(DBLE(KCC-2),DBLE(IKRV2-2),NN,BKRVI)
             DO IL=1,NN
              BKRV = BKRVI(IL)
              CALL SIX6K(DBLE(IGRV-2) ,DBLE(KCC-2)  ,DBLE(IGRVC-2),           &
                         DBLE(IKRV2-2),DBLE(IKRV1-2),BKRV         ,SIX,ISIX)
              B1     = IDIMK(DBLE(IKRV1-2))
              B2     = IDIMK(DBLE(IGRV-2))
              B3     = IDIMK(BKRV)
              B4     = IDIMK(DBLE(IKRV2-2))
              B5     = IDIMK(DBLE(KCC-2))
              B6     = IDIMK(DBLE(IAV2-2))
              B7     = IDIMK(DBLE(IKR1-2))
              B8     = IDIMK(DBLE(IKI))
              PHASE2 = B3*SQRT(B1*B2*B4*B5*B6*B7*B8)
              PH2    = 2*JI+1
              CALL NF9K(DBLE(IKR1-2),DBLE(IAV1-2),DBLE(IKRV1-2),DBLE(IG-2)  ,   &
                        DBLE(IGV-2) ,DBLE(IGRV-2),DBLE(IKI)    ,DBLE(IAV2-2),   &
                        BKRV        ,XSK         ,IN                         )
              CALL NF9K(DBLE(IKI)    ,DBLE(IAV2-2),BKRV        ,DBLE(KCC-2) ,   &
                        0.D0         ,DBLE(KCC-2) ,DBLE(IKR2-2),DBLE(IAV2-2),   &
                        DBLE(IKRV2-2),XSC         ,IM                        )
              CALL KSU2CV(1.D0     ,DBLE(JS),DBLE(JI),DBLE(KCC-2),DBLE(IKR2-2),   &
                          DBLE(IKI),XS      ,IR2                               )
              CALL KSU2CV(DBLE(KR)    ,DBLE(JI),DBLE(JI),DBLE(IG-2),DBLE(IKI),   &
                          DBLE(IKR1-2),XC      ,IP2                           )
              ELM2 = EMRRO(IO,KR,JI)
              IZ   = IGRV+KCC+IGRVC
              PHA  = (-1)**IZ
              IF( IR2+IP2 .EQ. 2 ) PHI = -PHI
              TERM2 = TERM2+PHI*PHA*SIX*PHASE2*XSK*XSC*XS*XC*ELM2/PH2
            ENDDO
          ENDDO
!
! SOMME DES 2 TERMES
!
          COEF = PHASE*EMRD(I2,I1)
          ELM  = (TERM1+TERM2)*COEF/6.D0
          ELM  = ELM*SQRT(DBLE((2*JI+1)*(2*JS+1)))
          NBELM = NBELM+1
          IF( NBELM .GT. MXELMT ) CALL RESIZE_MXELMT
          LI(NBELM) = IBRA
          KO(NBELM) = IKET
          H(NBELM)  = ELM
        ENDDO E4
      ENDDO E3
!
      RETURN
      END SUBROUTINE CALDI
