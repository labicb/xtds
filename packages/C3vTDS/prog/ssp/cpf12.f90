!
!  FONCTION DE PROBABILITE COMPLEXE DE HUMLICEK
!
      SUBROUTINE CPF12(X,Y,WR,WI)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: X,Y,WR,WI

      real(kind=dppr) ,dimension(6)  :: C = (/  1.01172805D0  ,     &
                                               -0.75197147D0  ,     &
                                                1.2557727D-2  ,     &
                                                1.00220082D-2 ,     &
                                               -2.42068135D-4 ,     &
                                                5.00848061D-7   /)
      real(kind=dppr) ,dimension(6)  :: S = (/  1.393237D0    ,     &
                                                0.231152406D0 ,     &
                                               -0.155351466D0 ,     &
                                                6.21836624D-3 ,     &
                                                9.19082986D-5 ,     &
                                               -6.27525958D-7   /)
      real(kind=dppr) ,dimension(6)  :: T = (/  0.314240376D0 ,     &
                                                0.947788391D0 ,     &
                                                1.59768264D0  ,     &
                                                2.27950708D0  ,     &
                                                3.02063703D0  ,     &
                                                3.8897249D0     /)

      real(kind=dppr)  :: D,D1,D2,D3,D4
      real(kind=dppr)  :: R,R2
      real(kind=dppr)  :: Y1,Y2,Y3

      integer          :: I
!
      WR = 0.D0
      WI = 0.D0
      Y1 = Y+1.5D0
      Y2 = Y1*Y1
      IF( Y      .GT. 0.85D0          .OR.           &
          ABS(X) .LT. 18.1D0*Y+1.65D0      ) GOTO 2
!
!  REGION II
!
      IF( ABS(X) .LT. 12.D0 ) WR = EXP(-X*X)
      Y3 = Y+3.D0
      DO I=1,6
        R  = X-T(I)
        R2 = R*R
        D  = 1.D0/(R2+Y2)
        D1 = Y1*D
        D2 = R*D
        WR = WR+Y*(C(I)*(R*D2-1.5D0*D1)+S(I)*Y3*D2)/(R2+2.25D0)
        R  = X+T(I)
        R2 = R*R
        D  = 1.D0/(R2+Y2)
        D3 = Y1*D
        D4 = R*D
        WR = WR+Y*(C(I)*(R*D4-1.5D0*D3)-S(I)*Y3*D4)/(R2+2.25D0)
        WI = WI+C(I)*(D2+D4)+S(I)*(D1-D3)
      ENDDO
      RETURN
!
!  REGION I
!
2     DO I=1,6
        R  = X-T(I)
        D  = 1.D0/(R*R+Y2)
        D1 = Y1*D
        D2 = R*D
        R  = X+T(I)
        D  = 1.D0/(R*R+Y2)
        D3 = Y1*D
        D4 = R*D
        WR = WR+C(I)*(D1+D3)-S(I)*(D2-D4)
        WI = WI+C(I)*(D2+D4)+S(I)*(D1-D3)
      ENDDO
!
      RETURN
      END SUBROUTINE CPF12
