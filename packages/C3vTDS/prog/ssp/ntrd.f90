!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
!        *** RELATION TRIANGULAIRE DANS SU(2).
!
      FUNCTION NTRD(AJ1,AJ2,AJ3)
      use mod_dppr
      IMPLICIT NONE
      integer          :: NTRD
      real(kind=dppr)  :: AJ1,AJ2,AJ3

!
      IF( AJ3 .GE. ABS(AJ1-AJ2) .OR.         &
          AJ3 .LE. AJ1+AJ2           ) THEN
        NTRD = 1
      ELSE
        NTRD = 0
      ENDIF
!
      RETURN
      END FUNCTION NTRD
