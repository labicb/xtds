!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
      FUNCTION IDIMK(AK)
      use mod_dppr
      IMPLICIT NONE
      integer          :: IDIMK
      real(kind=dppr)  :: AK

!
      IDIMK = 2
      IF( AK .LE. 0.D0 ) IDIMK = 1
!
      RETURN
      END FUNCTION IDIMK
