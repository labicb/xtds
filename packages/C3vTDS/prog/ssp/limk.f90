!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
      SUBROUTINE LIMK(AK,ISI,ISS,ISP)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: AK
      integer          :: ISI,ISS,ISP

!
      ISI = 1
      ISS = 2
      ISP = 1
      IF( AK .LE. 0.D0 ) THEN
        ISI = 1
        ISS = 1
        ISP = 1
      ENDIF
!
      RETURN
      END SUBROUTINE LIMK
