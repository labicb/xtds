!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
      SUBROUTINE VLNC(ICO,IV,IL)
      IMPLICIT NONE
      integer          :: ICO,IV,IL

!
      IV = ICO/10
      IL = ICO-10*IV
!
      RETURN
      END SUBROUTINE VLNC
