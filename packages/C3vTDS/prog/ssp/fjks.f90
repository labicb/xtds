!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
!      CE PROGRAMME DONNE LES COEFFICIENTS DE CLEBSCH-GORDAN ORIENTE  CIV
!      J'ASSOCIE FORMELLEMENT LA VALEUR (-1) A L IRREP 0-, PUIS 0 A 0+.
!
      FUNCTION FJKS(AJ1,AK1,IS1,AJ2,AK2,IS2,AJ3,AK3,IS3)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: FJKS
      real(kind=dppr)  :: AJ1,AK1,AJ2,AK2,AJ3,AK3
      integer          :: IS1,IS2,IS3

! functions
      real(kind=dppr)  :: TROIJD,WJKMS
      integer          :: KTR,NTRD

      real(kind=dppr)  :: AM1,AM2,AM3
      real(kind=dppr)  :: C
      real(kind=dppr)  :: F
      real(kind=dppr)  :: H1,H2,H3
      real(kind=dppr)  :: PH

      integer          :: K1D,K1P,K2D,K2P,K3D,K3P
      integer          :: M1D,M2D,M3D
!
      FJKS = 0.D0
      IF( AK1 .LE. 0.D0 ) THEN
        K1D = 0
        K1P = 1
      ELSE
        K1D = NINT(2.D0*AK1)
        K1P = 2*K1D
      ENDIF
      IF( AK2 .LE. 0.D0 ) THEN
        K2D = 0
        K2P = 1
      ELSE
        K2D = NINT(2.D0*AK2)
        K2P = 2*K2D
      ENDIF
      IF( AK3 .LE. 0.D0 ) THEN
        K3D = 0
        K3P = 1
      ELSE
        K3D = NINT(2.D0*AK3)
        K3P = 2*K3D
      ENDIF
      IF( AK1 .LE. AJ1 .AND.         &
          AK2 .LE. AJ2 .AND.         &
          AK3 .LE. AJ3       ) THEN
        IF( NTRD(AJ1,AJ2,AJ3) .NE. 0 .AND.         &
            KTR(AK1,AK2,AK3)  .NE. 0       ) THEN
          F = 0.D0
          DO M1D=-K1D,K1D,K1P
            AM1 = DBLE(M1D)/2.D0
            H1  = WJKMS(AJ1,AK1,AM1,IS1)
            DO M2D=-K2D,K2D,K2P
              AM2 = DBLE(M2D)/2.D0
              H2  = WJKMS(AJ2,AK2,AM2,IS2)
              DO M3D=-K3D,K3D,K3P
                AM3 = DBLE(M3D)/2.D0
                H3  = WJKMS(AJ3,AK3,AM3,IS3)
                PH  = SQRT(2.D0*AJ3+1.D0)*( (-1)**NINT(2*AJ2+AJ3-AM3) )
                C   = PH*TROIJD(AJ1,AJ2,AJ3,AM1,AM2,-AM3)
                F   = F+H1*H2*H3*C
              ENDDO
            ENDDO
          ENDDO
          FJKS = F
        ENDIF
      ENDIF
!
      RETURN
      END FUNCTION FJKS
