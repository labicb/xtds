!
! *** DIMENSIONS, PARITES ET DESIGNATIONS DES REPRESENTATIONS DE TD
!
! returns KC
!
      FUNCTION KC(I)
      use mod_par_tds
      use mod_com_sy
      IMPLICIT NONE
      character(len=   3)  :: KC
      integer          :: I

!
1000  FORMAT(I3.3)
8000  FORMAT(' !!! WRONG CALLING INTEGER ARGUMENT IN KC: ',I6,/,   &
             ' !!! MUST BE IN [0,',I3,'] RANGE.'                )
8001  FORMAT(' !!! ERROR WHILE FORMATING KC: ',I6)
!
      IF( I .LT. 1     .OR.         &
          I .GT. MXSYR      ) THEN
        PRINT 8000, I,MXSYR
        STOP
      ENDIF
      IF( I .LE. LKCS ) THEN
        KC = LKC(I)
      ELSE
        WRITE(KC,1000,ERR=1) I
      ENDIF
      RETURN
!
1     PRINT 8001, I
      STOP
      END FUNCTION KC
