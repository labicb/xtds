      PROGRAM HMODEL
!
!  6.7.88 FORTRAN 77 DU SUN4
!  REV 25 JAN 1990
!  REV    DEC 1992 T.GABARD
!  REV 29 SEP 1994
!  REV    JAN 1995 JPC,CW (PARAMETER)
!
! MODIF 01/99 V. BOUDON ---> SCHEMA DE POLYADE QUELCONQUE.
! MODIFIE 05/09 V. BOUDON, A. EL HILALI ---> XY3Z/C3v.
!
!      CODAGE DES OPERATEURS ROVIBRATIONNELS DE L'HAMILTONIEN EFFECTIF
!     POUR UNE RESTRICTION DONNEE D'UNE POLYADE VIBRATIONNELLE TYPE CH4
!
!     CALCUL DES 6-C
!
!     CALCUL DES K JMKCUB = FIXE DANS LE PROGRAMME
!
! APPEL : hmodel Pn Nm  Dk  v1  v2  v3  v4  v5  v6   ...   w1  w2  w3  w4  w5  w6            |
!                   Nm' Dk' v1' v2' v3' v4' v5' v6'  ...   w1' w2' w3' w4' w5' w6'           | (n+1 lines)
!                   ...........................                                              |
!
!  *****  LIMITATIONS DU PROGRAMME GEREES PAR PARAMETER.
!
!     MXOPH  = NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS
!               (ICODVI,ICODRO,ICODVC,ICODVA,ICODSY)
!     MXOCV  = NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS PAR CASE
!              VIBRATIONNELLE
!               (IRDRE,IDVI,IDRO,IDVC,IDVA,IDSY)
!     MXEMR  = NB MAXIMUM D'E.M.R.V. NON NULS
!               (EMOP,LI,IC)
!     MXSNV  = NB MAXIMUM TOTAL DE SOUS-NIVEAUX VIBRATIONNELS
!               (IFF)
!     MXSNB  = NB MAXIMUM DE SOUS-NIVEAUX VIBRATIONNELS POUR UN
!              ENSEMBLE DONNE DES VI
!               (IKVC,IKVA)
!     MXOPR  = NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS PAR OPERATEUR
!              VIBRATIONNEL
!               (IROCO)
!     MXNIV  = NB MAXIMUM DE NIVEAUX VIBRATIONNELS DANS UNE POLYADE DONNEE
!               (NVIB)
!     MXPOL  = NB MAXIMUM DE POLYADES
!               (NIVI,NVIB)
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_com_phase
      use mod_main_hmodel
      IMPLICIT NONE

! functions
      real(kind=dppr)  :: ELMR
      integer          :: KICTR
      character(len =   3)  :: KC

      real(kind=dppr) ,dimension(MDMIGA) :: AGAMA,AKK
      real(kind=dppr)  :: EM,EM1,EM2,EN
      real(kind=dppr)  :: SP

      integer ,dimension(NBVQN+1)  :: IBELMR,ICELMR,IAELMR,IKELMR
      integer ,dimension(NBVQN)    :: ILA,IL,ILC
      integer ,dimension(NBVQN)    :: IV,IVA,IVC
      integer ,dimension(MXSYR)    :: IROT
      integer ,dimension(MXPOL)    :: IORH,NIVI
      integer          :: NF_SUP,NFF_SUP,IOP_SUP,NROT_SUP,NBOPH_SUP
      integer          :: IROTA_SUP,NIV_SUP
      integer          :: I,IB,ICRT
      integer          :: ID,IDEGV,IDK,IDL,IDLL
      integer          :: IDROT,IF,IF1,IF2,IGA,IIL,IJK,IK,IK45,IK456,IM,IMIA
      integer          :: IOA,IOA1,IOC,IOC1,IOP,IOPA,IOPC,IPOL,IRO
      integer          :: IROTA,IU,IVI
      integer          :: IKA45,IKA456,IKC45,IKC456
      integer          :: J,J1,J2,J3,JJJ,JMKCUB
      integer          :: KNVIB,KNIVP,KPOL,KOP,KT
      integer          :: L,LOP
      integer          :: NBOPH,NBVA,NBVC,NF,NFF,NGAMA,NN
      integer          :: NPOL,NROT

      character(len =   1)  :: AVI,AORH
      character(len =  11)  :: CHAINE
      character(len = 120)  :: IFICH
!
1000  FORMAT('   EFFECTIVE ROVIBRATIONAL HAMILTONIAN :'  , /,   &
             ' VIBRATIONAL REDUCED MATRIX ELEMENTS (RME)',//,   &
             A                                               )
1001  FORMAT(//)
1002  FORMAT(I6,'  |[[',                   &
             I2,'(',I1,',',A,')*',         &
             I2,'(',I1,',',A,')*',         &
             I2,'(',I1,',',A,')*',         &
             I2,'(',I1,',',A,')*',         &
             I2,'(',I1,',',A,')]',A,'*',   &
             I2,'(',I1,',',A,')]',A,       &
             ' >'                       )
1004  FORMAT(I4,' TH VIBRATIONAL OPERATOR',//,   &
             A,1X,' {[[',                        &
             I2,'(',I1,',',A,')*',               &
             I2,'(',I1,',',A,')*',               &
             I2,'(',I1,',',A,')*',               &
             I2,'(',I1,',',A,')*',               &
             I2,'(',I1,',',A,')]',A,'*'          &
             I2,'(',I1,',',A,')]',A,             &
             '} '                         , /,   &
             2X,' {[[',                          &
             I2,'(',I1,',',A,')*',               &
             I2,'(',I1,',',A,')*',               &
             I2,'(',I1,',',A,')*',               &
             I2,'(',I1,',',A,')*',               &
             I2,'(',I1,',',A,')]',A,'*'          &
             I2,'(',I1,',',A,')]',A,             &
             '} ',A                           )
1005  FORMAT(/,              &
             'HMODEL : ',A)
1025  FORMAT(I4,' ROVIBRATIONAL OPERATORS',/)
1026  FORMAT(/,            &
             1X, 46('-'))
1027  FORMAT(/,                                          &
             ' HAMILTONIAN DEVELOPMENT ORDER :  ',10I2)
1028  FORMAT(A)
1029  FORMAT(/,                     &
             I5,' NON ZERO RME',/)
1030  FORMAT(2X,A1,'<',I5,'|',3X,'|',I5,'>',3X,F19.13)
1031  FORMAT(/,                               &
             I4,' ROTATIONAL OPERATOR(S)',/)
1032  FORMAT(I1)
1033  FORMAT(/,                         &
             ' POLYAD NUMBER : ',I2,/)
1034  FORMAT(5X,'(v1;v2;v3;v4;v5;v6) = (',5(I2,';'),I2,')')                                        ! ATTENTION respecter cette regle dans tous les packages pour VAMDC
1035  FORMAT(I2)
1100  FORMAT(/,                                  &
             I4,' VIBRATIONAL SUBLEVEL(S) :',/)
1313  FORMAT(I4,I3,'(',I1,',',A,')',1X,6I1,1X,A,1X,6I1,1X,A,1X,A,1X,A,I8,I7)                       ! 6 = NBVQN
2001  FORMAT(' >> ',A)
8000  FORMAT(' !!! HMODEL : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      NF_SUP    = -1
      NFF_SUP   = -1
      IOP_SUP   = -1
      NROT_SUP  = -1
      NBOPH_SUP = -1
      IROTA_SUP = -1
      NIV_SUP   = -1
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1028,END=9997) FDATE
      PRINT 1005,            FDATE
!
! NUMERO DE LA POLYADE COURANTE
!
      READ(10,1028,END=9997) CHAINE
      READ (CHAINE(2:2),1032) IVI
      WRITE(AVI,1032)         IVI
!
! NOMBRE TOTAL DE POLYADES
!
      NPOL = IVI+1
!
! NOM DU FICHIER DE SORTIE
!
      IFICH = 'MH_P'//AVI//'_D'
!
! LECTURE DES NIVEAUX VIBRATIONNELS
!
      JMKCUB = 0
      DO KPOL=1,NPOL
        READ(10,1028,END=9997) CHAINE
        READ(CHAINE(2:3),1035) NIVI(KPOL)
        IF( NIVI(KPOL) .GT. NIV_SUP ) NIV_SUP = NIVI(KPOL)
        DO WHILE( NIVI(KPOL) .GT. MXNIV )
          CALL RESIZE_MXNIV
        ENDDO
        READ(10,1028,END=9997) CHAINE
        READ (CHAINE(2:2),1032) IORH(KPOL)
        WRITE(AORH,1032)        IORH(KPOL)
        IFICH = TRIM(IFICH)//AORH
        DO KNIVP=1,NIVI(KPOL)
          DO KNVIB=1,NBVQN
            READ(10,1032,END=9997) NVIB(KPOL,KNIVP,KNVIB)
            IF( KNVIB .GT. 2 ) JMKCUB = MAX(JMKCUB,NVIB(KPOL,KNIVP,KNVIB))
          ENDDO
        ENDDO
      ENDDO
      CLOSE(10)
      PRINT 2001, TRIM(IFICH)
!
! INITIALISATIONS
!
      CALL FACTO
      OPEN(20,FILE=IFICH,STATUS='UNKNOWN')
      WRITE(20,1000) FDATE
      WRITE(20,1033) IVI
      DO I=1,NIVI(NPOL)
        WRITE(20,1034) (NVIB(NPOL,I,J),J=1,NBVQN)
      ENDDO
!
! CODAGE DES SOUS-NIVEAUX VIBRATIONNELS
!
      NFF = 0
      DO IF1=1,NIVI(NPOL)
        DO IF2=1,NBVQN
          IV(IF2) = NVIB(NPOL,IF1,IF2)
        ENDDO
        CALL SIGVI(IV,'IFF',NFF,NF)
        NFF = NFF+NF
        IF( NF  .GT. NF_SUP  ) NF_SUP  = NF
        IF( NFF .GT. NFF_SUP ) NFF_SUP = NFF
      ENDDO
      WRITE(20,1100) NFF
!
! NFF est le nombre de sous-niveaux vibrationnels
!
      DO IF=1,NFF
        DO I=1,NBVQN
          CALL VLNC(IFF(I,IF),IV(I),IL(I))
        ENDDO
        CALL VLNC(IFF(NBVQN+1,IF),IK45,IK456)
        WRITE(20,1002) IF,                                        &
                       (IV(I),IL(I),KC(IL(I)+2),I=1,5),KC(IK45),  &
                        IV(6),IL(6),KC(IL(6)+2)       ,KC(IK456)
      ENDDO
      WRITE(20,1027) (IORH(IPOL),IPOL=1,NPOL)
      LOP   = 0
      NBOPH = 0
!
! BOUCLE SUR LES POLYADES
!
E36:  DO KPOL=1,NPOL
!
! DETERMINATION DES CREATEURS
!
E35:    DO IOC=1,NIVI(KPOL)
          DO IOC1=1,NBVQN
            IVC(IOC1) = NVIB(KPOL,IOC,IOC1)
          ENDDO
          CALL SIGVI(IVC,'IKVC',0,NBVC)
!
! DETERMINATION DES ANNIHILATEURS
!
E33:      DO IOA=1,NIVI(KPOL)
            DO IOA1=1,NBVQN
              IVA(IOA1) = NVIB(KPOL,IOA,IOA1)
            ENDDO
            IF( IOA .LT. IOC ) CYCLE E33
            CALL SIGVI(IVA,'IKVA',0,NBVA)
            IDEGV = 0
            DO IOP=1,NBVQN
              IDEGV = IVA(IOP)+IVC(IOP)+IDEGV
            ENDDO
            IDROT = IORH(KPOL)+2-IDEGV
            IF( IDROT .LT. 0 ) CYCLE E33
!
! COUPLAGE CREATEURS-ANNIHILATEURS
!
E1:         DO IOPC=1,NBVC
              IMIA = 1
              IF( IOA .EQ. IOC ) IMIA = IOPC
              DO I=1,NBVQN
                CALL VLNC(IKVC(I,IOPC),IVC(I),ILC(I))
              ENDDO
              CALL VLNC(IKVC(NBVQN+1,IOPC),IKC45,IKC456)
E2:           DO IOPA=IMIA,NBVA
                DO I=1,NBVQN
                  CALL VLNC(IKVA(I,IOPA),IVA(I),ILA(I))
                ENDDO
                CALL VLNC(IKVA(NBVQN+1,IOPA),IKA45,IKA456)
                CALL MULCIV(DBLE(IKC456-2),DBLE(IKA456-2),NGAMA,AGAMA)
                NROT = 0
E111:           DO I=1,NGAMA
E733:             DO IU=1,2
                    DO IJK=1,MXSYR
                      IROT(IJK) = 0
                    ENDDO
!
! DETERMINATION DES OPERATEURS ROTATIONNELS POSSIBLES
!  R(OMEGA,K,L)=R(ID,IDK,IDL)
                    IROTA = 0
                    IF( IDROT .LT. IU-1 ) CYCLE E733
E334:               DO ID=IU-1,IDROT,2
                      IF( IDEGV .EQ. 0 .AND.               &
                          ID    .EQ. 0       ) CYCLE E334
                      DO IDK=ID,IU-1,-2
                        DO IDL=0,IDK
                          IDLL = IDL
                          IF( IU  .EQ. 2 .AND.              &
                              IDL .EQ. 0       ) IDLL = -1
                          IROTA        = IROTA+1
                          IROT(IDLL+2) = IROT(IDLL+2)+1
                          IF( IROTA .GT. IROTA_SUP ) IROTA_SUP = IROTA
                          IF( IROTA .GT. MXOPR     ) CALL RESIZE_MXOPR
                          IROCO(IROTA) = 100*ID+10*IDK+IDLL+2
                        ENDDO
                      ENDDO
                    ENDDO E334
!
! CALCUL DU FACTEUR DE NORMALISATION
!      IU = 1, EPSILON = +
!      IU = 2, EPSILON = -
!
                    IF( IROT(INT(AGAMA(I)+2)) .EQ. 0 ) CYCLE E733
                    WRITE(*,*)'-----------------------'
                    SP        = (-1)**(IU+1+IKA456+IKC456+INT(AGAMA(I)))
                    WRITE(*,*)'HMODEL: SP = ',SP
                    IBELMR(:) = IKVC(:,IOPC)
                    ICELMR(:) = IKVC(:,IOPC)
                    IAELMR(:) = IKVA(:,IOPA)
                    IKELMR(:) = IKVA(:,IOPA)
                    EN        = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,INT(AGAMA(I)))
                    WRITE(*,*)'HMODEL: EN1 = ',EN
                    IBELMR(:) = IKVC(:,IOPC)
                    ICELMR(:) = IKVA(:,IOPA)
                    IKELMR(:) = IKVC(:,IOPC)
                    IKELMR(:) = IKVA(:,IOPA)
                    EN        = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,INT(AGAMA(I)))*SP+EN
                    WRITE(*,*)'HMODEL: EN2 = ',EN
                    IF( ABS(EN) .LT. 1.D-5 ) CYCLE E733
                    LOP = LOP+1
                    WRITE(20,1026)
                    WRITE(20,1004) LOP,AUG(IU),                                    &
                                   (IVC(J),ILC(J),KC(ILC(J)+2),J=1,5),KC(IKC45),   &
                                    IVC(6),ILC(6),KC(ILC(6)+2)       ,KC(IKC456),  &
                                   (IVA(J),ILA(J),KC(ILA(J)+2),J=1,5),KC(IKA45),   &
                                    IVA(6),ILA(6),KC(ILA(6)+2)       ,KC(IKA456),  &
                                   KC(INT(AGAMA(I))+2)
                    WRITE(*,1004)  LOP,AUG(IU),                                    &
                                   (IVC(J),ILC(J),KC(ILC(J)+2),J=1,5),KC(IKC45),   &
                                    IVC(6),ILC(6),KC(ILC(6)+2)       ,KC(IKC456),  &
                                   (IVA(J),ILA(J),KC(ILA(J)+2),J=1,5),KC(IKA45),   &
                                    IVA(6),ILA(6),KC(ILA(6)+2)       ,KC(IKA456),  &
                                   KC(INT(AGAMA(I))+2)
!
! CALCUL ET CODAGE DES E.M.R.V.
!
                    IOP = 0
                    DO IB=1,NFF
E5:                   DO IK=IB,NFF
                        IBELMR(:) = IFF(:,IB)
                        ICELMR(:) = IKVC(:,IOPC)
                        IAELMR(:) = IKVA(:,IOPA)
                        IKELMR(:) = IFF(:,IK)
                        EM1       = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,INT(AGAMA(I)))
                        IBELMR(:) = IFF(:,IB)
                        ICELMR(:) = IKVA(:,IOPA)
                        IAELMR(:) = IKVC(:,IOPC)
                        IKELMR(:) = IFF(:,IK)
                        EM2       = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,INT(AGAMA(I)))*SP
                        EM        = EM1+EM2
                        WRITE(*,*)'HMODEL: EM,EN = ',EM,EN
                        IF( ABS(EM) .LT. 1.D-10 ) CYCLE E5
                        EM = EM/EN
                        IF( IU .EQ. 2 ) EM = -EM
! changement de convention pour les operateurs V dont les elements
! matriciels sont imaginaires (pour retablir, dans la majorite des cas,
! les anciens signes des parametres)     4 / 5 / 93
                        IOP = IOP+1
                        IF( IOP .GT. IOP_SUP ) IOP_SUP = IOP
                        IF( IOP .GT. MXEMR   ) CALL RESIZE_MXEMR
                        LI(IOP)   = IK
                        IC(IOP)   = IB
                        EMOP(IOP) = EM
                        WRITE(*,1030)AIM(IU),IC(IOP),LI(IOP),EMOP(IOP)
                      ENDDO E5
                    ENDDO
                    WRITE(20,1029) IOP
                    DO KOP=1,IOP
                      WRITE(20,1030) AIM(IU),IC(KOP),LI(KOP),EMOP(KOP)
                    ENDDO
                    WRITE(20,1031) IROTA
!
! CODAGE DES OPERATEURS ROVIBRATIONNELS
!
                    DO IRO=1,IROTA
                      ICRT = IROCO(IRO)
                      IDLL = ICRT-(ICRT/10)*10-2
                      CALL MULCIV(DBLE(IDLL),AGAMA(I),NN,AKK)
E632:                 DO L=1,NN
                        IF( KICTR(AKK(L),1) .NE. 1 ) CYCLE E632
                        NROT = NROT+1
                        IF( NROT .GT. NROT_SUP ) NROT_SUP = NROT
                        IF( NROT .GT. MXOCV    ) CALL RESIZE_MXOCV
                        IDRO(NROT) = IROCO(IRO)
                        IDVC(NROT) = IVC(1)*100000+IVC(2)*10000+IVC(3)*1000+    &
                                     IVC(4)*100+IVC(5)*10+IVC(6)
                        IDVA(NROT) = IVA(1)*100000+IVA(2)*10000+IVA(3)*1000+    &
                                     IVA(4)*100+IVA(5)*10+IVA(6)
                        IDSY(NROT) = IKC456*100+IKA456*10+INT(AGAMA(I))+2
                        IDVI(NROT) = LOP*1000+IKC456*100+(INT(AGAMA(I))+2)*10+  &
                                     INT(AKK(L))+2
                      ENDDO E632
                    ENDDO
                  ENDDO E733
                ENDDO E111
!
! MISE EN ORDRE A L'INTERIEUR D'UNE CASE VIBRATIONNELLE DONNEE
!
                IF( NROT .EQ. 0 ) CYCLE E2
                CALL IRDER(NROT,IDRO,IRDRE,MXOCV)
                DO JJJ=1,NROT
                  NBOPH = NBOPH+1
                  IF( NBOPH .GT. NBOPH_SUP ) NBOPH_SUP = NBOPH
                  IF( NBOPH .GT. MXOPH     ) CALL RESIZE_MXOPH
                  IRO           = IRDRE(JJJ)
                  ICODRO(NBOPH) = IDRO(IRO)
                  ICODVC(NBOPH) = IDVC(IRO)
                  ICODVA(NBOPH) = IDVA(IRO)
                  ICODSY(NBOPH) = IDSY(IRO)
                  ICODVI(NBOPH) = IDVI(IRO)
                ENDDO
              ENDDO E2
            ENDDO E1
          ENDDO E33
        ENDDO E35
      ENDDO E36
      CALL DEBUG( 'HMODEL => MXOPVH=',LOP)
!
! STOCKAGE DES CODES DES OPERATEURS ROVIBRATIONNELS
!
      WRITE(20,1001)
      WRITE(20,1025) NBOPH
      DO IRO=1,NBOPH
        IM  = ICODRO(IRO)/100
        IK  = (ICODRO(IRO)-100*IM)/10
        IIL = ICODRO(IRO)-100*IM-10*IK
        KT  = ICODVI(IRO)-10*(ICODVI(IRO)/10)
        CALL V1TO6(ICODVC(IRO),IVC(1),IVC(2),IVC(3),IVC(4),IVC(5),IVC(6))
        CALL V1TO6(ICODVA(IRO),IVA(1),IVA(2),IVA(3),IVA(4),IVA(5),IVA(6))
        CALL V1TO6(ICODSY(IRO),J1,J2,J3,IKC456,IKA456,IGA)
        WRITE(20,1313) IRO,IM,IK,KC(IIL),                      &
                       (IVC(L),L=1,NBVQN),KC(IKC456),          &
                       (IVA(L),L=1,NBVQN),KC(IKA456),          &
                       KC(IGA),KC(KT),ICODRO(IRO),ICODVI(IRO)
      ENDDO
      CLOSE(20)
      CALL DEBUG( 'HMODEL => MXSNB=',NF_SUP)
      CALL DEBUG( 'HMODEL => MXSNV=',NFF_SUP)
      CALL DEBUG( 'HMODEL => MXEMR=',IOP_SUP)
      CALL DEBUG( 'HMODEL => MXOCV=',NROT_SUP)
      CALL DEBUG( 'HMODEL => MXOPH=',NBOPH_SUP)
      CALL DEBUG( 'HMODEL => MXOPR=',IROTA_SUP)
      CALL DEBUG( 'HMODEL => MXNIV=',NIV_SUP)
      GOTO 9000
!
9997  PRINT 8002
      CLOSE(10)
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM HMODEL
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
! *** COUPLAGE DES OPERATEURS CREATIONS ET ANNHILATIONS
!
! ***   < IB !! (IC * IA)IGAMA !! IK > = (-1)**(ICB+ICK+IC+IA) * SQRT(IGAMA)
!
!                        ( ICC , ICA ,IGAMA)
! ***   SOMME SUR IBK DE (                 ) <IB!!IC!!IBK><IBK!!IA!!IK>
!                        ( ICB , ICK , ICBK)
!
! ***   AVEC IX(I)       = 1000*VI+100*LI+10*NI+CI     POUR I=1,NBVQN
! ***   ET   IX(NBVQN+1) = 1000*C1+100*C2+10*C23+C234
! ***   AVEC C23=C2*C3   ET   C234=C23*C4
! ***   IX PEUT ETRE  IB,IA,IC OU IK
!
      FUNCTION ELMR(IB,IC,IA,IK,IGAMA)
      use mod_dppr
      use mod_par_tds
      use mod_com_sigvi
      IMPLICIT NONE
      real(kind=dppr)  :: ELMR
      integer ,dimension(NBVQN+1)  :: IB,IC,IA,IK
      integer          :: IGAMA

! functions
      real(kind=dppr)  :: E1TO6
      integer          :: IPSIK

      real(kind=dppr)  :: EM1,EM2
      real(kind=dppr)  :: P
      real(kind=dppr)  :: SIX

      integer ,dimension(NBVQN)          :: IPB
      integer ,dimension(NBVQN,4)        :: IBCAK
      integer          :: I,ICA,ICB,ICC,ICK,ICPK,IDGAM,IFF,II,IL,IV
      integer          :: J
      integer          :: NB
!
      ELMR = 0.D0
!
! *** TEST SUR LES FONCTIONS ET LES OPERATEURS.
!
      DO I=1,NBVQN
        IBCAK(I,1) = IB(I)
        IBCAK(I,2) = IC(I)
        IBCAK(I,3) = IA(I)
        IBCAK(I,4) = IK(I)
      ENDDO
!
! TEST DE LA COHERENCE DE L'ENSEMBLE DES NOMBRES QUANTIQUES
!
      DO J=1,4
        DO I=1,NBVQN
          CALL VLNC(IBCAK(I,J),IV,IL)
          II = 1
          IF( I               .GT. 3 ) II = 2
          IF( IPSIK(II,IV,IL) .EQ. 0 ) RETURN
        ENDDO
      ENDDO
!
! *** TEST SUR LES VI ET LES OMEGAI
!
      DO I=1,NBVQN
        IPK(I,1) = IB(I)/10-IC(I)/10
        IPB(I)   = IK(I)/10-IA(I)/10
        IF( IPB(I)   .LT. 0      ) RETURN
        IF( IPK(I,1) .NE. IPB(I) ) RETURN
      ENDDO
      ICB   = IB(NBVQN+1)-10*(IB(NBVQN+1)/10)
      ICK   = IK(NBVQN+1)-10*(IK(NBVQN+1)/10)
      ICC   = IC(NBVQN+1)-10*(IC(NBVQN+1)/10)
      ICA   = IA(NBVQN+1)-10*(IA(NBVQN+1)/10)
      IDGAM = 2
      IF( IGAMA .LT. 1 ) IDGAM = 1
      P = (-1)**(ICC+ICA+ICB+ICK)*SQRT(DBLE(IDGAM))
!
! *** SOMMATION SUR LES ETATS INTERMEDIAIRES.
!
      CALL SIGVI(IPB,'IPK',0,NB)
      DO I=1,NB
        EM1  = E1TO6(1,IB,IC,IPK(1,I))
        WRITE(*,*)'ELMR: EM1 = ',EM1
        EM2  = E1TO6(-1,IPK(1,I),IA,IK)
        WRITE(*,*)'ELMR: EM2 = ',EM2
        ICPK = IPK(NBVQN+1,I)-10*(IPK(NBVQN+1,I)/10)
        CALL SIX6K(DBLE(ICC-2),DBLE(ICA-2),DBLE(IGAMA),DBLE(ICK-2),  &
                   DBLE(ICB-2),DBLE(ICPK-2),SIX,IFF)
        ELMR = ELMR+P*SIX*EM1*EM2
      ENDDO
!
      RETURN
      END FUNCTION ELMR
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! HMODEL version
!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
! *** CHERCHE POUR UN ENSEMBLE DONNE DE VI : (IV(I),I=1,4)
! *** LES NB INDICES DE LA SIGNATURE VIBRATIONNELLE
! *** IKNC(I,K), I=1,5 ET K=ISN+1,ISN+NB
!
!  REV 29 SEP 94
!
! ATTENTION : SIGVI peut redimensionner les tableaux dependants de MXC
!
      SUBROUTINE SIGVI(IV,MXC,ISN,NB)
      use mod_dppr
      use mod_par_tds
      use mod_main_hmodel
      IMPLICIT NONE
      integer            ,dimension(NBVQN)  :: IV
      character(len = *)                    :: MXC
      integer                               :: ISN,NB

      integer ,parameter  :: MDMIB = 22

      real(kind=dppr) ,dimension(MDMIGA)  :: AK45,AK456

      integer ,pointer ,dimension(:,:)    :: IKNC
      integer          ,dimension(MDMIB)  :: IB4,IB5,IB6
      integer                             :: I4,I45,I456,I5,I6
      integer                             :: IK4,IK5,IK6
      integer                             :: K
      integer                             :: MDMKNC
      integer                             :: N45,N456
      integer                             :: NB4,NB5,NB6
!
8000  FORMAT(' !!! SIGVI  : STOP ON ERROR'   ,/,   &
             '              ',A,' UNEXPECTED'   )
!
      SELECT CASE( MXC )
        CASE( 'IFF' )
          MDMKNC = MXSNV
          IKNC => IFF
        CASE( 'IKVA' )
          MDMKNC = MXSNB
          IKNC => IKVA
        CASE( 'IKVC' )
          MDMKNC = MXSNB
          IKNC => IKVC
        CASE( 'IPK' )
          MDMKNC = MXSNB
          IKNC => IPK
        CASE DEFAULT
          PRINT 8000, MXC
          STOP
!
      END SELECT
      K = ISN
      CALL TCUBE(4,IV(4),NB4,IB4,MDMIB)
      CALL TCUBE(5,IV(5),NB5,IB5,MDMIB)
      CALL TCUBE(6,IV(6),NB6,IB6,MDMIB)
      DO I4=1,NB4
        IK4 = IB4(I4)-10*(IB4(I4)/10)
        DO I5=1,NB5
          IK5 = IB5(I5)-10*(IB5(I5)/10)
          CALL MULCIV(DBLE(IK4),DBLE(IK5),N45,AK45)
          DO I45=1,N45
            DO I6=1,NB6
              IK6 = IB6(I6)-10*(IB6(I6)/10)
              CALL MULCIV(AK45(I45),DBLE(IK6),N456,AK456)
E2:           DO I456=1,N456
                K = K+1
                IF( K .GT. MDMKNC ) THEN
                  SELECT CASE( MXC )
                    CASE( 'IFF' )
                      CALL RESIZE_MXSNV
                      MDMKNC = MXSNV
                      IKNC => IFF
                    CASE( 'IKVA' )
                      CALL RESIZE_MXSNB
                      MDMKNC = MXSNB
                      IKNC => IKVA
                    CASE( 'IKVC' )
                      CALL RESIZE_MXSNB
                      MDMKNC = MXSNB
                      IKNC => IKVC
                    CASE( 'IPK' )
                      CALL RESIZE_MXSNB
                      MDMKNC = MXSNB
                      IKNC => IPK
                  END SELECT
                ENDIF
                IKNC(1,K) = 10*IV(1)
                IKNC(2,K) = 10*IV(2)
                IKNC(3,K) = 10*IV(3)
                IKNC(4,K) = IB4(I4)
                IKNC(5,K) = IB5(I5)
                IKNC(6,K) = IB6(I6)
                IKNC(7,K) = (INT(AK45(I45))+2)*10+INT(AK456(I456))+2
              ENDDO E2
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      NB = K-ISN
!
      RETURN
      END SUBROUTINE SIGVI
