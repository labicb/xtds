      PROGRAM XTRIAS
!
! BASED UPON tri.f90
!
! SORT ASSIGNMENTS IN QUANTIC NUMBER INCREASING ORDER
! FOR LOWER THEN UPPER POLYAD
! Pinf Psup JCsup JCinf Nsup Ninf.
!
! WARNING :
! MAX NB OF LINES SIMULTANEOUSLY PROCESSED : NBLMAX
! MAX NB OF FIELDS                         : NBCMAX
! MAX LINE LENGTH (TRUNCATED ON INPUT)     : 152+36 cf. FORMAT 1002 of xasg0.f
!
! CALL : xtrias INFILE(S)
!
!
      use mod_dppr
      use mod_com_fdate
      use mod_main_xtri
      IMPLICIT NONE

      real(kind=dppr)  :: EV

      integer          :: DLIGNE
      integer          :: I,I1,I2,I3,IV
      integer          :: J
      integer          :: LUI,LUO,LUX
      integer          :: NBL
      integer          :: PLIGNE

      character(len =   1)  :: ORDO
      character(len =   4)  :: CMD
      character(len = 120)  :: NOM

      logical          :: EOF
!
1000  FORMAT(A)
8000  FORMAT(' !!! XTRIAS : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8004  FORMAT(' !!! FIRST LINE #:',I7,' > ',I7,': NB OF LINES OF THE FILE')
8006  FORMAT(' !!! ERROR WHILE NUMERICAL FIELD CONVERSION')
8007  FORMAT(' !!! ERROR WRITING SCRATCH FILE')
8008  FORMAT(' !!! ERROR READING SCRATCH FILE')
8010  FORMAT(' !!! ERROR WRITING OUTPUT FILE')
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      EOF = .FALSE.                                                                                ! DEFAULT
      LUX = 70
      LUI = 50
      OPEN(LUI,STATUS='SCRATCH')
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')                                                    ! command line arguments
      READ(10,1000,END=9997)
!
      NBL = 0                                                                                      ! nb of input lines
1     READ(10,1000,END=2) NOM
!
! INPUT FILE
      OPEN(LUX,FILE=TRIM(NOM),STATUS='OLD')
!
12    READ (LUX,1000,ERR=9002,END=13) LIGNE                                                        ! COPY INPUT TO LUI
      WRITE(LUI,1000,ERR=9003)        TRIM(LIGNE)
      NBL = NBL+1
      IF( NBL .GT. NBLMAX ) CALL RESIZE_NBLMAX
      GOTO 12
!
13    CLOSE(LUX)
      GOTO 1
!
2     CLOSE(10)
      ENDFILE(LUI)
      REWIND(LUI)
!
! OUTPUT FILE
      LUO = 60
      OPEN(LUO,STATUS='SCRATCH')
!
! COMMANDS
!
      ORDO    = 'i'                                                                                ! increasing order
      PLIGNE  =  1                                                                                 ! first line
      DLIGNE  = NBLMAX                                                                             ! last  line
      PCOL(1) = 78                                                                                 ! lower polyad number field
      DCOL(1) = 78
      TYPE(1) = 's'                                                                                ! string type
      PCOL(2) = 75                                                                                 ! upper polyad number field
      DCOL(2) = 75
      TYPE(2) = 's'                                                                                ! string type
      PCOL(3) = 63                                                                                 ! upper J             field
      DCOL(3) = 69
      TYPE(3) = 's'                                                                                ! string type
      PCOL(4) = 52                                                                                 ! lower J             field
      DCOL(4) = 58
      TYPE(4) = 's'                                                                                ! string type
      PCOL(5) = 70                                                                                 ! upper CP            field
      DCOL(5) = 72
      TYPE(5) = 's'                                                                                ! string type
      PCOL(6) = 59                                                                                 ! lower CP            field
      DCOL(6) = 61
      TYPE(6) = 's'                                                                                ! string type
      NBCHPS  =  6
      CMD     = 'sort'                                                                             ! sort command
      GOTO 500                                                                                     ! process command
!
! COMMANDS INITIALIZATION
!
500   NBLGN = DLIGNE-PLIGNE+1
      DO J=1,NBCHPS
        NBCOL(J) = DCOL(J)-PCOL(J)+1
      ENDDO
!
! COPY INPUT FILE BEGINNING
!
      DO I=1,PLIGNE-1
        READ (LUI,1000,ERR=9001,END=9006) LIGNE
        WRITE(LUO,1000,ERR=9004)          TRIM(LIGNE)
      ENDDO
!
! COMMAND PROCESS
!
      IF( CMD .EQ. 'sort' ) GOTO 400
!
! COPY INPUT FILE END
!
20    IF( EOF ) GOTO 21                                                                            ! END OF INPUT FILE ALREADY REACHED
      READ (LUI,1000,ERR=9001,END=21) LIGNE
      WRITE(LUO,1000,ERR=9004)        TRIM(LIGNE)
      GOTO 20
!
21    ENDFILE(LUO)                                                                                 ! CLOSE OUTPUT SCRATCH
      I   = LUI                                                                                    ! INVERT INPUT AND OUTPUT SCRATCH
      LUI = LUO
      LUO = I
      REWIND(LUI)
      REWIND(LUO)
      GOTO 100
!
! SORT
!
400   DO I=1,NBLGN                                                                                 ! READ LINES IN MEMORY
        READ (LUI,1000,ERR=9001,END=402) CHAINE(I)
      ENDDO
      EOF = .FALSE.                                                                                ! EOF NOT REACHED
      GOTO 405
!
402   NBLGN = I-1                                                                                  ! EFFECTIVE NB OF LINES
      EOF   = .TRUE.                                                                               ! EOF     REACHED
!
405   DO I=1,NBLGN                                                                                 ! FOR EACH LINE
        DO J=1,NBCHPS                                                                              ! FOR EACH FIELD
          IF( TYPE(J) .EQ. 'n' ) THEN                                                              ! NUMERICAL, READ DATA WITH FORMAT
            IF( VAR(J) .EQ. 'i' ) THEN
              READ(CHAINE(I)(PCOL(J):DCOL(J)),TRIM(FOR(J)),ERR=9005) IV
              DVAR(J,I) = IV
            ENDIF
            IF( VAR(J) .EQ. 'e' ) THEN
              READ(CHAINE(I)(PCOL(J):DCOL(J)),TRIM(FOR(J)),ERR=9005) EV
              DVAR(J,I) = EV
            ENDIF
            IF( VAR(J) .EQ. 'd' )                                               &
              READ(CHAINE(I)(PCOL(J):DCOL(J)),TRIM(FOR(J)),ERR=9005) DVAR(J,I)
          ENDIF
        ENDDO
      ENDDO
      CALL ORDER                                                                                   ! LINE ORDERING
      IF( ORDO .EQ. 'i' ) THEN                                                                     ! INCREASING
        I1 = 1
        I2 = NBLGN
        I3 = 1
      ELSE                                                                                         ! DECREASING
        I1 = NBLGN
        I2 =  1
        I3 = -1
      ENDIF
      DO I=I1,I2,I3                                                                                ! WRITE ORDERED LINES ON OUTPUT
        WRITE(LUO,1000,ERR=9004) TRIM(CHAINE(IND(I)))
      ENDDO
      GOTO 20
!
! END
! COPY LUI TO OUTPUT FILE
!
100   REWIND(LUI)
      OPEN(LUX,FILE='ASG_EXP')                                                                     ! SPECIFIC OUTPUT FILE
!
101   READ (LUI,1000,ERR=9002,END=9000) LIGNE
      WRITE(LUX,1000,ERR=9008)          TRIM(LIGNE)
      GOTO 101
!
9001  PRINT 8008
      GOTO  9999
9002  PRINT 8008
      GOTO  9000
9003  PRINT 8007
      GOTO  9000
9004  PRINT 8007
      GOTO  9999
9005  PRINT 8006
      GOTO  9999
9006  PRINT 8004, PLIGNE,I-1                                                                       ! EOF REACHED BEFORE FIRST LINE
      GOTO  9999
9008  PRINT 8010
      GOTO  9000
9997  PRINT 8002
      CLOSE(10)
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  CLOSE(LUI,STATUS='DELETE')
      CLOSE(LUO,STATUS='DELETE')
      CLOSE(LUX)
      END PROGRAM XTRIAS
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! ORDERING (GENERIC FORM USING COMP)
!
      SUBROUTINE ORDER
      use mod_com_xtri
      IMPLICIT NONE

! functions
      integer          :: COMP

      integer          :: I,I1,I2,IR,IT1,IT2
      integer          :: N
!
      N      = 1
      IND(1) = 1
      IF( NBLGN .LE. 1 ) RETURN
!
1     IT1 = N+1
      IF( COMP(IT1,IND(1)) .LE. 0 ) GOTO 2
      I1 = 1
      I2 = IT1
!
3     I = (I1+I2)/2
      IF( COMP(IT1,IND(I)) .GE. 0 ) GOTO 4
      I2 = I
      GOTO 5
!
4     I1 = I
!
5     IF( I2-I1 .EQ. 1 ) THEN
        GOTO 6
      ELSE
        GOTO 3
      ENDIF
!
2     I2 = 1
!
6     IF( I2 .EQ. IT1 ) GOTO 7
      IR = N
!
8     IT2      = IR+1
      IND(IT2) = IND(IR)
      IF( I2 .EQ. IR ) GOTO 7
      IR = IR-1
      GOTO 8
!
7     IND(I2) = IT1
      IF( NBLGN .EQ. IT1 ) RETURN
      N = N+1
      GOTO 1
!
      END SUBROUTINE ORDER
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! COMPARE TWO 'VARIABLES' for ORDER
! SPECIFIC PROCESSING FOR EACH DATA TYPE
!
! VAR(N1) < VAR(N2)      : COMP = -1
! VAR(N1) = VAR(N2)      : COMP =  0
! VAR(N1) > VAR(N2)      : COMP =  1
!
      FUNCTION COMP(N1,N2)
      use mod_com_xtri
      IMPLICIT NONE
      integer          :: COMP
      integer          :: N1,N2

      integer          :: J
!
E1:   DO J=1,NBCHPS
        IF( TYPE(J) .EQ. 's' ) THEN
          IF( LLT(CHAINE(N1)(PCOL(J):DCOL(J)),            &
                  CHAINE(N2)(PCOL(J):DCOL(J)) ) ) GOTO 2
          IF( CHAINE(N1)(PCOL(J):DCOL(J)) .NE.            &
              CHAINE(N2)(PCOL(J):DCOL(J))       ) GOTO 3
        ELSE
          IF    ( DVAR(J,N1) .LT. DVAR(J,N2) ) THEN
            GOTO 2
          ELSEIF( DVAR(J,N1) .EQ. DVAR(J,N2) ) THEN
            CYCLE E1
          ELSE                                                                                     ! .GT.
            GOTO 3
          ENDIF
        ENDIF
      ENDDO E1
      COMP =  0
      RETURN
!
2     COMP = -1
      RETURN
!
3     COMP =  1
!
      RETURN
      END FUNCTION COMP
