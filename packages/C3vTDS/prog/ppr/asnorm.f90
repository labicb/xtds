      PROGRAM ASNORM
!
! REV    SEPT 2006  CW
!
!  lire un fichier binaire (ex. INFILE) de type normal_eq.t
!  et l'ecrire en ASCII dans INFILE'_ASC'
!
! APPEL : asnorm
!
      use mod_dppr
      use mod_par_tds
      use mod_main_eqtds
      IMPLICIT NONE

      real(kind=dppr)  :: SS0,SS1

      integer          :: I,ICNTCL
      integer          :: JMAX
      integer          :: K
      integer          :: N1,N2,NFIT

      character(len =   1)  :: REP
      character(len = 150)  :: INFILE
      character(len = 160)  :: OUTFILE

      logical          :: TRANS
!
1000  FORMAT(A)
1020  FORMAT(/,            &
             'ASNORM : ')
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
7000  FORMAT('ENTER normal_eq.t TYPE FILE NAME [normal_eq.t] :')
7001  FORMAT('ENTER Hamiltonian OR Transition PARAMETER TYPE (H/T) :')
8000  FORMAT(' !!! ASNORM : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF FOR ',A)
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      PRINT 1020
!
!  FICHIER D'ENTREE
!
      PRINT 7000
      READ(*,1000) INFILE
      IF( LEN_TRIM(INFILE) .EQ. 0 ) THEN
        INFILE = 'normal_eq.t'
      ENDIF
!
!  TYPE DE PARAMETRES
!
1     PRINT 7001
      READ(*,1000) REP
      IF    ( REP .EQ. 'H' ) THEN
        TRANS = .FALSE.
      ELSEIF( REP .EQ. 'T' ) THEN
        TRANS = .TRUE.
      ELSE
        GOTO 1
      ENDIF
      PRINT 2000,  TRIM(INFILE)
      OPEN(92,FILE=TRIM(INFILE),FORM='UNFORMATTED',STATUS='OLD')
!
!  FICHIER DE SORTIE
!
      OUTFILE = TRIM(INFILE)//'_ASC'
      PRINT 2001,  TRIM(OUTFILE)
      OPEN(10,FILE=TRIM(OUTFILE))
!
!   LECTURE DES PARAMETRES
!
      CALL IOPBAC(92,10,N1)
      CALL IOPBAC(92,10,N2)
      IF ( TRANS ) THEN
        ICNTCL = N2
      ELSE
        ICNTCL = N1
      ENDIF
      DO WHILE( ICNTCL .GT. MXOPH )
        CALL RESIZE_MXOPH
      ENDDO
!
! ********************************************
! ***      STOCKAGE DE AS ET YS            ***
! ********************************************
!
      WRITE(10,*) '>>> NFIT,JMAX,SS0,SS1'
      READ (92,END=9003) NFIT,JMAX,SS0,SS1
      WRITE(10,*)        NFIT,JMAX,SS0,SS1
      WRITE(10,*) '>>> (YS(I),I=1,ICNTCL)'
      READ (92,END=9003) (YS(I),I=1,ICNTCL)
      WRITE(10,*)        (YS(I),I=1,ICNTCL)
      WRITE(10,*) '>>> (AS(K,I),K=I,ICNTCL)'
      DO I=1,ICNTCL
        READ (92,END=9003) (AS(K,I),K=I,ICNTCL)
        WRITE(10,*)        (AS(K,I),K=I,ICNTCL)
      ENDDO
      GOTO 9000
!
9003  PRINT 8003, TRIM(INFILE)
      PRINT 8000
!
9000  CLOSE(10)
      PRINT *
      END PROGRAM ASNORM
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!  LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOPBAC(LUI,LUO,NBOP)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      integer          :: LUI,LUO,NBOP

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! ASNORM : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPBAC')
!
      DO I=1,4
        READ (LUI,END=2000) TITRE
        WRITE(LUO,1000)     TRIM(TITRE)
      ENDDO
      READ (LUI,END=2000) NBOP,TITRE
      WRITE(LUO,1001)     NBOP,TRIM(TITRE)
      DO I=1,2
        READ (LUI,END=2000) TITRE
        WRITE(LUO,1000)     TRIM(TITRE)
      ENDDO
      DO IP=1,NBOP
        READ (LUI,END=2000) CHAINE,PARA,PREC
        WRITE(LUO,1002)     CHAINE,PARA,PREC
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPBAC
