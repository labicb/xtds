      FUNCTION NSYM2(J,ICC2V)
      use mod_par_tds
      use mod_com_sy
      IMPLICIT NONE
      integer          :: NSYM2
      integer          :: J,ICC2V

      integer          :: IOHC2
      integer          :: JC2V
!
      NSYM2 = 0
      DO JC2V=1,3
        IOHC2 = IC2VV(J,JC2V)
        IF( IOHC2 .EQ. ICC2V ) THEN
          NSYM2 = 1
          RETURN
        ENDIF
      ENDDO
!
      RETURN
      END FUNCTION NSYM2
