!
! *** SYMBOLES F DE TD
!
! D'APRES PROG. D'ORSAY 80
! MOD. T.GABARD DEC 92
!
      FUNCTION TROIC(C1,C2,C3,S1,S2,S3)
      use mod_dppr
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: TROIC
      integer          :: C1,C2,C3,S1,S2,S3

! functions
      integer          :: CTR

      real(kind=dppr)  :: T
      real(kind=dppr)  :: Y

      integer ,dimension(3)    :: IF1 = (/ 12, 12, -3 /)
      integer ,dimension(3)    :: IF2 = (/ -4,  4,  0 /)
      integer ,dimension(3,2)  :: CS
      integer          :: CC1,CC2,CC3,CI
      integer          :: IPER
      integer          :: SC,SI,SS1,SS2,SS3
!
      TROIC = 0.D0
      IF( CTR(C1,C2,C3) .EQ. 0 ) RETURN
      CS(1,1) = C1
      CS(2,1) = C2
      CS(3,1) = C3
      CS(1,2) = S1
      CS(2,2) = S2
      CS(3,2) = S3
      CALL RENGE(CS,3,2,IPER)
      CC1 = CS(1,1)
      CC2 = CS(2,1)
      CC3 = CS(3,1)
      SS1 = CS(1,2)
      SS2 = CS(2,2)
      SS3 = CS(3,2)
      Y   = 1.D0
      SC  = PC(CC1)*PC(CC2)*PC(CC3)
      IF( IPER .EQ. -1 ) Y = SC
      IF    ( CC1 .EQ. 1 ) THEN
        GOTO 1
      ELSEIF( CC1 .EQ. 2 ) THEN
        GOTO 2
      ELSEIF( CC1 .EQ. 3 ) THEN
        GOTO 3
      ELSE                                                                                         ! 4,5
        GOTO 4
      ENDIF
!
1     IF( SS2 .EQ. SS3 ) GOTO 10
      RETURN
!
2     IF(     CC2 .EQ. 4   ) GOTO 6
      IF    ( SS3 .EQ. SS2 ) THEN
        RETURN
      ELSEIF( SS3 .GT. SS2 ) THEN
        GOTO 10
      ENDIF
      Y = -Y
      GOTO 10
!
6     IF( SS2 .NE. SS3 ) RETURN
!
10    TROIC = Y/SQRT(DC(CC2))
      RETURN
!
3     IF( CC2 .GT. 3 ) GOTO 13
      SI = SS1+SS2+SS3-2
      IF    ( SI .EQ. 1      ) THEN
        Y = -Y
      ELSEIF( SI .EQ. 2 .OR.         &
              SI .EQ. 4      ) THEN
        RETURN
      ENDIF
      TROIC = Y*.5D0                                                                               ! 1,3
      RETURN
!
13    IF( SS2 .NE. SS3 ) RETURN
      CI = CC2+CC3+SS1-8
      IF( CI .EQ. 1 .OR.         &
          CI .EQ. 3      ) THEN
        T = IF1(SS2)
        IF( SS1 .EQ. 2 ) T = -T
      ELSE                                                                                         ! 2,4
        T = IF2(SS2)
      ENDIF
      IF( ABS(T) .LT. 1.D-6 ) RETURN
      TROIC = Y*SQRT(ABS(T))/T
      RETURN
!
4     IF(  SS1*SS2*SS3 .NE.  6             ) RETURN
      IF(  SC          .EQ. -1       .AND.           &
          (SS2-SS3     .EQ.  1 .OR.                  &
           SS2-SS3     .EQ. -2     )       ) Y = -Y
      TROIC = -Y/SQRT(6.D0)
!
      RETURN
      END FUNCTION TROIC
