      SUBROUTINE XOBMCA(M,FVEC,IFLAG)
!
! 2007 SEPT
! OBS-CAL CALCULUS FOR FCN OF xpafit.f90
!
! BASED UPON eq_tds.f90 AND eq_int.f90
!
      use mod_dppr
      use mod_par_tds
      use mod_com_const
      use mod_com_cas
      use mod_com_en
      use mod_com_spin
      use mod_com_xassi
      use mod_com_xfonc
      use mod_com_xpara
      use mod_com_xpoly
      use mod_com_xtran
      use mod_com_xvari
      IMPLICIT NONE
      real(kind=dppr) ,dimension(M)  :: FVEC
      integer          :: M,IFLAG

      real(kind=dppr)  :: A2
      real(kind=dppr)  :: DIFSA,DIFSR
      real(kind=dppr)  :: EN
      real(kind=dppr)  :: F
      real(kind=dppr)  :: HHT,HT
      real(kind=dppr)  :: TM,TR
      real(kind=dppr)  :: W0

      integer          :: IC,ICI,ICS,IFOBZC,II,IOBS,IOBZ,IPOL
      integer          :: IS,ISOBZC,ITR,IZC
      integer          :: J,JC,JCI,JCIOBS,JCS,JCSOBS,JI,JS
      integer          :: NFB,NFBC,NFBI,NFBS,NBFTRC,NBSTRC,NINF,NSUP

      character(len = 120)  :: FXEN,FXTRM
!
8000  FORMAT(' !!! XOBMCA : STOP ON ERROR')
8050  FORMAT(' !!! ERROR OPENING ENERGY FILE : ',A)
8105  FORMAT(' !!! ERROR OPENING TRANSITION MOMENT FILE : ',A)
8107  FORMAT(' !!! UNEXPECTED EOF FOR : ',A)
8203  FORMAT(' !!! ',I8,1X,A,' SOLVED, ',I8,' EXPECTED',/,   &
             ' !!! FOR TRANSITION MOMENT FILE : ',A       )
!
      IF( IFLAG .EQ. 1 ) THEN
        DO IZC=1,NBSOBZ                                                                            ! all intensities are supposed to be in range
          SASOZ(IZC) = '+'
        ENDDO
      ENDIF
      DO IPOL=1,NBPOL
        CALL XHDI(IPOL)                                                                            ! energy files
!
! STORE LOWER ENERGY
!
        FXEN = 'XEN_P'//CPOL(IPOL)//'_'                                                            ! polyad energy file
        OPEN(50,ERR=4050,FILE=FXEN,STATUS='OLD',FORM='UNFORMATTED')
!
1       READ(50,END=2) J,IC,NFB
        JC = J*10+IC
        DO NFBC=1,NFB
          READ(50,END=4150) EN
E4:       DO IOBS=1,NBOBS
            IF( CPOLI(IOBS) .NE. CPOL(IPOL) ) GOTO 5
            JCIOBS = JIOBS(IOBS)*10+ICIOBS(IOBS)                                                   ! inf
            IF( JCIOBS      .EQ. JC   .AND.         &
                NIOBS(IOBS) .EQ. NFBC       ) THEN
              ENINF(IOBS) = EN
            ENDIF
!
5           IF( CPOLS(IOBS) .NE. CPOL(IPOL) ) CYCLE E4
            JCSOBS = JSOBS(IOBS)*10+ICSOBS(IOBS)                                                   ! sup
            IF( JCSOBS      .EQ. JC   .AND.         &
                NSOBS(IOBS) .EQ. NFBC       ) THEN
              ENSUP(IOBS) = EN
            ENDIF
          ENDDO E4
        ENDDO
        GOTO 1
!
2       CLOSE(50)
      ENDDO
E200: DO ITR=1,NTR                                                                                 ! for each transition
!
! FREQUENCY PROCESS
!
        IF( .NOT. IONLY ) THEN
          NBFTRC = 0                                                                               ! current number of frequency data assigned to this transition
          IF( NBFTR(ITR) .NE. 0 ) THEN
E9:         DO IOBS=1,NBOBS
              IF( JTR(IOBS)   .NE. ITR .OR.             &
                  FASOT(IOBS) .NE. '+'      ) CYCLE E9
              F            = ENSUP(IOBS)-ENINF(IOBS)
              IFOBZC       = IFOBZ(IOBS)
              FVEC(IFOBZC) = (FOBS(IOBS)-F)*ZWGT(IFOBZC)
              NBFTRC       = NBFTRC+1
              IF( NBFTRC .EQ. NBFTR(ITR) ) GOTO 10
            ENDDO E9
!
10          CONTINUE
          ENDIF
        ENDIF
!
! INTENSITY PROCESS
!
        NBSTRC = 0                                                                                 ! current number of intensity data assigned to this transition
        IF( NBSTR(ITR) .NE. 0 ) THEN
          CALL XTRM(ITR)
          FXTRM = 'XTRM_'//CTR(ITR)                                                                ! transition moment file
          OPEN(80,ERR=4080,FILE=FXTRM,STATUS='OLD',FORM='UNFORMATTED')
          READ(80)
!
! ASSIGNMENT JC CODES
!
11        READ(80,END=13) JS,ICS,NFBS,  &
                          JI,ICI,NFBI
          JCS = JS*10+ICS
          JCI = JI*10+ICI
E14:      DO IS=1,NFBS
E15:        DO II=1,NFBI
              READ(80,END=4180) NSUP,NINF,TM
! LOOK FOR AN ASSIGNMENT
E12:          DO IOBS=1,NBOBS
                IF( JTR(IOBS)   .NE. ITR .OR.              &
                    SASOT(IOBS) .NE. '+'      ) CYCLE E12
                JCIOBS = JIOBS(IOBS)*10+ICIOBS(IOBS)                                               ! right transition
                JCSOBS = JSOBS(IOBS)*10+ICSOBS(IOBS)
                IF( JCIOBS      .EQ. JCI .AND.         &
                    NIOBS(IOBS) .EQ. II  .AND.         &
                    JCSOBS      .EQ. JCS .AND.         &
                    NSOBS(IOBS) .EQ. IS        ) THEN
                  ISOBZC = ISOBZ(IOBS)
                  IZC    = ISOBZC-NBFOBZ
                  IF( IFLAG      .NE. 1   .AND.         &
                      SASOZ(IZC) .EQ. '-'       ) THEN
                    FVEC(ISOBZC) = 0.D0
                    GOTO 16
                  ENDIF
                  F   = ENSUP(IOBS)-ENINF(IOBS)                                                    ! right assignment
                  A2  = TM*TM
                  HT  = ENINF(IOBS)*HCOVRK/TEMP(IOBS)
                  W0  = EXP(-HT)*SPIN(ICI)
                  HHT = F*HCOVRK/TEMP(IOBS)
                  TR  = A2*W0*F*COEF(IOBS)*(1.D0-EXP(-HHT))
                  IF( ISRAM(ITR) ) TR = A2*W0
                  DIFSA = SOBS(IOBS)-TR                                                            ! absolute delta
                  DIFSR = 100.D0*DIFSA/TR                                                          ! relative delta
                  IF( ABS(DIFSR) .LE. RMXOMC ) THEN                                                ! what about RMXOMC
                    FVEC(ISOBZC) = DIFSA*ZWGT(ISOBZC)
                  ELSE
                    FVEC(ISOBZC) = 0.D0
                    IF( IFLAG .EQ. 1 ) SASOZ(IZC) = '-'
                  ENDIF
!
16                NBSTRC = NBSTRC+1
                  IF( NBSTRC .EQ. NBSTR(ITR) ) GOTO 13
                ENDIF
              ENDDO E12
            ENDDO E15
          ENDDO E14
          GOTO 11                                                                                  ! next calculated transition
!
13        CLOSE(80)
        ENDIF
! CONSISTENCY CHECK
        IF( .NOT. IONLY ) THEN
          IF( NBFTRC .NE. NBFTR(ITR) ) THEN
            PRINT 8203, NBFTRC,'FREQUENCY',NBFTR(ITR),FXTRM
            GOTO  9999
          ENDIF
        ENDIF
        IF( NBSTRC .NE. NBSTR(ITR) ) THEN
          PRINT 8203, NBSTRC,'INTENSITY',NBSTR(ITR),FXTRM
          GOTO  9999
        ENDIF
      ENDDO E200
! VIRTUAL (TYPE 2) OBS-CAL PROCESS
      IOBZ = NBROBZ
      IF( IOBZ .NE. NBOBZ ) THEN
        DO J=1,NBOP
          IF( ICNTRL(J) .EQ. 2 ) THEN
            IOBZ       = IOBZ+1
            FVEC(IOBZ) = (VALAT(J)-PARA(J))/PRAT(J)
          ENDIF
        ENDDO
      ENDIF
      RETURN
!
4050  PRINT 8050, FXEN
      GOTO  9999
4150  PRINT 8107, FXEN
      GOTO  9999
4080  PRINT 8105, FXTRM
      GOTO  9999
4180  PRINT 8107, FXTRM
      GOTO  9999
9999  PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE XOBMCA
