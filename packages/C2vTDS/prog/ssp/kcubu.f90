!
! *** FACTEURS ISOSCALAIRES  O(3)-TD
!
! SMIL CHAMPION DEC 78
!
      SUBROUTINE KCUBU(J1,J2,J3,IU1,IU2,IU3,N1,N2,N3,IC1,IC2,IC3,AK,IK)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: AK
      integer          :: J1,J2,J3,IU1,IU2,IU3,N1,N2,N3,IC1,IC2,IC3,IK

      integer ,parameter  :: MDMK = 3

! functions
      real(kind=dppr)  :: TROIC

      real(kind=dppr)  :: F,FT

      integer ,dimension(MDMK)  :: ICL,ISL
      integer ,dimension(MDMK)  :: K
      integer          :: I
      integer          :: K1,K2,K3
!
      AK     = 0.D0
      IK     = 0
      ICL(1) = IC1
      ICL(2) = IC2
      ICL(3) = IC3
      DO I=1,MDMK
        ISL(I) = 1
      ENDDO
      CALL IRDER(MDMK,ICL,K,MDMK)
      K1 = K(1)
      K2 = K(2)
      K3 = K(3)
      IF    ( ICL(K1) .LT. 1 ) THEN
        GOTO 1
      ELSEIF( ICL(K1) .GT. 1 ) THEN
        GOTO 4
      ENDIF
      IF(     ICL(K2) .LE. 3 ) GOTO 1
!
3     ISL(K2) = 3
      ISL(K3) = 3
      GOTO 1
!
4     IF    ( ICL(K1) .LT. 2 ) THEN
        GOTO 1
      ELSEIF( ICL(K1) .GT. 2 ) THEN
        GOTO 7
      ENDIF
      IF    ( ICL(K2) .LT. 3 ) THEN
        GOTO 1
      ELSEIF( ICL(K2) .GT. 3 ) THEN
        GOTO 3
      ENDIF
      ISL(K3) = 2
      GOTO 1
!
7     IF    ( ICL(K1) .LT. 3       ) THEN
        GOTO 1
      ELSEIF( ICL(K1) .GT. 3       ) THEN
        GOTO 11
      ENDIF
      IF(     ICL(K2) .LE. 3       ) GOTO 1
      IF    ( ICL(K2) .EQ. ICL(K3) ) THEN
        GOTO 3
      ELSEIF( ICL(K2) .GT. ICL(K3) ) THEN
        GOTO 1
      ENDIF
      ISL(K1) = 2
      GOTO 3
!
11    ISL(K2) = 2
      ISL(K3) = 3
!
1     CALL FCUBU(J1,J2,J3,IU1,IU2,IU3,N1,N2,N3,IC1,IC2,IC3,ISL(1),ISL(2),ISL(3),F,IK)
      FT = TROIC(IC1,IC2,IC3,ISL(1),ISL(2),ISL(3))
      IF( FT .EQ. 0.D0 ) RETURN
      AK = F/FT
!
      RETURN
      END SUBROUTINE KCUBU
