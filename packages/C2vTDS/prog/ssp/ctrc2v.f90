!
! ***** Test sur les representations de C2v
!
! ***** M. Rotger 12/98
!
      FUNCTION CTRC2V(IC1,IC2,IC3)
      IMPLICIT NONE
      integer          :: CTRC2V
      integer          :: IC1,IC2,IC3

      integer ,dimension(1) :: IC
      integer          :: J
      integer          :: N
!
      CTRC2V = 0
      CALL MULC2V(IC1,IC2,N,IC)
      DO J=1,N
        IF( IC3 .EQ. IC(J) ) CTRC2V = 1
      ENDDO
!
      RETURN
      END FUNCTION CTRC2V
