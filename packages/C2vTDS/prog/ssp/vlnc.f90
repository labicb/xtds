      SUBROUTINE VLNC(ICO,IV,IL,IN,IC)                                                             ! SMIL G.PIERRE JUIN 82
      IMPLICIT NONE
      integer          :: ICO,IV,IL,IN,IC

!
      IV = ICO/1000
      IL = (ICO-1000*IV)/100
      IN = (ICO-100*(ICO/100))/10
      IC = ICO-10*(ICO/10)
!
      RETURN
      END SUBROUTINE VLNC
