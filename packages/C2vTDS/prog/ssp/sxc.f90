!
! *** RECHERCHE, PAR TRI DICHOTOMIQUE, D'UN 6C CALCULE PAR CAL6C.
!     S'IL N'EST PAS DANS LE TABLEAU, ON LE RESTITUE COMME 0.
!
      FUNCTION SXC(IC1,IC2,IC3,IC4,IC5,IC6)
      use mod_dppr
      use mod_com_com6c
      IMPLICIT NONE
      real(kind=dppr)  :: SXC
      integer          :: IC1,IC2,IC3,IC4,IC5,IC6

      integer          :: I,I1,I2,I6CC
!
      SXC  = 0.D0
      I6CC = 100000*IC1+10000*IC2+1000*IC3+100*IC4+10*IC5+IC6
      I1   = 1
      I2   = NB6C
      IF( I6CC .LT. I6C(I1) ) RETURN
      IF( I6CC .GT. I6C(I2) ) RETURN
      IF( I6CC .EQ. I6C(I2) ) GOTO 4
!
1     I = (I1+I2)/2
      IF    ( I6CC .EQ. I6C(I) ) THEN
        GOTO 5
      ELSEIF( I6CC .GT. I6C(I) ) THEN
        GOTO 3
      ENDIF
      IF( I2-I1 .EQ. 1 ) RETURN
      I2 = I
      GOTO 1
!
3     IF( I2-I1 .EQ. 1 ) RETURN
      I1 = I
      GOTO 1
!
4     I = I2
!
5     SXC = V6C(I)
!
      RETURN
      END FUNCTION SXC
