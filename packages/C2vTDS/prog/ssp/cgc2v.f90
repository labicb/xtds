!
! ***** Calcul des G orientes dans C2v
!
! ***** M. Rotger 12/98
!
      FUNCTION CGC2V(IC,IS,ICT,IST)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: CGC2V
      integer          :: IC,IS,ICT,IST

      integer ,parameter  :: IMIN = 1
      integer ,parameter  :: IMAX = 14

      real(kind=dppr) ,dimension(IMAX)  :: TVAL

      integer         ,dimension(IMAX)  :: ICODE = (/ 1111, 2121, 3111, 3221, 4131, 4141, 4231,   &
                                                      4241, 4321, 5131, 5141, 5231, 5241, 5311 /)
      integer          :: I,I1,I2,ICOD
!
      TVAL(1)  =  1.D0
      TVAL(2)  =  1.D0
      TVAL(3)  =  1.D0
      TVAL(4)  =  1.D0
      TVAL(5)  =  1.D0/SQRT(2.D0)
      TVAL(6)  =  1.D0/SQRT(2.D0)
      TVAL(7)  = -1.D0/SQRT(2.D0)
      TVAL(8)  =  1.D0/SQRT(2.D0)
      TVAL(9)  =  1.D0
      TVAL(10) =  1.D0/SQRT(2.D0)
      TVAL(11) =  1.D0/SQRT(2.D0)
      TVAL(12) =  1.D0/SQRT(2.D0)
      TVAL(13) = -1.D0/SQRT(2.D0)
      TVAL(14) =  1.D0
      CGC2V    =  0.D0
      I1       = IMIN
      I2       = IMAX
      ICOD     = 1000*IC+100*IS+10*ICT+IST
      IF( ICOD .LT. ICODE(I1) ) RETURN
      IF( ICOD .GT. ICODE(I2) ) RETURN
      IF( ICOD .EQ. ICODE(I2) ) GOTO 12
1     I = (I1+I2)/2
      IF    ( ICOD .EQ. ICODE(I) ) THEN
        GOTO 7
      ELSEIF( ICOD .GT. ICODE(I) ) THEN
        GOTO 3
      ENDIF
      IF( I2-I1 .EQ. 1 ) RETURN
      I2 = I
      GOTO 1
!
3     IF( I2-I1 .EQ. 1 ) RETURN
      I1 = I
      GOTO 1
!
12    I     = I2
7     CGC2V = TVAL(I)
!
      RETURN
      END FUNCTION CGC2V
