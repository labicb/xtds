!
!   *** SYMBOLES 6 - C DE C2V
!
      FUNCTION SXC1C2(ICT1,ICT2,ICT3,ICT4,ICT5,ICT6)                                               ! D'APRES PROG. ORSAY 80
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: SXC1C2
      integer          :: ICT1,ICT2,ICT3,ICT4,ICT5,ICT6

! functions
      integer          :: CTRC2V

!
      SXC1C2 = 0.D0
      IF( CTRC2V(ICT1,ICT2,ICT3) .EQ. 0 ) RETURN
      IF( CTRC2V(ICT1,ICT5,ICT6) .EQ. 0 ) RETURN
      IF( CTRC2V(ICT4,ICT2,ICT6) .EQ. 0 ) RETURN
      IF( CTRC2V(ICT4,ICT5,ICT3) .EQ. 0 ) RETURN
      SXC1C2 = 1.D0
!
      RETURN
      END FUNCTION SXC1C2
