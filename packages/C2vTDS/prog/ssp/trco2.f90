!
! ***** Calcul des 3 - C orientes dans C2v
!
! ***** M. Rotger 12/98
!
      FUNCTION TRCO2(IC1,IC2,IC3,ICT1,ICT2,ICT3,IST1,IST2,IST3)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: TRCO2
      integer          :: IC1,IC2,IC3,ICT1,ICT2,ICT3,IST1,IST2,IST3

! function
      real(kind=dppr)  :: CGC2V,TROIC
      integer          :: CTR,CTRC2V
      integer          :: ISCTC

      integer ,parameter  :: IMIN = 1
      integer ,parameter  :: IMAX = 10

      real(kind=dppr)  :: CGC2V1,CGC2V2,CGC2V3
      real(kind=dppr)  :: F

      integer ,dimension(IMAX,2) :: IVALB
      integer ,dimension(3,2)    :: IS
      integer ,dimension(3,3)    :: ITB
      integer ,dimension(IMAX)   :: ICODEB = (/ 111, 221, 311, 321, 421, 431, 441, 511, 531, 541 /)
      integer ,dimension(IMAX)   :: IDIMVB = (/   1,   1,   1,   1,   1,   2,   2,   1,   2,   2 /)
      integer ,dimension(3)      :: MEM
      integer          :: I,I1,I2,IBUF,IC,ICODB,ICT,IST,ITOTO,ITRI
      integer          :: J
      integer          :: L
      integer          :: M
      integer          :: N
!
      TRCO2       = 0.D0
      MEM(1)      = 0
      MEM(2)      = 0
      MEM(3)      = 0
      ITB(1,1)    = IC1
      ITB(1,2)    = ICT1
      ITB(1,3)    = IST1
      ITB(2,1)    = IC2
      ITB(2,2)    = ICT2
      ITB(2,3)    = IST2
      ITB(3,1)    = IC3
      ITB(3,2)    = ICT3
      ITB(3,3)    = IST3
      IVALB(1,1)  = 1
      IVALB(1,2)  = 0
      IVALB(2,1)  = 1
      IVALB(2,2)  = 0
      IVALB(3,1)  = 1
      IVALB(3,2)  = 0
      IVALB(4,1)  = 2
      IVALB(4,2)  = 0
      IVALB(5,1)  = 3
      IVALB(5,2)  = 0
      IVALB(6,1)  = 1
      IVALB(6,2)  = 2
      IVALB(7,1)  = 1
      IVALB(7,2)  = 2
      IVALB(8,1)  = 3
      IVALB(8,2)  = 0
      IVALB(9,1)  = 1
      IVALB(9,2)  = 2
      IVALB(10,1) = 1
      IVALB(10,2) = 2
E7:   DO J=1,3
        IC    = ITB(J,1)
        ICT   = ITB(J,2)
        IST   = ITB(J,3)
        I1    = IMIN
        I2    = IMAX
        ICODB = 100*IC+10*ICT+IST
        IF( ICODB .LT. ICODEB(I1) ) RETURN
        IF( ICODB .GT. ICODEB(I2) ) RETURN
        IF( ICODB .EQ. ICODEB(I2) ) GOTO 12
1       I = (I1+I2)/2
        IF    ( ICODB .EQ. ICODEB(I) ) THEN
          GOTO 17
        ELSEIF( ICODB .GT. ICODEB(I) ) THEN
          GOTO 3
        ENDIF
        IF( I2-I1 .EQ. 1 ) GOTO 50
        I2 = I
        GOTO 1
!
3       IF( I2-I1 .EQ. 1 ) GOTO 50
        I1 = I
        GOTO 1
!
12      I      = I2
17      MEM(J) = IDIMVB(I)
        DO L=1,MEM(J)
          IBUF    = IVALB(I,L)
          IS(J,L) = IBUF
        ENDDO
      ENDDO E7
50    ITRI = CTRC2V(ITB(1,2),ITB(2,2),ITB(3,2))
      IF( ITRI .LE. 0 ) RETURN
      ITRI = CTR(ITB(1,1),ITB(2,1),ITB(3,1))
      IF( ITRI .LE. 0 ) RETURN
      TRCO2 = 0.D0
      DO L=1,MEM(1)
        DO M=1,MEM(2)
          DO N=1,MEM(3)
            F      = TROIC(ITB(1,1),ITB(2,1),ITB(3,1),IS(1,L),IS(2,M),IS(3,N))
            CGC2V1 = 0.D0
            ITOTO  = ISCTC(ITB(1,1),ITB(1,2),ITB(1,3))
            IF( ITOTO .EQ. 0 ) RETURN
            CGC2V1 = CGC2V(ITB(1,1),IS(1,L),ITB(1,2),ITB(1,3))
            CGC2V2 = 0.D0
            ITOTO  = ISCTC(ITB(2,1),ITB(2,2),ITB(2,3))
            IF( ITOTO .EQ. 0 ) RETURN
            CGC2V2 = CGC2V(ITB(2,1),IS(2,M),ITB(2,2),ITB(2,3))
            CGC2V3 = 0.D0
            ITOTO  = ISCTC(ITB(3,1),ITB(3,2),ITB(3,3))
            IF( ITOTO .EQ. 0 ) RETURN
            CGC2V3 = CGC2V(ITB(3,1),IS(3,N),ITB(3,2),ITB(3,3))
            TRCO2  = TRCO2+CGC2V1*CGC2V2*CGC2V3*F
          ENDDO
        ENDDO
      ENDDO
!
      RETURN
      END FUNCTION TRCO2
