!
!*********************************************************************
! DIAGONALIZATION OF SYMETRIC AND FULL MATRIXES : H(I,J)=H(J,I)
!
! INPUT => H    ;   OUTPUT => H  AND  T  WITH :
! H=(T-1)*H*T   ;   EIGENVECT.= T(IB,IP)*BASE VECT.
!*********************************************************************
!
! V. BOUDON 2002 : USE OF B.SARTAKOV DMSPRO PROGRAM
!
      SUBROUTINE DIAGO(N)
      use mod_dppr
      use mod_par_tds
      use mod_com_pgdh
      IMPLICIT NONE
      integer          :: N

      real(kind=dppr) ,dimension((MXDIMS*MXDIMS+MXDIMS)/2)  :: A
      real(kind=dppr) ,dimension(MXDIMS*MXDIMS)             :: B
      real(kind=dppr)  :: ACC

      integer          :: I
      integer          :: J
      integer          :: K
!
      ACC = 1.D-16
      K   = 1
      DO I=1,N
        DO J=1,I
          A(K) = H(J,I)
          K    = K+1
        ENDDO
      ENDDO
      CALL DMSPR(A,B,N,0,ACC)
      DO I=1,N
        DO J=1,N
          T(J,I) = B(N*(I-1)+J)
          IF( J .EQ. I ) THEN
            H(I,J) = A((J*J+J)/2)
          ELSE
            H(I,J) = 0.D0
          ENDIF
        ENDDO
      ENDDO
!
      RETURN
      END SUBROUTINE DIAGO
