      FUNCTION AKPRIM(IC1,IC2,IC3,ICT1,ICT2,ICT3)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      real(kind=dppr)  :: AKPRIM
      integer          :: IC1,IC2,IC3,ICT1,ICT2,ICT3

! functions
      real(kind=dppr)  :: TRCO2,TRCC2V

      real(kind=dppr)  :: TROISB,TROISC

      integer ,dimension(MXSYM)  :: IDIMB = (/ 1, 1, 1, 1 /)
      integer          :: IST1,IST2,IST3
!
      AKPRIM = 0.D0
      DO IST1=1,IDIMB(ICT1)
        DO IST2=1,IDIMB(ICT2)
          DO IST3=1,IDIMB(ICT3)
            TROISC = TRCO2(IC1,IC2,IC3,ICT1,ICT2,ICT3,IST1,IST2,IST3)
            TROISB = TRCC2V(ICT1,ICT2,ICT3,IST1,IST2,IST3)
            IF( TROISC*TROISB .NE. 0.D0 ) GOTO 110
          ENDDO
        ENDDO
      ENDDO
      RETURN
!
110   AKPRIM = TROISC/TROISB
!
      RETURN
      END FUNCTION AKPRIM
