!
! *** VALEUR MAXIMALE DE M DU SYMBOLE G**J.
!
! SMIL CHAMPION DEC 78
!
      FUNCTION MMAXS(J,IC,IS)
      IMPLICIT NONE
      integer          :: MMAXS
      integer          :: J,IC,IS

!
8000  FORMAT(' !!! MMAXS : STOP ON ERROR')
8001  FORMAT(' !!! INVALID VALUE FOR J,IC,IS :',3I3)
!
      SELECT CASE( IC )
        CASE( 1 )
          GOTO 6
        CASE( 2 )
          GOTO 7
        CASE( 3 )
          SELECT CASE( IS )
            CASE( 1 )
              GOTO 6
            CASE( 2 )
              GOTO 7
          END SELECT
        CASE( 4 )
          SELECT CASE( IS )
            CASE( 1,2 )
              GOTO 8
            CASE( 3 )
              GOTO 6
          END SELECT
        CASE( 5 )
          SELECT CASE( IS )
            CASE( 1,2 )
              GOTO 8
            CASE( 3 )
              GOTO 7
          END SELECT
      END SELECT
      GOTO 9999
!
6     MMAXS = 4*(J/4)
      RETURN
!
7     MMAXS = 4*((J-2)/4)+2
      RETURN
!
8     MMAXS = 2*((J-1)/2)+1
      RETURN
!
9999  PRINT 8001, J,IC,IS
      PRINT 8000
      PRINT *
      STOP
!
      END FUNCTION MMAXS
