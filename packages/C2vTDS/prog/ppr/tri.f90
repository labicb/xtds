      PROGRAM TRI
!
! EXTRACTION , REMPLACEMENT , SUPPRESSION ET TRI DANS UN FICHIER.
! CRITERES DE SELECTION SELON PLUSIEURS CHAMPS (NUMERIQUES/ALPHABETIQUES)
!
! ATTENTION :
! LE NB MAXI DE LIGNES TRAITEES SIMULTANEMENT      EST NBLMAX
! LE NB MAXI DE CHAMPS                             EST NBCMAX
! LA LONGUEUR MAXI DE LIGNE (TRONQUEES A L'ENTREE) EST 152
!
! LE FICHIER D'ENTREE EST D'ABORD COPIE DANS LUI
! LES FICHIERS INTERMEDIAIRES SONT LUI ET LUO ('SCRATCH' ALTERNES)
! LUI EST COPIE DANS LA SORTIE PAR 'end'.
!
! APPEL : TRI
!
      use mod_dppr
      use mod_main_tri
      IMPLICIT NONE

! functions
      logical          :: VALIDE

      real(kind=dppr)  :: EV

      integer          :: DLIGNE
      integer          :: I,I1,I2,I3,IV
      integer          :: J
      integer          :: LUI,LUO,LUX
      integer          :: PLIGNE

      character(len =   1)  :: ORDO
      character(len =   1)  :: REP
      character(len =   4)  :: CMD
      character(len = 120)  :: NOM
      character(len = 152)  :: CHRE

      logical          :: EOF
!
1000  FORMAT(A)
1001  FORMAT('INPUT FILE                             : ')
1002  FORMAT('OUTPUT FILE                            : ')
1003  FORMAT('FIRST, LAST LINE      (,, =',I2,',',I7,') : ')
1005  FORMAT('STRING or NUMBER?                (s/n) : ')
1006  FORMAT('READ FORMAT           (ex. f8.4)       : ')
1008  FORMAT(/,                                            &
             'COMMAND  (extr/repl/del/sort/end/help) : ')
1009  FORMAT(/,                                            &
             'extr : EXTRACT LINES OF DEFINED FORMAT',/,   &
             'repl : REPLACE STRING'                 ,/,   &
             'del  : DELETE  LINES OF DEFINED FORMAT',/,   &
             'sort : SORT'                           ,/,   &
             'end  : END'                            ,/,   &
             'help : THIS HELP'                      ,/ )
1010  FORMAT('SEARCH STRING                          : ')
1011  FORMAT(/,                                               &
             'NUMBER OF EXTRACTED LINES              : ',I7)
1012  FORMAT(/,                                               &
             'NUMBER OF DELETED   LINES              : ',I7)
1013  FORMAT(/,                                            &
             'ONE MORE FIELD?                  (y/n) : ')
1014  FORMAT(/,                                               &
             'MAX NUMBER OF FIELDS REACHED           : ',I1)
1015  FORMAT('INCREASING or DECREASING ORDER?  (i/d) : ')
1016  FORMAT(/,                                                 &
             'FIELD #                                : ',I1,/)
1017  FORMAT('REPLACEMENT STRING                     : ')
1019  FORMAT(/,                    &
             'Please WAIT ...',/)
1020  FORMAT(/,                                               &
             'NUMBER OF MODIFIED  LINES              : ',I7)
1021  FORMAT('LIMITS OF STRING TO BE REPLACED ,')
1099  FORMAT('RESTRICTIONS:'                                             ,/,   &
             '    PROCESSING ',I7,' FIELDS MAX'                          ,/,   &
             '    FOR                LINES LIMITED TO ',I7,' CHARACTERS.',/ )
8004  FORMAT(' !!! FIRST LINE #:',I7,' > ',I7,': NB OF LINES OF THE FILE')
8005  FORMAT(/,                         &
             ' !!! COMMAND ABORTED',/)
8006  FORMAT(' !!! ERROR WHILE NUMERICAL FIELD CONVERSION')
8007  FORMAT(' !!! ERROR WRITING SCRATCH FILE')
8008  FORMAT(' !!! ERROR READING SCRATCH FILE')
8009  FORMAT(' !!! ERROR OPENING OUTPUT FILE')
8010  FORMAT(' !!! ERROR WRITING OUTPUT FILE')
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      EOF = .FALSE.                                                                                ! INITALIZE
      LUX = 70
      LUI = 50
      LUO = 60
! RESTRICTIONS
      PRINT 1099, NBCMAX,152                                                                       ! RESTRICTIONS
!
1     PRINT 1001                                                                                   ! INPUT FILE
!
! INPUT FILE
      READ(*,1000,ERR=1) NOM
      OPEN(LUX,FILE=TRIM(NOM),STATUS='OLD',ERR=1)
      OPEN(LUI,STATUS='SCRATCH')
!
12    READ (LUX,1000,ERR=9002,END=13) LIGNE                                                        ! COPY INPUT TO LUI
      WRITE(LUI,1000,ERR=9003)        TRIM(LIGNE)
      GOTO 12
!
13    CLOSE(LUX)
      ENDFILE(LUI)
      REWIND(LUI)
!
2     PRINT 1002                                                                                   ! OUTPUT FILE
!
! OUTPUT FILE
      READ(*,1000,ERR=2) NOM
      OPEN(LUO,STATUS='SCRATCH')
!
! ASK FOR A COMMAND
7     PRINT 1008                                                                                   ! COMMAND
      READ(*,1000) CMD
      IF( CMD .EQ. 'EXTR' ) CMD = 'extr'
      IF( CMD .EQ. 'REPL' ) CMD = 'repl'
      IF( CMD .EQ. 'DEL'  ) CMD = 'del'
      IF( CMD .EQ. 'SORT' ) CMD = 'sort'
      IF( CMD .EQ. 'END'  ) CMD = 'end'
      IF( CMD .EQ. 'HELP' ) CMD = 'help'
      IF( CMD .EQ. 'end'  ) GOTO 100                                                               ! END
      IF( CMD .EQ. 'help' ) THEN                                                                   ! HELP
        PRINT 1009
        GOTO 7
      ENDIF
      IF( CMD .NE. 'extr' .AND.           &
          CMD .NE. 'repl' .AND.           &
          CMD .NE. 'del'  .AND.           &
          CMD .NE. 'sort'       ) GOTO 7
!
! SORT
      IF( CMD .EQ. 'sort' ) THEN
!
16      PRINT 1015                                                                                 ! SORTING ORDER
        READ(*,1000) ORDO
        IF( ORDO .EQ. 'I'       ) ORDO = 'i'
        IF( ORDO .EQ. 'D'       ) ORDO = 'd'
        IF( ORDO .NE. 'i' .AND.            &
            ORDO .NE. 'd'       ) GOTO 16
      ENDIF
!
! CRITERIA
!
! LINES
3     PLIGNE = 1                                                                                   ! DEFAULT FIRST LINE
      DLIGNE = NBLMAX                                                                              ! DEFAULT LAST  LINE
      PRINT 1003,     PLIGNE,DLIGNE                                                                ! FIRST LINE, LAST  LINE
      READ(*,*,ERR=3) PLIGNE,DLIGNE
      IF( PLIGNE .LE. 0 .OR.           &
          DLIGNE .LE. 0      ) GOTO 3                                                              ! OUT OF RANGE
      IF( DLIGNE .LT. PLIGNE ) GOTO 3                                                              ! OUT OF RANGE
      NBLGN = DLIGNE-PLIGNE+1                                                                      ! NB OF LINES TO PROCESS
      DO WHILE( NBLGN .GT. NBLMAX )
        CALL RESIZE_NBLMAX
      ENDDO
! FIELDS
      J = 1
!
4     PRINT 1016, J                                                                                ! FIELDS
      CALL LICOL(J)                                                                                ! READ FIELD LIMITS
!
5     PRINT 1005                                                                                   ! FIELD TYPE
      READ(*,1000,ERR=5) TYPE(J)
      IF(  TYPE(J) .EQ. 'S'                ) TYPE(J) = 's'
      IF(  TYPE(J) .EQ. 'N'                ) TYPE(J) = 'n'
      IF(  TYPE(J) .NE. 's'          .AND.           &
           TYPE(J) .NE. 'n'                ) GOTO 5                                                ! WRONG TYPE
      IF(  TYPE(J) .EQ. 's'          .AND.         &                                               ! STRING
          (CMD     .EQ. 'extr' .OR.                &
           CMD     .EQ. 'repl' .OR.                &
           CMD     .EQ. 'del'      )       ) THEN
        PRINT 1010                                                                                 ! TEST STRING
        READ(*,1000) TEST(J)
      ENDIF
      IF( TYPE(J) .EQ. 'n' ) THEN                                                                  ! NUMERICAL
!
6       PRINT 1006                                                                                 ! FORMAT TEST
        READ(*,1000,ERR=6) LIGNE
        FOR(J) = '('//TRIM(LIGNE)//')'                                                             ! FORMAT
        DO I=1,LEN_TRIM(FOR(J))                                                                    ! NUMERICAL DATA TYPE SELF SET
          IF( FOR(J)(I:I) .EQ. 'i' .OR.         &
              FOR(J)(I:I) .EQ. 'I'      ) THEN                                                     ! INTEGER
            VAR(J) = 'i'
            GOTO 11
          ENDIF
          IF( FOR(J)(I:I) .EQ. 'e' .OR.         &
              FOR(J)(I:I) .EQ. 'E'      ) THEN                                                     ! FLOATING POINT SINGLE
            VAR(J) = 'e'
            GOTO 11
          ENDIF
          IF( FOR(J)(I:I) .EQ. 'f' .OR.         &
              FOR(J)(I:I) .EQ. 'F' .OR.         &
              FOR(J)(I:I) .EQ. 'd' .OR.         &
              FOR(J)(I:I) .EQ. 'D'      ) THEN                                                     ! FLOATING POINT DOUBLE
            VAR(J) = 'd'
            GOTO 11
          ENDIF
        ENDDO
        GOTO 6
!
11      CONTINUE
      ENDIF
      IF( J .EQ. NBCMAX ) THEN                                                                     ! LAST FIELD REACHED
        PRINT 1014, NBCMAX
        GOTO 15
      ENDIF
! NEXT FIELD
18    PRINT 1013                                                                                   ! AN OTHER FIELD?
      READ(*,1000) REP
      IF( REP .EQ. 'Y'       ) REP = 'y'
      IF( REP .EQ. 'N'       ) REP = 'n'
      IF( REP .NE. 'y' .AND.            &
          REP .NE. 'n'       ) GOTO 18                                                             ! WRONG ANSWER
      IF( REP .EQ. 'y'       ) THEN
        J = J+1
        GOTO 4
      ENDIF
!
15    NBCHPS = J                                                                                   ! NB OF FIELDS
      IF( CMD .EQ. 'repl' ) THEN
        PRINT 1021
        CALL LICOL(NRE)                                                                            ! LIMITS OF THE STRING TO REPLACE
        PRINT 1017
        READ(*,1000) CHRE                                                                          ! REPLACING STRING
      ENDIF
      PRINT 1019
!
! COPY INPUT FILE BEGINNING
!
      DO I=1,PLIGNE-1
        READ (LUI,1000,ERR=9001,END=9006) LIGNE
        WRITE(LUO,1000,ERR=9004)          TRIM(LIGNE)
      ENDDO
!
! COMMAND PROCESS
!
      IF( CMD .EQ. 'extr' ) GOTO 200
      IF( CMD .EQ. 'repl' ) GOTO 600
      IF( CMD .EQ. 'del'  ) GOTO 300
      IF( CMD .EQ. 'sort' ) GOTO 400
!
! COPY INPUT FILE END
!
20    IF( EOF ) GOTO 21                                                                            ! END OF INPUT FILE ALREADY REACHED
      READ (LUI,1000,ERR=9001,END=21) LIGNE
      WRITE(LUO,1000,ERR=9004)        TRIM(LIGNE)
      GOTO 20
!
21    ENDFILE(LUO)                                                                                 ! CLOSE OUTPUT SCRATCH
      I   = LUI                                                                                    ! INVERT INPUT AND OUTPUT SCRATCH
      LUI = LUO
      LUO = I
      REWIND(LUI)
      REWIND(LUO)
      GOTO 7                                                                                       ! NEXT COMMAND
!
! EXTRACTION
!
200   J = 0
      DO I=1,NBLGN
        READ(LUI,1000,ERR=9001,END=202) LIGNE
        IF( VALIDE() ) THEN                                                                        ! ALL FIELDS ARE VALID
          J = J+1
          WRITE(LUO,1000,ERR=9004)      TRIM(LIGNE)                                                ! COPY LINE
        ENDIF
      ENDDO
      EOF = .FALSE.                                                                                ! EOF NOT REACHED
      GOTO 203
!
202   EOF = .TRUE.                                                                                 ! EOF     REACHED
!
203   PRINT 1011, J                                                                                ! NB OF EXTRACTED LINES
      GOTO  20
!
! SUPPRESSION
!
300   J = 0
      DO I=1,NBLGN
        READ(LUI,1000,ERR=9001,END=302) LIGNE
        IF( VALIDE() ) THEN                                                                        ! ALL FIELDS ARE VALID, DO NOT COPY LINE
          J = J+1
        ELSE
          WRITE(LUO,1000,ERR=9004)      TRIM(LIGNE)
        ENDIF
      ENDDO
      EOF = .FALSE.                                                                                ! EOF NOT REACHED
      GOTO 303
!
302   EOF = .TRUE.                                                                                 ! EOF     REACHED
!
303   PRINT 1012, J                                                                                ! NB OF SUPPRESSED LINES
      GOTO  20
!
! SORT
!
400   DO I=1,NBLGN                                                                                 ! READ LINES IN MEMORY
        READ(LUI,1000,ERR=9001,END=402) CHAINE(I)
      ENDDO
      EOF = .FALSE.                                                                                ! EOF NOT REACHED
      GOTO 405
!
402   NBLGN = I-1                                                                                  ! EFFECTIVE NB OF LINES
      EOF   = .TRUE.                                                                               ! EOF     REACHED
!
405   DO I=1,NBLGN                                                                                 ! FOR EACH LINE
        DO J=1,NBCHPS                                                                              ! FOR EACH FIELD
          IF( TYPE(J) .EQ. 'n' ) THEN                                                              ! NUMERICAL, READ DATA WITH FORMAT
            IF( VAR(J) .EQ. 'i' ) THEN
              READ(CHAINE(I)(PCOL(J):DCOL(J)),TRIM(FOR(J)),ERR=9005) IV
              DVAR(J,I) = IV
            ENDIF
            IF( VAR(J) .EQ. 'e' ) THEN
              READ(CHAINE(I)(PCOL(J):DCOL(J)),TRIM(FOR(J)),ERR=9005) EV
              DVAR(J,I) = EV
            ENDIF
            IF( VAR(J) .EQ. 'd' )                                               &
              READ(CHAINE(I)(PCOL(J):DCOL(J)),TRIM(FOR(J)),ERR=9005) DVAR(J,I)
          ENDIF
        ENDDO
      ENDDO
      CALL ORDER                                                                                   ! LINE ORDERING
      IF( ORDO .EQ. 'i' ) THEN                                                                     ! INCREASING
        I1 = 1
        I2 = NBLGN
        I3 = 1
      ELSE                                                                                         ! DECREASING
        I1 = NBLGN
        I2 =  1
        I3 = -1
      ENDIF
      DO I=I1,I2,I3                                                                                ! WRITE ORDERED LINES ON OUTPUT
        WRITE(LUO,1000,ERR=9004) TRIM(CHAINE(IND(I)))
      ENDDO
      GOTO 20
!
! STRING REPLACEMENT
!
600   J = 0
      DO I=1,NBLGN
        READ(LUI,1000,ERR=9001,END=602) LIGNE
        IF( VALIDE() ) THEN                                                                        ! ALL FIELDS ARE VALID
          J                          = J+1
          LIGNE(PCOL(NRE):DCOL(NRE)) = CHRE(1:NBCOL(NRE))                                          ! REPLACE THE STRING
        ENDIF
        WRITE(LUO,1000,ERR=9004) TRIM(LIGNE)                                                       ! WRITE LINE ON OUTPUT
      ENDDO
      EOF = .FALSE.                                                                                ! EOF NOT REACHED
      GOTO 603
!
602   EOF = .TRUE.                                                                                 ! EOF     REACHED
!
603   PRINT 1020, J                                                                                ! NB OF MODIFIED LINES
      GOTO  20
!
! END
! COPY LUI TO OUTPUT FILE
!
100   REWIND(LUI)
      OPEN(LUX,FILE=TRIM(NOM),ERR=9007)
!
101   READ (LUI,1000,ERR=9002,END=9000) LIGNE
      WRITE(LUX,1000,ERR=9008)          TRIM(LIGNE)
      GOTO 101
!
9001  PRINT 8008
      GOTO  9999
9002  PRINT 8008
      GOTO  9000
9003  PRINT 8007
      GOTO  9000
9004  PRINT 8007
      GOTO  9999
9005  PRINT 8006
      GOTO  9999
9006  PRINT 8004, PLIGNE,I-1                                                                       ! EOF REACHED BEFORE FIRST LINE
      GOTO  9999
9007  PRINT 8009
      GOTO  9000
9008  PRINT 8010
      GOTO  9000
9999  PRINT 8005
      REWIND(LUI)
      REWIND(LUO)
      GOTO 7
!
9000  CLOSE(LUI,STATUS='DELETE')
      CLOSE(LUO,STATUS='DELETE')
      END PROGRAM TRI
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! ORDERING (GENERIC FORM USING COMP)
!
      SUBROUTINE ORDER
      use mod_com_tri
      IMPLICIT NONE

! functions
      integer          :: COMP

      integer          :: I,I1,I2,IR,IT1,IT2
      integer          :: N
!
      N      = 1
      IND(1) = 1
      IF( NBLGN .LE. 1 ) RETURN
!
1     IT1 = N+1
      IF( COMP(IT1,IND(1)) .LE. 0 ) GOTO 2
      I1 = 1
      I2 = IT1
!
3     I = (I1+I2)/2
      IF( COMP(IT1,IND(I)) .GE. 0 ) GOTO 4
      I2 = I
      GOTO 5
!
4     I1 = I
!
5     IF( I2-I1 .EQ. 1 ) THEN
        GOTO 6
      ELSE
        GOTO 3
      ENDIF
!
2     I2 = 1
!
6     IF( I2 .EQ. IT1 ) GOTO 7
      IR = N
!
8     IT2      = IR+1
      IND(IT2) = IND(IR)
      IF( I2 .EQ. IR ) GOTO 7
      IR = IR-1
      GOTO 8
!
7     IND(I2) = IT1
      IF( NBLGN .EQ. IT1 ) RETURN
      N = N+1
      GOTO 1
!
      END SUBROUTINE ORDER
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! COMPARE TWO 'VARIABLES' for ORDER
! SPECIFIC PROCESSING FOR EACH DATA TYPE
!
! VAR(N1) < VAR(N2)      : COMP = -1
! VAR(N1) = VAR(N2)      : COMP =  0
! VAR(N1) > VAR(N2)      : COMP =  1
!
      FUNCTION COMP(N1,N2)
      use mod_com_tri
      IMPLICIT NONE
      integer          :: COMP
      integer          :: N1,N2

      integer          :: J
!
E1:   DO J=1,NBCHPS
        IF( TYPE(J) .EQ. 's' ) THEN
          IF( LLT(CHAINE(N1)(PCOL(J):DCOL(J)),            &
                  CHAINE(N2)(PCOL(J):DCOL(J)) ) ) GOTO 2
          IF( CHAINE(N1)(PCOL(J):DCOL(J)) .NE.            &
              CHAINE(N2)(PCOL(J):DCOL(J))       ) GOTO 3
        ELSE
          IF    ( DVAR(J,N1) .LT. DVAR(J,N2) ) THEN
            GOTO 2
          ELSEIF( DVAR(J,N1) .EQ. DVAR(J,N2) ) THEN
            CYCLE E1
          ELSE                                                                                     ! .GT.
            GOTO 3
          ENDIF
        ENDIF
      ENDDO E1
      COMP =  0
      RETURN
!
2     COMP = -1
      RETURN
!
3     COMP =  1
!
      RETURN
      END FUNCTION COMP
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! VALIDITY TEST OF A FIELD ENSEMBLE
!
      FUNCTION VALIDE()
      use mod_dppr
      use mod_com_tri
      IMPLICIT NONE
      logical          :: VALIDE

      real(kind=dppr)  :: DV
      real(kind=dppr)  :: EV

      integer          :: IV
      integer          :: J
!
      DO J=1,NBCHPS
        IF( TYPE(J) .EQ. 's' ) THEN
          IF( LIGNE(PCOL(J):DCOL(J)) .NE.           &
              TEST(J)(1:NBCOL(J))         ) GOTO 2
        ELSE
          IF( VAR(J) .EQ. 'i' ) READ(LIGNE(PCOL(J):DCOL(J)),TRIM(FOR(J)),ERR=2) IV
          IF( VAR(J) .EQ. 'e' ) READ(LIGNE(PCOL(J):DCOL(J)),TRIM(FOR(J)),ERR=2) EV
          IF( VAR(J) .EQ. 'd' ) READ(LIGNE(PCOL(J):DCOL(J)),TRIM(FOR(J)),ERR=2) DV
        ENDIF
      ENDDO
      VALIDE = .TRUE.
      RETURN
!
2     VALIDE = .FALSE.
!
      RETURN
      END FUNCTION VALIDE
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! SET FIELD COLUMNS
!
      SUBROUTINE LICOL(J)
      use mod_com_tri
      IMPLICIT NONE
      integer          :: J
!
1000  FORMAT('FIRST, LAST COLUMN        (,, =',I2,',',I3,') : ')
8005  FORMAT(' !!! LINES LIMITED TO 152 CHARACTERS')
!
1     PCOL(J) =   1                                                                                ! DEFAULT FIRST COLUMN
      DCOL(J) = 152                                                                                ! DEFAULT LAST ONE
      PRINT 1000,     PCOL(J),DCOL(J)                                                              ! FIRST, LAST COLUMN
      READ(*,*,ERR=1) PCOL(J),DCOL(J)
      IF( PCOL(J) .LE. 0   .OR.           &
          DCOL(J) .LE. 0        ) GOTO 1                                                           ! OUT OF RANGE
      IF( PCOL(J) .GT. 152 .OR.         &
          DCOL(J) .GT. 152      ) THEN
        PRINT 8005
        GOTO 1
      ENDIF
      IF( DCOL(J) .LT. PCOL(J) ) GOTO 1
      NBCOL(J) = DCOL(J)-PCOL(J)+1                                                                 ! NB OF COLUMNS OF THE FIELD
!
      RETURN
      END SUBROUTINE LICOL
