
      module mod_main_eqint

      use mod_dppr
      use mod_par_tds
      use mod_com_eqint
      use mod_com_pa
      use mod_com_stats


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_eqint
      call alloc_pa
      call alloc_stats
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXENI
      IMPLICIT NONE

      integer  :: M_ENI

      M_ENI = MXRES(MXENI)
      call RESIZE_MXENI_EQINT(M_ENI)
      MXENI = M_ENI
!
      return
      end subroutine RESIZE_MXENI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPH
      IMPLICIT NONE

      integer  :: M_OPH

      M_OPH = MXRES(MXOPH)
      call RESIZE_MXOPH_EQINT(M_OPH)
      MXOPH = M_OPH
!
      return
      end subroutine RESIZE_MXOPH

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPT
      IMPLICIT NONE

      integer  :: M_OPT

      M_OPT = MXRES(MXOPT)
      call RESIZE_MXOPT_EQINT(M_OPT)
      call RESIZE_MXOPT_PA(M_OPT)
      MXOPT = M_OPT
!
      return
      end subroutine RESIZE_MXOPT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXSNV
      IMPLICIT NONE

      integer  :: M_SNV

      M_SNV = MXRES(MXSNV)
      call RESIZE_MXSNV_EQINT(M_SNV)
      call RESIZE_MXSNV_STATS(M_SNV)
      MXSNV = M_SNV
!
      return
      end subroutine RESIZE_MXSNV

      end module mod_main_eqint
