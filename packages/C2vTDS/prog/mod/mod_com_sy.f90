
      module mod_com_sy
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE

      real(kind=dppr)      ,dimension(MXSYR)   ,parameter  :: DC    = (/  1.D0,  1.D0,  2.D0,  3.D0,  3.D0 /)
      real(kind=dppr)      ,dimension(MXSYM)   ,parameter  :: DC2   = (/  1.D0,  1.D0,  1.D0,  1.D0        /)
      real(kind=dppr)      ,dimension(MXSYR)   ,parameter  :: PC    = (/  1.D0, -1.D0,  1.D0, -1.D0,  1.D0 /)
      real(kind=dppr)      ,dimension(MXSYM)   ,parameter  :: PC2V  = (/  1.D0,  1.D0,  1.D0,  1.D0        /)

      integer              ,dimension(MXSYR,3) ,parameter  :: IC2VV = reshape( source = (/ 1, 2, 1, 2, 1,       &
                                                                                           0, 0, 2, 3, 3,       &
                                                                                           0, 0, 0, 4, 4  /),   &
                                                                               shape  = shape(IC2VV)         )

      character(len =   1)                     ,parameter  :: PARGEN = ' '
      character(len =   2) ,dimension(MXSYR)   ,parameter  :: KC     = (/ 'A1', 'A2', 'E ', 'F1', 'F2' /)
      character(len =   2) ,dimension(MXSYM)   ,parameter  :: SYM    = (/ 'a1', 'a2', 'b1', 'b2'       /)

      end module mod_com_sy
