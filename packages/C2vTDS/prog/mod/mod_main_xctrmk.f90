
      module mod_main_xctrmk

      use mod_dppr
      use mod_par_tds
      use mod_com_xctrmk
      use mod_com_xpara
      use mod_com_xtran


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_xctrmk
      call alloc_xpara
      call alloc_xtran
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_LWA
      IMPLICIT NONE

      integer  :: M_LWA

      M_LWA = 5*MXOP+MXOBZ
      LWA = M_LWA
!
      return
      end subroutine RESIZE_LWA

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXATR
      IMPLICIT NONE

      integer  :: M_ATR

      M_ATR = MXRES(MXATR)
      call RESIZE_MXATR_XCTRMK(M_ATR)
      call RESIZE_MXATR_XPARA(M_ATR)
      call RESIZE_MXATR_XTRAN(M_ATR)
      MXATR = M_ATR
! dependencies
      CALL RESIZE_MXTRAD
!
      return
      end subroutine RESIZE_MXATR

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXATRT
      IMPLICIT NONE

      integer  :: M_ATRT

      M_ATRT = MXRES(MXATRT)
      call RESIZE_MXATRT_XPARA(M_ATRT)
      call RESIZE_MXATRT_XTRAN(M_ATRT)
      MXATRT = M_ATRT
! dependencies
      CALL RESIZE_MXOP
!
      return
      end subroutine RESIZE_MXATRT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOBZ
      IMPLICIT NONE

      integer  :: M_OBZ

      M_OBZ = 2*MXOBS+MXOP
      MXOBZ = M_OBZ
! dependencies
      CALL RESIZE_LWA
!
      return
      end subroutine RESIZE_MXOBZ

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOP
      IMPLICIT NONE

      integer  :: M_OP

      M_OP = MXOPH+MXOPT*MXATRT
      CALL RESIZE_MXOP_XPARA(M_OP)
      MXOP = M_OP
! dependencies
      CALL RESIZE_MXOBZ
!
      return
      end subroutine RESIZE_MXOP

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPH
      IMPLICIT NONE

      integer  :: M_OPH

      M_OPH = MXRES(MXOPH)
      call RESIZE_MXOPH_XPARA(M_OPH)
      MXOPH = M_OPH
! dependencies
      CALL RESIZE_MXOP
!
      return
      end subroutine RESIZE_MXOPH

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPT
      IMPLICIT NONE

      integer  :: M_OPT

      M_OPT = MXRES(MXOPT)
      call RESIZE_MXOPT_XPARA(M_OPT)
      MXOPT = M_OPT
! dependencies
      CALL RESIZE_MXOP
!
      return
      end subroutine RESIZE_MXOPT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXTRAD
      IMPLICIT NONE

      integer  :: M_TRAD

      M_TRAD = 2*MXATR
      CALL RESIZE_MXTRAD_XTRAN(M_TRAD)
      MXTRAD = M_TRAD
!
      return
      end subroutine RESIZE_MXTRAD

      end module mod_main_xctrmk
