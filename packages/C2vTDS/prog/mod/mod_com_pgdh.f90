
      module mod_com_pgdh

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,pointer ,dimension(:,:) ,save  :: H,T                                       ! (MXDIMS,MXDIMS)
      real(kind=dppr) ,pointer ,dimension(:)   ,save  :: EL                                        ! (MXELMD)

      integer         ,pointer ,dimension(:)   ,save  :: KO,LI                                     ! (MXELMD)
      integer         ,pointer ,dimension(:)   ,save  :: NUMI                                      ! (MXOPH+1)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_PGDH
      IMPLICIT NONE

      integer  :: ierr

      allocate(H(MXDIMS,MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PGDH_H')
      H = 0.d0
      allocate(T(MXDIMS,MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PGDH_T')
      T = 0.d0
      allocate(EL(MXELMD),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PGDH_EL')
      EL = 0.d0

      allocate(KO(MXELMD),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PGDH_KO')
      KO = 0
      allocate(LI(MXELMD),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PGDH_LI')
      LI = 0
      allocate(NUMI(MXOPH+1),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PGDH_NUMI')
      NUMI = 0
!
      return
      end subroutine ALLOC_PGDH

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXDIMS

      subroutine RESIZE_MXDIMS_PGDH(C_DIMS)
      IMPLICIT NONE
      integer :: C_DIMS

      integer :: i,ierr

! H
      allocate(rpd2(C_DIMS,C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_PGDH')
      rpd2 = 0.d0
      do i=1,MXDIMS
        rpd2(1:MXDIMS,i) = H(:,i)
      enddo
      deallocate(H)
      H => rpd2
! T
      allocate(rpd2(C_DIMS,C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_PGDH')
      rpd2 = 0.d0
      do i=1,MXDIMS
        rpd2(1:MXDIMS,i) = T(:,i)
      enddo
      deallocate(T)
      T => rpd2
!
      return
      end subroutine RESIZE_MXDIMS_PGDH

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXELMD

      subroutine RESIZE_MXELMD_PGDH(C_ELMD)
      IMPLICIT NONE
      integer :: C_ELMD

      integer :: ierr

! EL
      allocate(rpd1(C_ELMD),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXELMD_PDGH_EL')
      rpd1 = 0.d0
      rpd1(1:MXELMD) = EL(:)
      deallocate(EL)
      EL => rpd1
! KO
      allocate(ipd1(C_ELMD),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXELMD_PDGH_KO')
      ipd1 = 0
      ipd1(1:MXELMD) = KO(:)
      deallocate(KO)
      KO => ipd1
! LI
      allocate(ipd1(C_ELMD),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXELMD_PDGH_LI')
      ipd1 = 0
      ipd1(1:MXELMD) = LI(:)
      deallocate(LI)
      LI => ipd1
!
      return
      end subroutine RESIZE_MXELMD_PGDH

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPH

      subroutine RESIZE_MXOPH_PGDH(C_OPH)
      IMPLICIT NONE
      integer :: C_OPH

      integer :: ierr

! NUMI
      allocate(ipd1(C_OPH+1),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PDGH_NUMI')
      ipd1 = 0
      ipd1(1:MXOPH+1) = NUMI(:)
      deallocate(NUMI)
      NUMI => ipd1
!
      return
      end subroutine RESIZE_MXOPH_PGDH

      end module mod_com_pgdh
