
      module mod_main_predlx

      use mod_dppr
      use mod_par_tds
      use mod_com_predlx


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_predlx
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOBS
      IMPLICIT NONE

      integer  :: M_OBS

      M_OBS = MXRES(MXOBS)
      call RESIZE_MXOBS_PREDLX(M_OBS)
      MXOBS = M_OBS
!
      return
      end subroutine RESIZE_MXOBS

      end module mod_main_predlx
