      PROGRAM DIPMOD
!
!  14.12.88 FORTRAN 77 DU SUN4 REV 13 JAN 1989
!  REV 25 JAN 1990
!  MOD. DEC. 92 T.GABARD
! REV    JAN 1995 JPC,CW (PARAMETER)
! MODIF 05/09 V. BOUDON, A. EL HILALI ---> XY3Z/C3vs.
!
!   CODAGE DES OPERATEURS ROVIBRATIONNELS DU MOMENT DIPOLAIRE EFFECTIF
!
!     CALCUL DES K  JMKCUB = FIXE DANS LE PROGRAMME
!
! APPEL : dipmod Pn  Nm  v1  v2  v3  v4  v5  v6   ...   w1  w2  w3  w4  w5  w6            |
!                    Nm' v1' v2' v3' v4' v5' v6'  ...   w1' w2' w3' w4' w5' w6'           | (n+1 lines)
!                    ...........................
!                Pn' Nm" v1" v2" v3" v4" v5" v6"  ...   w"1 w"2 w"3 w"4 w"5 w"6           |
!                    ...........................                                          | (n'+1 lines)
!                Dp
!
!  *****  LIMITATIONS DU PROGRAMME GEREES PAR PARAMETER.
!
!     MXOPT                                                             !ICODVI:ICODRO:ICODVC:ICODVA:ICODSY
! NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS
!     MXOCV                                                             !IRDRE:IDVI:IDRO:IDVC:IDVA:IDSY
! NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS PAR CASE VIBRATIONNELLE
!     MXEMR                                                             !EMOP:LI:IC
! NB MAXIMUM D'E.M.R.V. NON NULS
!     MXSNV                                                             !IFFI:IFFS
! NB MAXIMUM TOTAL DE SOUS-NIVEAUX VIBRATIONNELS SUP ET INF
!     MXSNB                                                             !IKVC:IKVA
! NB MAXIMUM DE SOUS-NIVEAUX VIBRATIONNELS POUR UN ENSEMBLE DONNE DES VI
!     MXOPR                                                             !IROCO
! NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS PAR OPERATEUR VIBRATIONNEL
!     MXPOL                                                             ! NVIBI:NVIBS:NIVII:NIVIS
! NB MAXIMUM DE POLYADES
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_com_phase
      use mod_main_dipmod
      IMPLICIT NONE

! functions
      real(kind=dppr)  :: ELMR
      character(len=   3)  :: KC
      character(len=   5)  :: KCE

      real(kind=dppr) ,parameter  :: SYML = 1.D0
      real(kind=dppr) ,parameter  :: SYMS = 0.5D0

      real(kind=dppr) ,dimension(MDMIGA)  :: AGAMA,AKE,AKEV,AKKT,AKLS,AKR,AKT,AKX
      real(kind=dppr)  :: AKJ,AKL,AKM,AKS,AKV,AKZ
      real(kind=dppr)  :: EM,EM1,EM2,EMEL,EMET,EMP,EMRT,EN
      real(kind=dppr)  :: SP

      integer ,dimension(NBVQN+1)  :: IBELMR,ICELMR,IAELMR,IKELMR
      integer ,dimension(NBVQN)    :: ILA,IVA
      integer ,dimension(NBVQN)    :: ILC,IVC
      integer ,dimension(NBVQN)    :: IL,IV
      integer ,dimension(0:MXPOL)  :: IORTR
      integer ,dimension(MXSYR)    :: IELL,IELS,IROT
      integer ,dimension(MXPOL)    :: NIVII,NIVIS
      integer          :: NFS_SUP,NFFS_SUP,IOP_SUP,NROT_SUP,NBOPT_SUP
      integer          :: IEEI_SUP,IEES_SUP,IELLA_SUP,IELSA_SUP,IROTA_SUP,NIV_SUP
      integer          :: I,IAE,IAEV,IAV,IB,IBE,IBEV,IBV
      integer          :: ID,IDEGV,IDK
      integer          :: ICK,ICRT,IDL,IDLL,IDLS,IDS,IR
      integer          :: IDRL,IDROT,IEEI,IEES,IELLA,IELSA
      integer          :: IF,IFK,IFS,IF1,IF2,IGA,IH,IJK,IK,IKL,ILL,ILS,IM,IMIA,IOA
      integer          :: IK45,IK456,IKA45,IKA456,IKC45,IKC456
      integer          :: IOA1,IOC,IOC1,IOP,IOPA,IOPC,IPH,IPP
      integer          :: IRLL,IRO,IROTA
      integer          :: IBL,IBLL,IFI,IKS,ILO,IP,IS,ISO,IU,IUU,IVI,IVS,IZ
      integer          :: J,J1,J2,J3,JDLS,JJJ,JKL,JLL,JLS,JMINF,JMKCUB,JMSUP
      integer          :: KDP,KICTR,KREV,KTT
      integer          :: KNVIB,KNIVP,KOP,KPOL,KPOLI,KPOLS,KVI
      integer          :: L,LN,LOP,LR,LT,LX
      integer          :: M,MEV
      integer          :: NBOPT,NBVA,NBVC,NE,NEV,NEVI,NEVS,NFS,NFFI,NFFS,NFI,NGAMA,NK,NKK
      integer          :: NPOLI,NPOLS,NROT
      integer          :: NL,NM,NN,NV

      character(len =   1) ,dimension(0:MXPOL)  :: AORTR
      character(len =   1)  :: AVI,AVS
      character(len =  11)  :: CHAINE
      character(len = 120)  :: IFICH
!
1000  FORMAT('  EFFECTIVE ROVIBRATIONAL DIPOLE MOMENT :' , /,   &
             ' VIBRATIONAL REDUCED MATRIX ELEMENTS (RME)',//,   &
             A                                               )
1001  FORMAT(//)
1002  FORMAT(I6,'  |[[[',                        &
             I2  ,'(',I1  ,',',A,')*',           &
             I2  ,'(',I1  ,',',A,')*',           &
             I2  ,'(',I1  ,',',A,')*',           &
             I2  ,'(',I1  ,',',A,')*',           &
             I2  ,'(',I1  ,',',A,')]',A,'*',     &
             I2  ,'(',I1  ,',',A,')]',A,'*[',    &
             I2  ,'(',I2  ,',',A,')*',           &
             F3.1,'(',F3.1,',',A,')]',A,']',A,   &
             ' >'                             )
1004  FORMAT(I4,' TH VIBRONIC OPERATOR'        ,//,   &
             A,1X,' {[[',                             &
             I2,'(',I1,',',A,')*',                    &
             I2,'(',I1,',',A,')*',                    &
             I2,'(',I1,',',A,')*',                    &
             I2,'(',I1,',',A,')*',                    &
             I2,'(',I1,',',A,')]',A,'*'               &
             I2,'(',I1,',',A,')]',A,                  &
             '} '                              , /,   &
             2X,' {[[',                               &
             I2,'(',I1,',',A,')*',                    &
             I2,'(',I1,',',A,')*',                    &
             I2,'(',I1,',',A,')*',                    &
             I2,'(',I1,',',A,')*',                    &
             I2,'(',I1,',',A,')]',A,'*',              &
             I2,'(',I1,',',A,')]',A,'}',A,'*[[',      &
             I2,'(',I1,',',A,')','*',                 &
             I2,'(',I1,',',A,')',']',A,']',A       )
1006  FORMAT(I1)
1007  FORMAT(I2)
1010  FORMAT(A)
1029  FORMAT(/,                     &
             I5,' NON ZERO RME',/)
1030  FORMAT(2X,A1,'<',I5,'|',3X,'|',I5,'>',3X,F19.13)
1100  FORMAT(/,                                                  &
             I4,' VIBRATIONAL SUBLEVEL(S) OF UPPER POLYAD :',/)
1199  FORMAT(/,              &
             'DIPMOD : ',A)
1205  FORMAT(' UPPER LEVEL ',I2,' : (v1;v2;v3;v4;v5;v6) = (',5(I2,';'),I2,')')
1206  FORMAT(/,                                     &
             ' POLYAD ',I2,'  MINUS POLYAD ',I2,/)
1207  FORMAT(' LOWER LEVEL ',I2,' : (v1;v2;v3;v4;v5;v6) = (',5(I2,';'),I2,')')
1215  FORMAT(/,                                             &
             ' DIPOLE MOMENT DEVELOPMENT ORDER :  ',9(I2))                                         ! 9 = MXPOL-1
1218  FORMAT(/,                               &
             I4,' ROTATIONAL OPERATOR(S)',/)
1219  FORMAT(/,           &
             1X,46('-'))
1220  FORMAT(I4,' ROVIBRATIONAL OPERATORS',/)
1313  FORMAT(I4,I3,'(',I1,',',A,')',                              &
                I3,'(',I1,',',A,')',                              &
                I3,'(',I1,',',A,')',1X,A,                         &
             1X,6I1,1X,A,1X,6I1,1X,A,1X,A,1X,A,1X,A,I8,I8,I8,I8)                                   ! 6 = NBVQN
2001  FORMAT(' >> ',A)
8000  FORMAT(' !!! DIPMOD : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8003  FORMAT(' !!! INCOMPATIBLE ARGUMENTS')
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      NFS_SUP   = -1
      NFFS_SUP  = -1
      IOP_SUP   = -1
      NROT_SUP  = -1
      NBOPT_SUP = -1
      IROTA_SUP = -1
      NIV_SUP   = -1
      IEES_SUP  = -1
      IEEI_SUP  = -1
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1010,END=9997) FDATE
      PRINT 1199,            FDATE
!
! NUMERO DE LA POLYADE SUPERIEURE
!
      READ(10,1010,END=9997) CHAINE
      AVS = CHAINE(2:2)
      READ(AVS,1006) IVS
      NPOLS = IVS+1
!
! LECTURE DES NIVEAUX VIBRATIONNELS
!
      JMSUP = 0
      DO KPOL=1,NPOLS
        READ(10,1010,END=9997) CHAINE
        READ(CHAINE(2:3),1007) NIVIS(KPOL)
        IF( NIVIS(KPOL) .GT. NIV_SUP ) NIV_SUP = NIVIS(KPOL)
        DO WHILE( NIVIS(KPOL) .GT. MXNIV )
          CALL RESIZE_MXNIV
        ENDDO
        DO KNIVP=1,NIVIS(KPOL)
          DO KNVIB=1,NBVQN
            READ(10,1006,END=9997) NVIBS(KPOL,KNIVP,KNVIB)
            IF( KNVIB .GT. 2 ) JMSUP = MAX(JMSUP,NVIBS(KPOL,KNIVP,KNVIB))
          ENDDO
        ENDDO
      ENDDO
!
! NUMERO DE LA POLYADE INFERIEURE
!
      READ(10,1010,END=9997) CHAINE
      AVI = CHAINE(2:2)
      READ(AVI,1006) IVI
      NPOLI = IVI+1
!
! LECTURE DES NIVEAUX VIBRATIONNELS
!
      JMINF = 0
      DO KPOL=1,NPOLI
        READ(10,1010,END=9997) CHAINE
        READ(CHAINE(2:3),1007) NIVII(KPOL)
        IF( NIVII(KPOL) .GT. NIV_SUP ) NIV_SUP = NIVII(KPOL)
        DO WHILE( NIVII(KPOL) .GT. MXNIV )
          CALL RESIZE_MXNIV
        ENDDO
        DO KNIVP=1,NIVII(KPOL)
          DO KNVIB=1,NBVQN
            READ(10,1006,END=9997) NVIBI(KPOL,KNIVP,KNVIB)
            IF( KNVIB .GT. 2 ) JMINF = MAX(JMINF,NVIBI(KPOL,KNIVP,KNVIB))
          ENDDO
        ENDDO
      ENDDO
      JMKCUB = MAX(JMINF,JMSUP)
!
! ORDRE DU DEVELOPPEMENT
!
      READ(10,1010,END=9997) CHAINE
      IF( LEN_TRIM(CHAINE) .NE. IVI+2 ) GOTO 9996
      DO KVI=0,IVI
        I          = KVI+2
        AORTR(KVI) = CHAINE(I:I)
        READ(AORTR(KVI),1006) IORTR(KVI)
      ENDDO
!
!  NOM DU FICHIER DE SORTIE
!
      IFICH = 'MD_P'//AVS//'mP'//AVI//'_D'
      DO KVI=0,IVI
        IFICH = TRIM(IFICH)//AORTR(KVI)
      ENDDO
      CLOSE(10)
      PRINT 2001, TRIM(IFICH)
!
! INITIALISATIONS
!
      CALL FACTO
      OPEN(20,FILE=IFICH,STATUS='UNKNOWN')
      WRITE(20,1000) FDATE
      WRITE(20,1206) IVS,IVI
      DO I=1,NIVIS(NPOLS)
        WRITE(20,1205) I,(NVIBS(NPOLS,I,J),J=1,NBVQN)
      ENDDO
      DO I=1,NIVII(NPOLI)
        WRITE(20,1207) I,(NVIBI(NPOLI,I,J),J=1,NBVQN)
      ENDDO
!
!     CODAGE DES SOUS-NIVEAUX VIBRONIQUES
!
!     SOUS-NIVEAUX SUPERIEURS.
!
      NFFS = 0
      DO IF1=1,NIVIS(NPOLS)
        DO IF2=1,NBVQN
          IV(IF2) = NVIBS(NPOLS,IF1,IF2)
        ENDDO
        CALL SIGVI(IV,'IFFS',NFFS,NFS)
        NFFS = NFFS+NFS
        IF( NFS  .GT. NFS_SUP  ) NFS_SUP  = NFS
        IF( NFFS .GT. NFFS_SUP ) NFFS_SUP = NFFS
      ENDDO
!***********
      NEVS = 0
      IEES = 0
      DO IFS=1,NFFS
        CALL VLNC(IFFS(7,IFS),IK45,IK456)
        CALL MULCIV(SYML,SYMS,NK,AKE)
        DO LR=1,NK
          AKL = AKE(LR)
          CALL MULCIV(DBLE(IK456-2),AKL,NEV,AKT)
          NEVS = NEVS+NEV
        ENDDO
      ENDDO
      WRITE(20,1100) NEVS
!************
      DO IFS=1,NFFS
        DO 67 I=1,NBVQN
          CALL VLNC(IFFS(I,IFS),IV(I),IL(I))
67      CONTINUE
        CALL VLNC(IFFS(NBVQN+1,IFS),IK45,IK456)
        CAll MULCIV (SYML,SYMS,NK,AKE)
        DO LX=1,NK
          AKL = AKE(LX)
          CALL MULCIV(DBLE(IK456-2),AKL,NEV,AKT)
          DO M=1,NEV
            AKM  = AKT(M)
            IEES = IEES+1
            IF( IEES .GT. IEES_SUP ) IEES_SUP = IEES
            IF( IEES .GT. MXSNEV   ) CALL RESIZE_MXSNEV
            INDS(IEES) = IFS
            WRITE(20,1002) IEES,                                       &
                           (IV(I),IL(I),KC(IL(I)+2),I=1,5),KC(IK45),   &
                            IV(6),IL(6),KC(IL(6)+2)       ,KC(IK456),  &
                           INT(SYML),INT(SYML),KCE(3),                 &
                           SYMS,SYMS,                                  &
                           KCE(MXSYR+1),KCE(INT(AKL+(MXSYR+0.5D0))),   &
                           KCE(INT(AKM+(MXSYR+0.5D0)))
            ICODES(IEES) = INT(AKL+(MXSYR+0.5D0))*10000+INT(AKM+(MXSYR+0.5D0))*10+IK456
          ENDDO
        ENDDO
      ENDDO
!
!     SOUS-NIVEAUX INFERIEURS.
!
      NFFI = 0
      DO IF1=1,NIVII(NPOLI)
        DO IF2=1,NBVQN
          IV(IF2) = NVIBI(NPOLI,IF1,IF2)
        ENDDO
        CALL SIGVI(IV,'IFFI',NFFI,NFI)
        NFFI = NFFI+NFI
      ENDDO
      NEVI = 0
      IEEI = 0
      DO IF=1,NFFI
        CALL VLNC(IFFI(7,IF),IK45,IK456)
        CALL MULCIV(SYML,SYMS,NKK,AKR)
        DO IZ=1,NKK
          AKJ = AKR(IZ)
          CALL MULCIV(DBLE(IK456-2),AKJ,NV,AKZ)
          NEVI = NEVI+NV
        ENDDO
      ENDDO
      WRITE(20,1100) NEVI
      DO IFI=1,NFFI
        DO I=1,NBVQN
          CALL VLNC(IFFI(I,IFI),IV(I),IL(I))
        ENDDO
        CALL VLNC(IFFI(NBVQN+1,IFI),IK45,IK456)
        CAll MULCIV(SYML,SYMS,NM,AKE)
        DO LT=1,NM
          AKL = AKE(LT)
          CALL MULCIV(DBLE(IK456-2),AKL,NEV,AKT)
          DO M=1,NEV
            AKM  = AKT(M)
            IEEI = IEEI+1
            IF( IEEI .GT. IEEI_SUP ) IEEI_SUP = IEEI
            IF( IEEI .GT. MXSNEV   ) CALL RESIZE_MXSNEV
            INDI(IEEI) = IFI
            WRITE(20,1002) IEEI,                                       &
                           (IV(I),IL(I),KC(IL(I)+2),I=1,5),KC(IK45),   &
                            IV(6),IL(6),KC(IL(6)+2)       ,KC(IK456),  &
                           INT(SYML),INT(SYML),KCE(3),                 &
                           SYMS,SYMS,                                  &
                           KCE(MXSYR+1),KCE(INT(AKL+(MXSYR+0.5D0))),   &
                           KCE(INT(AKM+(MXSYR+0.5D0)))
            ICODEI(IEEI) = INT(AKL+(MXSYR+0.5D0))*10000+INT(AKM+(MXSYR+0.5D0))*10+IK456
          ENDDO
        ENDDO
      ENDDO
      WRITE(20,1215) (IORTR(I),I=0,IVI)
      LOP   = 0
      NBOPT = 0
!
!     BOUCLE SUR LES POLYADES INFERIEURES
!
E36:  DO KPOLI=1,NPOLI
!
!     DETERMINATION DES CREATEURS
!
E35:    DO IOC=1,NIVII(KPOLI)
          DO IOC1=1,NBVQN
            IVC(IOC1) = NVIBI(KPOLI,IOC,IOC1)
          ENDDO
          CALL SIGVI(IVC,'IKVC',0,NBVC)
!
!     POLYADES SUPERIEURES
!
          KPOLS = KPOLI+IVS-IVI
!
!     DETERMINATION DES ANNIHILATEURS
!
E33:      DO IOA=1,NIVIS(KPOLS)
            DO IOA1=1,NBVQN
              IVA(IOA1) = NVIBS(KPOLS,IOA,IOA1)
            ENDDO
            IF( IVI .EQ. IVS .AND.              &
                IOA .LT. IOC       ) CYCLE E33
            CALL SIGVI(IVA,'IKVA',0,NBVA)
            IDEGV = 0
            DO IOP=1,NBVQN
              IDEGV = IVA(IOP)+IVC(IOP)+IDEGV
            ENDDO
!
!     COUPLAGE CREATEURS-ANNIHILATEURS
!
E1:         DO IOPC=1,NBVC
              IMIA = 1
              IF( IOA .EQ. IOC ) IMIA = IOPC
              DO I=1,NBVQN
                CALL VLNC(IKVC(I,IOPC),IVC(I),ILC(I))
              ENDDO
              CALL VLNC(IKVC(NBVQN+1,IOPC),IKC45,IKC456)
E2:           DO IOPA=IMIA,NBVA
                DO I=1,NBVQN
                  CALL VLNC(IKVA(I,IOPA),IVA(I),ILA(I))
                ENDDO
                CALL VLNC(IKVA(NBVQN+1,IOPA),IKA45,IKA456)
                CALL MULCIV(DBLE(IKC456-2),DBLE(IKA456-2),NGAMA,AGAMA)
                NROT = 0
E111:           DO I=1,NGAMA
E733:             DO IU=1,2
                    DO IJK=1,MXSYR
                      IROT(IJK) = 0
                      IELL(IJK) = 0
                      IELS(IJK) = 0
                    ENDDO
!
! DETERMINATION DES OPERATEURS ELECTRONIQUE DE SPIN POSSIBLES
! ****E(OMEGAS,K,L)=E(IDES,IDES,ILS)***
!
                    IELSA = 0
                    DO IDLS=0,1
                      DO IDS=0,IDLS
                        ILS = IDS
                        IF( IDLS-(IDLS/2)*2 .EQ. 1 .AND.             &
                            IDS             .EQ. 0       ) ILS = -1
                        IELSA       = IELSA+1
                        IELS(ILS+2) = IELS(ILS+2)+1
                        IF( IELSA .GT. IELSA_SUP ) IELSA_SUP = IELSA
                        IF( IELSA .GT. MXOPR     ) CALL RESIZE_MXOPR
                        IELCOS(IELSA) = 100*IDLS+10*IDLS+ILS+2
                      ENDDO
                    ENDDO
!
! DETERMINATION DES OPERATEURS ELECTRONIQUE DE SPIN POSSIBLES
! ****E(OMEGAS,K,L)=E(IDES,IDES,ILS)***
!
                    IELLA = 0
                    DO IKL=0,2
                      DO IDL=0,IKL
                        ILL = IDL
                        IF( IKL-(IKL/2)*2 .EQ. 1 .AND.             &
                            IDL           .EQ. 0       ) ILL = -1
                        IELLA       = IELLA+1
                        IELL(ILL+2) = IELL(ILL+2)+1
                        IF( IELLA .GT. IELLA_SUP ) IELLA_SUP = IELLA
                        IF( IELLA .GT. MXOPR     ) CALL RESIZE_MXOPR
                        IELCOL(IELLA) = 100*IKL+10*IKL+ILL+2
                      ENDDO
                    ENDDO
E79:                DO ILO=1,IELLA
                      JKL = IELCOL(ILO)/100
                      JLL = IELCOL(ILO)-(IELCOL(ILO)/10)*10
E78:                  DO ISO=1,IELSA
                        JDLS = IELCOS(ISO)/100
                        JLS  = IELCOS(ISO)-(IELCOS(ISO)/10)*10
                        CALL MULCIV(DBLE(JLL-2),DBLE(JLS-2),NE,AKLS)
E123:                   DO IR=1,NE
                          AKS = AKLS(IR)
                          CALL MULCIV(AGAMA(I),AKS,MEV,AKEV)
E1234:                    DO IM=1,MEV
                            AKV   = AKEV(IM)
                            IUU   = (-1)**(JDLS+JKL+IU-1)
                            IP    = ABS((IUU-1)/2)
                            IDROT = IORTR(KPOLI-1)+1-IDEGV-JDLS-JKL
!C
!CCC****  DETERMINATION DES OPERATEURS ROTATIONNELS POSSIBLES
!C
                            IROTA = 0
                            IF( IDROT .LT. IU-1 ) CYCLE E78
E334:                       DO ID=IP,IDROT,2
                              IF( JDLS  .EQ. 0 .AND.             &
                                  JKL   .EQ. 0 .AND.             &
                                  IDEGV .EQ. 0 .AND.             &
                                  ID    .EQ. 0       ) CYCLE E334
                              DO IDK=ID,IP,-2
                                DO IDRL=0,IDK
                                  IDLL = IDRL
                                  IF( IDK-(IDK/2)*2 .NE. 0 .AND.              &
                                      IDRL          .EQ. 0       ) IDLL = -1
                                  IROTA        = IROTA+1
                                  IROT(IDLL+2) = IROT(IDLL+2)+1
                                  IF( IROTA .GT. IROTA_SUP ) IROTA_SUP = IROTA
                                  IF( IROTA .GT. MXOPR     ) CALL RESIZE_MXOPR
                                  IROCO(IROTA) = 10000*ID+1000*IDK+100*(IDLL+2)+10*(INT(AKV)+2)+INT(AKS)+2
                                ENDDO
                              ENDDO
                            ENDDO E334
!
! CALCUL DU FACTEUR DE NORMALISATION
!      IU = 1, EPSILON = +
!      IU = 2, EPSILON = -
!
                            IF( IROTA .EQ. 0 ) CYCLE E78
                            SP        = (-1.0D0)**(IUU-1+IKA456+IKC456+INT(AGAMA(I)))
                            IBELMR(:) = IKVC(:,IOPC)
                            ICELMR(:) = IKVC(:,IOPC)
                            IAELMR(:) = IKVA(:,IOPA)
                            IKELMR(:) = IKVA(:,IOPA)
                            EN        = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,INT(AGAMA(I)))
                            IBELMR(:) = IKVC(:,IOPC)
                            ICELMR(:) = IKVA(:,IOPA)
                            IKELMR(:) = IKVC(:,IOPC)
                            IKELMR(:) = IKVA(:,IOPA)
                            EN        = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,INT(AGAMA(I)))*SP+EN
                            EMET      = 0.D0
                            DO IB=1,IEEI
                              DO IK=1,IEES
                                IAE  = ICODEI(IB)/10000
                                IBE  = ICODES(IK)/10000
                                IAEV = ICODEI(IB)/10-(ICODEI(IB)/10000)*1000
                                IBEV = ICODES(IK)/10-(ICODES(IK)/10000)*1000
                                IAV  = ICODEI(IB)-(ICODEI(IB)/10)*10
                                IBV  = ICODES(IK)-(ICODES(IK)/10)*10
!CC
!CC***************   FIN EMR VIBRATIONNELS  ************
!C
                                CALL EMRVIB(DBLE(IAE)-(MXSYR+0.5D0),IAV-2,DBLE(IAEV)-(MXSYR+0.5D0),  &
                                            JDLS,JLS-2,JKL,JLL-2,INT(AKS),INT(AGAMA(I)),INT(AKV),    &
                                            DBLE(IBE)-(MXSYR+0.5D0),IBV-2,DBLE(IBEV)-(MXSYR+0.5D0),  &
                                            EMEL,IFK)
                                EMET = EMET+ABS(EMEL)
                              ENDDO
                            ENDDO
                            IF( ABS(EN*EMET) .LT. 1.D-05 ) CYCLE E78
                            LOP = LOP+1
                            WRITE(20,1219)
                            WRITE(20,1004) LOP,AUG(IU),                                    &
                                           (IVC(J),ILC(J),KC(ILC(J)+2),J=1,5),KC(IKC45),   &
                                            IVC(6),ILC(6),KC(ILC(6)+2)       ,KC(IKC456),  &
                                           (IVA(J),ILA(J),KC(ILA(J)+2),J=1,5),KC(IKA45),   &
                                            IVA(6),ILA(6),KC(ILA(6)+2)       ,KC(IKA456),  &
                                           KC(INT(AGAMA(I))+2),JDLS,JDLS,KC(JLS),JKL,JKL,  &
                                           KC(JLL),KC(INT(AKS)+2),KC(INT(AKV)+2)
!
! CALCUL ET CODAGE DES E.M.R.V.
!
                            IOP = 0
E4:                         DO IB=1,IEEI
E5:                           DO IK=1,IEES
                                IAE  = ICODEI(IB)/10000
                                IBE  = ICODES(IK)/10000
                                IAEV = ICODEI(IB)/10-(ICODEI(IB)/10000)*1000
                                IBEV = ICODES(IK)/10-(ICODES(IK)/10000)*1000
                                IAV  = ICODEI(IB)-(ICODEI(IB)/10)*10
                                IBV  = ICODES(IK)-(ICODES(IK)/10)*10
!
!                   ***  EMR VIBRATIONNELS ***
!
                                IBELMR(:) = IFFI(:,INDI(IB))
                                ICELMR(:) = IKVC(:,IOPC)
                                IAELMR(:) = IKVA(:,IOPA)
                                IKELMR(:) = IFFS(:,INDS(IK))
                                EM1       = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,INT(AGAMA(I)))
                                IBELMR(:) = IFFI(:,INDI(IB))
                                ICELMR(:) = IKVA(:,IOPA)
                                IAELMR(:) = IKVC(:,IOPC)
                                IKELMR(:) = IFFS(:,INDS(IK))
                                EM2       = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,INT(AGAMA(I)))*SP
                                EM        = EM1+EM2
                                IF( EM*EM .LT. 1.D-10 ) CYCLE E5
                                EM = EM/EN
                                IF( IU .EQ. 2 ) EM = -EM
!CC
!CC***************   FIN EMR VIBRATIONNELS  ************
!C
                                CALL EMRVIB(DBLE(IAE)-(MXSYR+0.5D0),IAV-2,DBLE(IAEV)-(MXSYR+0.5D0),  &
                                            JDLS,JLS-2,JKL,JLL-2,INT(AKS),INT(AGAMA(I)),INT(AKV),    &
                                            DBLE(IBE)-(MXSYR+0.5D0),IBV-2,DBLE(IBEV)-(MXSYR+0.5D0),  &
                                            EMP,IPP)
                                CALL EMRVIB(DBLE(IAE)-(MXSYR+0.5D0),IAV-2,DBLE(IAEV)-(MXSYR+0.5D0),  &
                                            JDLS,JLS-2,JKL,JLL-2,INT(AKS),INT(AGAMA(I)),INT(AKV),    &
                                            DBLE(IBE)-(MXSYR+0.5D0),IBV-2,DBLE(IBEV)-(MXSYR+0.5D0),  &
                                            EMP,IPP)
!CC
!CCCCC************   EMR VIBRONIQUES       ***********
!CCC

                                EMRT = EM*EMP
                                IH   = ABS(IU-IPP)
                                IPH  = IH-(IH/2)*2
                                IF( IU  .EQ. 2 .AND.                 &
                                    IPP .EQ. 1       ) EMRT = -EMRT
                                IF( ABS(EMRT) .LT. 1.D-10 ) CYCLE E5
                                IOP = IOP+1
                                IF( IOP .GT. IOP_SUP ) IOP_SUP = IOP
                                IF( IOP .GT. MXEMR   ) CALL RESIZE_MXEMR
                                LI(IOP)   = IK
                                IC(IOP)   = IB
                                EMOP(IOP) = EMRT
                              ENDDO E5
                            ENDDO E4
                            WRITE(20,1029) IOP
                            DO KOP=1,IOP
                              WRITE(20,1030) AIM(IPH+1),IC(KOP),LI(KOP),EMOP(KOP)
                            ENDDO
                            WRITE(20,1218) IROTA
!
!    CODAGE DES OPERATEURS ROVIBRATIONNELS
!*******   IRLL EST LA SYMETRIE ROTATIONNELLE
!*******   AKV  EST LA SYMETRIE VIBRONIQUE
!*******   AKKT EST LA SYMETRIE ROVIBRONIQUE
!*******   ICK  EST LA SYMETRIE DE L'OPERATEUR MOMENT DIPOLAIRE
!*******   AKX  EST LA SYMETRIE TOTALE
!
E631:                       DO IRO=1,IROTA
                              ICRT = IROCO(IRO)
                              IRLL = ICRT/100-(ICRT/1000)*100-2
                              CALL MULCIV(DBLE(IRLL),AKV,NN,AKKT)
E632:                         DO LN=1,NN
E6321:                          DO ICK=-1,1,2
                                  CALL MULCIV(AKKT(LN),DBLE(ICK),NL,AKX)
E6322:                            DO IS=1,NL
                                    IF( KICTR(AKX(IS),2) .NE. 1 ) CYCLE E6322
                                    NROT = NROT+1
                                    IF( NROT .GT. NROT_SUP ) NROT_SUP = NROT
                                    IF( NROT .GT. MXOCV    ) CALL RESIZE_MXOCV
                                    IDELS(NROT) = IELCOS(ISO)
                                    IDELL(NROT) = IELCOL(ILO)
                                    IDRO(NROT)  = IROCO(IRO)
                                    IDVC(NROT)  = IVC(1)*100000+IVC(2)*10000+  &
                                                  IVC(3)*1000+IVC(4)*100+      &
                                                  IVC(5)*10+IVC(6)
                                    IDVA(NROT)  = IVA(1)*100000+IVA(2)*10000+  &
                                                  IVA(3)*1000+IVA(4)*100+      &
                                                  IVA(5)*10+IVA(6)
                                    IDSY(NROT)  = IKC456*100+IKA456*10+  &
                                                  INT(AGAMA(I))+2
                                    IDVI(NROT)  = LOP*10000+                 &
                                                  INT(AGAMA(I)+2)*1000+      &
                                                  (INT(AKKT(LN))+2)*100+     &
                                                  (ICK+2)*10+INT(AKX(IS))+2
                                  ENDDO E6322
                                ENDDO E6321
                              ENDDO E632
                            ENDDO E631
                          ENDDO E1234
                        ENDDO E123
                      ENDDO E78
                    ENDDO E79
                  ENDDO E733
                ENDDO E111
!
!    MISE EN ORDRE A L'INTERIEUR D'UNE CASE VIBRATIONNELLE DONNEE
!
                IF( NROT .EQ. 0 ) CYCLE E2
                CALL IRDER(NROT,IDRO,IRDRE,MXOCV)
                DO JJJ=1,NROT
                  NBOPT = NBOPT+1
                  IF( NBOPT .GT. NBOPT_SUP ) NBOPT_SUP = NBOPT
                  IF( NBOPT .GT. MXOPT     ) CALL RESIZE_MXOPT
                  IRO           = IRDRE(JJJ)
                  ICODLS(NBOPT) = IDELS(IRO)
                  ICODLL(NBOPT) = IDELL(IRO)
                  ICODRO(NBOPT) = IDRO(IRO)
                  ICODVC(NBOPT) = IDVC(IRO)
                  ICODVA(NBOPT) = IDVA(IRO)
                  ICODSY(NBOPT) = IDSY(IRO)
                  ICODVI(NBOPT) = IDVI(IRO)
                ENDDO
              ENDDO E2
            ENDDO E1
          ENDDO E33
        ENDDO E35
      ENDDO E36
      CALL DEBUG( 'DIPMOD => MXOPVT=',LOP)
! STOCKAGE DES CODES DES OPERATEURS ROVIBRATIONNELS
!
!
! STOCKAGE DES CODES DES OPERATEURS ROVIBRATIONNELS
!
      WRITE(20,1001)
      WRITE(20,1220) NBOPT
      DO IRO=1,NBOPT
!CCCC*** PARTIE ROTATIONNELLE********
        IM   = ICODRO(IRO)/10000
        IK   = ICODRO(IRO)/1000-(ICODRO(IRO)/10000)*10
        IDL  = ICODRO(IRO)/100-(ICODRO(IRO)/1000)*10
        IKS  = ICODRO(IRO)-(ICODRO(IRO)/10)*10
!CCC***** PARTIE ELECTRONIQUE DE SPIN****
        IS   = ICODLS(IRO)/100
        ILS  = ICODLS(IRO)-(ICODLS(IRO)/10)*10
!CCC***** PARTIE ELECTRONIQUE ORBITALE ****
        IBL  = ICODLL(IRO)/100
        IBLL = ICODLL(IRO)-(ICODLL(IRO)/10)*10
!CCC***** PARTIE VIBRONIQUE ****
        KREV = ICODVI(IRO)/100-(ICODVI(IRO)/1000)*10
        KDP  = ICODVI(IRO)/10-(ICODVI(IRO)/100)*10
        KTT  = ICODVI(IRO)-(ICODVI(IRO)/10)*10
        CALL V1TO6(ICODVC(IRO),IVC(1),IVC(2),IVC(3),IVC(4),IVC(5),IVC(6))
        CALL V1TO6(ICODVA(IRO),IVA(1),IVA(2),IVA(3),IVA(4),IVA(5),IVA(6))
        CALL V1TO6(ICODSY(IRO),J1,J2,J3,IKC456,IKA456,IGA)
        WRITE(20,1313) IRO,                                             &
                       IM,IK,KC(IDL),                                   &
                       IS,IS,KC(ILS),                                   &
                       IBL,IBL,KC(IBLL),KC(IKS),                        &
                       (IVC(L),L=1,NBVQN),KC(IKC456),                   &
                       (IVA(L),L=1,NBVQN),KC(IKA456),                   &
                       KC(KREV),KC(KDP),KC(KTT),                        &
                       ICODRO(IRO),ICODVI(IRO),ICODLS(IRO),ICODLL(IRO)
      ENDDO
      CALL DEBUG( 'DIPMOD => MXSNB=',NFS_SUP)
      CALL DEBUG( 'DIPMOD => MXSNV=',NFFS_SUP)
      CALL DEBUG( 'DIPMOD => MXEMR=',IOP_SUP)
      CALL DEBUG( 'DIPMOD => MXOCV=',NROT_SUP)
      CALL DEBUG( 'DIPMOD => MXOPT=',NBOPT_SUP)
      CALL DEBUG( 'DIPMOD => MXOPR=',IROTA_SUP)
      CALL DEBUG( 'DIPMOD => MXNIV=',NIV_SUP)
      CALL DEBUG( 'DIPMOD => MXSNEV=',IEES_SUP)
      CALL DEBUG( 'DIPMOD => MXSNEV=',IEEI_SUP)
      GOTO 9000
!
9996  PRINT 8003
      GOTO  9999
9997  PRINT 8002
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  CLOSE(20)
      PRINT *
      END PROGRAM DIPMOD
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
! *** COUPLAGE DES OPERATEURS CREATIONS ET ANNHILATIONS
!
! ***   < IB !! (IC * IA)IGAMA !! IK > = (-1)**(ICB+ICK+IC+IA) * SQRT(IGAMA)
!
!                        ( ICC , ICA ,IGAMA)
! ***   SOMME SUR IBK DE (                 ) <IB!!IC!!IBK><IBK!!IA!!IK>
!                        ( ICB , ICK , ICBK)
!
! ***   AVEC IX(I)       = 1000*VI+100*LI+10*NI+CI     POUR I=1,NBVQN
! ***   ET   IX(NBVQN+1) = 1000*C1+100*C2+10*C23+C234
! ***   AVEC C23=C2*C3   ET   C234=C23*C4
! ***   IX PEUT ETRE  IB,IA,IC OU IK
!
      FUNCTION ELMR(IB,IC,IA,IK,IGAMA)
      use mod_dppr
      use mod_par_tds
      use mod_com_sigvi
      IMPLICIT NONE
      real(kind=dppr)  :: ELMR
      integer ,dimension(NBVQN+1)  :: IB,IC,IA,IK
      integer          :: IGAMA

! functions
      real(kind=dppr)  :: E1TO6
      integer          :: IPSIK

      real(kind=dppr)  :: EM1,EM2
      real(kind=dppr)  :: P
      real(kind=dppr)  :: SIX

      integer ,dimension(NBVQN)          :: IPB
      integer ,dimension(NBVQN,4)        :: IBCAK
      integer          :: I,ICA,ICB,ICC,ICK,ICPK,IDGAM,IFF,II,IL,IV
      integer          :: J
      integer          :: NB
!
      ELMR = 0.D0
!
! *** TEST SUR LES FONCTIONS ET LES OPERATEURS.
!
      DO I=1,NBVQN
        IBCAK(I,1) = IB(I)
        IBCAK(I,2) = IC(I)
        IBCAK(I,3) = IA(I)
        IBCAK(I,4) = IK(I)
      ENDDO
!
! TEST DE LA COHERENCE DE L'ENSEMBLE DES NOMBRES QUANTIQUES
!
      DO J=1,4
        DO I=1,NBVQN
          CALL VLNC(IBCAK(I,J),IV,IL)
          II = 1
          IF( I               .GT. 3 ) II = 2
          IF( IPSIK(II,IV,IL) .EQ. 0 ) RETURN
        ENDDO
      ENDDO
!
! *** TEST SUR LES VI ET LES OMEGAI
!
      DO I=1,NBVQN
        IPK(I,1) = IB(I)/10-IC(I)/10
        IPB(I)   = IK(I)/10-IA(I)/10
        IF( IPB(I)   .LT. 0      ) RETURN
        IF( IPK(I,1) .NE. IPB(I) ) RETURN
      ENDDO
      ICB   = IB(NBVQN+1)-10*(IB(NBVQN+1)/10)
      ICK   = IK(NBVQN+1)-10*(IK(NBVQN+1)/10)
      ICC   = IC(NBVQN+1)-10*(IC(NBVQN+1)/10)
      ICA   = IA(NBVQN+1)-10*(IA(NBVQN+1)/10)
      IDGAM = 2
      IF( IGAMA .LT. 1 ) IDGAM = 1
      P = (-1)**(ICC+ICA+ICB+ICK)*SQRT(DBLE(IDGAM))
!
! *** SOMMATION SUR LES ETATS INTERMEDIAIRES.
!
      CALL SIGVI(IPB,'IPK',0,NB)
      DO I=1,NB
        EM1  = E1TO6(1,IB,IC,IPK(1,I))
        EM2  = E1TO6(-1,IPK(1,I),IA,IK)
        ICPK = IPK(NBVQN+1,I)-10*(IPK(NBVQN+1,I)/10)
        CALL SIX6K(DBLE(ICC-2),DBLE(ICA-2),DBLE(IGAMA),DBLE(ICK-2),  &
                   DBLE(ICB-2),DBLE(ICPK-2),SIX,IFF)
        ELMR = ELMR+P*SIX*EM1*EM2
      ENDDO
!
      RETURN
      END FUNCTION ELMR
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! DIPMOD, POLMOD version
!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
! *** CHERCHE POUR UN ENSEMBLE DONNE DE VI : (IV(I),I=1,4)
! *** LES NB INDICES DE LA SIGNATURE VIBRATIONNELLE
! *** IKNC(I,K), I=1,5 ET K=ISN+1,ISN+NB
!
!  REV 29 SEP 94
!
! ATTENTION : SIGVI peut redimensionner les tableaux dependants de MXC
!
      SUBROUTINE SIGVI(IV,MXC,ISN,NB)
      use mod_dppr
      use mod_par_tds
      use mod_main_dipmod
      IMPLICIT NONE
      integer            ,dimension(NBVQN)  :: IV
      character(len = *)                    :: MXC
      integer                               :: ISN,NB

      integer ,parameter  :: MDMIB = 22

      real(kind=dppr) ,dimension(MDMIGA)  :: AK45,AK456

      integer ,pointer ,dimension(:,:)    :: IKNC
      integer          ,dimension(MDMIB)  :: IB4,IB5,IB6
      integer                             :: I4,I45,I456,I5,I6
      integer                             :: IK4,IK5,IK6
      integer                             :: K
      integer                             :: MDMKNC
      integer                             :: N45,N456
      integer                             :: NB4,NB5,NB6
!
8000  FORMAT(' !!! SIGVI  : STOP ON ERROR'   ,/,   &
             '              ',A,' UNEXPECTED'   )
!
      SELECT CASE( MXC )
        CASE( 'IFFI' )
          MDMKNC = MXSNV
          IKNC => IFFI
        CASE( 'IFFS' )
          MDMKNC = MXSNV
          IKNC => IFFS
        CASE( 'IKVA' )
          MDMKNC = MXSNB
          IKNC => IKVA
        CASE( 'IKVC' )
          MDMKNC = MXSNB
          IKNC => IKVC
        CASE( 'IPK' )
          MDMKNC = MXSNB
          IKNC => IPK
        CASE DEFAULT
          PRINT 8000, MXC
          STOP
!
      END SELECT
      K = ISN
      CALL TCUBE(4,IV(4),NB4,IB4,MDMIB)
      CALL TCUBE(5,IV(5),NB5,IB5,MDMIB)
      CALL TCUBE(6,IV(6),NB6,IB6,MDMIB)
      DO I4=1,NB4
        IK4 = IB4(I4)-10*(IB4(I4)/10)
        DO I5=1,NB5
          IK5 = IB5(I5)-10*(IB5(I5)/10)
          CALL MULCIV(DBLE(IK4),DBLE(IK5),N45,AK45)
          DO I45=1,N45
            DO I6=1,NB6
              IK6 = IB6(I6)-10*(IB6(I6)/10)
              CALL MULCIV(AK45(I45),DBLE(IK6),N456,AK456)
E2:           DO I456=1,N456
                K = K+1
                IF( K .GT. MDMKNC ) THEN
                  SELECT CASE( MXC )
                    CASE( 'IFFI' )
                      CALL RESIZE_MXSNV
                      MDMKNC = MXSNV
                      IKNC => IFFI
                    CASE( 'IFFS' )
                      CALL RESIZE_MXSNV
                      MDMKNC = MXSNV
                      IKNC => IFFS
                    CASE( 'IKVA' )
                      CALL RESIZE_MXSNB
                      MDMKNC = MXSNB
                      IKNC => IKVA
                    CASE( 'IKVC' )
                      CALL RESIZE_MXSNB
                      MDMKNC = MXSNB
                      IKNC => IKVC
                    CASE( 'IPK' )
                      CALL RESIZE_MXSNB
                      MDMKNC = MXSNB
                      IKNC => IPK
                  END SELECT
                ENDIF
                IKNC(1,K) = 10*IV(1)
                IKNC(2,K) = 10*IV(2)
                IKNC(3,K) = 10*IV(3)
                IKNC(4,K) = IB4(I4)
                IKNC(5,K) = IB5(I5)
                IKNC(6,K) = IB6(I6)
                IKNC(7,K) = (INT(AK45(I45))+2)*10+INT(AK456(I456))+2
              ENDDO E2
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      NB = K-ISN
!
      RETURN
      END SUBROUTINE SIGVI
