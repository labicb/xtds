!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
      FUNCTION AKIC(IC)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: AKIC
      integer          :: IC

!
      AKIC =  0.D0
      IF    ( IC .EQ. 1 ) THEN
        AKIC =  0.D0
      ELSEIF( IC .EQ. 2 ) THEN
        AKIC = -1.D0
      ELSEIF( IC .EQ. 3 ) THEN
        AKIC =  1.D0
      ELSEIF( IC .EQ. 4 ) THEN
        AKIC =  1.5D0
      ELSEIF( IC .EQ. 5 ) THEN
        AKIC =  1.5D0
      ELSEIF( IC .EQ. 6 ) THEN
        AKIC =  0.5D0
      ENDIF
!
      RETURN
      END FUNCTION AKIC
