!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
      SUBROUTINE ELMRO(AJ,AKR1,IO,KR,IG,AKR2,EMRO,ICR)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: AJ,AKR1,AKR2,EMRO
      integer          :: IO,KR,IG,ICR

! functions
      real(kind=dppr)  :: EMRRO
      integer          :: IDIMK

      real(kind=dppr)  :: ELM
      real(kind=dppr)  :: PH1,PHA
      real(kind=dppr)  :: XK

      integer          :: IFK
!
      CALL KSU2CV(DBLE(KR),AJ,AJ,DBLE(IG),AKR2,AKR1,XK,IFK)
      PH1  = 2.D0*AJ+1.D0
      PHA  = DBLE(IDIMK(AKR1))/PH1
      ELM  = EMRRO(IO,KR,AJ)
      EMRO = PHA*XK*ELM
      IF( IFK .EQ. 1 ) ICR = 1
!
      RETURN
      END SUBROUTINE ELMRO
