!
!     SUBROUTINE DMSPRO
!
!     B. SARTAKOV (ADAPTED TO TDS BY V. BOUDON).
!
!     THIS SUBROUTINE IMPLEMENTS THE METHOD OF JACOBI FOR
!     FINDING THE EIGENVALUES AND EIGENVECTORS OF AN (N*N)
!     REAL SYMMETRIC MATRIX A.
!
!     THE COMPUTATIONS ARE PERFORMED USING DOUBLE PRECISION
!     FLOATING POINT ARITHMETIC.
!
      SUBROUTINE DMSPR(A,B,N,KOD,EPS)
!
!     COMPILER DOUBLE PRECISION
!      DOUBLE PRECISION A,B,RN,RX,SEUIL,P,Q,S,SS,SC,C,CC,EPS
!     DIMENSION A((MXDIMS*MXDIMS+MXDIMS)/2),B(MXDIMS*MXDIMS)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
!     real(kind=dppr) ,dimension((MXDIMS*MXDIMS+MXDIMS)/2)  :: A
!     real(kind=dppr) ,dimension(MXDIMS*MXDIMS)             :: B
      real(kind=dppr) ,dimension(*)  :: A
      real(kind=dppr) ,dimension(*)  :: B
      real(kind=dppr)  :: EPS
      integer          :: N,KOD

      real(kind=dppr)  :: C,CC
      real(kind=dppr)  :: P
      real(kind=dppr)  :: Q,QQ
      real(kind=dppr)  :: RN,RX
      real(kind=dppr)  :: S,SC,SEUIL,SS

      integer          :: I,I1,IJ,IL,IL1,ILB,IM,IM1,IMB,INT
      integer          :: J
      integer          :: L,L1,LL,LM
      integer          :: M,M1,MM
!
!     SUBROUTINE USAGE.
!
!     INPUT PARAMETERS:
!
!     A    :UPPER TRIANGULAR PART OF THE REAL SYMMETRIC
!           MATRIX A.DIMENSION OF THE ARRAY A IS N*(N+1)/2.
!
!     N    :THE ORDER OF THE MATRIX.
!
!     KOD  :KOD=0 ; EIGENVALUES AND EIGENVECTORS WILL BE
!           KOD=1 ; ONLY EIGENVALUES WILL BE COMPUTED.
!
!     EPS  :TOLERANCE ,USED IN TERMINATION CRITERION.
!
!     RESULTS:
!
!     A    :WHEN THE MATRIX A HAS BEEN REDUCED TO DIAGONAL
!           FORM,THE EIGENVALUES WILL BE FOUND IN THE DIA-
!           GONAL POSITIONS.
!
!     B    :ARRAY CONTAINING THE EIGENVECTORS OF THE MATRIX A.
!           DIMENSION OF THE ARRAY B IS N*N.
!
      IF( KOD .EQ. 1 ) GOTO 2
      I1 = -N
      DO J=1,N
        I1 = I1+N
E1:     DO I=1,N
          IJ    = I1+I
          B(IJ) = 0.D0
          IF( I .NE. J ) CYCLE E1
          B(IJ) = 1.D0
        ENDDO E1
      ENDDO
!
2     RN = 0.D0
      DO I=1,N
E3:     DO J=1,N
          IF( I .EQ. J ) CYCLE E3
          IJ = I+(J*J-J)/2
          RN = RN+A(IJ)*A(IJ)
        ENDDO E3
      ENDDO
      IF( RN .LE. EPS ) RETURN
      RN    = SQRT(2.D0*RN)
      RX    = RN*EPS/DBLE(N)
      QQ    = 0.D0
      INT   = 0
      SEUIL = RN
!
4     SEUIL = SEUIL/DBLE(N)
!
5     L = 1
!
6     M = L+1
!
7     M1 = (M*M-M)/2
      L1 = (L*L-L)/2
      LM = L+M1
      IF( ABS(A(LM)) .LT. SEUIL ) GOTO 15
      INT = 1
      LL  = L1+L
      MM  = M1+M
      P   = 0.5D0*(A(LL)-A(MM))
      Q   = -A(LM)/SQRT(A(LM)*A(LM)+P*P)
      IF( P .GE. EPS ) GOTO 8
      Q  = -Q
      QQ = Q*Q
      IF( QQ .LT. 0.D0 ) QQ = 0.D0
      IF( QQ .GT. 1.D0 ) QQ = 1.D0
!
8     S   = Q/SQRT(2.D0*(1.D0+(SQRT(1.D0-QQ))))
      SS  = S*S
      C   = SQRT(1.D0-SS)
      CC  = C*C
      SC  = S*C
      IL1 = N*L-N
      IM1 = N*M-N
E14:  DO I=1,N
        I1 = (I*I-I)/2
        IF( I .EQ. L ) GOTO 13
        IF    ( I .EQ. M ) THEN
          GOTO 13
        ELSEIF( I .GT. M ) THEN
          GOTO 10
        ENDIF
        IM = M1+I
        GOTO 11
!
10      IM = I1+M
!
11      IF( I .GE. L ) GOTO 111
        IL = L1+I
        GOTO 12
!
111     IL = I1+L
!
12      P     = A(IL)*C-A(IM)*S
        A(IM) = A(IL)*S+A(IM)*C
        A(IL) = P
!
13      IF( KOD .EQ. 1 ) CYCLE E14
        ILB    = IL1+I
        IMB    = IM1+I
        P      = B(ILB)*C-B(IMB)*S
        B(IMB) = B(ILB)*S+B(IMB)*C
        B(ILB) = P
      ENDDO E14
      P     = 2.D0*A(LM)*SC
      Q     = A(LL)*CC+A(MM)*SS-P
      P     = A(LL)*SS+A(MM)*CC+P
      A(LM) = (A(LL)-A(MM))*SC+A(LM)*(CC-SS)
      A(LL) = Q
      A(MM) = P
!
15    IF( M .EQ. N ) GOTO 16
      M = M+1
      GOTO 7
!
16    IF( L .EQ. N-1 ) GOTO 17
      L = L+1
      GOTO 6
!
17    IF( INT .NE. 1 ) GOTO 18
      INT = 0
      GOTO 5
!
18    IF( SEUIL .GT. RX ) GOTO 4
!
      RETURN
      END SUBROUTINE DMSPR
