!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
! CE PROGRAMME DONNE LES COEFFICIENTS DE LA MATRICE DE WANG
! A LA REPRESENTATION (0-) J'ASSOCIE FORMELLEMENT -1, ET A (0+) J'ASSOCIE 0.
!
      SUBROUTINE WJKMS(AJ,AK,AM,IS,WJK,IP)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: AJ,AK,AM,WJK
      integer          :: IS,IP

      real(kind=dppr)  :: PH,PHS

      integer          :: IMP,IPJ,IPK
      integer          :: JP
      integer          :: MP
!
      WJK = 0.D0
      IP  = 0
      PH  = (-1.D0)**INT(AJ)/SQRT(2.D0)
      PHS = 1.D0/SQRT(2.D0)
      IPJ = INT(2.*AJ)-2*(INT(2.*AJ)/2)
      IPK = INT(2.*AK)-2*(INT(2.*AK)/2)
      IF( AJ  .LT. AK  ) RETURN
      IF( IPJ .NE. IPK ) RETURN
      IF( IPJ .EQ. IPK  .AND.            &
          IPK .EQ. 1          ) GOTO 10
      IF( IPJ .EQ. IPK  .AND.            &
          IPK .EQ. 0          ) GOTO 20
!
10    MP  = INT(AJ+0.5D0)
      IMP = MP-(MP/2)*2
      IF( ABS(AM) .EQ.  AK   .AND.                &
          AM      .GT.  0.D0 .AND.                &
          IS      .EQ.  1          ) WJK =  PHS
      IF( ABS(AM) .EQ.  AK   .AND.                &
          AM      .LT.  0.D0 .AND.                &
          IS      .EQ.  1          ) WJK =  PHS
      IF( ABS(AM) .EQ.  AK   .AND.                &
          AM      .GT.  0.D0 .AND.                &
          IS      .EQ.  2          ) WJK = -PHS
      IF( ABS(AM) .EQ.  AK   .AND.                &
          AM      .LT.  0.D0 .AND.                &
          IS      .EQ.  2          ) WJK =  PHS
!CC CARACTERE IMAGINAIRE DE WJKS
      IF( ABS(AM) .EQ.  AK   .AND.                &
          AM      .LT.  0.D0 .AND.                &
          IS      .EQ.  1          ) IP  =  1
      IF( ABS(AM) .EQ.  AK   .AND.                &
          AM      .LT.  0.D0 .AND.                &
          IS      .EQ.  2          ) IP  =  1
!CC LES SIGNES MOINS QUI SORTENT DE (i)^J
      IF( IMP     .EQ.  0    .AND.                &
          ABS(AM) .EQ.  AK   .AND.                &
          AM      .LT.  0.D0 .AND.                &
          IS      .EQ.  2          ) WJK = -PHS
      IF( IMP     .EQ.  0    .AND.                &
          ABS(AM) .EQ.  AK   .AND.                &
          AM      .LT.  0.D0 .AND.                &
          IS      .EQ.  1          ) WJK = -PHS
      RETURN
!
20    JP = INT(AJ)-2*(INT(AJ)/2)
      IF( JP      .EQ.  0    .AND.                &
          AK      .EQ.  0.D0       ) WJK =  1.D0
      IF( JP      .EQ.  1    .AND.                &
          AK      .EQ. -1.D0       ) WJK =  1.D0
      IF( ABS(AM) .EQ.  AK   .AND.                &
          AM      .GT.  0.D0 .AND.                &
          IS      .EQ.  1          ) WJK =  PHS
      IF( ABS(AM) .EQ.  AK   .AND.                &
          AM      .LT.  0.D0 .AND.                &
          IS      .EQ.  1          ) WJK =  PH
      IF( ABS(AM) .EQ.  AK   .AND.                &
          AM      .GT.  0.D0 .AND.                &
          IS      .EQ.  2          ) WJK = -PHS
      IF( ABS(AM) .EQ.  AK   .AND.                &
          AM      .LT.  0.D0 .AND.                &
          IS      .EQ.  2          ) WJK =  PH
!
      RETURN
      END SUBROUTINE WJKMS
