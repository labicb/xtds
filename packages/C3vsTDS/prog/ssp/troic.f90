!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
! CE PROGRAMME CALCULE LES COEFFICIENTS 3C-IL
!
      SUBROUTINE TROIC(IC1,IC2,IC3,IL1,IL2,IL3,TRC,ITRC)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: TRC
      integer          :: IC1,IC2,IC3,IL1,IL2,IL3,ITRC

! functions
      real(kind=dppr)  :: AKIC,EPSKC2,NKC

      real(kind=dppr)  :: AK1,AK2,AK3,AN
      real(kind=dppr)  :: DMC3
      real(kind=dppr)  :: EPS2
      real(kind=dppr)  :: Y

      integer          :: IFF
!
      TRC  = 0.D0
      ITRC = 0
!
      AK1 = AKIC(IC1)
      AK2 = AKIC(IC2)
      AK3 = AKIC(IC3)
      IF( IC1 .EQ. 3 .AND.               &
          IC2 .EQ. 3 .AND.               &
          IC3 .EQ. 3       ) AK3 = 2.D0
      CALL FKCL(AK1,AK2,AK3,IC1,IC2,IC3,IL1,IL2,IL3,Y,IFF)
      EPS2 = EPSKC2(AK1,AK2,AK3)
      AN   = NKC(AK1,IC1,AK2,IC2,AK3,IC3)
      IF( IC3 .EQ. 3 .OR.         &
          IC3 .EQ. 6      ) THEN
        DMC3 = 2.D0
      ELSE
        DMC3 = 1.D0
      ENDIF
      IF(  AN   .NE. 0.D0             ) TRC = TRC+(SQRT(DMC3)/SQRT(AN))*Y
      IF(  EPS2 .LT. 0.D0 .AND.                     &
           IFF  .EQ. 1                ) TRC = -TRC
      IF( (EPS2 .LT. 0.D0 .AND.                   &
           IFF  .EQ. 0         ) .OR.             &
          (EPS2 .GT. 0.D0 .AND.                   &
           IFF  .EQ. 1         )      ) ITRC = 1
!
      RETURN
      END SUBROUTINE TROIC
