!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
!            *** COUPLAGE DES OPERATEURS VIBRONIQUES ***
!
!      <((G,IACE)*IACV)IACEV || ((CE(CE),IGE)*IGV)IGEV || ((G,IBCE)*IBCV)IBCEV >=
!
!         =  ([IACEV][IBCEV][IGEV])**(1/2) X PHASE X
!
!                    ( IACE  IACV  IACEV)
!                    (                  )   ( CE    G)  IACE
!                    ( IGE   IGV   IGEV ) K                  X
!                    (                  )    IGE IBCE  ( G)
!                    ( IBCE  IBCV  IBCEV)
!
!
!         X  ((G || E**CE(CE) || G)=EMRL(CE,G)) * function  elmr dans hcomplet!
!
      SUBROUTINE EMRVIB(AKE,IAV,ACEV,LS,IES,LL,IEL,IGE,IGV,IGEV,BCE,IBV,BCEV,EMRV,IKF)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: AKE,ACEV,BCE,BCEV,EMRV
      integer          :: IAV,LS,IES,LL,IEL,IGE,IGV,IGEV,IBV,IKF

! functions
      real(kind=dppr)  :: EMRRO
      integer          :: IDIMK

      real(kind=dppr)  :: ELM,ELML,ELMS
      real(kind=dppr)  :: NFK
      real(kind=dppr)  :: P1,P2,P3,P4,P5,P6,PH12,PHASE1,PHASE2
      real(kind=dppr)  :: XK,XKK,XNF

      integer          :: IFK,IFKK,INFK
      integer          :: NF
!
      CALL KSU2CV(DBLE(LS),0.5D0,0.5D0,DBLE(IES),0.5D0,0.5D0,XK ,IFK )
      CALL KSU2CV(DBLE(LL),1.0D0,1.0D0,DBLE(IEL),1.0D0,1.0D0,XKK,IFKK)
      CALL NF9K(0.5D0,1.D0,AKE,DBLE(IES),DBLE(IEL),DBLE(IGE),0.5D0,1.D0,BCE,NFK,INFK)
      CALL NF9K(AKE,DBLE(IAV),ACEV,DBLE(IGE),DBLE(IGV),DBLE(IGEV),BCE,DBLE(IBV),BCEV,XNF,NF)
!
      P1     = DBLE(IDIMK(ACEV))
      P2     = DBLE(IDIMK(BCEV))
      P3     = DBLE(IDIMK(DBLE(IGEV)))
      P4     = DBLE(IDIMK(AKE))
      P5     = DBLE(IDIMK(BCE))
      P6     = DBLE(IDIMK(DBLE(IGE)))
      PHASE1 = SQRT(P1*P2*P3)
      PHASE2 = SQRT(P4*P5*P6)
      PH12   = PHASE1*PHASE2
      IKF    = 0
      ELMS   = EMRRO(LS,LS,0.5D0)
      ELML   = EMRRO(LL,LL,1.D0)
      ELM    = ELMS*ELML
      EMRV   = SQRT(2.D0/3.D0)*PH12*ELM*XK*XKK*NFK*XNF
      IF    ( IFK+IFKK .EQ. 2 ) THEN
        EMRV = -EMRV
      ELSEIF( IFK+IFKK .EQ. 1 ) THEN
        IKF  = 1
      ENDIF
!
      RETURN
      END SUBROUTINE EMRVIB
