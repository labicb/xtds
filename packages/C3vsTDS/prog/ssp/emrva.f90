!
! SMIL CHAMPION MAI 82
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
!     ELEMENTS MATRICIELS REDUITS DES OPERATEURS VIBRATIONNELS
!     ELEMENTAIRES, LES OPERATEURS SONT CARACTERISES PAR :
!                 IDEG    = DEGENERESCENCE DE L'OSCILLATEUR ASSOCIE
!                 IPM     = +1 POUR UN OPERATEUR CREATION
!                         = -1 POUR UN OPERATEUR ANNIHILATION
!                 OMEGA   = DEGRE VIBRATIONNEL < OU = 4
!                 K       = OMEGA , OMEGA-2 , ... , 1 OU 0
!                 N       = MULTIPLICITE (TRIPLEMENT DEGENERE SEULEMENT)
!                 C       = SYMETRIE DANS TD (A1,A2,E,F1,F2)
!
      FUNCTION EMRVA(IDEG,VP,LP,IPM,OMEGA,K,V,L)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: EMRVA
      integer          :: IDEG,VP,LP,IPM,OMEGA,K,V,L

! functions
      real(kind=dppr)  :: A1N,A21K,A22,A23,A24
      integer          :: IDIMK,IPSIK

      real(kind=dppr)  :: DL
!
      DL    = DBLE(IDIMK(DBLE(L)))
      EMRVA = 0.D0
      IF( IPSIK(IDEG,VP,LP)   .EQ. 0.D0  ) RETURN
      IF( IPSIK(IDEG,V,L)     .EQ. 0.D0  ) RETURN
      IF( IPSIK(IDEG,OMEGA,K) .EQ. 0     ) RETURN
      IF( IPM*(VP-V)          .NE. OMEGA ) RETURN
      IF( OMEGA               .NE. 0     ) GOTO 10
      IF( L-LP                .NE. 0     ) RETURN
      EMRVA = SQRT(DL)
      RETURN
!
10    IF( OMEGA .LT. 0 ) RETURN
      SELECT CASE( IDEG )
        CASE( 1 )
          EMRVA = A1N(IPM,VP,V,OMEGA)
        CASE( 2 )
          SELECT CASE( OMEGA )
            CASE( 1 )
              EMRVA = A21K(IPM,VP,LP,V,L)
            CASE( 2 )
              EMRVA = A22(IPM,VP,LP,K,V,L)
            CASE( 3 )
              EMRVA = A23(IPM,VP,LP,K,V,L)
            CASE( 4 )
              EMRVA = A24(IPM,VP,LP,K,V,L)
          END SELECT
      END SELECT
!
      RETURN
      END FUNCTION EMRVA
