!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
!     CE PROGRAMME CALCULE LES COEFFICIENTS 9K DE CIVS
!
      SUBROUTINE NF9K(AK1,AK2,AK3,AK4,AK5,AK6,AK7,AK8,AK9,NFK,INFK)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: AK1,AK2,AK3,AK4,AK5,AK6,AK7,AK8,AK9,NFK
      integer          :: INFK

! functions
      integer          :: IDIMK,KTR

      real(kind=dppr)  :: PH9K
      real(kind=dppr)  :: TRK1,TRK2,TRK3,TRK4,TRK5,TRK6

      integer          :: IDM1,IDM2,IDM3,IDM4,IDM5,IDM6,IDM7,IDM8,IDM9
      integer          :: IS1,IS2,IS3,IS4,IS5,IS6,IS7,IS8,IS9
      integer          :: ITRK1,ITRK2,ITRK3,ITRK4,ITRK5,ITRK6,ITRKTT
!
      NFK  = 0.D0
      INFK = 0
      IF( KTR(AK1,AK2,AK3) .EQ. 0 .OR.           &
          KTR(AK4,AK5,AK6) .EQ. 0 .OR.           &
          KTR(AK7,AK8,AK9) .EQ. 0 .OR.           &
          KTR(AK1,AK4,AK7) .EQ. 0 .OR.           &
          KTR(AK2,AK5,AK8) .EQ. 0 .OR.           &
          KTR(AK3,AK6,AK9) .EQ. 0      ) RETURN
!
      IDM1 = IDIMK(AK1)
      IDM2 = IDIMK(AK2)
      IDM3 = IDIMK(AK3)
      IDM4 = IDIMK(AK4)
      IDM5 = IDIMK(AK5)
      IDM6 = IDIMK(AK6)
      IDM7 = IDIMK(AK7)
      IDM8 = IDIMK(AK8)
      IDM9 = IDIMK(AK9)
!
      DO IS1=1,IDM1
        DO IS2=1,IDM2
          DO IS3=1,IDM3
            DO IS4=1,IDM4
              DO IS5=1,IDM5
                DO IS6=1,IDM6
                  DO IS7=1,IDM7
                    DO IS8=1,IDM8
                      DO IS9=1,IDM9
                        CALL TROIK(AK1,AK2,AK3,IS1,IS2,IS3,TRK1,ITRK1)
                        CALL TROIK(AK4,AK5,AK6,IS4,IS5,IS6,TRK2,ITRK2)
                        CALL TROIK(AK3,AK6,AK9,IS3,IS6,IS9,TRK3,ITRK3)
                        CALL TROIK(AK1,AK4,AK7,IS1,IS4,IS7,TRK4,ITRK4)
                        CALL TROIK(AK2,AK5,AK8,IS2,IS5,IS8,TRK5,ITRK5)
                        CALL TROIK(AK7,AK8,AK9,IS7,IS8,IS9,TRK6,ITRK6)
!
                        IF( ITRK1 .EQ. 1 ) TRK1 = -TRK1
                        IF( ITRK2 .EQ. 1 ) TRK2 = -TRK2
                        IF( ITRK3 .EQ. 1 ) TRK3 = -TRK3
                        PH9K   = (1.D0)/(DBLE(IDM9)*SQRT(DBLE(IDM3*IDM6*IDM7*IDM8)))
                        INFK   = 0
                        ITRKTT = 0
                        ITRKTT = ITRK1+ITRK2+ITRK3+ITRK4+ITRK5+ITRK6
                        IF( ITRKTT .EQ. 1 .OR.              &
                            ITRKTT .EQ. 3 .OR.              &
                            ITRKTT .EQ. 5      ) INFK =  1
                        IF( ITRKTT .EQ. 2 .OR.                 &
                            ITRKTT .EQ. 3 .OR.                 &
                            ITRKTT .EQ. 6      ) PH9K = -PH9K
                        NFK = NFK+TRK1*TRK2*TRK3*TRK4*TRK5*TRK6*PH9K
                      ENDDO
                    ENDDO
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
!
      RETURN
      END SUBROUTINE NF9K
