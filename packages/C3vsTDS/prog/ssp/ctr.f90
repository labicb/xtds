!
! *** MULTIPLICATION DANS C3V ET C3VS (REPRESENTATIONS ENTIERES ET DEMI-ENTIERES).
!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v et XY3Z/C3vs.
!
!    A1  ------> 1
!    A2  ------> 2
!    E   ------> 3
!    A'1 ------> 4
!    A'2 ------> 5
!    E'  ------> 6
!
      FUNCTION CTR(IC1,IC2,IC3)
      IMPLICIT NONE
      integer          :: CTR
      integer          :: IC1,IC2,IC3

      integer          :: I1,I2
!
      CTR = 0
      IF( IC1 .LE. IC2 ) THEN
        I1 = IC1
        I2 = IC2
        GOTO 3
      ENDIF
!
2     I1 = IC2
      I2 = IC1
!
3     IF( I1  .GT. 2        ) GOTO 8
      IF( I1  .LT. 2  .AND.            &
          IC3 .EQ. I2       ) CTR = 1
!
      IF( I2  .LT. 2  .AND.            &
          IC3 .EQ. 2        ) CTR = 1
      IF( I2  .LE. 2  .AND.            &
          IC3 .EQ. 1        ) CTR = 1
!
      IF( I2  .LT. 4  .AND.            &
          IC3 .EQ. 3        ) CTR = 1
      IF( I2  .LE. 4  .AND.            &
          IC3 .EQ. 5        ) CTR = 1
!
      IF( I2  .GT. 6        ) GOTO 2
      IF( I2  .LT. 6  .AND.            &
          IC3 .EQ. 4        ) CTR = 1
      IF( IC3 .EQ. 6        ) CTR = 1
!
8     IF( I1  .EQ. 4        ) GOTO 15
      IF( I1  .GT. 4        ) GOTO 19
      IF( I2  .LT. 4  .AND.            &
         (IC3 .EQ. 1  .OR.             &
          IC3 .EQ. 2  .OR.             &
          IC3 .EQ. 3      ) ) CTR = 1
      IF( I2  .LE. 4  .AND.            &
          IC3 .EQ. 6        ) CTR = 1
      IF( I2  .LE. 5  .AND.            &
          IC3 .EQ. 6        ) CTR = 1
      IF( IC3 .EQ. 4  .OR.             &
          IC3 .EQ. 5  .OR.             &
          IC3 .EQ. 6        ) CTR = 1
!
15    IF( I2  .LT. 5  .AND.            &
          IC3 .EQ. 3        ) CTR = 1
      IF( I2  .LE. 5  .AND.            &
          IC3 .EQ. 1        ) CTR = 1
      IF( IC3 .EQ. 3        ) CTR = 1
!
19    IF( I1  .GT. I2       ) GOTO 2
      IF( I1  .LT. I2 .AND.            &
          IC3 .EQ. 3        ) CTR = 1
      IF( I2  .LE. 5  .AND.            &
          IC3 .EQ. 2        ) CTR = 1
      IF( IC3 .EQ. 1  .OR.             &
          IC3 .EQ. 2  .OR.             &
          IC3 .EQ. 3        ) CTR = 1
!
      RETURN
      END FUNCTION CTR
