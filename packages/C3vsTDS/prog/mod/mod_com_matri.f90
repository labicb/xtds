
      module mod_com_matri

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,pointer ,dimension(:,:) ,save  :: EMRD                                      ! (MXSNV,MXSNV)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_MATRI
      IMPLICIT NONE

      integer  :: ierr

      allocate(EMRD(MXSNV,MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MATRI_EMRD')
      EMRD = 0.D0
!
      return
      end subroutine ALLOC_MATRI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNV

      subroutine RESIZE_MXSNV_MATRI(C_SNV)
      IMPLICIT NONE
      integer :: C_SNV

      integer :: i,ierr

! EMRD
      allocate(rpd2(C_SNV,C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_MATRI_EMRD')
      rpd2 = 0.d0
      do i=1,MXSNV
        rpd2(1:MXSNV,i) = EMRD(:,i)
      enddo
      deallocate(EMRD)
      EMRD => rpd2
!
      return
      end subroutine RESIZE_MXSNV_MATRI

      end module mod_com_matri
