
      module mod_main_hmatri

      use mod_dppr
      use mod_par_tds
      use mod_com_hmatri
      use mod_com_matri


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_hmatri
      call alloc_matri
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXDIMS
      IMPLICIT NONE

      integer  :: M_DIMS

      M_DIMS = MXRES(MXDIMS)
      call RESIZE_MXDIMS_HMATRI(M_DIMS)
      MXDIMS = M_DIMS
!
      return
      end subroutine RESIZE_MXDIMS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXELMH
      IMPLICIT NONE

      integer  :: M_ELMH

      M_ELMH = MXRES(MXELMH)
      call RESIZE_MXELMH_HMATRI(M_ELMH)
      MXELMH = M_ELMH
!
      return
      end subroutine RESIZE_MXELMH

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXEMR
      IMPLICIT NONE

      integer  :: M_EMR

      M_EMR = MXRES(MXEMR)
      call RESIZE_MXEMR_HMATRI(M_EMR)
      MXEMR = M_EMR
!
      return
      end subroutine RESIZE_MXEMR

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPH
      IMPLICIT NONE

      integer  :: M_OPH

      M_OPH = MXRES(MXOPH)
      call RESIZE_MXOPH_HMATRI(M_OPH)
      MXOPH = M_OPH
!
      return
      end subroutine RESIZE_MXOPH

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPVH
      IMPLICIT NONE

      integer  :: M_OPVH

      M_OPVH = MXRES(MXOPVH)
      call RESIZE_MXOPVH_HMATRI(M_OPVH)
      MXOPVH = M_OPVH
!
      return
      end subroutine RESIZE_MXOPVH

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXSNV
      IMPLICIT NONE

      integer  :: M_SNV

      M_SNV = MXRES(MXSNV)
      call RESIZE_MXSNV_MATRI(M_SNV)
      MXSNV = M_SNV
!
      return
      end subroutine RESIZE_MXSNV

      end module mod_main_hmatri
