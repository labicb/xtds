
      module mod_com_sy
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE

      integer              ,dimension(MXSYM) ,parameter  :: PC     = (/  1, -1,  1 /)
      integer                                ,parameter  :: LKCS   = 9
      integer                                ,parameter  :: LKCES  = 8

      character(len =   1)                   ,parameter  :: PARGEN = ' '
      character(len =   2) ,dimension(MXSYM) ,parameter  :: SYM    = (/ 'A1'   , 'A2'   , 'E '     /)
      character(len =   3) ,dimension(LKCS)  ,parameter  :: LKC    = (/ 'S- '  , 'S+ '  , 'P  '  ,     &
                                                                        'D  '  , 'F  '  , 'G  '  ,     &
                                                                        'H  '  , 'I  '  , 'J  '    /)
      character(len =   5) ,dimension(LKCES) ,parameter  :: LKCE   = (/ "S'   ", "P'   ", "D'   ",     &
                                                                        "F'   ", "G'   ", "H'   ",     &
                                                                        "I'   ", "J'   "           /)

      end module mod_com_sy
