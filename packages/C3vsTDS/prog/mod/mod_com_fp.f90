
      module mod_com_fp

      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE

      real(kind=dppr) ,dimension(NBVQN) ,save  :: VIBNU
      real(kind=dppr)                   ,save  :: ABUND
      real(kind=dppr)                   ,save  :: SPINY
      real(kind=dppr)                   ,save  :: FPVIB,B0,A0,BB0,DD0

      contains

      function FPART(TROT,TVIB)
      use mod_dppr
      use mod_par_tds
      use mod_com_const
      IMPLICIT NONE
      real(kind=dppr)  :: FPART
      real(kind=dppr)  :: TROT,TVIB

      real(kind=dppr)  :: FRMD
      real(kind=dppr)  :: A
      real(kind=dppr)  :: B
      real(kind=dppr)  :: AA,ALPHA
      real(kind=dppr)  :: BETA
      real(kind=dppr)  :: GAMA
      real(kind=dppr)  :: ZETA

      integer ,dimension(NBVQN)  :: IDEG = (/ 1, 1, 1, 2, 2, 2 /)
      integer          :: I
!
      A = PLANK*CLUM/BOLT
      IF( FPVIB .EQ. 0.D0 ) THEN
        FPVIB = 1.D0
        DO I=1,NBVQN
          B     = 1.D0-EXP(-VIBNU(I)*A/TVIB)
          B     = B**IDEG(I)
          FPVIB = FPVIB/B
        ENDDO
      ENDIF
!
! *** PARTITION FUNCTION FROM MCDOWELL J.Q.S.R.T. 38  337-346 (1987)
!
      BETA = PLANK*CLUM*B0/BOLT/TROT
!
! ALPHA = KT/HC
!
      ALPHA = B0/BETA
      ZETA  = ALPHA*ALPHA*ALPHA
      GAMA  = PI*ZETA/(B0*B0*A0)
      AA    = 1.D0-B0/A0
      FRMD  = SQRT(GAMA)*(1.D0+AA*BETA/12.D0+7.D0*AA*AA*BETA*BETA/480.D0)*EXP(BETA/4.D0)
      FPART = FRMD*FPVIB
!
      RETURN
      end function FPART

      end module mod_com_fp
