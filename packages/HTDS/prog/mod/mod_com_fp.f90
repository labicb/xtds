
      module mod_com_fp

      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE

      real(kind=dppr) ,dimension(NBVQN) ,save  :: VIBNU
      real(kind=dppr)                   ,save  :: ABUND
      real(kind=dppr)                   ,save  :: SPINY
      real(kind=dppr)                   ,save  :: FPVIB,B0,D0


      contains

      function FPART(TROT,TVIB)
      use mod_dppr
      use mod_par_tds
      use mod_com_const
      IMPLICIT NONE
      real(kind=dppr)  :: FPART
      real(kind=dppr)  :: TROT,TVIB

      real(kind=dppr)  :: A
      real(kind=dppr)  :: B,BETA
      real(kind=dppr)  :: C
      real(kind=dppr)  :: D,DELTA
      real(kind=dppr)  :: FRMD

      integer ,dimension(NBVQN)  :: IDEG = (/ 1, 2, 3, 3, 3, 3 /)
      integer          :: I
!
      A = PLANK*CLUM/BOLT
      IF( FPVIB .EQ. 0.D0 ) THEN
        FPVIB = 1.D0
        DO I=1,NBVQN
          B     = 1.D0-EXP(-VIBNU(I)*A/TVIB)
          B     = B**IDEG(I)
          FPVIB = FPVIB/B
        ENDDO
      ENDIF
!
! *** PARTITION FUNCTION FROM MCDOWELL J.Q.S.R.T. 38  337-346 (1987)
!
      BETA  = PLANK*CLUM*B0/BOLT/TROT
      C     = PI*PI/BETA
      C     = -C
      B     = 2.D0*SPINY+1.D0
      A     = PI*(-3.D0/SQRT(2.D0)*B*EXP(C/16.D0)+16.D0/3.D0/SQRT(3.D0)*EXP(C/9.D0)+3.D0*B*(B-2.D0)*EXP(C/4.D0))
      DELTA = A/B/B/B/B
      B     = B*B*B*B*B*B
      D     = B/24.D0*SQRT(PI)*EXP(BETA/4.D0)*( BETA**(-1.5D0) )
      FRMD  = D*(1.D0+DELTA)*(1.D0+15.D0*D0/4.D0/BETA/B0)
      FPART = FRMD*FPVIB
!
      RETURN
      end function FPART

      end module mod_com_fp
