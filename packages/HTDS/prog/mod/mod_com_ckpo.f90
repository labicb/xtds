
      module mod_com_ckpo

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer ,dimension(0:MXJ+3)   ,save  :: INJDK  = 0
      integer ,dimension(2,2,MXJ+3) ,save  :: INJODK = 0
!
      real(kind=dppr) ,pointer ,dimension(:,:,:) ,save  :: VODK                                    ! (2,2,MXODK)

      integer         ,pointer ,dimension(:,:,:) ,save  :: IODKX                                   ! (2,2,MXODK)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_CKPO
      IMPLICIT NONE

      integer  :: ierr

      allocate(VODK(2,2,MXODK),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_CKPO_VODK')
      VODK = 0.d0

      allocate(IODKX(2,2,MXODK),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_CKPO_IODKX')
      IODKX = 0
!
      return
      end subroutine ALLOC_CKPO

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXODK

      subroutine RESIZE_MXODK_CKPO(C_ODK)
      IMPLICIT NONE
      integer :: C_ODK

      integer :: i,ierr
      integer :: j

! VODK
      allocate(rpd3(2,2,C_ODK),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXODK_CKPO_VODK')
      rpd3 = 0.d0
      do j=1,2
        do i=1,2
          rpd3(i,j,1:MXODK) = VODK(i,j,:)
        enddo
      enddo
      deallocate(VODK)
      VODK => rpd3
! IODKX
      allocate(ipd3(2,2,C_ODK),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXODK_CKPO_IODKX')
      ipd3 = 0
      do j=1,2
        do i=1,2
          ipd3(i,j,1:MXODK) = IODKX(i,j,:)
        enddo
      enddo
      deallocate(IODKX)
      IODKX => ipd3
!
      return
      end subroutine RESIZE_MXODK_CKPO

      end module mod_com_ckpo
