
      module mod_com_xfonc                                                                         ! obs-cal related COMMON

      use mod_dppr
      use mod_par_x
      use mod_alloc
      IMPLICIT NONE

      integer ,save  :: NBFOBZ = 0
      integer ,save  :: NBOBZ  = 0
      integer ,save  :: NBROBZ = 0
      integer ,save  :: NBSOBZ = 0
!
      real(kind=dppr)      ,pointer ,dimension(:) ,save  :: FOMC                                   ! (MXOBZ)
      real(kind=dppr)      ,pointer ,dimension(:) ,save  :: SDZCAL                                 ! (MXOBZ)
      real(kind=dppr)      ,pointer ,dimension(:) ,save  :: SDZEXP                                 ! (MXOBZ)
      real(kind=dppr)      ,pointer ,dimension(:) ,save  :: ZWGT                                   ! (MXOBZ)

      integer              ,pointer ,dimension(:) ,save  :: INDOBS                                 ! (MXOBZ)

      character(len =   1) ,pointer ,dimension(:) ,save  :: SASOZ                                  ! (MXOBZ)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_XFONC
      IMPLICIT NONE

      integer  :: ierr

      allocate(FOMC(MXOBZ),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XFONC_FOMC')
      FOMC = 0.d0
      allocate(SDZCAL(MXOBZ),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XFONC_SDZCAL')
      SDZCAL = 0.d0
      allocate(SDZEXP(MXOBZ),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XFONC_SDZEXP')
      SDZEXP = 0.d0
      allocate(ZWGT(MXOBZ),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XFONC_ZWGT')
      ZWGT = 0.d0

      allocate(INDOBS(MXOBZ),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XFONC_INDOBS')
      INDOBS = 0

      allocate(SASOZ(MXOBZ),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XFONC_SASOZ')
      SASOZ = ''
!
      return
      end subroutine ALLOC_XFONC

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOBZ

      subroutine RESIZE_MXOBZ_XFONC(C_OBZ)
      IMPLICIT NONE
      integer :: C_OBZ

      integer :: ierr

      character(len =   1) ,pointer ,dimension(:)  :: cpd1

! FOMC
      allocate(rpd1(C_OBZ),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBZ_XFONC_FOMC')
      rpd1 = 0.d0
      rpd1(1:MXOBZ) = FOMC(:)
      deallocate(FOMC)
      FOMC => rpd1
! SDZCAL
      allocate(rpd1(C_OBZ),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBZ_XFONC_SDZCAL')
      rpd1 = 0.d0
      rpd1(1:MXOBZ) = SDZCAL(:)
      deallocate(SDZCAL)
      SDZCAL => rpd1
! SDZEXP
      allocate(rpd1(C_OBZ),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBZ_XFONC_SDZEXP')
      rpd1 = 0.d0
      rpd1(1:MXOBZ) = SDZEXP(:)
      deallocate(SDZEXP)
      SDZEXP => rpd1
! ZWGT
      allocate(rpd1(C_OBZ),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBZ_XFONC_ZWGT')
      rpd1 = 0.d0
      rpd1(1:MXOBZ) = ZWGT(:)
      deallocate(ZWGT)
      ZWGT => rpd1

! INDOBS
      allocate(ipd1(C_OBZ),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBZ_XFONC_INDOBS')
      ipd1 = 0
      ipd1(1:MXOBZ) = INDOBS(:)
      deallocate(INDOBS)
      INDOBS => ipd1

! SASOZ
      allocate(cpd1(C_OBZ),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBZ_XFONC_SASOZ')
      cpd1 = ''
      cpd1(1:MXOBZ) = SASOZ(:)
      deallocate(SASOZ)
      SASOZ => cpd1
!
      return
      end subroutine RESIZE_MXOBZ_XFONC

      end module mod_com_xfonc
