
      module mod_par_x

      use mod_par_tds
      IMPLICIT NONE

      integer ,save       :: MXOP   = 1+1*1                                                        ! MXOPH+MXOPT*MXATRT
      integer ,save       :: MXOBZ  = 2*1+1+1*1                                                    ! 2*MXOBS+MXOP
      integer ,save       :: LWA    = 5*(1+1*1)+2*1+1+1*1                                          ! 5*MXOP+MXOBZ
      integer ,save       :: MXTRAD = 2*1                                                          ! 2*MXATR

      end module mod_par_x
