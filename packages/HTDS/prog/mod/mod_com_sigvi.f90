
      module mod_com_sigvi

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer ,pointer ,dimension(:,:) ,save  :: IKVA,IKVC,IPK                                     ! (NBVQN+2,MXSNB)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_SIGVI
      IMPLICIT NONE

      integer  :: ierr

      allocate(IKVA(NBVQN+2,MXSNB),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIGVI_IKVA')
      IKVA = 0
      allocate(IKVC(NBVQN+2,MXSNB),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIGVI_IKVC')
      IKVC = 0
      allocate(IPK(NBVQN+2,MXSNB),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIGVI_IPK')
      IPK = 0
!
      return
      end subroutine ALLOC_SIGVI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNB

      subroutine RESIZE_MXSNB_SIGVI(C_SNB)
      IMPLICIT NONE
      integer :: C_SNB

      integer :: i,ierr

! IKVA
      allocate(ipd2(NBVQN+2,C_SNB),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNB_SIGVI_IKVA')
      ipd2 = 0
      do i=1,MXSNB
        ipd2(:,i) = IKVA(:,i)
      enddo
      deallocate(IKVA)
      IKVA => ipd2
! IKVC
      allocate(ipd2(NBVQN+2,C_SNB),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNB_SIGVI_IKVC')
      ipd2 = 0
      do i=1,MXSNB
        ipd2(:,i) = IKVC(:,i)
      enddo
      deallocate(IKVC)
      IKVC => ipd2
! IPK
      allocate(ipd2(NBVQN+2,C_SNB),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNB_SIGVI_IPK')
      ipd2 = 0
      do i=1,MXSNB
        ipd2(:,i) = IPK(:,i)
      enddo
      deallocate(IPK)
      IPK => ipd2
!
      return
      end subroutine RESIZE_MXSNB_SIGVI

      end module mod_com_sigvi
