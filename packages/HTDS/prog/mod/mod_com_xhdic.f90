
      module mod_com_xhdic

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,pointer ,dimension(:) ,save  :: HD                                          ! (MXDIMS)
      real(kind=dppr) ,pointer ,dimension(:) ,save  :: PCENT                                       ! (MXSNV)

      integer         ,pointer ,dimension(:) ,save  :: ICENT                                       ! (MXSNV)
      integer         ,pointer ,dimension(:) ,save  :: K                                           ! (MXDIMS)
      integer         ,pointer ,dimension(:) ,save  :: NRCOD,NVCOD                                 ! (MXDIMS)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_XHDIC
      IMPLICIT NONE

      integer  :: ierr

      allocate(HD(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XHDIC_HD')
      HD = 0.d0
      allocate(PCENT(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XHDIC_PCENT')
      PCENT = 0.d0

      allocate(ICENT(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XHDIC_ICENT')
      ICENT = 0
      allocate(K(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XHDIC_K')
      K = 0
      allocate(NRCOD(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XHDIC_NRCOD')
      NRCOD = 0
      allocate(NVCOD(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XHDIC_NVCOD')
      NVCOD = 0
!
      return
      end subroutine ALLOC_XHDIC

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXDIMS

      subroutine RESIZE_MXDIMS_XHDIC(C_DIMS)
      IMPLICIT NONE
      integer :: C_DIMS

      integer :: ierr

! HD
      allocate(rpd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_XHDIC_HD')
      rpd1 = 0.d0
      rpd1(1:MXDIMS) = HD(:)
      deallocate(HD)
      HD => rpd1

! K
      allocate(ipd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_XHDIC_K')
      ipd1 = 0
      ipd1(1:MXDIMS) = K(:)
      deallocate(K)
      K => ipd1
! NRCOD
      allocate(ipd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_XHDIC_NRCOD')
      ipd1 = 0
      ipd1(1:MXDIMS) = NRCOD(:)
      deallocate(NRCOD)
      NRCOD => ipd1
! NVCOD
      allocate(ipd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_XHDIC_NVCOD')
      ipd1 = 0
      ipd1(1:MXDIMS) = NVCOD(:)
      deallocate(NVCOD)
      NVCOD => ipd1
!
      return
      end subroutine RESIZE_MXDIMS_XHDIC

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNV

      subroutine RESIZE_MXSNV_XHDIC(C_SNV)
      IMPLICIT NONE
      integer :: C_SNV

      integer :: ierr

! PCENT
      allocate(rpd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_XHDIC_PCENT')
      rpd1 = 0.d0
      rpd1(1:MXSNV) = PCENT(:)
      deallocate(PCENT)
      PCENT => rpd1

! ICENT
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_XHDIC_ICENT')
      ipd1 = 0
      ipd1(1:MXSNV) = ICENT(:)
      deallocate(ICENT)
      ICENT => ipd1
!
      return
      end subroutine RESIZE_MXSNV_XHDIC

      end module mod_com_xhdic
