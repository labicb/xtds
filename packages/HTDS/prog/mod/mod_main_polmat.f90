
      module mod_main_polmat

      use mod_dppr
      use mod_par_tds
      use mod_com_ckdipo
      use mod_com_ckpo
      use mod_com_dipomat
      use mod_com_matri
      use mod_com_polmat


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_ckdipo
      call alloc_ckpo
      call alloc_dipomat
      call alloc_matri
      call alloc_polmat
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXDIMI
      IMPLICIT NONE

      integer  :: M_DIMI

      M_DIMI = MXRES(MXDIMI)
      call RESIZE_MXDIMI_POLMAT(M_DIMI)
      call RESIZE_MXDIMI_DIPOMAT(M_DIMI)
      MXDIMI = M_DIMI
!
      return
      end subroutine RESIZE_MXDIMI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXDIMS
      IMPLICIT NONE

      integer  :: M_DIMS

      M_DIMS = MXRES(MXDIMS)
      call RESIZE_MXDIMS_DIPOMAT(M_DIMS)
      MXDIMS = M_DIMS
!
      return
      end subroutine RESIZE_MXDIMS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXDK
      IMPLICIT NONE

      integer  :: M_DK

      M_DK = MXRES(MXDK)
      call RESIZE_MXDK_CKDIPO(M_DK)
      MXDK = M_DK
!
      return
      end subroutine RESIZE_MXDK

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXELMT
      IMPLICIT NONE

      integer  :: M_ELMT

      M_ELMT = MXRES(MXELMT)
      call RESIZE_MXELMT_DIPOMAT(M_ELMT)
      MXELMT = M_ELMT
!
      return
      end subroutine RESIZE_MXELMT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXEMR
      IMPLICIT NONE

      integer  :: M_EMR

      M_EMR = MXRES(MXEMR)
      call RESIZE_MXEMR_DIPOMAT(M_EMR)
      MXEMR = M_EMR
!
      return
      end subroutine RESIZE_MXEMR

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXODK
      IMPLICIT NONE

      integer  :: M_ODK

      M_ODK = MXRES(MXODK)
      call RESIZE_MXODK_CKPO(M_ODK)
      MXODK = M_ODK
!
      return
      end subroutine RESIZE_MXODK

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPT
      IMPLICIT NONE

      integer  :: M_OPT

      M_OPT = MXRES(MXOPT)
      call RESIZE_MXOPT_DIPOMAT(M_OPT)
      call RESIZE_MXOPT_POLMAT(M_OPT)
      MXOPT = M_OPT
!
      return
      end subroutine RESIZE_MXOPT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPVT
      IMPLICIT NONE

      integer  :: M_OPVT

      M_OPVT = MXRES(MXOPVT)
      call RESIZE_MXOPVT_DIPOMAT(M_OPVT)
      MXOPVT = M_OPVT
!
      return
      end subroutine RESIZE_MXOPVT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXSNV
      IMPLICIT NONE

      integer  :: M_SNV

      M_SNV = MXRES(MXSNV)
      call RESIZE_MXSNV_MATRI(M_SNV)
      MXSNV = M_SNV
!
      return
      end subroutine RESIZE_MXSNV

      end module mod_main_polmat
