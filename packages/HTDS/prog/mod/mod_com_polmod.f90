
      module mod_com_polmod

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer         ,pointer ,dimension(:)     ,save  :: IDANIS                                  ! (MXOCV)
      integer         ,pointer ,dimension(:)     ,save  :: ICODRA                                  ! (MXOPR)
      integer         ,pointer ,dimension(:)     ,save  :: ICODAN                                  ! (MXOPT)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_POLMOD
      IMPLICIT NONE

      integer  :: ierr

      allocate(IDANIS(MXOCV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_POLMOD_IDANIS')
      IDANIS = 0
      allocate(ICODRA(MXOPR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_POLMOD_ICODRA')
      ICODRA = 0
      allocate(ICODAN(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_POLMOD_ICODAN')
      ICODAN = 0
!
      return
      end subroutine ALLOC_POLMOD

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOCV

      subroutine RESIZE_MXOCV_POLMOD(C_OCV)
      IMPLICIT NONE
      integer :: C_OCV

      integer :: ierr

! IDANIS
      allocate(ipd1(C_OCV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOCV_POLMOD_IDANIS')
      ipd1 = 0
      ipd1(1:MXOCV) = IDANIS(:)
      deallocate(IDANIS)
      IDANIS => ipd1
!
      return
      end subroutine RESIZE_MXOCV_POLMOD

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPR

      subroutine RESIZE_MXOPR_POLMOD(C_OPR)
      IMPLICIT NONE
      integer :: C_OPR

      integer :: ierr

! ICODRA
      allocate(ipd1(C_OPR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPR_POLMOD_ICODRA')
      ipd1 = 0
      ipd1(1:MXOPR) = ICODRA(:)
      deallocate(ICODRA)
      ICODRA => ipd1
!
      return
      end subroutine RESIZE_MXOPR_POLMOD

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPT

      subroutine RESIZE_MXOPT_POLMOD(C_OPT)
      IMPLICIT NONE
      integer :: C_OPT

      integer :: ierr

! ICODAN
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_POLMOD_ICODAN')
      ipd1 = 0
      ipd1(1:MXOPT) = ICODAN(:)
      deallocate(ICODAN)
      ICODAN => ipd1
!
      return
      end subroutine RESIZE_MXOPT_POLMOD

      end module mod_com_polmod
