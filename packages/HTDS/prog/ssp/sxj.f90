!
! *** SYMBOLE SIX - J DE SO(3)
!     (ALGORITHME DE ROTENBERG ET AL. - 1959)
!
      FUNCTION SXJ(J1,J2,J3,IL1,IL2,IL3)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: SXJ
      integer          :: J1,J2,J3,IL1,IL2,IL3

! functions
      integer          :: NTR

      real(kind=dppr)  :: PHI,PHIK
      real(kind=dppr)  :: Z,Z0,Z1,Z2,Z3,Z4,ZK

      integer          :: II1,II2,IK
      integer          :: IS1,IS2,IS3,IS4,IS5,ISUP
      integer          :: JTK
      integer          :: N1,N2,N3,N4,N5,N6,N7,N8
!
      SXJ = 0.D0
      Z   = 0.D0
      PHI = (-1)**(J1+J2+IL1+IL2)
      IF( NTR(J1,J2,J3)   .EQ. 0 ) RETURN
      IF( NTR(IL1,IL2,J3) .EQ. 0 ) RETURN
      IF( NTR(J1,IL2,IL3) .EQ. 0 ) RETURN
      IF( NTR(IL1,J2,IL3) .EQ. 0 ) RETURN
      CALL DELTA(J1,J2,J3,Z1)
      CALL DELTA(IL1,IL2,J3,Z2)
      CALL DELTA(IL1,J2,IL3,Z3)
      CALL DELTA(J1,IL2,IL3,Z4)
      Z0   = Z1*Z2*Z3*Z4*PHI
      IS1  = J1+J2+IL1+IL2+1
      IS2  = J1+J2-J3
      IS3  = IL1+IL2-J3
      IS4  = J1+IL2-IL3
      IS5  = IL1+J2-IL3
      II1  = J3+IL3-J1-IL1
      II2  = J3+IL3-J2-IL2
      ISUP = MIN(IS2,IS3,IS4,IS5)
E1:   DO IK=0,ISUP
        PHIK = (-1)**IK
        N1   = IK
        N2   = IS2-IK
        N3   = IS3-IK
        N4   = IS4-IK
        N5   = IS5-IK
        N6   = II1+IK
        N7   = II2+IK
        N8   = IS1-IK
        IF( N6 .LT. 0 ) CYCLE E1
        IF( N7 .LT. 0 ) CYCLE E1
        CALL PROFB(N1,N2,N3,N4,N5,N6,N7,N8,ZK,JTK)
        Z   = ZK*( (10.D0)**JTK )
        SXJ = SXJ+PHIK/Z
      ENDDO E1
      SXJ = SXJ*Z0
!
      RETURN
      END FUNCTION SXJ
