!
! *** MULTIPLICITE DE IC DANS D**J(U OU G)
!
! SMIL H.BERGER DEC 78
!
      FUNCTION NSYM1(J,IUG,IC)
      use mod_par_tds
      IMPLICIT NONE
      integer          :: NSYM1
      integer          :: J,IUG,IC

      integer ,dimension(MXSYM)  :: N
      integer          :: N1,N2,N3,N4,N5
!
      CALL NBJC(J,IUG,N1,N2,N3,N4,N5)
      N(1)  = N1
      N(2)  = N2
      N(3)  = N3
      N(4)  = N4
      N(5)  = N5
      NSYM1 = N(IC)
!
      RETURN
      END FUNCTION NSYM1
