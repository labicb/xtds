!
!  EXTRAIRE, PAR TRI DICHOTOMIQUE, UN K CALCULE PAR CALK.
!  S'IL N'EST PAS DANS LE TABLEAU, ON LE RESTITUE COMME 0.
!  POUR UTILISATION DANS LE PROGRAMME DIP_MATRIX.F
!
! REV    JAN 1995 JPC,CW (PARAMETER)
!
      FUNCTION ODKUB(J2,J3,N2,N3,IC2,IC3)
      use mod_dppr
      use mod_com_ckdi
      use mod_com_ckdipo
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: ODKUB
      integer          :: J2,J3,N2,N3,IC2,IC3

      real(kind=dppr)  :: PHAS

      integer          :: I,I1,I2,ICR2,ICR3,IODKXC
      integer          :: JR3
      integer          :: NR2,NR3
!
      IF( J2 .GT. J3 ) THEN
        JR3  = J2
        NR2  = N3
        NR3  = N2
        ICR2 = IC3
        ICR3 = IC2
        PHAS = -PC(ICR2)*PC(ICR3)
      ELSE
        JR3  = J3
        NR2  = N2
        NR3  = N3
        ICR2 = IC2
        ICR3 = IC3
        PHAS = 1.D0
      ENDIF
      ODKUB  = 0.D0
      IODKXC = 100000*ICR3+10000*ICR2+100*NR3+NR2
      I1     = INJODK(JR3)
      I2     = INJODK(JR3+1)-1
      IF( IODKXC .LT. IODKX(I1) ) RETURN
      IF( IODKXC .GT. IODKX(I2) ) RETURN
      IF( IODKXC .EQ. IODKX(I2) ) GOTO 4
      IF( I1     .EQ. I2        ) RETURN
!
1     I = (I1+I2)/2
      IF    ( IODKXC .EQ. IODKX(I) ) THEN
        GOTO 5
      ELSEIF( IODKXC .GT. IODKX(I) ) THEN
        GOTO 3
      ENDIF
      IF( I2-I1 .EQ. 1 ) RETURN
      I2 = I
      GOTO 1
!
3     IF( I2-I1 .EQ. 1 ) RETURN
      I1 = I
      GOTO 1
!
4     I = I2
!
5     ODKUB = VODK(I)*PHAS
!
      RETURN
      END FUNCTION ODKUB
