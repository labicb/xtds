!
! *** MULTIPLICATION DES PARITES.
!
! V. BOUDON MARS 98.
!
      FUNCTION MULGU(IP1,IP2)
      IMPLICIT NONE
      integer          :: MULGU
      integer          :: IP1,IP2

!
      MULGU = IP1+IP2-1
      IF( MULGU .EQ. 3 ) MULGU = 1
!
      RETURN
      END FUNCTION MULGU
