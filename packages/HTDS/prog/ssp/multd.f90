!
! *** MULTIPLICATION DANS TD.
!
!  SMIL CHAMPION DEC 78
!
      SUBROUTINE MULTD(IC1,IC2,N,IC)
      use mod_par_tds
      IMPLICIT NONE
      integer ,dimension(MDMIGA) :: IC
      integer          :: IC1,IC2,N

      integer          :: I1,I2
!
      IF( IC1 .GT. IC2 ) GOTO 2
      I1 = IC1
      I2 = IC2
      GOTO 3
!
2     I1 = IC2
      I2 = IC1
!
3     IF    ( I1 .EQ. 2 ) THEN
        GOTO 5
      ELSEIF( I1 .GT. 2 ) THEN
        GOTO 11
      ENDIF
      N     = 1
      IC(1) = I2
      RETURN
!
5     N = 1
      IF    ( I2 .EQ. 3 ) THEN
        GOTO 7
      ELSEIF( I2 .GT. 3 ) THEN
        GOTO 8
      ENDIF
      IC(1) = 1
      RETURN
!
7     IC(1) = 3
      RETURN
!
8     IF( I2 .GT. 4 ) GOTO 10
      IC(1) = 5
      RETURN
!
10    IC(1) = 4
      RETURN
!
11    IF( I1 .GE. 4 ) GOTO 15
      IF( I2 .GT. 3 ) GOTO 14
      N     = 3
      IC(1) = 1
      IC(2) = 2
      IC(3) = 3
      RETURN
!
14    N     = 2
      IC(1) = 4
      IC(2) = 5
      RETURN
!
15    N     = 4
      IC(2) = 3
      IC(3) = 4
      IC(4) = 5
      IF( I1 .NE. I2 ) GOTO 17
      IC(1) = 1
      RETURN
!
17    IC(1) = 2
!
      RETURN
      END SUBROUTINE MULTD
