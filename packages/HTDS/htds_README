
*************************
INSTALLATION INSTRUCTIONS
*************************

The present file has been extracted from the archive file by:

      gunzip <HTDS.tar.gz   | tar xvf -
      which creates the ./HTDS   directory.

The procedure

      htds_INSTALL   (or ./htds_INSTALL  )

needs to be executed to customize all executable files and compile all programs.
It updates pathes in the different scripts and runs the compile script.
Once uncompressed and extracted HTDS.tar.gz   can be purged.

The HTDS   package will then be ready for use (see next Section).

NOTES:

> Compilation and execution have been tested on the following UNIX platforms:

      HP, IBM, SUN, Compaq Alpha, Mac G3/G4/G5/Intel.

      The compiling options depend on the compiler type.

      SGI workstations can also run the package
      with a few modifications. Please contact us.

      A PC LINUX version of this package has also been tested and
      is named HTDS_PC.tar.gz.

> For complex band systems a free disk space of several hundreds of
      Megabytes is required.

> To use jobs it is highly recommanded to create first a calculation directory
  outside install directory.

***********
DESCRIPTION
***********

The installation procedure described above creates the following directories:

  HTDS/bin/
  HTDS/ctrp/
  HTDS/exp/
  HTDS/gtd/
  HTDS/jobs/
  HTDS/para/
  HTDS/prog/

- HTDS/bin/    contains the executables (compiled FORTRAN programs only).

- HTDS/ctrp/   contains parameter control files for fits arranged by molecule
               (one sub-directory per molecule).
               Example:

                  HTDS/ctrp/32SF6/CL_freq_2nu3

- HTDS/exp/    contains assignment files arranged by molecule and by transition
               type.
               Example:

                  HTDS/exp/32SF6/dip/ASG_Herman

- HTDS/gtd/    contains necessary coefficients for matrix element calculations.

- HTDS/jobs/   contains UNIX jobs for

               1) calculation of spectra (one directory per molecule),
               2) experimental data fit (examples only in fit_examples).
               3) simulation of synthetic spectra (examples only in simul_examples).

- HTDS/para/   contains Hamiltonian, dipole moment and polarisability parameters
               arranged by molecule.
               Example:

                  HTDS/para/32SF6/Pa_010001m000000

- HTDS/prog/   contains three sub-directories:

               1) exe contains:
                 - compile that compiles all the package programs. After installation,
                   it can be re-runned if different compiling options are needed.
                 - xasg, exasg and passx files that are used by the jobs.

               2) ppr contains the F95 sources of the different main programs.

               3) ssp contains the F95 sources of the different subroutines and functions.

               4) mod contains the F95 sources of the different modules.

************************************
MOLECULES AND BAND SYSTEMS INCLUDED
************************************

Please check the Web site

  http://icb.u-bourgogne.fr/OMR/SMA/SHTDS/HTDS.html

for news and updates.

NOTE: The jobs that can be found in the subdirectories of HTDS/jobs   can be used
      just as they are.

HTDS is described in the following article:

  - Ch. Wenger V. Boudon, J.-P. Champion and G. Pierre, J. Quant. Spectrosc. Radiat. Transfer, 66, 1-16 (2000).

For those who want to create their own calculation and fit jobs, they can modify the given
examples. The HTDS   syntax is explained in the syntax_README file.

********
CONTACTS
********

         G.Pierre, V.Boudon and Ch.Wenger
         E-mail Gerard.Pierre@u-bourgogne.fr
                Vincent.Boudon@u-bourgogne.fr
                Christian.Wenger@u-bourgogne.fr

