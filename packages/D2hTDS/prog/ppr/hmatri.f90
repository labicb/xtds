      PROGRAM HMATRI
!
!  8.7.88 FORTRAN 77 POUR SUN4
!  REV 25 JAN 1990
!  REV 15 FEV 1990
!  REV 27 MAR 1990
!  REV 22 JAN 1992   CH.WENGER  J.P.CHAMPION
!  REV    DEC 1992   T.GABARD
! REV    JAN 1995 JPC,CW (PARAMETER)
! MODIFIE 04/2002 W. RABALLAND ---> C2H4/D2h
!
! ***  HAMILTONIEN ROVIBRATIONNEL EFFECTIF DE C2H4
! ***  RESTRICTION VIBRATIONNELLE D'UNE POLYADE
! ***  CALCUL ET STOCKAGE DES ELEMENTS MATRICIELS NON NULS
! ***  A PARTIR DES DONNEES DU FICHIER ISSU DE HMODEL
!
! APPEL : hmatri Pn Nm Dk Jmax
!
!  ******    LIMITATIONS DU PROGRAMME
!
! DIMENSION MAXIMALE D'UN BLOC J,C
!     MXDIMS              !NRCOD,NVCOD
!
! NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS
!     MXOPH               !ICODR:ICODV:IGAV
!
! NB MAXIMUM D'OPERATEURS VIBRATIONNELS
!     MXOPVH              !LIR:KOR:EMRV
!
! NB MAXIMUM DE SOUS-NIVEAUX VIBRATIONNELS
!     MXSNV               !EMRD
!
! NB MAXIMUM D'ELEMENTS MATRICIELS NON NULS D'UN OPERATEUR
!     MXELMH              !H:LI:KO
!
! NB MAXIMUM D'ELEMENTS MATRICIELS REDUITS NON NULS D'UN OPERATEUR
! VIBRATIONNEL
!     MXEMR               !LIR:KOR:EMRV
!
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_main_hmatri
      IMPLICIT NONE

      integer          :: JCL_SUP,NB_SUP,NFB_SUP
      integer          :: I,IC,ICOO,ICOPH,IDUM,IEL,IGV,IMR
      integer          :: ICP
      integer          :: IO,IOPH,IOV,IP,ISV
      integer          :: J,JC,JL,JMAX,JMTEMP
      integer          :: NEL,NELMA,NFB,NNIV,NSV

      character(len = NBCTIT)  :: IDENT,TITRE
      character(len =  11) ,dimension(3)  :: CARG
      character(len =  40)  :: IDEMR
      character(len = 120)  :: FCFB,FEMRV,FSEM
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1007  FORMAT(4X,I5,5X,I5,4X,F19.13)
1008  FORMAT(I4,A,I8,I7)                                                                           ! cf. 1313 de hmodel.f
1010  FORMAT(/,              &
             'HMATRI : ',A)
1011  FORMAT(' HMATRI -> J = ',I3,'/',I3)
1030  FORMAT(/,     &
             I4,/)
1031  FORMAT(/////,   &
             I5,/  )
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
3000  FORMAT(I2)
8000  FORMAT(' !!! HMATRI : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8113  FORMAT(' !!! INCOMPATIBLE JMAX : ',I3)
8114  FORMAT(' !!! JMAX TOO LARGE'                ,/,   &
             ' !!! MXJ+2  EXCEEDED : ',I8,' > ',I8   )
8119  FORMAT(' !!! UNEXPECTED EOF IN BASIS FUNCTIONS CODE FILE')
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1000,END=9997) FDATE
      PRINT 1010,            FDATE
      DO I=1,3
        READ(10,1000,END=9997) CARG(I)
      ENDDO
      READ(CARG(2)(2:3),3000) NNIV
      READ(10,*,END=9997) JMAX
      FEMRV = 'MH_'//TRIM(CARG(1))//'_'//CARG(3)
      FCFB  = 'FN_'//TRIM(CARG(1))//'_'
      FSEM  = 'HA_'//TRIM(CARG(1))//'_'//TRIM(CARG(3))//'_'
      CLOSE(10)
      PRINT 2000, TRIM(FEMRV)
      PRINT 2000, TRIM(FCFB)
      PRINT 2001, TRIM(FSEM)
      IF( JMAX .LT. 0 ) THEN
        PRINT 8113, JMAX
        GOTO  9999
      ENDIF
!
! APPLICATION DES DIRECTIVES
!
      IF( JMAX .GT. MXJ+2 ) THEN
        PRINT 8114, JMAX,MXJ+2
        GOTO  9999
      ENDIF
      OPEN(20,STATUS='OLD',FILE=FEMRV)                                                             ! FICHIER DES E.M.R.V.
      OPEN(30,STATUS='OLD',FILE=FCFB,FORM='UNFORMATTED')                                           ! FICHIER DE CODES DES FCTS DE BASE
      OPEN(40,FILE=FSEM,FORM='UNFORMATTED',STATUS='UNKNOWN')                                       ! FICHIER DE STOCKAGE DES E.M.
!
! *** LECTURE DES G, CALCUL DES FACTORIELLES, CALCUL DES 6-C
!
      JMTEMP = MAX(12,JMAX)
      CALL FACTO
      CALL CAL6C
!
! LECTURE DES CARACTERISTIQUES GENERALES
!
      DO I=1,3
        READ (20,1000) TITRE
        WRITE(40)      TITRE
        READ(30,END=3994)
      ENDDO
      READ (20,1000) IDEMR
      WRITE(40)      IDEMR
      READ(30,END=3994)
      DO I=1,4+NNIV
        READ (20,1000) TITRE
        WRITE(40)      TITRE
        READ(30,END=3994)
      ENDDO
      READ (20,1001) NSV,TITRE
      WRITE(40)      NSV,TITRE
      READ(30,END=3994)
      READ (20,1000) TITRE
      WRITE(40)      TITRE
      READ(30,END=3994)
      DO ISV=1,NSV
        READ (20,1000) IDENT(:99)
        WRITE(40)      IDENT(:99)
        READ(30,END=3994)
      ENDDO
      DO I=1,3
        READ (20,1000) TITRE
        WRITE(40)      TITRE
      ENDDO
      WRITE(40) ' ROVIBRATIONAL MATRIX ELEMENTS  -  ',FDATE
!
! LECTURE DES ELEMENTS MATRICIELS REDUITS
!
      IOV = 0
!
5     READ(20,1000) TITRE
      IF( TITRE(1:3) .EQ. '   ' ) GOTO 2201
      IOV = IOV+1
      IF( IOV .GT. MXOPVH ) CALL RESIZE_MXOPVH
      READ(20,1031) NEL
      DO WHILE( NEL .GT. MXEMR )
        CALL RESIZE_MXEMR
      ENDDO
!
!      KOR <=> < | BRA
!      LIR <=> | > KET
!
      DO IEL=1,NEL
        READ(20,1007) KOR(IEL,IOV),LIR(IEL,IOV),EMRV(IEL,IOV)
      ENDDO
      DO I=1,4
        READ(20,1000)
      ENDDO
      GOTO 5
!
2201  CALL DEBUG( 'HMATRI => MXOPVH=',IOV)
!
! OPERATEURS DE L'HAMILTONIEN
!
      READ (20,1030) NBOPH
      WRITE(40)      NBOPH,' ROVIBRATIONAL OPERATORS'
      DO WHILE( NBOPH .GT. MXOPH )
        CALL RESIZE_MXOPH
      ENDDO
      DO IOPH=1,NBOPH
        READ(20,1008) IO,IDENT(:NBCLAB-4),ICODR(IOPH),ICODV(IOPH)                                  ! cf. 1313 de hmodel.f
        ICOO       = ICODV(IOPH)
        IGAV(IOPH) = ICOO-(ICOO/10)*10
      ENDDO
!
! ***  BOUCLE J
!
      JCL_SUP = -1
      NB_SUP  = -1
      NFB_SUP = -1
E12:  DO J=0,JMAX
!
! ***  INITIALISATION SYMBOLES K
!
! ***  BOUCLE IC
!
        DO IC=1,MXSYM
E13:      DO ICP=1,2
            READ(30,END=3994) IDUM,IDUM,IDUM,NELMA,NFB
            DO WHILE( NFB .GT. MXDIMS )
              CALL RESIZE_MXDIMS
            ENDDO
            IF( NFB .GT. NFB_SUP ) NFB_SUP = NFB
            WRITE(40) J,IC,ICP,NELMA,NFB
            IF( NFB .EQ. 0 ) CYCLE E13
            READ (30,END=3994) (NVCOD(IP),NRCOD(IP),IP=1,NFB)
            WRITE(40)          (NVCOD(IP),NRCOD(IP),IP=1,NFB)
!
! ***  BOUCLE OP H.
!
E14:        DO IOPH=1,NBOPH
              EMRD = 0.D0
              IOV  = ICODV(IOPH)/1000
              DO IMR=1,MXEMR
                IF( LIR(IMR,IOV) .EQ. 0 ) GOTO 639
!
! *** JL <=> KET
! *** JC <=> BRA
!
                JL = LIR(IMR,IOV)
                JC = KOR(IMR,IOV)
                DO WHILE( JC .GT. MXSNV .OR.    &
                          JL .GT. MXSNV      )
                  CALL RESIZE_MXSNV
                ENDDO
                IF( JC .GT. JCL_SUP ) JCL_SUP = JC
                IF( JL .GT. JCL_SUP ) JCL_SUP = JL
                EMRD(JL,JC) = EMRV(IMR,IOV)
              ENDDO
!
639           IGV   = IGAV(IOPH)
              ICOPH = ICODR(IOPH)
              CALL CALEM(J,IC,NFB,ICOPH,IGV)
              IF( NBELM .GT. NB_SUP ) NB_SUP = NBELM
              WRITE(40) J,IC,ICP,NELMA,NFB,IOPH,NBELM
              IF( NBELM .EQ. 0 ) CYCLE E14
              WRITE(40)(LI(I),KO(I),H(I),I=1,NBELM)
            ENDDO E14
          ENDDO E13
        ENDDO
      ENDDO E12
      CALL DEBUG( 'HMATRI => MXSNV=',JCL_SUP)
      CALL DEBUG( 'HMATRI => MXELMH=',NB_SUP)
      CALL DEBUG( 'HMATRI => MXDIMS=',NFB_SUP)
      CLOSE(20)
      CLOSE(30)
      CLOSE(40)
      PRINT 1011, J-1,JMAX
      GOTO  9000                                                                                   ! BONNE FIN
!
3994  PRINT 8119
      GOTO  9999
9997  PRINT 8002
      CLOSE(10)
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM HMATRI
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! ***  CALCULE LES ELEM. MAT. DES OP. DE H.
!
! SMIL G.P.  J.P.C. JUIL 85
! MOD. DEC 92 T.GABARD
!
      SUBROUTINE CALEM(J,IC,NFB,ICOR,IGV)
      use mod_dppr
      use mod_par_tds
      use mod_com_const
      use mod_com_sy
      use mod_main_hmatri
      IMPLICIT NONE
      integer          :: J,IC,NFB,ICOR,IGV

! functions
      real(kind=dppr)  :: EMRRO,SXC

      real(kind=dppr)  :: AK
      real(kind=dppr)  :: COEF
      real(kind=dppr)  :: FROT
      real(kind=dppr)  :: HH
      real(kind=dppr)  :: RMUN
      real(kind=dppr)  :: S

      integer          :: I1,I2,IBRA,ICR1,ICR2,IG
      integer          :: IKET,IO,IP1,IP2,IV1,IV2
      integer          :: KR,KRNGIG,KSI
      integer          :: NG,NR1,NR2,NRC1,NRC2,NV1,NV2
!
      RMUN = -1.D0
      COEF =  1.D0
!
! IO = DEGRE OP. ROTATIONNEL
!
      IO     = ICOR/1000
      KRNGIG = ICOR-IO*1000
!
! KR = RANG OP. ROTATIONNEL
!
      KR = KRNGIG/100
!
! IG = SYMETRIE OP. ROTATIONNEL
!
      IG = (KRNGIG-KR*100)/10
      IF( KR .NE. 0 ) GOTO 6
!
! RENORMALISATION DE R(OMEGA,0,N,GAMMA)
!
      COEF = SQRT(DC(IGV))*(COKG**(IO/2))
!
6     NG = KRNGIG-KR*100-IG*10
!
! NG = INDICE DE MULTIPLICITE OP. ROTATIONNEL
!
      FROT  = EMRRO(IO,KR,J)
      NBELM = 0
!
! IJ,IVJ,NRJ,ICRJ (J=1,2) : NUM. SS NIVEAU VIBRATIONNEL, SYMETRIE VIBR-
! ATIONNELLE, INDICE DE MULTIPLICITE ET SYMETRIE ROTATIONNELLE DES KET
! ET BRAS
!
E3:   DO IKET=1,NFB
        NV1  = NVCOD(IKET)
        I1   = NV1/100
        IP1  = NV1-100*I1
        IV1  = IP1/10
        IP1  = IP1-10*IV1
        NRC1 = NRCOD(IKET)
        NR1  = NRC1/10
        ICR1 = NRC1-10*NR1
E4:     DO IBRA=1,IKET
          NV2 = NVCOD(IBRA)
          I2  = NV2/100
          IF( EMRD(I1,I2) .EQ. 0.D0 ) CYCLE E4
          IP2  = NV2-100*I2
          IV2  = IP2/10
          IP2  = IP2-10*IV2
          NRC2 = NRCOD(IBRA)
          NR2  = NRC2/10
          ICR2 = NRC2-10*NR2
          S    = SXC(IV2,ICR2,IC,ICR1,IV1,IG)
          KSI  = 1
          IF( ICR2 .EQ. 2 .OR.             &
              ICR2 .EQ. 4      ) KSI = -1
          CALL KCUBU(KR,J,J,NG,NR1,NR2,IG,ICR1,ICR2,AK)
          HH = FROT*PC(IC)*EMRD(I1,I2)*PC(IV1)*S*AK*PC(IG)*KSI
          IF( HH    .EQ. 0.D0   ) CYCLE E4
          NBELM = NBELM+1
          IF( NBELM .GT. MXELMH ) CALL RESIZE_MXELMH
          LI(NBELM) = IKET
          KO(NBELM) = IBRA
          H(NBELM)  = HH*COEF
        ENDDO E4
      ENDDO E3
!
      RETURN
      END SUBROUTINE CALEM
