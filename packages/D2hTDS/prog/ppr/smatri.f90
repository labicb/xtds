      PROGRAM SMATRI
!
!  2007 FORTRAN 77
!  04/2010 M.SANZHAROV ---> C2H4/D2h.
!
! *****  CALCUL DES CORRECTIONS DE STARK  *****
!
!
!  ******    LIMITATIONS DU PROGRAMME
!
! DIMENSION MAXIMALE D'UN BLOC J,C
!     MXDIMS      !NVCOD:NRCOD:K:T:H:HL:HC:HD:TC:TL
!
! NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS
!     MXOPH       !PARA
!
! NB MAXIMUM D'ELEMENTS MATRICIELS NON NULS D'UN BLOC J,C
!     MXELMD      !LI:KO:EL
!
! VALEUR MAXIMALE DE J
!     MXJS
! NB MAXIMUM DE SOUS-NIVEAUX VIBRATIONNELS
!     MXSNV
!
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_main_smatri
      IMPLICIT NONE

! functions
      real(kind=dppr)  :: TROIJ

      real(kind=dppr)  :: HST,HST1,HST2

      integer          :: I,I16,IALB,IALK,IC,ICILU,ICP,ICPILU,ICPCSLU,ICPSLU,ICS,ICSLU
      integer          :: IGAM,ILG,IM,ING,IOP,IOPLU,IP,ISVI,ISVS
      integer          :: JB,JI,JILU,JK,JMAX,JS,JSLU
      integer          :: NBELM,NBOTR,NELMALU,NFBILU,NFBSLU,NNIVI,NNIVS,NSV,NSVI,NSVS

      character(len = NBCTIT)  :: IDENT,TITRE
      character(len =  11) ,dimension(5)  :: CARG
      character(len = 120)  :: FCFB,FED11,FPARA,FSEM,NOM
!
1000  FORMAT(A)
1036  FORMAT(I2)
1020  FORMAT(/,                 &
             'SMATRI    : ',A)
1021  FORMAT(' SMATRI      -> J = ',I3,'/',I3)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
8000  FORMAT(' !!! SMATRI : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8025  FORMAT(' !!! MXJST  EXCEEDED : ',I8,' > ',I8)
8128  FORMAT(' !!! UNEXPECTED EOF IN WAVEFUNCTION FILE')
8031  FORMAT(' !!! MXDIMS EXCEEDED : ',I8,' > ',I8)
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1000,END=9997) FDATE
      PRINT 1020,            FDATE
      DO I=1,5
        READ(10,1000,END=9997) CARG(I)
      ENDDO
      READ(CARG(2)(2:3),1036) NNIVS
      READ(CARG(4)(2:3),1036) NNIVI
      READ(10,*,END=9997) JMAX
      READ(10,1000,END=9997) FPARA
      FSEM  = 'PO_'//TRIM(CARG(1))//'m'//TRIM(CARG(3))//'_'//TRIM(CARG(5))//'_'
      FED11 = 'P'//TRIM(CARG(1))//'_st'
      FCFB  = 'FN_'//TRIM(CARG(1))//'_'
      PRINT 2000, TRIM(FSEM)
      PRINT 2000, TRIM(FPARA)
      PRINT 2000, TRIM(FCFB)
      PRINT 2001, TRIM(FED11)
!
! APPLICATION DES DIRECTIVES
!
      IF( JMAX .GT. MXJST )THEN
        PRINT 8025, JMAX,MXJST
        GOTO  9999
      ENDIF
      OPEN(20,FILE=FSEM,STATUS='OLD',FORM='UNFORMATTED')                                           ! ELEMENTS MATRICIELS
      OPEN(30,FILE=FPARA,STATUS='OLD')                                                             ! PARAMETRES
      OPEN(40,FILE=FCFB,STATUS='OLD',FORM='UNFORMATTED')
      OPEN(50,FILE=FED11,STATUS='UNKNOWN',FORM='UNFORMATTED')
      CALL FACTO
!
! LECTURE DES PARAMETRES
!
      CALL PARAM(30)
      CLOSE(30)
!
! LECTURE DES CARACTERISTIQUES GENERALES
!
      DO I=1,3
        READ(20) TITRE
        READ(40) TITRE
      ENDDO
      READ(20)
      READ(40)
      DO I=1,4+NNIVI+NNIVS
        READ(20) TITRE
      ENDDO
      DO I=1,4+NNIVI
        READ(40) TITRE
      ENDDO
!
! CARACTERISTIQUES DE LA POLYADE SUPERIEURE
!
      READ(20) NSVS,TITRE
      READ(40) NSV,TITRE
!
! SOUS-NIVEAUX VIB DE LA POLYADE SUPERIEURE
!
      READ(40) TITRE
      DO ISVS=1,NSVS
        READ(20) IDENT(:99)
        READ(40) IDENT(:99)
      ENDDO
!
! CARACTERISTIQUES DE LA POLYADE INFERIEURE
!
      READ(20) NSVI,TITRE
!
! SOUS-NIVEAUX VIB DE LA POLYADE INFERIEURE
!
      DO ISVI=1,NSVI
        READ(20) IDENT(:99)
      ENDDO
!
!  ORDRE DU DEVELOPPEMENT
!
      DO I=1,3
        READ(20) TITRE
      ENDDO
      READ(20) TITRE(:67)
      READ(20) NBOTR,NOM(:24)
!
! BOUCLE J
!
      DO JS=0,JMAX
!
! BOUCLE IC
!
        DO IC=1,MXSYM
E1:       DO ICP=1,2
            READ(40) JSLU,ICSLU,ICPCSLU,NELMALU,NFBSLU
            READ(20) JSLU,ICSLU,ICPSLU,NFBSLU
            DO WHILE( NFBSLU .GT. MXDIMS )
              CALL RESIZE_MXDIMS
            ENDDO
            INFB(JSLU,ICSLU,ICPSLU) = NFBSLU
            ICROT(1)                = 0
            IF( NFBSLU .NE. 0 ) THEN
              READ(40) (NVCOD(IP),NRCOD(IP),IP=1,NFBSLU)
              DO IP=1,NFBSLU
                ICROT(IP) = NRCOD(IP)-(NRCOD(IP)/10)*10
              ENDDO
            ENDIF
            IF( NFBSLU .EQ. 0      ) CYCLE E1
            IF( NFBSLU .GT. MXDIMS ) THEN
              PRINT 8031, NFBSLU,MXDIMS
              GOTO  9999
            ENDIF
E14:        DO JI=MAX(JS-2,0),JS+2
              READ(20) JSLU,ICSLU,ICPSLU,NFBSLU,  &
                       JILU,ICILU,ICPILU,NFBILU
              IF( NFBILU .EQ. 0 ) CYCLE E14
E15:          DO IOP=1,NBOTR
                READ(20) JSLU,ICSLU,ICPSLU,NFBSLU,  &
                         JILU,ICILU,ICPILU,NFBILU,  &
                         IOPLU,NBELM,ILG,ING,IGAM
                DO WHILE( NBELM .GT. MXELMT )
                  CALL RESIZE_MXELMT
                ENDDO
                IF( NBELM .EQ. 0 ) CYCLE E15
                READ(20) (LI(I),KO(I),H(I),I=1,NBELM)
                DO I16=1,NBELM
                  DO WHILE( LI(I16) .GT. MXDIMS .OR.    &
                            KO(I16) .GT. MXDIMS      )
                    CALL RESIZE_MXDIMS
                  ENDDO
                ENDDO
                DO I16=1,NBELM
                  IF( ILG .EQ. 0 ) THEN
                    P0(JSLU,JILU,ICSLU,ICPSLU,LI(I16),KO(I16)) =                   &
                    P0(JSLU,JILU,ICSLU,ICPSLU,LI(I16),KO(I16))+H(I16)*PARA(IOPLU)
                  ENDIF
                  IF( ILG .EQ. 2 ) THEN
                    P2(ING,JSLU,JILU,ICSLU,ICPSLU,LI(I16),KO(I16)) =                   &
                    P2(ING,JSLU,JILU,ICSLU,ICPSLU,LI(I16),KO(I16))+H(I16)*PARA(IOPLU)
                  ENDIF
                ENDDO
              ENDDO E15
            ENDDO E14
          ENDDO E1
        ENDDO
      ENDDO
!
! CALCUL DES ELEMENTS MATRICIELS STARK
!
      DO ICS=1,MXSYM
        DO ICP=1,2
          DO IM=0,JMAX
            DO JB=IM,JMAX
              DO IALB=1,INFB(JB,ICS,ICP)
                DO JK=IM,JMAX
                  DO IALK=1,INFB(JK,ICS,ICP)
                    HST1 = P0(JK,JB,ICS,ICP,IALB,IALK)*TROIJ(JK,0,JB,-IM,0,IM)
                    HST1 = HST1*(-1)**(JB-IM)/SQRT(3.D0)
                    HST2 = P2(0,JK,JB,ICS,ICP,IALB,IALK)+P2(1,JK,JB,ICS,ICP,IALB,IALK)
                    HST2 = HST2*TROIJ(JK,2,JB,-IM,0,IM)
                    HST2 = HST2*(-1)**(JB-IM)*SQRT(2.D0/3.D0)
                    HST  = (HST2-HST1)*5.034036D+26
                    WRITE(50) HST
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      PRINT 1021, JS-1,JMAX
      GOTO 9000
!
9997  PRINT 8002
      CLOSE(10)
      PRINT 8128
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
9000  PRINT *
      END PROGRAM SMATRI
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! LECTURE DES PARAMETERS
!
      SUBROUTINE PARAM(LUI)
      use mod_dppr
      use mod_par_tds
      use mod_com_smatri
      IMPLICIT NONE
      integer          :: LUI

      real(kind=dppr)  :: PARH,PREC

      integer          :: IP,IPAL,IR,IR2
      integer          :: NBOAL,NBOH

      character(len = NBCTIT)     :: TITRE
      character(len = NBCLAB+10)  :: CHAINE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1005  FORMAT(A,E18.11,E14.7)
!
      DO IR=1,4
        READ(LUI,1000) TITRE
      ENDDO
      READ(LUI,1001) NBOH,TITRE
      READ(LUI,1000) TITRE
      READ(LUI,1000) TITRE
      DO IP=1,NBOH
        READ(LUI,1005) CHAINE,PARH,PREC
      ENDDO
      DO IR2=1,4
        READ(LUI,1000) TITRE
      ENDDO
      READ(LUI,1001) NBOAL
      READ(LUI,1000) TITRE
      READ(LUI,1000) TITRE
      DO IPAL=1,NBOAL
        READ(LUI,1005) CHAINE,PARA(IPAL),PREC
      ENDDO
      RETURN
      END SUBROUTINE PARAM
