!
! *** SYMBOLES "3J".
!     SYMBOLES "V BARRE" DE FANO ET RACAH (1959)
!
! SMIL M. LOETE DEC 78
!
      FUNCTION TROIJ(J1,J2,J3,M1,M2,M3)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: TROIJ
      integer          :: J1,J2,J3,M1,M2,M3

      real(kind=dppr)  :: A
      real(kind=dppr)  :: C
      real(kind=dppr)  :: T
      real(kind=dppr)  :: X
      real(kind=dppr)  :: Y
      real(kind=dppr)  :: Z

      integer          :: I
      integer          :: K,KT,KX,KY,KZ
      integer          :: L,L1,L2,L3,L4,L5,L6,L7,L8,L9
!
      IF( M1+M2+M3 .EQ. 0 ) GOTO 12
!
11    TROIJ = 0.D0
      RETURN
!
12    L1 = J1+J2-J3
      L2 = J1-J2+J3
      L3 = J2-J1+J3
      L4 = J1+J2+J3+1
      CALL PROFB(L1,L2,L3,0,0,0,0,L4,Z,KZ)
      IF( Z .EQ. 0.D0 ) GOTO 11
      CALL PROFB(J1-M1,J2-M2,J3-M3,J1+M1,J2+M2,J3+M3,0,1,Y,KY)
      IF( Y .EQ. 0.D0 ) GOTO 11
      T  = SQRT(Z*Y)
      KT = (KZ+KY)/2
      I  = MAX(0,J2-J3-M1,J1-J3+M2)
      L  = MIN(J1+J2-J3,J1-M1,J2+M2)
      C  = 0.D0
      A  = (-1)**(J3+M3+I+1)
      DO K=I,L
        A  = -A
        L5 = J1+J2-J3-K
        L6 = J1-M1-K
        L7 = J3-J2+M1+K
        L8 = J2+M2-K
        L9 = J3-J1-M2+K
        CALL PROFB(K,L5,L6,L7,L8,L9,0,1,X,KX)
        C = C+(A*T)/X*( (10.D0)**(KT-KX) )
      ENDDO
      TROIJ = C
!
      RETURN
      END FUNCTION TROIJ
