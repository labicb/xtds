!
!     RMS for frequencies
!
      FUNCTION RMSH(M,ISEL)
      use mod_dppr
      use mod_par_tds
      use mod_par_x
      use mod_com_xassi
      use mod_com_xfonc
      IMPLICIT NONE
      real(kind=dppr)        :: RMSH
      integer                :: M
      integer ,dimension(M)  :: ISEL

! functions
      real(kind=dppr)  :: ENORM

      real(kind=dppr) ,dimension(MXOBZ)  :: FVECA

      integer          :: I
      integer          :: J
!
      DO I=1,M
        J        = ISEL(I)
        FVECA(I) = FOMC(J)/ZWGT(J)
      ENDDO
      RMSH = ENORM(M,FVECA)
      RMSH = RMSH/SQRT(DBLE(M))
!
      RETURN
      END FUNCTION RMSH
