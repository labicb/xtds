!
! *** SYMBOLES "3J" CUBIQUES.
!
! SMIL CHAMPION DEC 78
!
! MODIFIE 03/2002 W. RABALLAND ---> D2H/C2H4
!
      SUBROUTINE FCUBU(J1,J2,J3,N1,N2,N3,ICC1,ICC2,ICC3,F)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: F
      integer          :: J1,J2,J3,N1,N2,N3,ICC1,ICC2,ICC3

! functions
      real(kind=dppr)  :: TROIJ
      integer          :: NABSM

      real(kind=dppr)  :: GJ1,GJ2,GJ3

      integer          :: KN1,KN2
      integer          :: MM1,MM2,MM3
      integer          :: NN1,NN2,NN3
!
      F   = 0.D0
      NN1 = NABSM(J1,N1,ICC1)
      NN2 = NABSM(J2,N2,ICC2)
      NN3 = NABSM(J3,N3,ICC3)
      KN1 = 2*NN1
      KN2 = 2*NN2
      IF( KN1 .EQ. 0 ) KN1 = 1
      IF( KN2 .EQ. 0 ) KN2 = 1
      DO MM1=-NN1,NN1,KN1
        DO MM2=-NN2,NN2,KN2
          MM3 = -MM1-MM2
          CALL GJCMS(J1,MM1,NN1,ICC1,GJ1)
          CALL GJCMS(J2,MM2,NN2,ICC2,GJ2)
          CALL GJCMS(J3,MM3,NN3,ICC3,GJ3)
          F = F+GJ1*GJ2*GJ3*TROIJ(J1,J2,J3,MM1,MM2,MM3)
        ENDDO
      ENDDO
      RETURN
      END SUBROUTINE FCUBU
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      FUNCTION NABSM(JJJ,NNN,ICCC)
      IMPLICIT NONE
      integer          :: NABSM
      integer          :: JJJ,NNN,ICCC

!
      NABSM = 0
      IF( ICCC .EQ. 1 ) NABSM = 2*(NNN+MOD(JJJ,2))
      IF( ICCC .EQ. 2 ) NABSM = 2*(NNN+(1-MOD(JJJ,2)))
      IF( ICCC .EQ. 3 ) NABSM = 2*NNN+1
      IF( ICCC .EQ. 4 ) NABSM = 2*NNN+1
!
      RETURN
      END FUNCTION NABSM
