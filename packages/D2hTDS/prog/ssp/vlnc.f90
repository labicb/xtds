!
!  VLNC MODIFIE MARS 02 ---> C2H4/D2h (W. RABALLAND).
!
      SUBROUTINE VLNC(ICO,IV,IC,IP)
      IMPLICIT NONE
      integer          :: ICO,IV,IC,IP

!
      IV = ICO/100
      IC = (ICO-100*IV)/10
      IP = MOD(ICO,10)
!
      RETURN
      END SUBROUTINE VLNC
