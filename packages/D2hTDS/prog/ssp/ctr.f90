!
! *** RELATION TRIANGULAIRE DANS D2 : CTR=1 SI OUI,=0 SI NON
!
! SMIL CHAMPION DEC 78
! MODIFIE 03/02 W. RABALLAND ---> C2H4/D2h
!
      FUNCTION CTR(C1,C2,C3)
      IMPLICIT NONE
      integer          :: CTR
      integer          :: C1,C2,C3

      integer          :: C
      integer          :: NDUM
!
      CTR = 0
      CALL MULD2H(C1,C2,NDUM,C)
      IF( C .EQ. C3 ) CTR = 1
!
      RETURN
      END FUNCTION CTR
