
      module mod_com_spects

      use mod_dppr
      IMPLICIT NONE

      integer ,pointer ,dimension(:)    :: ISUMS
      integer ,pointer ,dimension(:,:)  :: ICODEMS

      real(kind=dppr) ,save  :: BS     = 0.d0
      real(kind=dppr) ,save  :: COEF   = 0.d0
      real(kind=dppr) ,save  :: EN0    = 0.d0
      real(kind=dppr) ,save  :: ESUP   = 0.d0
      real(kind=dppr) ,save  :: FMIN   = 0.d0
      real(kind=dppr) ,save  :: FMAX   = 0.d0
      real(kind=dppr) ,save  :: RINMI  = 0.d0
      real(kind=dppr) ,save  :: TROT   = 0.d0

      integer         ,save  :: I110   = 0
      integer         ,save  :: IBS    = 0
      integer         ,save  :: ICI    = 0
      integer         ,save  :: ICODES = 0
      integer         ,save  :: ICPI   = 0
      integer         ,save  :: ICPS   = 0
      integer         ,save  :: ICS    = 0
      integer         ,save  :: ILIGS  = 0
      integer         ,save  :: IMS    = 0
      integer         ,save  :: INCT   = 0
      integer         ,save  :: JS     = 0

      end module mod_com_spects
