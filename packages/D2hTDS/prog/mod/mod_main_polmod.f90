
      module mod_main_polmod

      use mod_par_tds
      use mod_com_dipomod
      use mod_com_elmr
      use mod_com_model
      use mod_com_polmod

      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_dipomod
      call alloc_elmr
      call alloc_model
      call alloc_polmod
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXEMR
      IMPLICIT NONE

      integer  :: M_EMR

      M_EMR = MXRES(MXEMR)
      call RESIZE_MXEMR_MODEL(M_EMR)
      MXEMR = M_EMR
!
      return
      end subroutine RESIZE_MXEMR

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOCV
      IMPLICIT NONE

      integer  :: M_OCV

      M_OCV = MXRES(MXOCV)
      call RESIZE_MXOCV_MODEL(M_OCV)
      call RESIZE_MXOCV_POLMOD(M_OCV)
      MXOCV = M_OCV
!
      return
      end subroutine RESIZE_MXOCV

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPR
      IMPLICIT NONE

      integer  :: M_OPR

      M_OPR = MXRES(MXOPR)
      call RESIZE_MXOPR_MODEL(M_OPR)
      call RESIZE_MXOPR_POLMOD(M_OPR)
      MXOPR = M_OPR
!
      return
      end subroutine RESIZE_MXOPR

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPT
      IMPLICIT NONE

      integer  :: M_OPT

      M_OPT = MXRES(MXOPT)
      call RESIZE_MXOPT_DIPOMOD(M_OPT)
      call RESIZE_MXOPT_POLMOD(M_OPT)
      MXOPT = M_OPT
!
      return
      end subroutine RESIZE_MXOPT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXNIV
      IMPLICIT NONE

      integer  :: M_NIV

      M_NIV = MXRES(MXNIV)
      call RESIZE_MXNIV_DIPOMOD(M_NIV)
      MXNIV = M_NIV
!
      return
      end subroutine RESIZE_MXNIV

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXSNV
      IMPLICIT NONE

      integer  :: M_SNV

      M_SNV = MXRES(MXSNV)
      call RESIZE_MXSNV_DIPOMOD(M_SNV)
      call RESIZE_MXSNV_ELMR(M_SNV)
      MXSNV = M_SNV
!
      return
      end subroutine RESIZE_MXSNV

      end module mod_main_polmod
