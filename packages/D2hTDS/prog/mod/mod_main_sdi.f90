
      module mod_main_sdi

      use mod_dppr
      use mod_par_tds
      use mod_com_sdi


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_sdi
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXELMH
      IMPLICIT NONE

      integer  :: M_ELMH

      M_ELMH = MXRES(MXELMH)
      call RESIZE_MXELMH_SDI(M_ELMH)
      MXELMH = M_ELMH
!
      return
      end subroutine RESIZE_MXELMH

      end module mod_main_sdi
