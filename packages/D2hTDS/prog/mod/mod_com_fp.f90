
      module mod_com_fp

      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE

      real(kind=dppr) ,dimension(NBVQN) ,save  :: VIBNU
      real(kind=dppr)                   ,save  :: ABUND
      real(kind=dppr)                   ,save  :: SPINY
      real(kind=dppr)                   ,save  :: FPVIB,AA,BB,CC,B0,D0


      contains

      function FPART(TROT,TVIB)
      use mod_dppr
      use mod_par_tds
      use mod_com_const
      IMPLICIT NONE
      real(kind=dppr)  :: FPART
      real(kind=dppr)  :: TROT,TVIB

      real(kind=dppr)  :: A
      real(kind=dppr)  :: B,BCR,BETA
      real(kind=dppr)  :: FRMD
      real(kind=dppr)  :: SIG
      real(kind=dppr)  :: X

      integer ,dimension(NBVQN)  :: IDEG = (/ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 /)
      integer          :: I
!
      A = PLANK*CLUM/BOLT
      IF( FPVIB .EQ. 0.D0 ) THEN
        FPVIB = 1.D0
        DO I=1,NBVQN
          B     = 1.D0-EXP(-VIBNU(I)*A/TVIB)
          B     = B**IDEG(I)
          FPVIB = FPVIB/B
        ENDDO
      ENDIF
!
! *** PARTITION FUNCTION
!
      BETA = PLANK*CLUM/BOLT/TROT
      SIG  = ( (2.D0*SPINY+1.D0)**4 )/4.D0
      BCR  = SQRT(BB*CC)
      X    = (1.D0-BCR/AA)*BCR*BETA
      FRMD = SIG*EXP(BCR*BETA/4.D0)*SQRT(PI/( BETA**3 )/AA/BB/CC)
      FRMD = FRMD*(1.D0+X/12.D0+7.D0*X*X/480.D0)
      FPART = FRMD*FPVIB
!
      RETURN
      end function FPART

      end module mod_com_fp
