!
! ***** Calcul des 3 - C orientes dans C4v
!
! ***** M. Rotger 12/98
!
      FUNCTION TRCO4(IC1,IC2,IC3,IP1,IP2,IP3,ICT1,ICT2,ICT3,IST1,IST2,IST3)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: TRCO4
      integer          :: IC1,IC2,IC3,IP1,IP2,IP3,ICT1,ICT2,ICT3,IST1,IST2,IST3

! function
      real(kind=dppr)  :: CGC4V,TROIC
      integer          :: CTR,CTRC4V
      integer          :: ISCTC
      integer          :: MULGU

      integer ,parameter  :: IMIN = 1
      integer ,parameter  :: IMAX = 20

      real(kind=dppr)  :: CGC4V1,CGC4V2,CGC4V3
      real(kind=dppr)  :: F

      integer ,dimension(IMAX) :: IVALB  = (/ 1, 1, 1, 2, 3, 2, 1, 3, 2, 1,    &
                                              1, 1, 1, 2, 3, 1, 2, 3, 1, 2 /)
      integer ,dimension(3)    :: IS
      integer ,dimension(3,4)  :: ITB
      integer ,dimension(IMAX) :: ICODEB = (/ 1111, 1231, 1311, 1331, 1421,    &
                                              1451, 1452, 1541, 1551, 1552,    &
                                              2121, 2241, 2321, 2341, 2411,    &
                                              2451, 2452, 2531, 2551, 2552 /)
      integer          :: I,I1,I2,IC,ICODB,ICT,IP,IP3C,IST,ITOTO,ITRI
      integer          :: J
!
      TRCO4    = 0.D0
      ITB(1,1) = IC1
      ITB(1,2) = IP1
      ITB(1,3) = ICT1
      ITB(1,4) = IST1
      ITB(2,1) = IC2
      ITB(2,2) = IP2
      ITB(2,3) = ICT2
      ITB(2,4) = IST2
      ITB(3,1) = IC3
      ITB(3,2) = IP3
      ITB(3,3) = ICT3
      ITB(3,4) = IST3
      TRCO4    = 0.D0
      DO J=1,3
        IC    = ITB(J,1)
        IP    = ITB(J,2)
        ICT   = ITB(J,3)
        IST   = ITB(J,4)
        I1    = IMIN
        I2    = IMAX
        ICODB = 1000*IP+100*IC+10*ICT+IST
        IF( ICODB .LT. ICODEB(I1) ) RETURN
        IF( ICODB .GT. ICODEB(I2) ) RETURN
        IF( ICODB .EQ. ICODEB(I2) ) GOTO 12
!
1       I = (I1+I2)/2
        IF    ( ICODB .EQ. ICODEB(I) ) THEN
          GOTO 7
        ELSEIF( ICODB .GT. ICODEB(I) ) THEN
          GOTO 3
        ENDIF
        IF( I2-I1 .EQ. 1 ) GOTO 50
        I2 = I
        GOTO 1
!
3       IF( I2-I1 .EQ. 1 ) GOTO 50
        I1 = I
        GOTO 1
!
12      I     = I2
7       IS(J) = IVALB(I)
      ENDDO
!
50    ITRI = CTRC4V(ITB(1,3),ITB(2,3),ITB(3,3))
      IF( ITRI .LE. 0 ) RETURN
      ITRI = CTR(ITB(1,1),ITB(2,1),ITB(3,1))
      IF( ITRI .LE. 0 ) RETURN
      IP3C = MULGU(ITB(1,2),ITB(2,2))
      IF( IP3C .NE. ITB(3,2) ) RETURN
      F      = TROIC(ITB(1,1),ITB(2,1),ITB(3,1),IS(1),IS(2),IS(3))
      CGC4V1 = 0.D0
      ITOTO  = ISCTC(ITB(1,1),ITB(1,2),ITB(1,3))
      IF( ITOTO .EQ. 0 ) RETURN
      CGC4V1 = CGC4V(ITB(1,1),ITB(1,2),IS(1),ITB(1,3),ITB(1,4))
      CGC4V2 = 0.D0
      ITOTO  = ISCTC(ITB(2,1),ITB(2,2),ITB(2,3))
      IF( ITOTO .EQ. 0 ) RETURN
      CGC4V2 = CGC4V(ITB(2,1),ITB(2,2),IS(2),ITB(2,3),ITB(2,4))
      CGC4V3 = 0.D0
      ITOTO  = ISCTC(ITB(3,1),ITB(3,2),ITB(3,3))
      IF( ITOTO .EQ. 0 ) RETURN
      CGC4V3 = CGC4V(ITB(3,1),ITB(3,2),IS(3),ITB(3,3),ITB(3,4))
      TRCO4  = CGC4V1*CGC4V2*CGC4V3*F
!
      RETURN
      END FUNCTION TRCO4
