      FUNCTION NSYM2(J,IUG,ICC4V)
      use mod_par_tds
      use mod_com_sy
      IMPLICIT NONE
      integer          :: NSYM2
      integer          :: J,IUG,ICC4V

      integer          :: IOHC4
      integer          :: JC4V
!
      NSYM2 = 0
      DO JC4V=1,2
        IOHC4 = IC4VV(J,IUG,JC4V)
        IF( IOHC4 .EQ. ICC4V ) THEN
          NSYM2 = 1
          RETURN
        ENDIF
      ENDDO
!
      RETURN
      END FUNCTION NSYM2
