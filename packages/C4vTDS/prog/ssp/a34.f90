!
!  VB CW NOV 2009
!
      FUNCTION A34(IPM,VP,LP,NP,CP,K,GAMA,V,L,N,C,IO3TD)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      real(kind=dppr)  :: A34
      integer          :: IPM,VP,LP,NP,CP,K,GAMA,V,L,N,C,IO3TD

! functions
      real(kind=dppr)  :: A31,A32,A33,SXJ,XMKUB
      integer          :: IPSI

      real(kind=dppr)  :: AK
      real(kind=dppr)  :: E

      integer          :: CS
      integer          :: GAMAI,GAMAJ
      integer          :: IUG,IUGP
      integer          :: KI,KJ
      integer          :: LD,LS
      integer          :: VS
!
      A34 = 0.D0
      IF( IO3TD              .NE. 0 .AND.           &
          IPSI(3,4,K,0,GAMA) .EQ. 0       ) RETURN
      IF( IPM*(VP-V)         .NE. 4       ) RETURN
      IF    ( K .EQ. 0 ) THEN
        KI = 0
        KJ = 0
        VS = V+IPM*2                                                                               ! 2 = OMEGA
      ELSEIF( K .EQ. 2 ) THEN
        KI = 0
        KJ = 2
        VS = V+IPM*2                                                                               ! 2 = OMEGA
      ELSE                                                                                         ! K = 4
        KI = 3
        KJ = 1
        VS = V+IPM*1                                                                               ! 1 = OMEGA
      ENDIF
      GAMAI = 1                                                                                    ! fictive value (unused)
      GAMAJ = 1                                                                                    ! fictive value (unused)
E2:   DO LD=-KJ,KJ,2
        LS = L+LD
        DO CS=1,MXSYM
          IF( IPSI(3,VS,LS,0,CS) .NE. 0 ) GOTO 20
        ENDDO
        CYCLE E2
!
20      IF    ( K .EQ. 0 ) THEN
          E = A32(IPM,VP,LP,NP,CP,KI,GAMAI,VS,LS,0,CS,1)**2
        ELSEIF( K .EQ. 2 ) THEN
          E = A32(IPM,VP,LP,NP,CP,KI,GAMAI,VS,LS,0,CS,1)*  &
              A32(IPM,VS,LS,0,CS,KJ,GAMAJ,V,L,N,C,1)
        ELSE                                                                                       ! K = 4
          E = A33(IPM,VP,LP,NP,CP,KI,GAMAI,VS,LS,0,CS,1)*  &
              A31(IPM,VS,LS,0,CS,V,L,N,C,1)
        ENDIF
        IF( E .EQ. 0.D0 ) CYCLE E2
        A34 = A34+E*( (-1)**(L+LP+KI+KJ) )*SXJ(KI,KJ,K,L,LP,LS)
      ENDDO E2
      A34 = A34*SQRT(2.D0*K+1.D0)
      IF( IO3TD .NE. 0 ) RETURN
      IUG  = MOD(L,2)+1
      IUGP = MOD(LP,2)+1
      AK   = XMKUB(K,L,LP,1,IUG,IUGP,0,N,NP,GAMA,C,CP)
      A34  = ( (-1)**LP )*AK*A34
!
      RETURN
      END FUNCTION A34
