!
! ***** Calcul des G orientes dans C4v
!
! ***** M. Rotger 12/98
!
      FUNCTION CGC4V(IC,IP,IS,ICT,IST)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: CGC4V
      integer          :: IC,IP,IS,ICT,IST

      integer ,parameter  :: IMIN = 1
      integer ,parameter  :: IMAX = 20

      integer ,dimension(IMAX)  :: ICODE  = (/ 11111, 12131, 13111, 13231, 14152,     &
                                               14251, 14321, 15152, 15251, 15341,     &
                                               21121, 22141, 23121, 23241, 24151,     &
                                               24252, 24311, 25151, 25252, 25331  /)
      integer ,dimension(IMAX)  :: IVALUE = (/     1,     1,     1,     1,     1,     &
                                                  -1,     1,     1,     1,     1,     &
                                                   1,     1,     1,     1,     1,     &
                                                   1,     1,     1,    -1,     1  /)
      integer          :: I,I1,I2,ICOD
!
      CGC4V = 0.D0
      I1    = IMIN
      I2    = IMAX
      ICOD  = 10000*IP+1000*IC+100*IS+10*ICT+IST
      IF( ICOD .LT. ICODE(I1) ) RETURN
      IF( ICOD .GT. ICODE(I2) ) RETURN
      IF( ICOD .EQ. ICODE(I2) ) GOTO 12
1     I = (I1+I2)/2
      IF    ( ICOD .EQ. ICODE(I) ) THEN
        GOTO 7
      ELSEIF( ICOD .GT. ICODE(I) ) THEN
        GOTO 3
      ENDIF
      IF( I2-I1 .EQ. 1 ) RETURN
      I2 = I
      GOTO 1
!
3     IF( I2-I1 .EQ. 1 ) RETURN
      I1 = I
      GOTO 1
!
12    I     = I2
7     CGC4V = IVALUE(I)
!
      RETURN
      END FUNCTION CGC4V
