!
!   *** SYMBOLES 6 - C DE C4V
!
      FUNCTION SXC1C4(ICT1,ICT2,ICT3,ICT4,ICT5,ICT6)                                               ! D'APRES PROG. ORSAY 80
      use mod_dppr
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: SXC1C4
      integer          :: ICT1,ICT2,ICT3,ICT4,ICT5,ICT6

! functions
      real(kind=dppr)  :: TRCC4V
      integer          :: CTRC4V

      real(kind=dppr)  :: F1,F2,F3,F4

      integer          :: IST1,IST1M,IST2,IST2M,IST3,IST3M,IST4,IST4M,IST5,IST5M,IST6,IST6M
!
      SXC1C4 = 0.D0
      IF( CTRC4V(ICT1,ICT2,ICT3) .EQ. 0 ) RETURN
      IF( CTRC4V(ICT1,ICT5,ICT6) .EQ. 0 ) RETURN
      IF( CTRC4V(ICT4,ICT2,ICT6) .EQ. 0 ) RETURN
      IF( CTRC4V(ICT4,ICT5,ICT3) .EQ. 0 ) RETURN
      IST1M = DC4(ICT1)
      IST2M = DC4(ICT2)
      IST3M = DC4(ICT3)
      DO IST1=1,IST1M
        DO IST2=1,IST2M
          DO IST3=1,IST3M
            F1 = TRCC4V(ICT1,ICT2,ICT3,IST1,IST2,IST3)
            IF( F1 .NE. 0.D0 ) GOTO 2
          ENDDO
        ENDDO
      ENDDO
      RETURN
!
2     IST4M = DC4(ICT4)
      DO IST4=1,IST4M
        IST5M = DC4(ICT5)
E11:    DO IST5=1,IST5M
          F2 = TRCC4V(ICT3,ICT4,ICT5,IST3,IST4,IST5)
          IF( F2 .EQ. 0.D0 ) CYCLE E11
          IST6M = DC4(ICT6)
E12:      DO IST6=1,IST6M
            F3 = TRCC4V(ICT1,ICT6,ICT5,IST1,IST6,IST5)
            IF( F3 .EQ. 0.D0 ) CYCLE E12
            F4 = TRCC4V(ICT2,ICT4,ICT6,IST2,IST4,IST6)
            IF( F4 .EQ. 0.D0 ) CYCLE E12
            SXC1C4 = SXC1C4+F2*F3*F4
          ENDDO E12
        ENDDO E11
      ENDDO
      SXC1C4 = (SXC1C4/F1)*PC4V(ICT1)*PC4V(ICT2)*PC4V(ICT4)*PC4V(ICT5)
!
      RETURN
      END FUNCTION SXC1C4
