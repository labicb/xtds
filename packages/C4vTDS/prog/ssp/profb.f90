!
! *** PRODUITS DE FACTORIELLES UTILISES DANS TROIJ,SXJ,EMREP ...
!
! SMIL J.HILICO JAN 70
!
      SUBROUTINE PROFB(N1,N2,N3,N4,N5,N6,N7,LI,Z,JT)
      use mod_dppr
      use mod_com_fa
      IMPLICIT NONE
      real(kind=dppr)  :: Z
      integer          :: N1,N2,N3,N4,N5,N6,N7,LI,JT

      integer ,dimension(7)  :: NT
      integer          :: I
      integer          :: K
!
      NT(1) = N1
      NT(2) = N2
      NT(3) = N3
      NT(4) = N4
      NT(5) = N5
      NT(6) = N6
      NT(7) = N7
      Z     = 1.D0
      JT    = 0
E101: DO I=1,7
        IF    ( NT(I) .LT. 0 ) THEN
          GOTO 104
        ELSEIF( NT(I) .EQ. 0 ) THEN
          CYCLE E101
        ENDIF
        K  = NT(I)
        Z  = FACT(K)*Z
        JT = JT+KFAC(K)
      ENDDO E101
      IF    ( LI .LT. 0 ) THEN
        GOTO 104
      ELSEIF( LI .GT. 0 ) THEN
        GOTO 102
      ENDIF
      LI = 1
!
102   JT = JT-KFAC(LI)
      Z  = Z/FACT(LI)
      RETURN
!
104   Z = 0.D0
!
      RETURN
      END SUBROUTINE PROFB
