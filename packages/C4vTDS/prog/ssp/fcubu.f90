!
! *** SYMBOLES "3J" CUBIQUES.
!
! SMIL CHAMPION DEC 78
!
      SUBROUTINE FCUBU(J1,J2,J3,IU1,IU2,IU3,N1,N2,N3,ICC1,ICC2,ICC3,II1,II2,II3,F,I)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: F
      integer          :: J1,J2,J3,IU1,IU2,IU3,N1,N2,N3,ICC1,ICC2,ICC3,II1,II2,II3,I

! functions
      real(kind=dppr)  :: TROIJ
      integer          :: IPACS,MMAXS,NTR

      real(kind=dppr)  :: GJ1,GJ2,GJ3
      real(kind=dppr)  :: S1,S2,S3

      integer          :: I1,I2,I3,IC1,IC2,IC3
      integer          :: IG,IG1,IG2,IG3,IGG1,IGG2,IGG3
      integer          :: IM1,IM2
      integer          :: IP1,IP2
      integer          :: IU
      integer          :: M1,M2,M21,M22,M3,MA1,MA2,MA3
      integer          :: NT
!
      IG1 = 0                                                                                      ! INITIALISATION PAR DEFAUT
      IG2 = 0                                                                                      ! INITIALISATION PAR DEFAUT
      IG3 = 0                                                                                      ! INITIALISATION PAR DEFAUT
      IU  = IU1+IU2+IU3
      IF( IU .EQ. (IU/2)*2 ) RETURN
      NT = NTR(J1,J2,J3)
      F  = 0.D0
      I  = 0
      IF( NT .NE. 0 ) GOTO 1
      RETURN
!
1     IC1 = ICC1
      IC2 = ICC2
      IC3 = ICC3
      S1  = 1.D0
      S2  = 1.D0
      S3  = 1.D0
      I1  = II1
      I2  = II2
      I3  = II3
      IF( IU1 .NE. 2 ) GOTO 101
      CALL UG(ICC1,II1,IC1,I1,S1)
!
101   IF( IU2 .NE. 2 ) GOTO 103
      CALL UG(ICC2,II2,IC2,I2,S2)
!
103   IF( IU3 .NE. 2 ) GOTO 105
      CALL UG(ICC3,II3,IC3,I3,S3)
!
105   MA1 = MMAXS(J1,IC1,I1)
      MA2 = MMAXS(J2,IC2,I2)
      MA3 = MMAXS(J3,IC3,I3)
      M21 = MA1*2+1
      M22 = MA2*2+1
      IP1 = IPACS(IC1,I1)
      IP2 = IPACS(IC2,I2)
      DO IM1=1,M21,IP1
        M1 = IM1-MA1-1
        CALL GJCMS(J1,M1,N1,IC1,I1,GJ1,IGG1)
        IF( GJ1 .NE. 0.D0 ) IG1 = IGG1
E3:     DO IM2=1,M22,IP2
          M2 = IM2-MA2-1
          CALL GJCMS(J2,M2,N2,IC2,I2,GJ2,IGG2)
          IF( GJ2 .NE. 0.D0 ) IG2 = IGG2
          M3 = -M1-M2
          IF( ABS(M3) .GT. MA3 ) CYCLE E3
          CALL GJCMS(J3,M3,N3,IC3,I3,GJ3,IGG3)
          IF( GJ3 .NE. 0.D0 ) IG3 = IGG3
          F = F+GJ1*GJ2*GJ3*TROIJ(J1,J2,J3,M1,M2,M3)
        ENDDO E3
      ENDDO
      F  = F*S1*S2*S3
      IG = IG1+IG2
      IF( IG .LE. 1 ) GOTO 5
      IG = 0
      F  = -F
!
5     IF    ( IG+IG3 .EQ. 1 ) THEN
        GOTO 7
      ELSEIF( IG+IG3 .GT. 1 ) THEN
        GOTO 8
      ENDIF
      RETURN
!
7     I = 1
      RETURN
!
8     F = -F
!
      RETURN
      END SUBROUTINE FCUBU
