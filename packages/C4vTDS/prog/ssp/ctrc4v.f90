!
! ***** Test sur les representations de C4v
!
! ***** M. Rotger 12/98
!
      FUNCTION CTRC4V(IC1,IC2,IC3)
      IMPLICIT NONE
      integer          :: CTRC4V
      integer          :: IC1,IC2,IC3

      integer ,dimension(4) :: IC
      integer          :: J
      integer          :: N
!
      CTRC4V = 0
      CALL MULC4V(IC1,IC2,N,IC)
      DO J=1,N
        IF( IC3 .EQ. IC(J) ) CTRC4V = 1
      ENDDO
!
      RETURN
      END FUNCTION CTRC4V
