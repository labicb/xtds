!
! VB CW NOV 2009
!
      FUNCTION A1N(IPM,VP,V,N)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: A1N
      integer          :: IPM,VP,V,N

      real(kind=dppr)  :: DVM

      integer          :: I,IDVM
!
      A1N = 0.D0
      IF( IPM*(VP-V) .NE. N ) RETURN
      DVM  = MAX(VP,V)
      IDVM = DVM
      DO I=1,N-1
        IDVM = IDVM*(DVM-I)
      ENDDO
      A1N = SQRT(DBLE(IDVM))
!
      RETURN
      END FUNCTION A1N
