      PROGRAM NULPAR
!
!  COPIER UN FICHIER DE PARA. DANS UN NOUVEAU FICHIER DE PARA.
!        EN METTANT A ZERO LES PARA. DONT L'ETAT DE CONTROLE EST A ZERO
!
!  APPEL : nulpar H/T CLF FOPAR FNPAR
!
!  H/T   : 1er/2eme jeu de parametres
!  CLF   : fichier de controle des parametres
!  FOPAR : fichier de parametres original
!  FNPAR : fichier de parametres modifie
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      IMPLICIT NONE

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,ICNTRL
      integer          :: NBOP,NBOPCL,NCL1,NCL2,NSTAR

      character(len = NBCLAB+10)  :: CHAINE,COPAJ
      character(len = NBCTIT)     :: TITRE
      character(len =   1)  :: HT
      character(len = 120)  :: CLF,FNPAR,FOPAR

      logical          :: TRANS
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
1020  FORMAT(/,              &
             'NULPAR : ',A)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
2002  FORMAT(I1)
8000  FORMAT(' !!! NULPAR : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8003  FORMAT(' !!! H/T COMMAND LINE PARAMETER MISSING')
8004  FORMAT(' !!! UNEXPECTED EOF WHILE READING PARAMETER CONTROL FILE')
8124  FORMAT(' !!! NB OF OPERATORS MUST BE EQUAL IN'   ,/,   &                                     ! FORMAT 8124 de eq_tds, eq_int
             '         PARAMETER CONTROL FILE:',I8,2X,A,/,   &
             '     AND PARAMETER         FILE:',I8,2X,A   )
8208  FORMAT(' !!! INCONSISTENT ',A,' PARAMETERS :',/,   &
             '     ',A,' : ',A                     ,/,   &
             '     ',A,' : ',A                        )
!
!  ARGUMENTS D'APPEL
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')                                                    ! fichier des arguments
      READ(10,1000,END=9997) FDATE
      PRINT 1020,            FDATE
      READ(10,1000,END=9997) HT
      IF    ( HT .EQ. 'T' ) THEN
        TRANS = .TRUE.                                                                             ! PARAS TRANSITION
      ELSEIF( HT .EQ. 'H' ) THEN
        TRANS = .FALSE.                                                                            ! PARAS HAMILTONIEN
      ELSE
        PRINT 8003
        GOTO  9999
      ENDIF
      READ(10,1000,END=9997) CLF                                                                   ! FICHIER DE CONTROLE DES PARA.
      READ(10,1000,END=9997) FOPAR                                                                 ! FICHIER DE PARA. ORIGINAL
      READ(10,1000,END=9997) FNPAR                                                                 ! FICHIER DE PARA. MODIFIE
      CLOSE(10)
!
!  OUVERTURE DES FICHIERS
!
      PRINT 2000,  TRIM(CLF)
      OPEN(11,FILE=TRIM(CLF),STATUS='OLD')                                                         ! fichier de controle
      PRINT 2000,  TRIM(FOPAR)
      OPEN(20,FILE=TRIM(FOPAR),STATUS='OLD')                                                       ! fichier de para original
      PRINT 2001,  TRIM(FNPAR)
      OPEN(30,FILE=TRIM(FNPAR))                                                                    ! fichier de para modifie
!
!  RECOPIE DES PARAMETRES H SI NECESSAIRE
!
      IF( TRANS ) THEN
        DO I=1,4                                                                                   ! ENTETE
          READ (20,1000) TITRE
          WRITE(30,1000) TRIM(TITRE)
        ENDDO
        READ (20,1001) NBOP,TITRE                                                                  ! NOMBRE DE PARA.
        WRITE(30,1001) NBOP,TRIM(TITRE)
        DO I=1,2
          READ (20,1000) TITRE
          WRITE(30,1000) TRIM(TITRE)
        ENDDO
        DO I=1,NBOP
          READ (20,1002) CHAINE,PARA,PREC
          WRITE(30,1002) CHAINE,PARA,PREC                                                          ! RECOPIE
        ENDDO
      ENDIF
!
! MODIFICATION DES PARAMETRES CONCERNES
!
      DO I=1,4                                                                                     ! ENTETE
        READ(11,*)
        READ (20,1000) TITRE
        WRITE(30,1000) TRIM(TITRE)
      ENDDO
      READ(11,1001) NBOPCL                                                                         ! nb de para controles
      READ(20,1001) NBOP,TITRE                                                                     ! NOMBRE DE PARA.
      IF( NBOPCL .NE. NBOP ) THEN
        PRINT 8124, NBOPCL,TRIM(CLF),   &
                    NBOP,TRIM(FOPAR)
        GOTO  9999
      ENDIF
      WRITE(30,1001) NBOP,TRIM(TITRE)
      DO I=1,2
        READ(11,*)
        READ (20,1000) TITRE
        WRITE(30,1000) TRIM(TITRE)
      ENDDO
      NCL1 = NBCLAB+1+NBAM+1                                                                       ! Hmn  included
      NCL2 = NBCLAB+1+NBAM+1+6                                                                     ! Frdm included
      DO I=1,NBOP
        READ(11,1000,END=9996) COPAJ
!  lecture d'une eventuelle unique ligne de dependances
        NSTAR = 0                                                                                  ! si EOF
        READ(11,1000,END=3003) TITRE                                                               ! EOF = pas de dependances et I=NBOP
        NSTAR = INDEX(TRIM(TITRE),'*')
! traitement des differents cas
        IF( NSTAR .LT. 1 ) THEN                                                                    ! pas une ligne de dependances
          BACKSPACE(11)                                                                            ! se repositionner pour le parametre suivant
        ENDIF
!
3003    READ(COPAJ(NCL2:NCL2),2002) ICNTRL                                                         ! ETAT DU PARA
        READ(20,1002) CHAINE,PARA,PREC
        IF( NSTAR        .LT. 1             .AND.         &                                        ! pas de ligne de dependances
            COPAJ(:NCL1) .NE. CHAINE(:NCL1)       ) THEN                                           ! tester la coherence des parametres
          PRINT 8208, HT,CHAINE(:NCL1),TRIM(FOPAR),COPAJ(:NCL1),TRIM(CLF)
          GOTO  9999
        ENDIF
        IF( ICNTRL .EQ. 0 ) THEN                                                                   ! MISE A ZERO
          PARA = 0.D0
          PREC = 0.D0
        ENDIF
        WRITE(30,1002) CHAINE,PARA,PREC                                                            ! RECOPIE
      ENDDO
!
!  RECOPIE DES PARAMETRES T SI NECESSAIRE
!
      IF( .NOT. TRANS ) THEN
        DO I=1,4                                                                                   ! ENTETE
          READ (20,1000) TITRE
          WRITE(30,1000) TRIM(TITRE)
        ENDDO
        READ (20,1001) NBOP,TITRE                                                                  ! NOMBRE DE PARA.
        WRITE(30,1001) NBOP,TRIM(TITRE)
        DO I=1,2
          READ (20,1000) TITRE
          WRITE(30,1000) TRIM(TITRE)
        ENDDO
        DO I=1,NBOP
          READ (20,1002) CHAINE,PARA,PREC
          WRITE(30,1002) CHAINE,PARA,PREC                                                          ! RECOPIE
        ENDDO
      ENDIF
      CLOSE(30)
      CLOSE(20)
      CLOSE(11)
      GOTO 9000
!
9996  PRINT 8004
      GOTO  9999
9997  PRINT 8002
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM NULPAR
