
      module mod_com_tra

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,pointer ,dimension(:) ,save  :: ENINF                                       ! (MXENI)

      integer         ,pointer ,dimension(:) ,save  :: ICENT                                       ! (MXSNV)
      integer         ,pointer ,dimension(:) ,save  :: JC,JCENTI                                   ! (MXENI)
      integer         ,pointer ,dimension(:) ,save  :: NFB                                         ! (MXENI)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_TRA
      IMPLICIT NONE

      integer  :: ierr

      allocate(ENINF(MXENI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_TRA_ENINF')
      ENINF = 0.d0

      allocate(ICENT(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_TRA_ICENT')
      ICENT = 0
      allocate(JC(MXENI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_TRA_JC')
      JC = 0
      allocate(JCENTI(MXENI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_TRA_JCENTI')
      JCENTI = 0
      allocate(NFB(MXENI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_TRA_NFB')
      NFB = 0
!
      return
      end subroutine ALLOC_TRA

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXENI

      subroutine RESIZE_MXENI_TRA(C_ENI)
      IMPLICIT NONE
      integer :: C_ENI

      integer :: ierr

! ENINF
      allocate(rpd1(C_ENI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXENI_TRA_ENINF')
      rpd1 = 0.d0
      rpd1(1:MXENI) = ENINF(:)
      deallocate(ENINF)
      ENINF => rpd1
! JC
      allocate(ipd1(C_ENI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXENI_TRA_JC')
      ipd1 = 0
      ipd1(1:MXENI) = JC(:)
      deallocate(JC)
      JC => ipd1
! JCENTI
      allocate(ipd1(C_ENI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXENI_TRA_JCENTI')
      ipd1 = 0
      ipd1(1:MXENI) = JCENTI(:)
      deallocate(JCENTI)
      JCENTI => ipd1
! NFB
      allocate(ipd1(C_ENI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXENI_TRA_NFB')
      ipd1 = 0
      ipd1(1:MXENI) = NFB(:)
      deallocate(NFB)
      NFB => ipd1
!
      return
      end subroutine RESIZE_MXENI_TRA

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNV

      subroutine RESIZE_MXSNV_TRA(C_SNV)
      IMPLICIT NONE
      integer :: C_SNV

      integer :: ierr

! ICENT
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_TRA_ICENT')
      ipd1 = 0
      ipd1(1:MXSNV) = ICENT(:)
      deallocate(ICENT)
      ICENT => ipd1
!
      return
      end subroutine RESIZE_MXSNV_TRA

      end module mod_com_tra
