
      module mod_com_sy

      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE

      real(kind=dppr)      ,dimension(MXSYR)     ,parameter  :: DC    = (/  1.D0,  1.D0,  2.D0,  3.D0,  3.D0 /)
      real(kind=dppr)      ,dimension(MXSYM)     ,parameter  :: DC4   = (/  1.D0,  1.D0,  1.D0,  1.D0,  2.D0 /)
      real(kind=dppr)      ,dimension(MXSYR)     ,parameter  :: PC    = (/  1.D0, -1.D0,  1.D0, -1.D0,  1.D0 /)
      real(kind=dppr)      ,dimension(MXSYM)     ,parameter  :: PC4V  = (/  1.D0, -1.D0,  1.D0,  1.D0,  1.D0 /)

      integer              ,dimension(MXSYR,2,2) ,parameter  :: IC4VV = reshape( source = (/ 1, 3, 1, 2, 4,       &
                                                                                             2, 4, 2, 1, 3,       &
                                                                                             0, 0, 3, 5, 5,       &
                                                                                             0, 0, 4, 5, 5  /),   &
                                                                                 shape  = shape(IC4VV)         )

      character(len =   1)                       ,parameter  :: PARGEN = ' '
      character(len =   2) ,dimension(MXSYR)     ,parameter  :: KC     = (/ 'A1', 'A2', 'E ', 'F1', 'F2' /)
      character(len =   2) ,dimension(MXSYM)     ,parameter  :: SYM    = (/ 'a1', 'a2', 'b1', 'b2', 'e ' /)

      end module mod_com_sy
