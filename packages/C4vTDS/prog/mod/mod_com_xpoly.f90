
      module mod_com_xpoly                                                                         ! polyad related COMMON

      use mod_par_tds
      IMPLICIT NONE

      integer              ,dimension(MXPOL)            ,save  :: NNIV  = 0
      integer              ,dimension(MXPOL)            ,save  :: JMAX  = 0
      integer                                           ,save  :: NBPOL = 0

      character(len =   1) ,dimension(MXPOL)            ,save  :: CPOL  = ''                       !  1 due to MXPOL
      character(len =  10) ,dimension(MXPOL)            ,save  :: CDEV  = ''                       ! 10 due to MXPOL
      character(len =  13)                   ,parameter        :: FASSI = 'assignments.t'

      logical              ,dimension(MXPOL)            ,save  :: ISSUP = .false.
      logical              ,dimension(MXPOL)            ,save  :: ISINF = .false.

      end module mod_com_xpoly
