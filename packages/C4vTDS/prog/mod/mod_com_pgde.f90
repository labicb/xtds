
      module mod_com_pgde

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,pointer ,dimension(:) ,save  :: DERI                                        ! (MXOPH)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_PGDE
      IMPLICIT NONE

      integer  :: ierr

      allocate(DERI(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PGDE_DERI')
      DERI = 0.d0
!
      return
      end subroutine ALLOC_PGDE

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPH

      subroutine RESIZE_MXOPH_PGDE(C_OPH)
      IMPLICIT NONE
      integer :: C_OPH

      integer :: ierr

! DERI
      allocate(rpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PGDE_DERI')
      rpd1 = 0.d0
      rpd1(1:MXOPH) = DERI(:)
      deallocate(DERI)
      DERI => rpd1
!
      return
      end subroutine RESIZE_MXOPH_PGDE

      end module mod_com_pgde
