
      module mod_com_stats

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,dimension(0:MXJ) ,save  :: ECUS  = 0.d0
      real(kind=dppr) ,dimension(0:MXJ) ,save  :: ECU0  = 0.d0
      real(kind=dppr) ,dimension(0:MXJ) ,save  :: SPCUS = 0.d0
      real(kind=dppr) ,dimension(0:MXJ) ,save  :: SPCU0 = 0.d0

      integer         ,dimension(0:MXJ) ,save  :: NCUS  = 0
      integer         ,dimension(0:MXJ) ,save  :: NCU0  = 0
!
      real(kind=dppr) ,pointer ,dimension(:,:) ,save  :: SMJS,SPOIS,SPRORS                         ! (0:MXJ,0:MXSNV)

      integer         ,pointer ,dimension(:,:) ,save  :: NFJS                                      ! (0:MXJ,0:MXSNV)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_STATS
      IMPLICIT NONE

      integer  :: ierr

      allocate(SMJS(0:MXJ,0:MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_STATS_SMJS')
      SMJS = 0.d0
      allocate(SPOIS(0:MXJ,0:MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_STATS_SPOIS')
      SPOIS = 0.d0
      allocate(SPRORS(0:MXJ,0:MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_STATS_SPRORS')
      SPRORS = 0.d0

      allocate(NFJS(0:MXJ,0:MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_STATS_NFJS')
      NFJS = 0
!
      return
      end subroutine ALLOC_STATS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNV

      subroutine RESIZE_MXSNV_STATS(C_SNV)
      IMPLICIT NONE
      integer :: C_SNV

      integer :: i,ierr

! SMJS
      allocate(rpd2(0:MXJ,0:C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_STATS_SMJS')
      rpd2 = 0.d0
      do i=0,MXSNV
        rpd2(:,i) = SMJS(:,i)
      enddo
      deallocate(SMJS)
      SMJS => rpd2
! SPOIS
      allocate(rpd2(0:MXJ,0:C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_STATS_SPOIS')
      rpd2 = 0.d0
      do i=0,MXSNV
        rpd2(:,i) = SPOIS(:,i)
      enddo
      deallocate(SPOIS)
      SPOIS => rpd2
! SPRORS
      allocate(rpd2(0:MXJ,0:C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_STATS_SPRORS')
      rpd2 = 0.d0
      do i=0,MXSNV
        rpd2(:,i) = SPRORS(:,i)
      enddo
      deallocate(SPRORS)
      SPRORS => rpd2
! NFJS
      allocate(ipd2(0:MXJ,0:C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_STATS_NFJS')
      ipd2 = 0
      do i=0,MXSNV
        ipd2(:,i) = NFJS(:,i)
      enddo
      deallocate(NFJS)
      NFJS => ipd2
!
      return
      end subroutine RESIZE_MXSNV_STATS

      end module mod_com_stats
