
      module mod_main_tri

      use mod_dppr
      use mod_par_tds
      use mod_com_tri


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_tri
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_NBLMAX
      IMPLICIT NONE

      integer  :: M_LMAX

      M_LMAX = MXRES(NBLMAX)
      call RESIZE_NBLMAX_TRI(M_LMAX)
      NBLMAX = M_LMAX
!
      return
      end subroutine RESIZE_NBLMAX

      end module mod_main_tri
