
      module mod_com_xwf

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer              ,pointer ,dimension(:)   ,save  :: JCENTT,NCODNI,NCODNS                 ! (MXSNV)
      integer              ,pointer ,dimension(:,:) ,save  :: IPCSN,NUSVSU                         ! (MXOBS,MXSNV)
      integer              ,pointer ,dimension(:)   ,save  :: IPCINF,IPCSUP                        ! (MXOBS)

      character(len =  20) ,pointer ,dimension(:)   ,save  :: CODNI,CODNS                          ! (MXNIV) ! 20 : NBVQN not taken into account


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_XWF
      IMPLICIT NONE

      integer  :: ierr

      allocate(JCENTT(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWF_JCENTT')
      JCENTT = 0
      allocate(NCODNI(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWF_NCODNI')
      NCODNI = 0
      allocate(NCODNS(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWF_NCODNS')
      NCODNS = 0
      allocate(IPCSN(MXOBS,MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWF_IPCSN')
      IPCSN = 0
      allocate(NUSVSU(MXOBS,MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWF_NUSVSU')
      NUSVSU = 0
      allocate(IPCINF(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWF_IPCINF')
      IPCINF = 0
      allocate(IPCSUP(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWF_IPCSUP')
      IPCSUP = 0

      allocate(CODNI(MXNIV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWF_CODNI')
      CODNI = ''
      allocate(CODNS(MXNIV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWF_CODNS')
      CODNS = ''
!
      return
      end subroutine ALLOC_XWF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXNIV

      subroutine RESIZE_MXNIV_XWF(C_NIV)
      IMPLICIT NONE
      integer :: C_NIV

      integer :: ierr

      character(len =  20) ,pointer ,dimension(:)  :: cpd1

! CODNI
      allocate(cpd1(C_NIV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNIV_XWF_CODNI')
      cpd1 = ''
      cpd1(1:MXNIV) = CODNI(:)
      deallocate(CODNI)
      CODNI => cpd1
! CODNS
      allocate(cpd1(C_NIV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNIV_XWF_CODNS')
      cpd1 = ''
      cpd1(1:MXNIV) = CODNS(:)
      deallocate(CODNS)
      CODNS => cpd1
!
      return
      end subroutine RESIZE_MXNIV_XWF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOBS

      subroutine RESIZE_MXOBS_XWF(C_OBS)
      IMPLICIT NONE
      integer :: C_OBS

      integer :: i,ierr

! IPCSN
      allocate(ipd2(C_OBS,MXSNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_XWF_IPCSN')
      ipd2 = 0
      do i=1,MXSNV
        ipd2(1:MXOBS,i) = IPCSN(:,i)
      enddo
      deallocate(IPCSN)
      IPCSN => ipd2
! NUSVSU
      allocate(ipd2(C_OBS,MXSNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_XWF_NUSVSU')
      ipd2 = 0
      do i=1,MXSNV
        ipd2(1:MXOBS,i) = NUSVSU(:,i)
      enddo
      deallocate(NUSVSU)
      NUSVSU => ipd2
! IPCINF
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_XWF_IPCINF')
      ipd1 = 0
      ipd1(1:MXOBS) = IPCINF(:)
      deallocate(IPCINF)
      IPCINF => ipd1
! IPCSUP
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_XWF_IPCSUP')
      ipd1 = 0
      ipd1(1:MXOBS) = IPCSUP(:)
      deallocate(IPCSUP)
      IPCSUP => ipd1
!
      return
      end subroutine RESIZE_MXOBS_XWF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNV

      subroutine RESIZE_MXSNV_XWF(C_SNV)
      IMPLICIT NONE
      integer :: C_SNV

      integer :: i,ierr

! JCENTT
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_XWF_JCENTT')
      ipd1 = 0
      ipd1(1:MXSNV) = JCENTT(:)
      deallocate(JCENTT)
      JCENTT => ipd1
! NCODNI
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_XWF_NCODNI')
      ipd1 = 0
      ipd1(1:MXSNV) = NCODNI(:)
      deallocate(NCODNI)
      NCODNI => ipd1
! NCODNS
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_XWF_NCODNS')
      ipd1 = 0
      ipd1(1:MXSNV) = NCODNS(:)
      deallocate(NCODNS)
      NCODNS => ipd1
! IPCSN
      allocate(ipd2(MXOBS,C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_XWF_IPCSN')
      ipd2 = 0
      do i=1,MXSNV
        ipd2(:,i) = IPCSN(:,i)
      enddo
      deallocate(IPCSN)
      IPCSN => ipd2
! NUSVSU
      allocate(ipd2(MXOBS,C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_XWF_NUSVSU')
      ipd2 = 0
      do i=1,MXSNV
        ipd2(:,i) = NUSVSU(:,i)
      enddo
      deallocate(NUSVSU)
      NUSVSU => ipd2
!
      return
      end subroutine RESIZE_MXSNV_XWF

      end module mod_com_xwf
