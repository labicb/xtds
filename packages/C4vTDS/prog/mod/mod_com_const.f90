
      module mod_com_const

      use mod_dppr
      IMPLICIT NONE

      real(kind=dppr) ,parameter  :: BOLT   =  1.3806504D-16
      real(kind=dppr) ,parameter  :: CL     =  2.686754D+19
      real(kind=dppr) ,parameter  :: CLUM   =  2.99792458D+10
      real(kind=dppr) ,parameter  :: COKG   = -0.4330127018D0
      real(kind=dppr) ,parameter  :: HCOVRK =  1.43883D0
      real(kind=dppr) ,parameter  :: PI     =  3.141592653589793D0
      real(kind=dppr) ,parameter  :: PLANK  =  6.62606896D-27

      real(kind=dppr) ,parameter  :: CMHZ   =  2.99792458D+4                                       ! 1 cm-1 = 1 MHz / CMHZ
      real(kind=dppr) ,parameter  :: CGHZ   =  2.99792458D+1                                       ! 1 cm-1 = 1 GHz / CGHZ
      real(kind=dppr) ,parameter  :: T0     =  273.15D0

      end module mod_com_const
