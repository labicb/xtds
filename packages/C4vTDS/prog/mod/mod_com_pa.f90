
      module mod_com_pa

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer ,save  :: NBOPT = 0
!
      real(kind=dppr) ,pointer ,dimension(:) ,save  :: PARAT                                       ! (MXOPT)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_PA
      IMPLICIT NONE

      integer  :: ierr

      allocate(PARAT(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PA_PARAT')
      PARAT = 0.d0
!
      return
      end subroutine ALLOC_PA

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPT

      subroutine RESIZE_MXOPT_PA(C_OPT)
      IMPLICIT NONE
      integer :: C_OPT

      integer :: ierr

! PARAT
      allocate(rpd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_PA_PARAT')
      rpd1 = 0.d0
      rpd1(1:MXOPT) = PARAT(:)
      deallocate(PARAT)
      PARAT => rpd1
!
      return
      end subroutine RESIZE_MXOPT_PA

      end module mod_com_pa
