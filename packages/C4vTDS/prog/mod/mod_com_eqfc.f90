
      module mod_com_eqfc

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,pointer ,dimension(:)   ,save  :: DERIN,DERSU                               ! (MXOPH)
      real(kind=dppr) ,pointer ,dimension(:,:) ,save  :: VACO                                      ! (MXOPH,MXOPH)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_EQFC
      IMPLICIT NONE

      integer  :: ierr

      allocate(DERIN(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQFC_DERIN')
      DERIN = 0.d0
      allocate(DERSU(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQFC_DERSU')
      DERSU = 0.d0
      allocate(VACO(MXOPH,MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQFC_VACO')
      VACO = 0.d0
!
      return
      end subroutine ALLOC_EQFC

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPH

      subroutine RESIZE_MXOPH_EQFC(C_OPH)
      IMPLICIT NONE
      integer :: C_OPH

      integer :: i,ierr

! DERIN
      allocate(rpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_EQFC_DERIN')
      rpd1 = 0.d0
      rpd1(1:MXOPH) = DERIN(:)
      deallocate(DERIN)
      DERIN => rpd1
! DERSU
      allocate(rpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_EQFC_DERSU')
      rpd1 = 0.d0
      rpd1(1:MXOPH) = DERSU(:)
      deallocate(DERSU)
      DERSU => rpd1
! VACO
      allocate(rpd2(C_OPH,C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_EQFC_VACO')
      rpd2 = 0.d0
      do i=1,MXOPH
        rpd2(1:MXOPH,i) = VACO(:,i)
      enddo
      deallocate(VACO)
      VACO => rpd2
!
      return
      end subroutine RESIZE_MXOPH_EQFC

      end module mod_com_eqfc
