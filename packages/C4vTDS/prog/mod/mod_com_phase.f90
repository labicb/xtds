
      module mod_com_phase

      use mod_par_tds
      IMPLICIT NONE

      character(len =   1) ,dimension(2) ,parameter  :: AIM  = (/ ' ', 'I' /)                      ! PARITE DES OPERATEURS VIBRATIONNELS
      character(len =   1) ,dimension(2) ,parameter  :: AUG  = (/ '+', '-' /)                      ! PARITE DES OPERATEURS VIBRATIONNELS
      character(len =   1) ,dimension(2) ,parameter  :: ACGU = (/ 'g', 'u' /)                      ! PARITE DES OPERATEURS VIBRATIONNELS

      end module mod_com_phase
