This panel creates a job to calculate rovibrational energy levels.

You need to:

- Choose a molecule and a Jmax value.
  For D2hTDS package choose a molecular axis representation ("Ir", "IIr" or "IIIr").
- Choose a parameter file containing Hamiltonian parameters for the polyad under consideration, which will be chosen later.
- Define the polyad scheme. Each polyad n is defined thanks to a set of integers (i1,i2,...,in) so that:

    n = i1*v1+i2*v2+ ... +in*vn,

  v1,v2,...,vn being the vibrational quantum numbers.
- Check "Mixings" radio button if you want to calculate vibrational level mixings. In this case, you can specify an assignment file to only take into account levels implied into some assigned transitions.
- Choose a polyad number n.
- If necessary, change the v1,v2,...,vn maximum values in the "Quanta Limit" section. This allows to put restrictions to the polyad scheme defined above, for instance to suppress some levels.
- Choose the development orders for polyads P0 to Pn.
- Have a look at the text area below to check if the polyads are correctly defined.
- Click the "Save" button to create the job.

