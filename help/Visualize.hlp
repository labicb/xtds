This panel allows to visualize the results of the different types of calculations stored in the following types of files:

- jener.xy, jener.t and starkl.t files:
  List of rovibrational levels as a function of J.
  Output of a level job (without mixing calculation).

- Ener[_assign]_Pn file:
  List of rovibrational levels and mixings as a function of J.
  Output of a level job (with mixing calculation, Ener_Pn file: without assignment indication, Ener_assign_Pn file: with assignment indication).
  In the "BASICS" section, you should indicate the energy (in cm-1) and J ranges. Check the "Assigned" radio button if you want to visualize assigned levels only of an Ener_assign_Pn file.
  Using the "Add File" button, define the files to visualize.
  Using the "Add Component" button, define the groups of vibrational level contributions that will be represented by different colors.
  Using the "Save Comp. File" to save component file from already defined components.
  Using the "Load Comp. File" to load previously saved component file.

- spectr.xy and spectr.t files:
  Stick spectrum.

- simul.xy file:
  Synthetic spectrum.

- Pred_mix file:
  Observed minus caculated positions or intensities. Prediction file created from a fit job.
  Crosses:  lines with a + mark in assignment file (taken into account for the fit).
  Squares:  lines with a - mark in assignment file (not taken into account for the fit).
  Rhombuses: lines with an other mark in assignment file (not taken into account for the fit).
  Vibrational components, if defined, are distinguished by different colors.
