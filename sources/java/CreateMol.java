/*
 * Class for new molecule creation, called by JobPlay
 */

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import java.text.*;
import java.util.*;

/**
 * This panel allows to create a molecule definition file not yet existing in one of the original packages.
 */
public class CreateMol extends JPanel
                       implements  ActionListener {

    private JLabel       jlpack;                                     // package choice
    private JComboBox    jcbpack;                                    // package jcb

    private String         nomf;                                     // various file names
    private String         str;                                      // string
    private BufferedReader br;                                       // and buffer for file reading

    // panels
    private JPanel pnord;
    private JPanel   pno;
    private JPanel   pnc;
    private JPanel psud;

    private JTextField jtfmol;
    private JFormattedTextField[] jftfspin;
    private JFormattedTextField   jftfspiny;
    private JFormattedTextField[] jftfD2hABC;
    private JFormattedTextField[] jftffreq;
    private JButton jbreset;                                         // reset button
    private JButton jbcreate;                                        // create button

    // package characteristics
    private String   playd;                                          // XTDS installation directory
    private String[] packdirs;                                       // package directories
    private String   npack;                                          // package name
    private int      nbmol;                                          // nb of molecules
    private String[] moldirs;                                        // molecule directories
    private int      nbspin;                                         // nb of spin statistics
    private int      nbvqn;                                          // nb of vibrational quantum numbers

    private int[]    vspin;
    private float    vspiny;
    private double[] vD2hABC;
    private double[] vfreq;

    // Number Format
    private NumberFormat nffloat;
    private NumberFormat nffloat1;
    private NumberFormat nfint;

    private PrintWriter out1;
    private String lnsep;                                            // line separator
    private String fisep;                                            // file separator

    private File oldPa_skel;                                         // original Pa_skel file

////////////////////////////////////////////////////////////

    /**
     * Constructs a new CreateMol.
     *
     * @param cpackdirs   packages directories
     */
    public CreateMol(String[] cpackdirs) {

        playd = System.getProperty("xtds.home");                     // XTDS installation directory
        packdirs = (String[]) cpackdirs.clone();                     // package directories

        lnsep = System.getProperty("line.separator");
        fisep = System.getProperty("file.separator");
        setName("CreateMol");                                        // for help files
        askPack();                                                   // choose a package
    }

    // choose a package
    private void askPack() {

        removeAll();                                                 // remove all panel components
        setLayout(new FlowLayout());                                 // layout choice

        npack = "";                                                  // no chosen package
        // package
        jlpack = new JLabel("Choose a Package :");
        jcbpack = new JComboBox();
        jcbpack.addItem("");                                         // add       ""
        for( int i=0; i<packdirs.length; i++ ) {                     // add directories
            jcbpack.addItem(packdirs[i]);
        }
        jcbpack.setBackground(Color.WHITE);
        jcbpack.setSelectedItem("");                                 // defaut to ""
        jcbpack.addActionListener(this);                             // manage this jcb

        Box box=Box.createVerticalBox();                             // choice box
        box.add(Box.createVerticalStrut(100));
        box.add(jlpack);                                             // package choice
        box.add(Box.createVerticalStrut(10));
        box.add(jcbpack);

        add(box);                                                    // display all
    }

    /**
     * Processes the events.
     */
    public void actionPerformed(ActionEvent evt) {

        // reset button
        if (evt.getSource() == jbreset) {
            setVisible(false);                                       // hide everything
            askPack();                                               // restart from beginning
            return;
        }
        // creation button
        if (evt.getSource() == jbcreate) {
            if( createNewMol() ) {
                setVisible(false);                                   // hide everything
                askPack();                                           // restart from beginning
            }
            return;
        }
        // package choice
        if (evt.getSource() == jcbpack ) {
            npack = (String) jcbpack.getSelectedItem();              // package name
        }
        if(npack != "") {                                            // package chosen
            if(createCar()) {                                        // if characteristics are read
                createJP();
            }
            else {
                askPack();                                           // restart from beginning
            }
        }
    }

    // read basic characteristics
    private boolean createCar() {

        // get package characteristics
        File paradir = new File(playd+fisep+"packages"+fisep+npack+fisep+"para");  // possible molecules
        moldirs = paradir.list();
        nbmol = moldirs.length;
        Arrays.sort(moldirs);                                        // sort
        if( ! readTdsPF() ) {                                        // read mod_par_tds
            return false;
        }
        return true;
    }

    // read mod_par_tds
    private boolean readTdsPF() {

        nbspin = 0;
        nbvqn  = 0;
        try {
            nomf = playd+fisep+"packages"+fisep+npack+fisep+"prog"+fisep+"mod"+fisep+"mod_par_tds.f90";
            File f = new File(nomf);
            br = new BufferedReader (new FileReader(f));             // open
            while ((str = br.readLine()) != null) {
               if(str.startsWith("      integer ,parameter  :: MXSYM  =")) {  // search for the right line
                   str = str.substring(38,38+7);                              // select field
                   str = str.trim();                                          // remove spaces
                   nbspin = Integer.parseInt(str);                            // convert
               }
               if(str.startsWith("      integer ,parameter  :: NBVQN  =")) {
                   str = str.substring(38,38+7);
                   str = str.trim();
                   nbvqn  = Integer.parseInt(str);
               }
            }
        }
        catch(IOException ioe) {                                     // IO error
            JOptionPane.showMessageDialog(null,"IO error while reading file"+lnsep+
                                               nomf                         +lnsep+
                                               ioe);
            return false;
        }
        finally {                                                    // close file
            if (br !=null) {
                try {
                    br.close();
                }
                catch (IOException ignored) {
                }
            }
        }

        // happy end check
        if(nbspin == 0 ) {                                           // not found
            JOptionPane.showMessageDialog(null,"MXSYM  not found in "+nomf);
            return false;
        }
        if(nbvqn  == 0 ) {                                           // not found
            JOptionPane.showMessageDialog(null,"NBVQN  not found in "+nomf);
            return false;
        }
        return true;
    }

    // display one of the CJPanel
    private void createJP() {
        removeAll();
        setLayout(new BorderLayout());

        // manage CJPanel reset button
        jbreset = new JButton("Reset All");
        jbreset.setBackground(Color.WHITE);
        jbreset.addActionListener (this);                            // manage it
        jbcreate  = new JButton("Create");                           // create molecule
        jbcreate.setBackground(Color.WHITE);
        jbcreate.addActionListener (this);

        Box boxsud = Box.createHorizontalBox();                      // choice box
        boxsud.add(jbreset);
        boxsud.add(Box.createHorizontalStrut(15));
        boxsud.add(jbcreate);

        // north panel
        pnord = new JPanel(new BorderLayout());
        pnord.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),"Molecule Creation for "+npack));
        if( npack.equals("D2hTDS") ) {
            pno   = new JPanel(new GridLayout(5,0,5,5));
            pnc   = new JPanel(new GridLayout(5,0,5,5));
        }
        else {
            pno   = new JPanel(new GridLayout(4,0,5,5));
            pnc   = new JPanel(new GridLayout(4,0,5,5));
        }

        // catching formats
        nffloat = NumberFormat.getInstance(Locale.US);
        nffloat.setGroupingUsed(false);                              // ie. no thousands separator
        nffloat.setMaximumFractionDigits(10);                        // ie. no more then 10 digits for fractionnal part
        nffloat1 = NumberFormat.getInstance(Locale.US);
        nffloat1.setGroupingUsed(false);                             // ie. no thousands separator
        nffloat1.setMaximumFractionDigits(1);                        // ie. no more then  1 digit  for fractionnal part
        nfint = NumberFormat.getInstance();
        nfint.setGroupingUsed(false);                                // ie. no thousands separator
        nfint.setParseIntegerOnly(true);                             // ie. only integers
        nfint.setMaximumIntegerDigits(7);                            // ie. no more then 7 digits
        // molecule
        pno.add(new JLabel(" Molecule  "));
        jtfmol = new JTextField();
        jtfmol.setBackground(Color.WHITE);
        pnc.add(jtfmol);
        // spin
        pno.add(new JLabel(" Spin Stat.  "));
        jftfspin = new JFormattedTextField[nbspin];
        vspin = new int[nbspin];
        JPanel jpspin = new JPanel(new GridLayout(0,nbspin,5,5));
        for( int i=0; i<nbspin; i++ ) {
            jftfspin[i] = new JFormattedTextField(nfint);
            jftfspin[i].setValue(new Integer(0));
            jpspin.add(jftfspin[i]);
        }
        pnc.add(jpspin);
        // spiny
        pno.add(new JLabel(" SPIN Y  "));
        jftfspiny = new JFormattedTextField(nffloat1);
        jftfspiny.setValue(new Float(0.));
        pnc.add(jftfspiny);
        // D2h -> A, B, C
        if( npack.equals("D2hTDS") ) {
            pno.add(new JLabel(" A, B, C  "));
            jftfD2hABC = new JFormattedTextField[3];
            vD2hABC    = new double[3];
            JPanel jlABC = new JPanel(new GridLayout(0,3,5,5));
            for( int i=0; i<3; i++ ) {
                jftfD2hABC[i] = new JFormattedTextField(nffloat);
                jftfD2hABC[i].setValue(new Double(0.));
                jlABC.add(jftfD2hABC[i]);
            }
            pnc.add(jlABC);
        }
        // frequencies
        pno.add(new JLabel(" Frequencies  "));
        jftffreq = new JFormattedTextField[nbvqn];
        vfreq = new double[nbvqn];
        JPanel jpfreq = new JPanel(new GridLayout(0,nbvqn,5,5));
        for( int i=0; i<nbvqn; i++ ) {
            jftffreq[i] = new JFormattedTextField(nffloat);
            jftffreq[i].setValue(new Double(0.));
            jpfreq.add(jftffreq[i]);
        }
        pnc.add(jpfreq);

        pnord.add(pno,"West");
        pnord.add(pnc,"Center");

        // south panel
        psud = new JPanel();
        psud.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),null));
        psud.add(boxsud);

        add(pnord,"North");
        add(psud, "South");
    }

    // create a new molecule
    private boolean createNewMol() {

        // MOLECULE
        // name choice
        String nmol = jtfmol.getText().trim();
        if( nmol.length() == 0 ) {
            JOptionPane.showMessageDialog(null,"You have to choose a molecule name");
            return false;
        }
        // must not yet exist
        File moldir = new File(playd+fisep+"packages"+fisep+npack+fisep+"para"+fisep+nmol);
        if( moldir.exists() ) {
            JOptionPane.showMessageDialog(null,"The directory"         +lnsep+
                                               moldir.getAbsolutePath()+lnsep+
                                               "should not exist.");
            jtfmol.setText("");
            return false;
        }
        // SPIN
        for( int i=0; i<nbspin; i++ ) {
            vspin[i] = ((Number)jftfspin[i].getValue()).intValue();
            if( vspin[i] < 0 ) {
                JOptionPane.showMessageDialog(null,"Spin >= 0 requested");
                jftfspin[i].setValue(new Integer(0));
                return false;
            }
        }
        // SPINY
        vspiny = ((Number)jftfspiny.getValue()).floatValue();
        if( vspiny < 0 ) {
            JOptionPane.showMessageDialog(null,"SPIN Y >= 0 requested");
            jftfspiny.setValue(new Float(0.));
            return false;
        }
        float v2 = (float)2.*vspiny;
        if( (float)((int) v2) != v2 ) {
            JOptionPane.showMessageDialog(null,"SPIN Y has to be integer or half integer");
            jftfspiny.setValue(new Float(0.));
            return false;
        }
        // D2h A, B, C
        if( npack.equals("D2hTDS") ) {
            for( int i=0; i<3; i++ ) {
                vD2hABC[i] = ((Number)jftfD2hABC[i].getValue()).doubleValue();
                if( vD2hABC[i] <= 0. ) {
                    JOptionPane.showMessageDialog(null,"A, B and C >0 requested");
                    jftfD2hABC[i].setValue(new Double(0.));
                    return false;
                }
            }
            if( (vD2hABC[0] < vD2hABC[1]) ||
                (vD2hABC[1] < vD2hABC[2])    ) {
                JOptionPane.showMessageDialog(null,"A >= B >= C requested");
                for( int i=0; i<3; i++ ) {
                    jftfD2hABC[i].setValue(new Double(0.));
                }
                return false;
            }
        }
        // FREQUENCIES
        for( int i=0; i<nbvqn; i++ ) {
            vfreq[i] = ((Number)jftffreq[i].getValue()).doubleValue();
            if( vfreq[i] <= 0. ) {
                JOptionPane.showMessageDialog(null,"Frequencies >= 0 requested");
                jftffreq[i].setValue(new Double(0.));
                return false;
            }
        }
        // FILE CREATION
        // opening of 1st existing Pa_skel
        boolean found;
        found = false;                                               // no valid existing Pa_skel already found
        for( int i=0; i<nbmol; i++ ) {
            oldPa_skel = new File(playd+fisep+"packages"+fisep+npack+fisep+"para"+fisep+moldirs[i]+fisep+"Pa_skel");
            try {
                br = new BufferedReader( new FileReader( oldPa_skel ) );  // open
            }
            catch(FileNotFoundException fnfe) {                      // no file found
                continue;                                            // try next one
            }
            found = true;                                            // found
            break;
        }
        if( ! found ) {
            JOptionPane.showMessageDialog(null,"No valid existing Pa_skel found in"             +lnsep+
                                               playd+fisep+"packages"+fisep+npack+fisep+"para/*"       );
            return false;
        }
        // create directory
        if( ! moldir.mkdir() ) {
            JOptionPane.showMessageDialog(null,"Unable to create the directory"+lnsep+
                                               moldir.getAbsolutePath());
            jtfmol.setText("");
            return false;
        }
        // create new Pa_skel
        try {
            out1 = new PrintWriter(new BufferedWriter(new FileWriter(moldir.getAbsolutePath()+fisep+"Pa_skel")));  // write the file

            // line #1
            if( ! inOldPa_skel() ) {
                return false;
            }
            out1.println(str);
            // line #2
            if( ! inOldPa_skel() ) {
                return false;
            }
            for( int i=0; i<nbspin; i++) {
                out1.print  (" "+jftfspin[i].getText().trim()+". ");
            }
            out1.print  ("          "+vspiny);
            if( npack.equals("D2hTDS") ) {
                out1.print  ("     ");
                for( int i=0; i<3; i++) {
                    out1.print  (" "+vD2hABC[i]);
                }
                out1.println("  Spin Statistics , Spin Y , GS Rot. Const.  A, B, C");
            }
            else {
                out1.println("      Spin Statistics , Spin Y");
            }
            // line #3
            if( ! inOldPa_skel() ) {
                return false;
            }
            out1.println(str);
            // line #4
            if( ! inOldPa_skel() ) {
                return false;
            }
            for( int i=0; i<nbvqn; i++) {
                out1.print  (" "+vfreq[i]+" ");
            }
            if( npack.equals("C3vTDS")  ||
                npack.equals("C3vsTDS")    ) {
                // B0 A0 BB0 DD0
                out1.println(" 0.0E+00 0.0E+00 0.0E+00 0.0E+00");
            }
            else {
                // B0 D0
                out1.println(" 0.0000000E+00     0.0000000E+00");
            }
            // line #5
            if( ! inOldPa_skel() ) {
                return false;
            }
            out1.println(str);
            // line #6
            if( ! inOldPa_skel() ) {
                return false;
            }
            out1.println(nmol);
            // lines #7 -> #14
            for( int i=0; i<8; i++ ) {
                if( ! inOldPa_skel() ) {
                    return false;
                }
                out1.println(str);
            }
        }
        catch (IOException ioe) {                                    // IO error
            JOptionPane.showMessageDialog(null,"IO error while writing file"           +lnsep+
                                               moldir.getAbsolutePath()+fisep+"Pa_skel"+lnsep+
                                               ioe);
            return false;
        }
        finally {
            if( out1 != null ) {
                out1.close();
                if( out1.checkError() ) {
                    JOptionPane.showMessageDialog(null,"PrintWriter error while creating file"+lnsep+
                                                       moldir.getAbsolutePath()+fisep+"Pa_skel");
                    return false;
                }
            }
        }
        // close existing Pa_skel
        try {
            br.close();
        }
        catch (Exception ignored) {
        }
        // happy end
        JOptionPane.showMessageDialog(null,"New molecule skeleton file has been created"+lnsep+
                                           moldir.getAbsolutePath()+fisep+"Pa_skel"     +lnsep);
        return true;
    }

    // read one line of the original Pa_skel
    private boolean inOldPa_skel() {

        try {
            if( (str = br.readLine()) == null) {
                JOptionPane.showMessageDialog(null,"Unexpected EOF while reading file"+lnsep+
                                                   oldPa_skel.getAbsolutePath());
                return false;
            }
        }
        catch(IOException ioe) {                                     // IO error
            JOptionPane.showMessageDialog(null,"IO error while reading file"+lnsep+
                                               oldPa_skel.getAbsolutePath() +lnsep+
                                               ioe);
            return false;
        }
        return true;
    }

}
