/*
 * Class to draw jobs results
 */

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.print.*;
import java.io.*;
import java.text.*;
import java.util.*;
import javax.swing.*;

////////////////////////////////////////////////////////////

/**
 * This panel is used to paint the graphs.
 */
class PanAffich extends    JPanel
                implements MouseListener, MouseMotionListener, Printable {

    private Graphics2D g2d;                                          // to be able to rotate text

    // all variables beginning with p_... contain pixel values (int or double)
    // others are in their own units : frequency, intensity, J, energy, ...
    // the following are related only to screen drawing
    private int p_xmouse;                                            // x mouse location
    private int p_ymouse;                                            // y mouse location
    private int p_wxma;                                              // window width
    private int p_wyma;                                              // window height
    private int prevp_wxma;                                          // previous p_wxma
    private int prevp_wyma;                                          // previous p_wyma
    private int p_gap;                                               // gap along axis
    private int p_cxmi;                                              // x min of data area
    private int p_cxma;                                              // x max of data area
    private int p_cymi;                                              // y min of data area
    private int p_cyma;                                              // y max of data area
    private int p_cross;                                             // obs-cal cross branch size
    //
    private double p_zx0;                                            // starting x of zoom/selection area
    private double p_zy0;                                            // starting y of zoom/selection area
    private double p_zx1;                                            // ending   x of zoom/selection area
    private double p_zy1;                                            // ending   y of zoom/selection area
    private double prevp_zx1;                                        // previous p_zx1
    private double prevp_zy1;                                        // previous p_zy1
    //
    private int    p_paintx;                                         // starting X of repaint area
    private int    p_painty;                                         // starting Y of repaint area
    private int    p_paintwidth;                                     // width      of repaint area
    private int    p_paintheight;                                    // height     of repaint area
    //
    // sc_ variables define scale while drawing/printing
    private double sc_fontsz = .015;                                 // font size % ddp_wyma
    private double sc_gap    = .005;                                 // gap       % ddp_wxma
    private double sc_cross  = .005;                                 // cross     % ddp_wxma
    private double[]  x;                                             // X data
    private double[]  y;                                             // Y data
    private int       iemin;                                         // index of Y min for jener/trni/predmix
    private int       iemax;                                         // index of Y max for jener/trni/predmix
    private int       nbxy;                                          // nb of data points
    private int       ixdeb;                                         // window first point X index
    private int       ixfin;                                         // window last  point X index
    private boolean   use_red;                                       // use reduced data
    private int[]     i_red;                                         // index of reduced data
    private int       nbxy_red;                                      // reduced nb of data points
    private String[]  nxyf;                                          // data file names
    private int       nbf;                                           // nb of data files
    private String    ntfile;                                        // type file names
    private String    ntrans;                                        // transition name
    private int[][]   concomp;                                       // component contributions
    private int[][]   limcomp;                                       // component limits
    private int       xnbcomp;                                       // extended nb of components
    private int[]     icompa;                                        // displayable component index
    private int       nbcomp;                                        // nb    of displayable component
    private String[]  smark;                                         // selection mark related to predmix data
    private String[]  jsyn;                                          // assignments    related to predmix data
    private int[]     comp;                                          // component      related to predmix data
    private Color[]   coul;                                          // predefined colors
    private double    xmi;                                           // X min
    private double    xma;                                           // X max
    private double    ymi;                                           // Y min
    private double    yma;                                           // Y max

    // jener, trni, Pred_mix
    private boolean halfi;                                           // C3vsTDS half integer

    // Pred_mix type
    private boolean omcarea;                                         // draw obs-cal selection area
    private boolean pm_freq;                                         // Pred_mix freq or int
    private boolean pm_I;                                            // Pred_mix X-coordinate integer or real
    private JFText  jft;                                             // to show selected obs-cal

    private boolean zoom;                                            // draw zoom area

    // mouse location
    private JLabel jlx;                                              // X mouse in data scale
    private JLabel jly;                                              // Y mouse in data scale

    // first tick , interval, nb of intervals, last tick
    private double[] cadreX = {0., 0., 0., 0.};                      // X axis
    private double[] cadreY = {0., 0., 0., 0.};                      // Y axis
    private Rectangle2D rectS;                                       // rectangle defined by a string

    private String lnsep;                                            // line separator
    private Font   monoFt;                                           // Monospaced font

/////////////////////////////////////////////////////////////////////

    /**
     * Construct a new PanAffich.
     *
     * @param cx        x coordinates of data
     * @param cy        y coordinates of data
     * @param cnxyf     names of data files
     * @param cntfile   type of data (jener | simul | spectr | spectrt)
     * @param cjlx      x mouse location label
     * @param cjly      y mouse location label
     */
    public PanAffich(double[] cx, double[] cy, String[] cnxyf, String cntfile, JLabel cjlx, JLabel cjly) {

        construct(cx, cy, cnxyf, cntfile, cjlx, cjly);               // construct PanAffich
        if( x[0] != (double)( (int) x[0] ) ) {                       // AJ or J ?
            // AJ, C3vsTDS half integer
            halfi = true;
        }
        else {
            // J, integer
            halfi = false;
        }
    }

    /**
     * Construct a new PanAffich to draw Ener[_assign]_Pn file.
     *
     * @param cx        x coordinates of data
     * @param cy        y coordinates of data
     * @param cnxyf     names of data files
     * @param cntfile   type of data (trni)
     * @param cjlx      x mouse location label
     * @param cjly      y mouse location label
     * @param cconcomp  component contribution (for each data point and each component)
     * @param climcomp  component limits       (for each component)
     * @param cicompa   displayable component index
     * @param ccoul     component colors
     */
    public PanAffich(double[] cx, double[] cy, String[] cnxyf, String cntfile, JLabel cjlx, JLabel cjly,
                     int[][] cconcomp, int[][] climcomp, int[] cicompa, Color[] ccoul) {

        // Ener[_assign]_Pn case
//      concomp = new int[nbxy][xnbcomp];                            // component contribution
        concomp = cconcomp;                                          // point to it
        xnbcomp = climcomp[0].length;                                // nb of components
        limcomp = new int[2][xnbcomp];                               // component limits
        limcomp = climcomp;                                          // point to it
        nbcomp  = cicompa.length;                                    // nb    of displayable component
//      icompa  = new int[nbcomp];                                   // index of displayable component
        icompa  = cicompa;                                           // point to it
        coul    = ccoul;                                             // component colors

        construct(cx, cy, cnxyf, cntfile, cjlx, cjly);               // construct PanAffich
    }

    /**
     * Construct a new PanAffich to draw Pred_mix file.
     *
     * @param cx        x coordinates of data
     * @param cy        y coordinates of data
     * @param cnxyf     names of data files
     * @param cntfile   type of data (predmix)
     * @param cntrans   transition name
     * @param cjlx      x mouse location label
     * @param cjly      y mouse location label
     * @param csmark    selection mark related to data
     * @param cjsyn     assignments    related to data
     * @param ccomp     component      related to data
     * @param climcomp  component limits       (for each component)
     * @param cicompa   displayable component index
     * @param ccoul     component colors
     */
    public PanAffich(double[] cx, double[] cy, String[] cnxyf, String cntfile, String cntrans,
                     JLabel cjlx, JLabel cjly, String[] csmark, String[] cjsyn, int[] ccomp,
                     int[][] climcomp, int[] cicompa, Color[] ccoul) {

        // Pred_mix case
//      smark = new String[nbxy];                                    // define the String array
        smark = csmark;                                              // point to it
//      jsyn  = new String[nbxy];                                    // define the String array
        jsyn  = cjsyn;                                               // point to it
//      comp  = new int[nbxy];                                       // define the int    array
        comp  = ccomp;                                               // point to it
        xnbcomp = climcomp[0].length;                                // nb of components
        limcomp = new int[2][xnbcomp];                               // component limits
        limcomp = climcomp;                                          // point to it
        nbcomp  = cicompa.length;                                    // nb    of displayable component
//      icompa  = new int[nbcomp];                                   // index of displayable component
        icompa  = cicompa;                                           // point to it
        coul    = ccoul;                                             // component colors
        // types
        if( cntfile.equals("predmixf_I") ||
            cntfile.equals("predmixf_R")    ) {
            pm_freq = true;                                          // frequency
        }
        else {
            pm_freq = false;                                         // intensity
        }
        if( cntfile.equals("predmixf_I") ||
            cntfile.equals("predmixi_I")    ) {
            pm_I = true;                                             // integer
        }
        else {
            pm_I = false;                                            // real
        }
        ntrans = cntrans;                                            // transition name

        construct(cx, cy, cnxyf, cntfile, cjlx, cjly);               // construct PanAffich
    }

    // Construct drawing panel
    private void construct(double[] cx, double[] cy, String[] cnxyf, String cntfile, JLabel cjlx, JLabel cjly) {

        lnsep = System.getProperty("line.separator");
        x = cx;                                                      // X data
        y = cy;                                                      // Y data
        nbxy = x.length;                                             // nb of data points
        nbf = cnxyf.length;                                          // nb of data files
        nxyf = new String[nbf];                                      // data file names
        nxyf = cnxyf;                                                // point to it
        if     ( cntfile.equals("predmixf_I") ||
                 cntfile.equals("predmixi_I")    ) {
            ntfile = "predmix_I";
        }
        else if( cntfile.equals("predmixf_R") ||
                 cntfile.equals("predmixi_R")    ) {
            ntfile = "predmix_R";                                      // generic name
        }
        else {
            ntfile = cntfile;                                        // file type
        }

        jlx = cjlx;                                                  // label for X mouse location
        jly = cjly;                                                  // label for Y mouse location

        if( ntfile.equals("jener")     ||
            ntfile.equals("trni")      ||
            ntfile.equals("predmix_I") ||
            ntfile.equals("predmix_R")    ) {
            // set iemin, iemax
            double denmex  = 0.;                                     // max density of levels/ernegy
            double cdenmex = 0.;                                     // max density of levels/ernegy
            int    nbpts   = 0;
            int    ciemin  = 0;
            int    ciemax  = 0;
            double xc      = x[0];                                   // current x
            double yc;

            iemin = 0;
            iemax = iemin;
            for( int i=0; i<nbxy; i++ ) {                            // for each data point
                //
                if( x[i] != xc     ||
                    i    == nbxy-1    ) {                            // new x (J)
                    yc = y[ciemax]-y[ciemin];
                    if( yc != 0. ) {
                        cdenmex = nbpts/yc;
                        if( cdenmex > denmex ) {
                            denmex = cdenmex;                        // set nbmex
                            iemin  = ciemin;
                            iemax  = ciemax;
                        }
                    }
                    nbpts  = 0;                                      // reset
                    ciemin = i;
                    ciemax = i;
                    xc     = x[i];                                   // reset
                }
                else {
                    yc = y[i];                                       // current y
                    if( yc < y[ciemin] ) {
                        ciemin = i;
                    }
                    if( yc > y[ciemax] ) {
                        ciemax = i;
                    }
                }
                nbpts ++;
            }
        }

        init();                                                      // init everything

        addMouseListener(this);                                      // mouse listener
        addMouseMotionListener(this);                                // mouse motion listener
        zoom = false;                                                // NO zoom           area
        omcarea = false;                                             // NO pred selection area
    }

    /**
     * Initializes drawing of all data.
     */
    public void init() {

        // set data area limits
        // X
        xmi = x[0];
        xma = x[nbxy-1];
        // Y
        ymi = y[0];
        yma = ymi;
        for(int i=0; i<nbxy; i++) {
            if( y[i] < ymi ) {
                ymi = y[i];
            }
            if( y[i] > yma ) {
                yma = y[i];
            }
        }
        if( ntfile.equals("jener") || ntfile.equals("trni") || ntfile.equals("predmix_I") ) {
            xmi = xmi-.33;
            xma = xma+.33;
            if( ymi == yma ) {
                // must NOT be null => correction for cadre()
                double corr = Math.max((double).5,ymi/100.);         // arbitrary correction
                ymi = ymi - corr;
                yma = yma + corr;
            }
        }
        else if( ntfile.equals("spectr") || ntfile.equals("spectrt") ) {
            ymi = 0.;
        }

        // set intervals
        cadre(xmi, xma-xmi, cadreX);
        cadre(ymi, yma-ymi, cadreY);
        defxlim();                                                   // define start and end indexes
        prevp_wxma = -1;                                             // force call to setRedSpect at next repaint

        unPredsel();                                                 // reset prediction selection
    }

/////////////////////////////////////////////////////////////////////////////

    /**
     * repaint with setRedSpect
     */
    public void redRepaint() {

        prevp_wxma = -1;                                             // force call to setRedSpect at next repaint
        repaint();                                                   // all
    }

    private void myrepaint(int x, int y, int width, int height) {

        // area slightly extended
        int x0 = Math.max(0,x-1);
        int y0 = Math.max(0,y-1);
        int w0 = Math.min(x+width ,p_wxma)-x0+1;
        int h0 = Math.min(y+height,p_wyma)-y0+1;
        repaint(x0, y0, w0, h0);
    }

    /**
     * Draw this panel content.
     */
    public void paintComponent(Graphics g) {

        super.paintComponent(g);                                     // prepare the panel

        p_wxma = this.getWidth() -1;                                 // pixel window X max
        p_wyma = this.getHeight()-1;                                 // pixel window Y max
        g2d = (Graphics2D) g;                                        // for extended functions

        boolean screen = true;                                       // screen drawing
        Dimension ss = Toolkit.getDefaultToolkit().getScreenSize();
        int p_width  = (int) ss.getWidth();
        int p_height = (int) ss.getHeight();
        drawData( g2d, p_wxma, p_wyma, screen, p_width, p_height );  // draw
    }

    /**
     * Print this panel content.
     */
    public int print(Graphics g, PageFormat pf, int pi) throws PrinterException {

        if (pi >= 1) {                                               // number of the page to print
            return Printable.NO_SUCH_PAGE;                           // only one
        }

        double pfIW = pf.getImageableWidth();
        double pfIH = pf.getImageableHeight();
        // we have to reduce printed area to fit various OS behaviours
        int prp_wxma = (int) (.9*pfIW -1);                           // paper printable X max
        int prp_wyma = (int) (.9*pfIH -1);                           // paper printable Y max

        g2d = (Graphics2D) g;                                        // for extended functions
        g2d.translate(pf.getImageableX()+.05*pfIW,                   // translate to printable area
                      pf.getImageableY()+.05*pfIH );

        boolean screen = false;                                      // NOT screen but printer drawing
        int p_width  = (int) pf.getWidth();
        int p_height = (int) pf.getHeight();
        drawData( g2d, prp_wxma, prp_wyma, screen, p_width, p_height );  // draw

        return Printable.PAGE_EXISTS;                                // page ready
    }

/////////////////////////////////////////////////////////////////////////////

    // create the page to print or draw on screen
    private void drawData(Graphics2D g2d, int ddp_wxma, int ddp_wyma, boolean screen, int ddp_width, int ddp_height) {

        // all dd_ and ddp_ variables are drawData local
        // usefull variables
        int    ddp_x0;
        int    ddp_x1;
        int    ddp_x2;
        int    ddp_x3;
        int    ddp_y0;
        int    ddp_y1;
        int    ddp_y2;
        int    ddp_y3;
        String dd_str;

        // paint
        g2d.setColor(Color.BLACK);                                   // black
        int dd_fontsz = (int) Math.round(ddp_height*sc_fontsz);      // font size
        monoFt = new Font("Monospaced", Font.PLAIN, dd_fontsz);      // default font
        g2d.setFont( monoFt );
        FontMetrics fm = g2d.getFontMetrics();                       // for its methods

        // define font for file names
        int dd_fontF = dd_fontsz;                                    // file name font size
        for( int i=0; i<nbf; i++ ) {                                 // for each file
            rectS = fm.getStringBounds(nxyf[i],g2d);                 // nyf string size
            int fs = (int) Math.round(dd_fontsz*.75*ddp_height/rectS.getWidth());  // font max size
            fs = Math.min(fs,dd_fontsz);                             // limited to dd_fontsz
            if( fs < dd_fontF ) {
                dd_fontF = fs;                                       // save the smallest one
            }
        }
        // TRNI : define font for component limits
        int dd_fontC = dd_fontsz;                                    // component limits font size
        String aStr;
        if( ntfile.equals("trni")      ||
            ntfile.equals("predmix_I") ||
            ntfile.equals("predmix_R")    ) {
            // string of component limits
            aStr = "";
            if( (! ntfile.equals("trni")) &&
                (! ntrans.equals("")    )    ) {
                aStr = "("+ntrans+") ";
            }
            for( int j=0; j<nbcomp; j++ ) {
                int i = icompa[j];
                // displayable
                if( j != 0 ) {
                    // not 1st one
                    aStr = aStr+",";
                }
                aStr = aStr+Math.abs(limcomp[0][i])+"-"+Math.abs(limcomp[1][i]);
            }
            rectS  = fm.getStringBounds(aStr,g2d);                   // string size
            int fs = (int) Math.round(dd_fontsz*.75*ddp_height/rectS.getWidth());  // font max size
            dd_fontC = Math.min(fs,dd_fontsz);                       // limited to dd_fontsz
        }

        // ddp_wxma and ddp_wyma are defined before call to drawData
        // in paintComponent (screen) or print (printer)
        int ddp_gap   = (int) Math.round(ddp_width*sc_gap);          // gap
        int ddp_cxmi  = 2*ddp_gap+(int) Math.round(dd_fontsz*2.5);   // x min
        int ddp_cxma  = ddp_wxma-ddp_cxmi;                           // x max
        int ddp_cymi0 = ddp_cxmi;                                    // default y min
        int ddp_cymi  = Math.max((int) Math.round(dd_fontF*(1.3*nbf+.5)),ddp_cymi0);  // y min (at least p_cymi0)
        if( ntfile.equals("trni") ) {
            ddp_cymi  = Math.max((int) Math.round(dd_fontF*(1.3*nbf+.5)+dd_fontC*1.3),ddp_cymi0);  // y min (at least p_cymi0)
        }
        int ddp_cyma  = ddp_wyma-ddp_cymi0;                          // y max
        int ddp_cross = (int) Math.round(ddp_width*sc_cross);        // cross branch size
        // save some for external usage (mouse location, reduced data)
        if( screen ) {
            p_gap   = ddp_gap;
            p_cxmi  = ddp_cxmi;
            p_cxma  = ddp_cxma;
            p_cymi  = ddp_cymi;
            p_cyma  = ddp_cyma;
            p_cross = ddp_cross;
            if( prevp_wxma == -1                                                                                                      ||
                (ddp_wxma  != prevp_wxma && (   ntfile.equals("simul") ||   ntfile.equals("spectr") ||   ntfile.equals("spectrt") ) ) ||
                (ddp_wyma  != prevp_wyma && ( ! ntfile.equals("simul") && ! ntfile.equals("spectr") && ! ntfile.equals("spectrt") ) )    ) {
                setRedSpect();                                       // reduce spectrum data if necessary
            }
            prevp_wxma = ddp_wxma;                                   // keep for next call
            prevp_wyma = ddp_wyma;                                   // keep for next call
        }

        // in-clip area to (re)paint
        Rectangle rect = g2d.getClipBounds();                        // in-clip rectangle
        int    ddp_clipxmi = Math.max(0,rect.x-1);                   // lower Y pixel ??? one more needed
        int    ddp_clipxma = rect.x+rect.width;                      // upper Y pixel ??? one more needed
        double dd_clipxmi  = p2X(ddp_clipxmi, ddp_cxmi, ddp_cxma);   // lower  X
        double dd_clipxma  = p2X(ddp_clipxma, ddp_cxmi, ddp_cxma);   // higher X
        int    ddp_clipymi = Math.max(0,rect.y-1);                   // lower Y pixel ??? one more needed
        int    ddp_clipyma = rect.y+rect.height;                     // upper Y pixel ??? one more needed
        double dd_clipymi  = p2Y(ddp_clipyma, ddp_cymi, ddp_cyma);   // lower  Y
        double dd_clipyma  = p2Y(ddp_clipymi, ddp_cymi, ddp_cyma);   // higher Y

        // draw file names
        // in-clip area not taken into account
        g2d.setFont(new Font("Monospaced",Font.PLAIN, dd_fontF));    // chosen font
        for( int i=0; i<nbf; i++ ) {
            g2d.drawString(nxyf[i], ddp_cxmi, (int) Math.round((i+1)*dd_fontF*1.3));
        }
        // TRNI|PREDMIX : draw component limits
        if( ntfile.equals("trni")      ||
            ntfile.equals("predmix_I") ||
            ntfile.equals("predmix_R")    ) {
            g2d.setFont(new Font("Monospaced",Font.PLAIN, dd_fontC));  // chosen font
            ddp_x0 = ddp_cxmi;
            ddp_y0 = (int) Math.round((nbf*dd_fontF+dd_fontC)*1.3);
            if( ntfile.equals("predmix_I") ||
                ntfile.equals("predmix_R")    ) {
                if( ! ntrans.equals("") ) {
                    aStr = "("+ntrans+") ";
                    g2d.drawString(aStr, ddp_x0, ddp_y0);
                    rectS  = fm.getStringBounds(aStr,g2d);                     // string size
                    ddp_x0 = ddp_x0 + Math.round( (float) rectS.getWidth() );  // next position
                }
            }
            for( int j=0; j<nbcomp; j++ ) {
                int i = icompa[j];
                // displayable
                aStr = "";
                if( j != 0 ) {
                    // not 1st one, insert a comma
                    aStr = ","+aStr;
                }
                aStr = aStr+Math.abs(limcomp[0][i])+"-"+Math.abs(limcomp[1][i]);     // added string
                g2d.setColor(coul[j - coul.length*(j/coul.length)]);
                g2d.drawString(aStr, ddp_x0, ddp_y0);
                rectS  = fm.getStringBounds(aStr,g2d);                     // string size
                ddp_x0 = ddp_x0 + Math.round( (float) rectS.getWidth() );  // next position
            }
            g2d.setColor(Color.BLACK);                               // black
        }
        // default font
        g2d.setFont( monoFt );

        // axis and ticks
        // in-clip area not taken into account
        // X data axis
        ddp_y0 = ddp_cyma+ddp_gap;
        ddp_y1 = ddp_y0+ddp_gap;
        ddp_y2 = ddp_y1+(int) Math.round(dd_fontsz*1.2);             // text location
        g2d.drawLine( ddp_cxmi, ddp_y0,
                      ddp_cxma, ddp_y0);                             // draw axis
        if( ntfile.equals("jener") || ntfile.equals("trni") || ntfile.equals("predmix_I") ) {  // jener/trni/predmix file type
            for( int i = 0; i<=(int) cadreX[2]; i++) {               // for each tick mark
                double xtic = cadreX[0]+i*cadreX[1];                 // tick mark location (double)
                if( Math.abs(xtic%1.) <= .0001 ) {                   // has to be integer (~ 1/10000)
                    if( halfi ) {
                        // AJ
                        xtic = Math.round(xtic)+0.5;
                    }
                    if( xtic > xmi &&
                        xtic < xma ) {                               // in limits
                        ddp_x0 = ip_X(xtic, ddp_cxmi, ddp_cxma);
                        g2d.drawLine(ddp_x0, ddp_y0,
                                     ddp_x0, ddp_y1);                // draw tick mark
                        if( halfi ) {
                            dd_str = ""+xtic;
                        }
                        else {
                            dd_str = ""+(int) Math.round(xtic);
                        }
                        rectS  = fm.getStringBounds(dd_str,g2d);     // string size
                        g2d.drawString(dd_str,
                                       (int) Math.round(ddp_x0-rectS.getWidth()/2.), ddp_y2);  // center value of each tick
                    }
                }
            }
        }
        else {                                                       // spectr/spectrt/simul file type
            for( int i = 0; i<=(int) cadreX[2]; i++) {               // for each tick mark
                ddp_x0 = ip_X(cadreX[0]+i*cadreX[1], ddp_cxmi, ddp_cxma);
                g2d.drawLine(ddp_x0, ddp_y0,
                             ddp_x0, ddp_y1);                        // draw tick mark
            }
            dd_str = ""+(float) cadreX[0];
            rectS  = fm.getStringBounds(dd_str,g2d);
            ddp_x0 = Math.max(0, (int) Math.round( ddp_cxmi-rectS.getWidth()/2. ));                                                // center value of 1st tick in limits
            g2d.drawString(dd_str,
                           ddp_x0, ddp_y2);
            dd_str = "("+(float) cadreX[1]+")";
            rectS  = fm.getStringBounds(dd_str,g2d);
            ddp_x0 = (int) Math.round( (ddp_cxmi+ddp_cxma-rectS.getWidth())/2. );                                                  // center interval value
            g2d.drawString(dd_str,
                           ddp_x0, ddp_y2);
            dd_str = ""+(float) cadreX[3];
            rectS  = fm.getStringBounds(dd_str,g2d);
            ddp_x0 = Math.min( (int) Math.round( ddp_cxma-rectS.getWidth()/2. ), (int) Math.round( ddp_wxma-rectS.getWidth() ) );  // center value of last tick in limits
            g2d.drawString(dd_str,
                           ddp_x0, ddp_y2);
        }

        // Y data axis
        ddp_x0 = ddp_cxmi-ddp_gap;
        ddp_x1 = ddp_x0-ddp_gap;
        g2d.drawLine( ddp_x0, ddp_cyma,
                      ddp_x0, ddp_cymi );                            // draw axis
        for( int i = 0; i<=(int) cadreY[2]; i++) {                   // for each tick mark
            ddp_y0 = ip_Y(cadreY[0]+i*cadreY[1], ddp_cymi, ddp_cyma);
            g2d.drawLine( ddp_x0, ddp_y0,
                          ddp_x1, ddp_y0);                           // draw tick mark
        }
        ddp_x2 = ddp_x1-(int) Math.round(dd_fontsz*.4);              // text location
        dd_str = ""+ (float) cadreY[0];
        rectS  = fm.getStringBounds(dd_str,g2d);
        ddp_y0 = Math.min( ddp_y1, (int) Math.round( ddp_cyma+rectS.getWidth()/2. ) );                             // center value of 1st tick in limits
        drawRotString(dd_str,
                      ddp_x2, ddp_y0);
        dd_str = "("+(float) cadreY[1]+")";
        rectS  = fm.getStringBounds(dd_str,g2d);
        ddp_y0 = (int) Math.round( (ddp_cymi+ddp_cyma+rectS.getWidth())/2. );                                      // center interval value
        drawRotString(dd_str,
                      ddp_x2, ddp_y0);
        dd_str = ""+ (float) cadreY[3];
        rectS  = fm.getStringBounds(dd_str,g2d);
        ddp_y0 = Math.max( (int) Math.round( ddp_cymi+rectS.getWidth()/2. ), (int) Math.round(rectS.getWidth()));  // center value of last tick in limits
        drawRotString(dd_str,
                      ddp_x2, ddp_y0);

        // draw data
        if( (ddp_clipxma >= ddp_cxmi && ddp_clipxmi <= ddp_cxma) &&
            (ddp_clipyma >= ddp_cymi && ddp_clipymi <= ddp_cyma)    ) {  // in in-clip area

            g2d.setClip( ddp_cxmi,            ddp_cymi,              // define out-clip area
                         ddp_cxma-ddp_cxmi+1, ddp_cyma-ddp_cymi+1 );
            g2d.setColor(Color.RED);                                 // data color
            //
            int ci;
            int cim1;
            int cixdeb;
            int cixfin;
            //
            boolean reduce;
            reduce = use_red && screen;
            if( reduce ) {
                // use reduced data
                cixdeb = 0;
                cixfin = nbxy_red-1;
            }
            else {
                // use raw data (printing or no reduction)
                cixdeb = ixdeb;
                cixfin = ixfin;
            }
            // specific clip area
            double c_clipxmi;
            double c_clipxma;
            double c_clipymi;
            double c_clipyma;
            // jener and trni case
            c_clipxmi = dd_clipxmi-.33;
            c_clipxma = dd_clipxma+.33;
            if     ( ntfile.equals("jener") ) {                      // draw levels
                // jener
                for( int i=cixdeb; i<=cixfin; i++ ) {
                    if( reduce ) {
                        ci   = i_red[i];
                    }
                    else {
                        ci = i;
                    }
                    if( x[ci] < c_clipxmi ) {
                        continue;
                    }
                    if( x[ci] > c_clipxma ) {
                        break;
                    }
                    if( y[ci] < dd_clipymi ||
                        y[ci] > dd_clipyma    ) {
                        continue;
                    }
                    ddp_x0 = ip_X(x[ci]-.33, ddp_cxmi, ddp_cxma);    // 1/3rd before
                    ddp_x1 = ip_X(x[ci]+.33, ddp_cxmi, ddp_cxma);    // 1/3rd after
                    ddp_y0 = ip_Y(y[ci],     ddp_cymi, ddp_cyma);    // energy
                    ddp_y1 = ddp_y0;
                    g2d.drawLine( ddp_x0, ddp_y0,
                                  ddp_x1, ddp_y1);
                }
            }
            else if( ntfile.equals("trni") ) {                       // draw sticks
                // trni
                double  xdeb;
                double  xfin;
                int     ccomp;
                ComPoint[] cpt = new ComPoint[xnbcomp];              // xdeb, xfin of displayable components
                int cnbdcomp;                                        // current number of displayable components
                for( int i=cixdeb; i<=cixfin; i++ ) {
                    if( reduce ) {
                        ci   = i_red[i];
                    }
                    else {
                        ci = i;
                    }
                    if( x[ci] < c_clipxmi ) {
                        continue;
                    }
                    if( x[ci] > c_clipxma ) {
                        break;
                    }
                    if( y[ci] < dd_clipymi ||
                        y[ci] > dd_clipyma    ) {
                        continue;                                    // out of Y range
                    }
                    ddp_y0 = ip_Y(y[ci], ddp_cymi, ddp_cyma);        // energy
                    ddp_y1 = ddp_y0;
                    xdeb = x[ci]-.33;
                    cnbdcomp = 0;
                    int nbc;
                    for( int j=0; j<xnbcomp; j++ ) {                 // add components
                        xfin = xdeb+concomp[ci][j]*.0001*.66;
                        if( limcomp[0][j] > 0 ) {
                            // displayable
                            if( xfin != xdeb ) {
                                for( nbc=0; nbc<nbcomp; nbc++ ) {  // search for its index
                                    if( icompa[nbc] == j ) {
                                        break;
                                    }
                                }
                                // save it
                                cpt[cnbdcomp] = new ComPoint( ip_X(xdeb, ddp_cxmi, ddp_cxma),
                                                              ip_X(xfin, ddp_cxmi, ddp_cxma),
                                                              nbc                             );
                                cnbdcomp ++;
                            }
                        }
                        xdeb = xfin;
                    }
                    if ( cnbdcomp != 0 ) {
                        // something to draw
                        ComPoint ccpt[] = new ComPoint[cnbdcomp];
                        System.arraycopy(cpt,0,ccpt,0,cnbdcomp);
                        Arrays.sort(ccpt);                           // increasing size order
                        for(int k=cnbdcomp-1; k >= 0; k --) {        // draw big first
                            ddp_x0 = ccpt[k].getIp_deb();            // beginning of the component contribution
                            ddp_x1 = ccpt[k].getIp_fin();            // end       of the component contribution
                            nbc    = ccpt[k].getIcol();              // color index
                            g2d.setColor(coul[nbc -coul.length*(nbc/coul.length)]);
                            g2d.drawLine( ddp_x0, ddp_y0,
                                        ddp_x1, ddp_y1);
                        }
                    }
                }
            }
            else if( ntfile.equals("predmix_I") ||
                     ntfile.equals("predmix_R")    ) {               // draw discrepancy
                // predmix
                g2d.setColor(Color.BLUE);
                ddp_y0 = ip_Y(0., ddp_cymi, ddp_cyma);
                g2d.drawLine( ddp_cxmi, ddp_y0,
                              ddp_cxma, ddp_y0);                     // zero line
                // specific clip area including cross size
                c_clipxmi = p2X(ddp_clipxmi-ddp_cross, ddp_cxmi, ddp_cxma);
                c_clipxma = p2X(ddp_clipxma+ddp_cross, ddp_cxmi, ddp_cxma);
                c_clipymi = p2Y(ddp_clipyma+ddp_cross, ddp_cymi, ddp_cyma);
                c_clipyma = p2Y(ddp_clipymi-ddp_cross, ddp_cymi, ddp_cyma);
                for( int i=cixdeb; i<=cixfin; i++ ) {
                    if( reduce ) {
                        ci   = i_red[i];
                    }
                    else {
                        ci = i;
                    }
                    if( x[ci] < c_clipxmi ) {
                        continue;
                    }
                    if( x[ci] > c_clipxma ) {
                        break;
                    }
                    if( y[ci] < c_clipymi ||
                        y[ci] > c_clipyma    ) {
                        continue;                                    // out of Y range
                    }
                    if( limcomp[0][comp[ci]] > 0 ) {
                        // displayabale
                        int ic = comp[ci]-coul.length*(comp[ci]/coul.length);
                        g2d.setColor(coul[ic]);                      // color
                        //
                        int ddp_xc = ip_X(x[ci], ddp_cxmi, ddp_cxma);
                        int ddp_yc = ip_Y(y[ci], ddp_cymi, ddp_cyma);
                        if     ( smark[ci].equals("+") ) {           // cross
                            ddp_x0 = ddp_xc-ddp_cross;
                            ddp_y0 = ddp_yc;
                            ddp_x1 = ddp_xc+ddp_cross;
                            ddp_y1 = ddp_yc;
                            ddp_x2 = ddp_xc;
                            ddp_y2 = ddp_yc-ddp_cross;
                            ddp_x3 = ddp_xc;
                            ddp_y3 = ddp_yc+ddp_cross;
                            g2d.drawLine( ddp_x0, ddp_y0, ddp_x1, ddp_y1);
                            g2d.drawLine( ddp_x2, ddp_y2, ddp_x3, ddp_y3);
                        }
                        else {
                            if( smark[ci].equals("-") ) {            // square
                                ddp_x0 = ddp_xc-ddp_cross;
                                ddp_y0 = ddp_yc-ddp_cross;
                                ddp_x1 = ddp_x0;
                                ddp_y1 = ddp_yc+ddp_cross;
                                ddp_x2 = ddp_xc+ddp_cross;
                                ddp_y2 = ddp_y1;
                                ddp_x3 = ddp_x2;
                                ddp_y3 = ddp_y0;
                            }
                            else {                                   // rhombus
                                ddp_x0 = ddp_xc-ddp_cross;
                                ddp_y0 = ddp_yc;
                                ddp_x1 = ddp_xc;
                                ddp_y1 = ddp_yc+ddp_cross;
                                ddp_x2 = ddp_xc+ddp_cross;
                                ddp_y2 = ddp_yc;
                                ddp_x3 = ddp_xc;
                                ddp_y3 = ddp_yc-ddp_cross;
                            }
                            g2d.drawLine( ddp_x0, ddp_y0, ddp_x1, ddp_y1);
                            g2d.drawLine( ddp_x1, ddp_y1, ddp_x2, ddp_y2);
                            g2d.drawLine( ddp_x2, ddp_y2, ddp_x3, ddp_y3);
                            g2d.drawLine( ddp_x3, ddp_y3, ddp_x0, ddp_y0);
                        }
                    }
                }
                g2d.setColor(Color.BLACK);
            }
            else if( ntfile.equals("spectr") || ntfile.equals("spectrt") ) {  // draw sticks
                // spectr
                if( cixdeb <= cixfin ) {
                    for( int i=cixdeb; i<=cixfin; i++ ) {
                        if( reduce ) {
                            ci   = i_red[i];
                        }
                        else {
                            ci = i;
                        }
                        if( x[ci] < dd_clipxmi ) {
                            continue;
                        }
                        if( x[ci] > dd_clipxma ) {
                            break;
                        }
                        if( y[ci] < dd_clipymi ) {
                            continue;
                        }
                        ddp_x0 = ip_X(x[ci], ddp_cxmi, ddp_cxma);
                        ddp_x1 = ddp_x0;
                        ddp_y0 = ddp_cyma;                           // data area base
                        ddp_y1 = ip_Y(y[ci], ddp_cymi, ddp_cyma);
                        g2d.drawLine( ddp_x0, ddp_y0,
                                      ddp_x1, ddp_y1);
                    }
                }
            }
            else if( ntfile.equals("simul") ) {                      // draw spectrum
                // simul
                if( cixdeb <= cixfin ) {
                    for( int i=cixdeb+1; i<=cixfin; i++ ) {
                    if( reduce ) {
                        ci   = i_red[i];
                        cim1 = i_red[i-1];
                    }
                    else {
                        ci = i;
                        cim1 = i-1;
                    }
                        if( x[ci]   < dd_clipxmi ) {
                            continue;
                        }
                        if( x[cim1] > dd_clipxma ) {
                            break;
                        }
                        ddp_x0 = ip_X(x[cim1], ddp_cxmi, ddp_cxma);
                        ddp_x1 = ip_X(x[ci],   ddp_cxmi, ddp_cxma);
                        ddp_y0 = ip_Y(y[cim1], ddp_cymi, ddp_cyma);
                        ddp_y1 = ip_Y(y[ci],   ddp_cymi, ddp_cyma);
                        g2d.drawLine( ddp_x0, ddp_y0,
                                      ddp_x1, ddp_y1);
                    }
                }
            }
        }
        //
        g2d.setClip(null);
        if( zoom || omcarea ) {                                      // if zoom or selection area
            // in-clip area not taken into account
            g2d.setColor(Color.BLACK);                               // black
            g2d.drawRect((int) Math.min(p_zx0,p_zx1), (int) Math.min(p_zy0,p_zy1),
                         (int) Math.abs(p_zx1-p_zx0), (int) Math.abs(p_zy1-p_zy0));  // zoom/selection area
        }
    }

    // convert X from double to pixel
    private int ip_X( double x, int cup_cxmi, int cup_cxma) {

        double cup_cwidth = cup_cxma-cup_cxmi;
        return (int) Math.round(cup_cxmi+cup_cwidth*((x-cadreX[0])/(cadreX[1]*cadreX[2])));
    }
    // convert X from pixel to double
    private double p2X( int p_x, int cup_cxmi, int cup_cxma) {

        double cup_cwidth = cup_cxma-cup_cxmi;
        return cadreX[0]+(cadreX[1]*cadreX[2])*(p_x-cup_cxmi)/cup_cwidth;
    }

    // convert Y from double to pixel
    private int ip_Y( double y, int cup_cymi, int cup_cyma) {

        double cup_cheight = cup_cyma-cup_cymi;
        return (int) Math.round(cup_cyma-cup_cheight*((y-cadreY[0])/(cadreY[1]*cadreY[2])));
    }
    // convert Y from pixel to double
    private double p2Y( int p_y, int cup_cymi, int cup_cyma) {

        double cup_cheight = cup_cyma-cup_cymi;
        return cadreY[0]+(cadreY[1]*cadreY[2])*(cup_cyma-p_y)/cup_cheight;
    }

    // write a string along Y axis
    private void drawRotString( String s, int x, int y) {

        AffineTransform at = g2d.getTransform();
        g2d.rotate(-Math.PI/2., x, y);                               // rotate
        g2d.drawString(s, x, y);
        g2d.setTransform( at );                                      // back to previous transform
    }

/////////////////////////////////////////////////////////////////////////////

// mouse events management

    /**
     * Mouse moved without button pressed.
     * <br>Shows its location.
     */
    public void mouseMoved(MouseEvent mevt) {

        p_xmouse = mevt.getX();                                      // save mouse X
        p_ymouse = mevt.getY();                                      // and  mouse Y
        showXY();                                                    // display mouse location
    }

    /**
     * Mouse button pressed.
     * <br>(zoom/omcarea starting point)
     */
    public void mousePressed(MouseEvent mevt) {

        unPredsel();

        p_zx0 = mevt.getX();                                         // save zoom/selection starting X
        p_zy0 = mevt.getY();                                         // save zoom/selection starting Y
        // limited to data area
        if     ( p_zx0 < p_cxmi-p_gap/2 ) {
            p_zx0 = p_cxmi-p_gap/2;
        }
        else if( p_zx0 > p_cxma+p_gap/2 ) {
            p_zx0 = p_cxma+p_gap/2;
        }
        if     ( p_zy0 < p_cymi-p_gap/2 ) {
            p_zy0 = p_cymi-p_gap/2;
        }
        else if( p_zy0 > p_cyma+p_gap/2 ) {
            p_zy0 = p_cyma+p_gap/2;
        }
        prevp_zx1 = p_zx0;
        prevp_zy1 = p_zy0;
    }

    /**
     * Mouse moved with button pressed.
     * <br>(zoom/omcarea area definition)
     */
    public void mouseDragged(MouseEvent mevt) {

        p_xmouse = mevt.getX();                                      // save mouse X
        p_ymouse = mevt.getY();                                      // and  mouse Y
        showXY();                                                    // display mouse location

        if     ( SwingUtilities.isLeftMouseButton(mevt) ) {
            // left button
            zoom = true;                                             // zoom area has to be shown
        }
        else if( SwingUtilities.isRightMouseButton(mevt) ) {
            // right button
            if( ntfile.equals("predmix_I") ||
                ntfile.equals("predmix_R")    ) {
                omcarea = true;                                      // prediction selection area has to be shown
            }
            else {
                return;
            }
        }
        else {
            // unmanaged button
            return;
        }

        p_zx1 = p_xmouse;                                            // save zoom/selection current X
        p_zy1 = p_ymouse;                                            // save zoom/selection current Y
        // limited to data area
        if     ( p_zx1 < p_cxmi-p_gap/2 ) {
            p_zx1 = p_cxmi-p_gap/2;
        }
        else if( p_zx1 > p_cxma+p_gap/2 ) {
            p_zx1 = p_cxma+p_gap/2;
        }
        if     ( p_zy1 < p_cymi-p_gap/2 ) {
            p_zy1 = p_cymi-p_gap/2;
        }
        else if( p_zy1 > p_cyma+p_gap/2 ) {
            p_zy1 = p_cyma+p_gap/2;
        }

        // actualize zoom/selection in 2 steps, one for each edge
        p_paintx      = (int) Math.min(prevp_zx1, p_zx1);
        p_painty      = (int) Math.min(p_zy0,Math.min(prevp_zy1,p_zy1));
        p_paintwidth  = (int) Math.abs(p_zx1-prevp_zx1)+1;
        p_paintheight = (int) Math.max(Math.abs(p_zy0-p_zy1),Math.abs(p_zy0-prevp_zy1))+1;
        myrepaint(p_paintx, p_painty, p_paintwidth, p_paintheight);
        p_paintx      = (int) Math.min(p_zx0,Math.min(prevp_zx1,p_zx1));
        p_painty      = (int) Math.min(prevp_zy1, p_zy1);
        p_paintwidth  = (int) Math.max(Math.abs(p_zx0-p_zx1),Math.abs(p_zx0-prevp_zx1))+1;
        p_paintheight = (int) Math.abs(p_zy1-prevp_zy1)+1;
        myrepaint(p_paintx, p_painty, p_paintwidth, p_paintheight);
        //
        prevp_zx1 = p_zx1;                                           // save as previous location
        prevp_zy1 = p_zy1;
    }

    /**
     * Mouse button released.
     * <br>(zoom/omcarea ending point)
     */
    public void mouseReleased(MouseEvent mevt) {

        double p_x1 = mevt.getX();                                   // save zoom/selection ending X
        double p_y1 = mevt.getY();                                   // save zoom/selection ending Y

        if( SwingUtilities.isRightMouseButton(mevt) ) {
            // right button
            if( ! ntfile.equals("predmix_I") &&
                ! ntfile.equals("predmix_R")    ) {
                return;
            }
        }
        else if( ! SwingUtilities.isLeftMouseButton(mevt) ) {
            // unmanaged button
            return;
        }

        p_zx1 = p_x1;
        p_zy1 = p_y1;
        // limited to data area
        if     ( p_x1 < p_cxmi-p_gap/2 ) {
            p_zx1 = p_cxmi-p_gap/2;
        }
        else if( p_x1 > p_cxma+p_gap/2 ) {
            p_zx1 = p_cxma+p_gap/2;
        }
        if     ( p_y1 < p_cymi-p_gap/2 ) {
            p_zy1 = p_cymi-p_gap/2;
        }
        else if( p_y1 > p_cyma+p_gap/2 ) {
            p_zy1 = p_cyma+p_gap/2;
        }
        if( p_zx1 == p_zx0 &&
            p_zy1 == p_zy0    ) {
            return;
        }
        // test if zoom/selection area is large enough
        if( Math.abs(p_zx1-p_zx0) < 5 ||
            Math.abs(p_zy1-p_zy0) < 5    ) {
            p_paintx      = (int) Math.min(p_zx0, p_zx1);
            p_painty      = (int) Math.min(p_zy0, p_zy1);
            p_paintwidth  = (int) Math.abs(p_zx1-p_zx0)+1;
            p_paintheight = (int) Math.abs(p_zy1-p_zy0)+1;
            JOptionPane.showMessageDialog(null,"Selected area to small");
            // give up
            zoom    = false;
            omcarea = false;
            myrepaint(p_paintx, p_painty, p_paintwidth, p_paintheight);
            return;
        }

        // zoom/selection area limits in data units
        double cxmi;
        double cxma;
        double cymi;
        double cyma;

        if( SwingUtilities.isRightMouseButton(mevt) ) {
            // right button : prediction selection
            cxmi = p2X((int) Math.min(p_zx0,p_zx1)-p_cross, p_cxmi, p_cxma);
            cxma = p2X((int) Math.max(p_zx0,p_zx1)+p_cross, p_cxmi, p_cxma);
            cymi = p2Y((int) Math.max(p_zy0,p_zy1)+p_cross, p_cymi, p_cyma);
            cyma = p2Y((int) Math.min(p_zy0,p_zy1)-p_cross, p_cymi, p_cyma);
            ArrayList alipred = new ArrayList();                     // to save selected predictions
            for( int i=0; i<nbxy; i++ ) {
                if( x[i] < cxmi ) {
                    continue;
                }
                if( x[i] > cxma ) {
                    break;
                }
                if( y[i] < cymi ||
                    y[i] > cyma    ) {
                    continue;
                }
                alipred.add(new Integer(i));                         // in selection area
            }
            int nbipred = alipred.size();                            // nb of selected predictions
            if( nbipred != 0 ) {
                String[] str = new String[nbipred];                  // displayed strings
                for( int i=0; i<nbipred; i++ ) {
                    int j = ((Integer)alipred.get(i)).intValue();    // index of the current prediction
                    str[i] = smark[j]+" X = "+formX(x[j])+", delta = "+formDiff(y[j])+" ass = "+jsyn[j]+lnsep;  // describing string
                }
                if(jft != null ) {
                    // free old frame
                    jft.dispose();
                }
                Point pt = this.getLocationOnScreen();               // to allow relative location
                jft = new JFText( str, this, (int) (p_zx1+pt.getX()), (int)(p_zy1+pt.getY()));  // create new selected predictions frame
                jft.setVisible(true);
            }
            else {
                // reset prediction selection
                unPredsel();
            }
            return;
        }
        // left button : zoom
        zoom = false;                                                // NO zoom area has to be drawn

        // set new data area to draw
        cxmi = p2X((int) Math.min(p_zx0,p_zx1), p_cxmi, p_cxma);
        cxma = p2X((int) Math.max(p_zx0,p_zx1), p_cxmi, p_cxma);
        cymi = p2Y((int) Math.max(p_zy0,p_zy1), p_cymi, p_cyma);
        cyma = p2Y((int) Math.min(p_zy0,p_zy1), p_cymi, p_cyma);
        if( ntfile.equals("jener") || ntfile.equals("trni") || ntfile.equals("predmix_I") ) {  // jener/trni/predmix
            cxmi = Math.ceil(cxmi-.33);
            cxma = Math.floor(cxma+.33);
        }
        //
        xmi = cxmi;
        xma = cxma;
        ymi = cymi;
        yma = cyma;
        if( ntfile.equals("jener") || ntfile.equals("trni") || ntfile.equals("predmix_I") ) {
            // set in limits
            xmi = Math.max(x[0], xmi);
            xmi = Math.min(xmi, x[nbxy-1]);
            xma = Math.max(x[0], xma);
            xma = Math.min(xma, x[nbxy-1]);
            //
            xmi = xmi-.33;
            xma = xma+.33;
        }
        else if( ( ntfile.equals("spectr") || ntfile.equals("spectrt") ) && ymi < 0. ) {
            ymi = 0.;
        }

        // set intervals
        cadre(xmi, xma-xmi, cadreX);
        cadre(ymi, yma-ymi, cadreY);
        defxlim();                                                   // define start and end indexes
        prevp_wxma = -1;                                             // force call to setRedSpect at next repaint
        repaint();                                                   // all
    }

    /**
     * Not implemented.
     */
    public void mouseClicked(MouseEvent mevt) {
    }
    /**
     * Not implemented.
     */
    public void mouseEntered(MouseEvent mevt) {
    }
    /**
     * Not implemented.
     */
    public void mouseExited(MouseEvent mevt) {
    }

/////////////////////////////////////////////////////////////////////////////

    // show mouse XY location
    private void showXY() {

        if( p_xmouse > p_cxmi-p_gap &&                               // if in data area
            p_xmouse < p_cxma+p_gap &&
            p_ymouse > p_cymi-p_gap &&
            p_ymouse < p_cyma+p_gap ) {
            // X
            double xmouse = p2X(p_xmouse, p_cxmi, p_cxma);           // in X units
            if( ntfile.equals("jener") || ntfile.equals("trni") || ntfile.equals("predmix_I") ) {  // jener/trni/predmix
                String  valmouse = "";                               // empty if not in valid area
                if( halfi ) {
                    // half integer value only
                    double xmousi = xmouse-0.5;
                    if( (xmousi >= -.33) && (Math.abs(xmousi-Math.round(xmousi)) <= .33) ) {
                        valmouse = ""+((int) Math.round(xmousi)+0.5);
                    }
                }
                else {
                    // integer value only
                    if( (xmouse >= -.33) && (Math.abs(xmouse-Math.round(xmouse)) <= .33) ) {
                        valmouse = ""+(int) Math.round(xmouse);
                    }
                }
                jlx.setText(valmouse);
            }
            else {
                // float value
                jlx.setText(String.valueOf((float) xmouse));
            }
            // Y
            double ymouse = p2Y(p_ymouse, p_cymi, p_cyma);           // in Y units
            jly.setText(String.valueOf((float) ymouse));
        }
        else {                                                       // out of data area
            jlx.setText("");
            jly.setText("");
        }
    }

    // set ticks and interval inside the range
    // input  :
    //   xdeb         <- beginning X
    //   delta        <- range
    // output :
    //   cd[0]=cxdeb  -> beginning X, set to an interval multiple ( <= xdeb)
    //   cd[1]=cint   -> interval    (1Exx, 2Exx ou 5Exx)
    //   cd[2]=nbint  -> nb of intervals in adjusted range
    //   cd[3]        -> ending    X, set to an interval multiple ( >= xdeb+delta)
    private void cadre( double xdeb, double delta ,double[] cd) {

        NumberFormat nf;                                             // to set interval
        DecimalFormat df;                                            // idem
        String cStr;                                                 // current string
        int iexp;                                                    // exponent
        int imant;                                                   // mantissa
        double cxdeb;                                                // 1st X tick
        double cdelta;                                               // current range
        double cint;                                                 // interval
        int    nbint;                                                // nb of intervals inside the range
        double cx;                                                   // variable

        nf = NumberFormat.getNumberInstance(Locale.US);              // ask a plain format
        df = (DecimalFormat)nf;                                      // reduce to decimal format
        df.applyPattern(".0E00");                                    // define pattern

        cdelta = Math.abs(delta);                                    // current range
        cStr = df.format(cdelta);                                    // format it
        cx = Double.parseDouble(cStr);                               // reread
        if( cx < cdelta ) {                                          // rounded up
            cdelta = cdelta + Double.parseDouble(".05"+cStr.substring(2));  // fit range
            cStr = df.format(cdelta);
        }
        iexp = Integer.parseInt(cStr.substring(3));                  // exponent
        imant = Integer.parseInt(cStr.substring(1,2));               // mantissa

        // interval = 1, 2 or 5Exx
        if( imant > 5 ) {
            imant = 1;
            iexp = iexp+1;
        }
        else if( imant > 2 ) {
            imant = 5;
        }
        cint = imant*Math.pow( 10, iexp-2 );                         // interval

        // 1st X tick
        long l = (long) (xdeb/cint);                                 // nb of intervals in xdeb
        cx = xdeb - l*cint;                                          // difference
        if( xdeb >= 0. ) {
            // xdeb very close (1%) to an interval multiple ?
            if( cx > cint*0.99 ) {                                   // no
                l++;
            }
        }
        else {
            // xdeb very close (1%) to an interval multiple ?
            if( cx < -cint*0.01 ) {                                  // no
                l--;
            }
        }
        cxdeb = l*cint;                                              // fit xdeb

        // nb of intervals
        cdelta = xdeb + Math.abs(delta) - cxdeb;
        nbint  = (int) (cdelta/cint);                                // nb of intervals in range
        cx = cdelta - nbint*cint;
        // range very close (1%) to an interval multiple ?
        if( cx > cint*0.01 ) {                                       // no
            nbint++;
        }

        cd[0] = cxdeb;
        cd[1] = cint;
        cd[2] = nbint;
        cd[3] = cxdeb+nbint*cint;
    }

    // define first and last points in data area
    private void defxlim() {

        int j;
        double cxmi = cadreX[0];                                     // beginning of data area
        double cxma = cadreX[0]+cadreX[1]*cadreX[2];                 // end       of data area
        if( ntfile.equals("jener") || ntfile.equals("trni") || ntfile.equals("predmix_I") ) {
            cxmi = cxmi+.33;
            cxma = cxma-.33;
        }
        for( j=0; j<nbxy; j++ ) {                                    // for each data point
            if( x[j] >= cxmi ) {
                 // first point found
                 break;
            }
        }
        ixdeb = Math.min(j,nbxy-1);                                  // first point in data area or last data point
        //
        for( j=ixdeb; j<nbxy; j++ ) {                                // for each next points
            if( x[j] > cxma ) {
                // last point exceeded
                break;
            }
        }
        ixfin = Math.max(0,j-1);                                     // first data point or last point in data area or last data point
        if( ntfile.equals("spectrum") ) {
            // one more point at each end
            ixdeb = Math.max(0,       ixdeb-1);
            ixfin = Math.min(ixfin+1, nbxy-1 );
        }
    }

    // format a diff
    private String formDiff( double cx ) {

        // format
        NumberFormat  nff;                                           // float
        DecimalFormat dff;

        StringBuffer sb;
        boolean neg;                                                 // negative
        double locx = cx;                                            // local value

        nff = NumberFormat.getNumberInstance(Locale.US);             // ask for a plain format
        dff = (DecimalFormat)nff;                                    // reduce to decimal format

        if( locx < 0. ) {
            // keep a place for sign
            neg = true;
            locx = -locx;
            if( pm_freq ) {
                dff.applyPattern("00000.0000");                      // define pattern FORMAT 2005 of eq_tds
            }
            else {
                dff.applyPattern("000.0");                           // define pattern FORMAT 2005 of eq_int
            }
        }
        else {
            neg = false;
            if( pm_freq ) {
                dff.applyPattern("000000.0000");                     // define pattern FORMAT 2005 of eq_tds
            }
            else {
                dff.applyPattern("0000.0");                          // define pattern FORMAT 2005 of eq_int
            }
        }

        sb = new StringBuffer(dff.format(locx));                     // format
        for( int i=0; i<sb.length(); i++ ) {
            if( sb.charAt(i) == '0' ) {
                sb.setCharAt(i, ' ');                                // replace leading 0s with spaces
            }
            else {
                // end of leading 0s
                if( neg ) {
                    sb.insert(i, "-");
                }
                break;
            }
        }
        return sb.toString();
    }

    // format a jinf, jsup, freq or int
    private String formX( double cx ) {

        // format
        NumberFormat  nff;                                           // float
        DecimalFormat dff;

        StringBuffer sb;

        nff = NumberFormat.getNumberInstance(Locale.US);             // ask a plain format
        dff = (DecimalFormat)nff;                                    // reduce to decimal format
        if( pm_I ) {
            dff.applyPattern("000");                                 // J
        }
        else {
            // freq or int
            if( cx < 1. ) {
                dff.applyPattern("0.000E0");
            }
            else {
                dff.applyPattern("0000.000");
            }
        }
        sb = new StringBuffer(dff.format(cx));                       // format
        for( int i=0; i<(sb.length()-1); i++ ) {                     // don't test last char
            if( sb.charAt(i) == '0' ) {
                sb.setCharAt(i, ' ');                                // replace leading 0s with spaces
            }
            else {
                // end of leading 0s
                break;
            }
        }
        return sb.toString();
    }

    /**
     * Do not longer show pred selection area
     *
     */
    public void unPredsel() {

        if( jft != null ) {
            // free previous frame
            jft.dispose();
        }
        if( omcarea ) {
            // running prediction selection
            omcarea = false;
            // repaint released selection area
            p_paintx      = (int) Math.min(p_zx0,p_zx1);
            p_painty      = (int) Math.min(p_zy0,p_zy1);
            p_paintwidth  = (int) Math.abs(p_zx0-p_zx1)+1;
            p_paintheight = (int) Math.abs(p_zy0-p_zy1)+1;
            myrepaint(p_paintx, p_painty, p_paintwidth, p_paintheight);
        }
    }

    // Set reduced spectra for speeding draw
    private void setRedSpect() {

        int[] ci_red = new int[0];                                   // index of reduced data
        int ind0;                                                    // running index : original (non reduced) data
        int ip_0;                                                    // pixel position of running original point
        int ind_red = 0;                                             // running index : reduced  data
        int ciymi;
        int ciyma;
        double cymi;                                                 // Y min of current pixel area
        double cyma;                                                 // Y max of current pixel area
        double cx;                                                   // current x
        double cy;                                                   // current y
        double cyr;                                                  // current y (reduced)

        use_red = false;                                             // status
        int     nbred   = 0;                                         // original nb of data points for one pixel width area

        if( ntfile.equals("jener") ) {
            int nbpix = ip_Y(y[iemin], p_cymi, p_cyma)-ip_Y(y[iemax], p_cymi, p_cyma)+1;  // visible area full nb of pixel (in actual scale)
            nbpix = Math.max(1, nbpix);
            nbred = (iemax-iemin+1)/nbpix;                           // max nb of points/pixel along Y axis
            // the reduced data has 1 point/pixel
            //     =      original point with this pixel value and Y max value
            // Y value : mean energy of 1st and last original points of the pixel area
            if( nbred > 1 ) {                                        // do we have to reduce ?
                // reduction
                use_red = true;

                ci_red  = new int[nbxy];                             // define array
                cx      = x[ixdeb]-1;                                // current x     : different (-1) from ixdeb x
                ip_0    = ip_Y(y[ixdeb],p_cymi, p_cyma)-1;           // current pixel : different (-1) from ixdeb pixel
                ind_red = 0;                                         // index of 1st reduced point
                for( int i=ixdeb; i<ixfin+1; i++ ) {
                    if( y[i] < cadreY[0] ||
                        y[i] > cadreY[3]    ) {
                        continue;
                    }
                    if( x[i]                      != cx     ||       // new X column
                        ip_Y(y[i],p_cymi, p_cyma) != ip_0   ||       // new pixel area
                        i                         == nbxy-1    ) {   // end of the original data
                        // point
                        ci_red[ind_red] = i;                         // save index
                        ind_red ++;                                  // next reduced point index
                        //
                        cx   = x[i];
                        ip_0 = ip_Y(y[i],p_cymi, p_cyma);            // pixel position of this point
                    }
                }
            }
        }
        else if( ntfile.equals("trni") ) {
            int nbpix = ip_Y(y[iemin], p_cymi, p_cyma)-ip_Y(y[iemax], p_cymi, p_cyma)+1;  // visible area full nb of pixel (in actual scale)
            nbpix = Math.max(1, nbpix);
            nbred = (iemax-iemin+1)/nbpix;                           // max nb of points/pixel along Y axis
            // the reduced data has 1 point/pixel
            //     =      original point with this pixel value and Y max value
            // Y value : mean energy of 1st and last original points of the pixel area
            if( nbred > 1 ) {                                        // do we have to reduce ?
                // reduction
                use_red = true;

                ci_red  = new int[nbxy];                             // define array
                cx      = x[ixdeb]-1;                                // current x     : different (-1) from ixdeb x
                ip_0    = ip_Y(y[ixdeb],p_cymi, p_cyma)-1;           // current pixel : different (-1) from ixdeb pixel
                ind_red = 0;                                         // index of 1st reduced point
                for( int i=ixdeb; i<ixfin+1; i++ ) {
                    if( y[i] < cadreY[0] ||
                        y[i] > cadreY[3]    ) {
                        continue;
                    }
                    boolean displayable = false;
                    for( int j=0; j<xnbcomp; j++ ) {
                        if( limcomp[0][j] > 0 ) {                    // displayable, has to be taken into account
                            displayable = true;
                            break;
                        }
                    }
                    if( ! displayable ) {                            // can be ignored
                        continue;
                    }
                    if( x[i]                      != cx     ||       // new X column
                        ip_Y(y[i],p_cymi, p_cyma) != ip_0   ||       // new pixel area
                        i                         == nbxy-1    ) {   // end of the original data
                        // point
                        ci_red[ind_red] = i;                         // save index
                        ind_red ++;                                  // next reduced point index
                        //
                        cx   = x[i];
                        ip_0 = ip_Y(y[i],p_cymi, p_cyma);            // pixel position of this point
                    }
                }
            }
        }
        else if( ntfile.equals("predmix_I") ||
                 ntfile.equals("predmix_R")    ) {
            int nbpix = ip_Y(y[iemin], p_cymi, p_cyma)-ip_Y(y[iemax], p_cymi, p_cyma)+1;  // visible area full nb of pixel (in actual scale)
            nbpix = Math.max(1, nbpix);
            nbred = (iemax-iemin+1)/nbpix;                           // max nb of points/pixel along Y axis
            // the reduced data has 1 point/pixel
            //     =      original point with this pixel value and Y max value
            // Y value : mean energy of 1st and last original points of the pixel area
            if( nbred > 1 ) {                                        // do we have to reduce ?
                // reduction
                use_red = true;

                ci_red  = new int[nbxy];                             // define array
                cx      = x[ixdeb]-1;                                // current x     : different (-1) from ixdeb x
                ip_0    = ip_Y(y[ixdeb],p_cymi, p_cyma)-1;           // current pixel : different (-1) from ixdeb pixel
                ind_red = 0;                                         // index of 1st reduced point
                for( int i=ixdeb; i<ixfin+1; i++ ) {
                    if( y[i] < cadreY[0] ||
                        y[i] > cadreY[3]    ) {
                        continue;
                    }
                    if( limcomp[0][comp[i]] < 0 ) {                  // not displayable
                        continue;
                    }
                    if( ! smark[i].equals("+") ) {                   // not a cross, always displayed
                        // point
                        ci_red[ind_red] = i;                         // save index
                        ind_red ++;                                  // next reduced point index
                        continue;
                    }
                    if( x[i]                      != cx     ||       // new X column
                        ip_Y(y[i],p_cymi, p_cyma) != ip_0   ||       // new pixel area
                        i                         == nbxy-1    ) {   // end of the original data
                        // point
                        ci_red[ind_red] = i;                         // save index
                        ind_red ++;                                  // next reduced point index
                        //
                        cx   = x[i];
                        ip_0 = ip_Y(y[i],p_cymi, p_cyma);            // pixel position of this point
                    }
                }
            }
        }
        else if( ntfile.equals("simul") ) {                          // spectrum style
            int nbpix = ip_X(x[nbxy-1],p_cxmi, p_cxma)-ip_X(x[0],p_cxmi, p_cxma)+1;  // visible area full nb of pixel of the spectrum (in actual scale)
            nbpix = Math.max(1, nbpix);
            nbred = nbxy/nbpix;                                      // nb of points/pixel
            // the reduced spectrum has up to 4 points/pixel
            // 1st = 1st  original point with this pixel value
            // 2nd =      original point with this pixel value and Y min value
            // 3rd =      original point with this pixel value and Y max value
            // 4th = last original point with this pixel value
            // if 2nd and/or 3rd are yet 1st or 4th, they are not duplicated (usefull if nbred near 4)
            // they all have the same X value : mean frequency of these 1st and last original points
            if( nbred > 4 ) {                                        // do we have to reduce ?
                // reduction
                use_red = true;

                ci_red = new int[nbxy];                              // define array
                ind0  = ixdeb;                                       // index of 1st original point of 1st pixel area
                ip_0 = ip_X(x[ind0],p_cxmi, p_cxma);                 // pixel value of this pixel area
                ciymi = ind0;
                cymi = y[ciymi];                                     // Y min of this pixel area
                ciyma = ciymi;
                cyma = y[ciyma];                                     // Y max of this pixel area
                ind_red = 0;                                         // index of 1st reduced point
                for( int i=ixdeb; i<ixfin+1; i++ ) {                 // for each original point
                    if( ip_X(x[i],p_cxmi, p_cxma) != ip_0   ||       // new pixel area
                        i                         == nbxy-1    ) {   // end of the original data
                        // 1st point
                        ci_red[ind_red] = ind0;                      // frequency of this pixel area
                        ind_red ++;                                  // next reduced point index
                        // MIN point if not the same as 1st and/or last one
                        if( cymi != y[ind0] &&                       // NOT the same as the 1st  one
                            cymi != y[i-1]     ) {                   // NOT the same as the last one
                            ci_red[ind_red] = ciymi;                 // frequency of this pixel area
                            ind_red ++;                              // next reduced point index
                        }
                        // MAX point if not the same as 1st and/or last one
                        if( cyma != y[ind0] &&                       // NOT the same as the 1st  one
                            cyma != y[i-1]     ) {                   // NOT the same as the last one
                            ci_red[ind_red] = ciyma;                 // frequency of this pixel area
                            ind_red ++;                              // next reduced point index
                        }
                        // last point
                        ci_red[ind_red] = i-1;                       // frequency of this pixel area
                        ind_red ++;                                  // next reduced point index
                        //
                        ind0  = i;                                   // index of the 1st original point of the next pixel area
                        ip_0 = ip_X(x[ind0],p_cxmi, p_cxma);         // pixel position of this point
                        ciymi = ind0;
                        cymi  = y[ciymi];                            // Y min of this pixel area
                        ciyma = ciymi;
                        cyma  = y[ciyma];                            // Y max of this pixel area
                    }
                    else {                                           // end of the pixel area
                        cy = y[i];                                   // current Y
                        if( cy < cymi ) {                            // test min
                            ciymi = i;
                            cymi = cy;
                        }
                        if( cy > cyma ) {                            // test max
                            ciyma = i;
                            cyma = cy;
                        }
                    }
                }
            }
        }
        else if( ntfile.equals("spectr") || ntfile.equals("spectrt") ) {  // stick style
            int nbpix = ip_X(x[nbxy-1],p_cxmi, p_cxma)-ip_X(x[0],p_cxmi, p_cxma)+1;  // visible area full nb of pixel of the stick spectrum (in actual scale)
            nbpix = Math.max(1, nbpix);
            nbred = nbxy/nbpix;                                      // nb of points/pixel
            // the reduced stick spectrum has 1 point/pixel
            //     =      original point with this pixel value and Y max value
            // X value : mean frequency of 1st and last original points of the pixel area
            if( nbred > 1 ) {                                        // do we have to reduce ?
                // reduction
                use_red = true;

                ci_red = new int[nbxy];                              // define array
                ind0  = ixdeb;                                       // index of 1st original point of 1st pixel area
                ip_0 = ip_X(x[ind0], p_cxmi, p_cxma);                // pixel value of this pixel area
                ciyma = ind0;
                cyma = y[ciyma];                                     // Y max of this pixel area
                ind_red = 0;                                         // index of 1st reduced point
                for( int i=ixdeb; i<ixfin+1; i++ ) {
                    if( y[i] < cadreY[0] ) {
                        continue;
                    }
                    if( ip_X(x[i], p_cxmi, p_cxma) != ip_0   ||      // new pixel area
                        i                         == nbxy-1    ) {   // end of the original data
                        // point
                        ci_red[ind_red] = ciyma;
                        ind_red ++;                                  // next reduced point index
                        //
                        ind0 = i;                                    // index of the 1st original point of the next pixel area
                        ip_0 = ip_X(x[ind0], p_cxmi, p_cxma);        // pixel position of this point
                        ciyma = ind0;
                        cyma  = y[ciyma];                            // Y max of this pixel area
                    }
                    else {                                           // end of the pixel area
                        cy = y[i];                                   // current Y
                        if( cy > cyma ) {                            // test max
                            ciyma = i;
                            cyma = cy;
                        }
                    }
                }
            }
        }
        if( use_red ) {
            nbxy_red = ind_red;                                      // effective nb of reduced points
            i_red = new int[nbxy_red];
            System.arraycopy(ci_red,0,i_red,0,nbxy_red);
        }
    }

}
