/*
 * XTDS application entry
 */

import java.awt.*;
import java.util.*;

/**
 * This is the entry point to XTDS application.
 */
public class Main {

    /**
     * Starts XTDS application.
     */
    public static void main(String[] args) {

        Locale.setDefault(Locale.ENGLISH);                           // english used for messages
        JobPlay jp = new JobPlay();                                  // instanciate JobPlay main panel
        //jp.setSize(1024, 690);
        jp.setSize(1280, 690);                                       // starting window size
        jp.setBackground(Color.WHITE);
        jp.setVisible(true);
    }

}
