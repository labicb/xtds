/*
 * Class for spectrum simulation job creation, called by CreateJob
 */

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import java.text.*;
import java.util.*;

/**
 * This panel creates a job to simulate a spectrum from one or more line files.
 */
public class CJPSimul extends    JPanel
                      implements ActionListener {

    // panels
    private JPanel pnord;
    private JPanel pnc;
    private JPanel pouest;
    private JPanel poc;
    private JPanel pcentre;
    private JPanel pcn;
    private JPanel[] pcnx;
    private JPanel[] pcnxn;
    private JPanel[] pcnxno;
    private JPanel[] pcnxnc;
    private JPanel[] pcnxc;
    private JPanel psud;

    // north panel accessories
    private JFormattedTextField   jftfumin;                          // UMIN text field
    private float     vumin;                                         // lower frequency (output unit)
    private JFormattedTextField   jftfumax;                          // UMAX text field
    private float     vumax;                                         // upper frequency (output unit)
    private JFormattedTextField   jftftemp;                          // TEMP text field
    private float     vtemp;                                         // temperature (K)
    private JFormattedTextField   jftfres;                           // RES text field
    private float     vres;                                          // resolution (output unit)
    private JFormattedTextField   jftfpaf;                           // PAF text field
    private float     vpaf;                                          // Drawing Steps Size (output unit)
    private JFormattedTextField   jftfthres;                         // THRES text field
    private float     vthres;                                        // Intensity Threshold (cm-2/atm)
    private String[]  tsptype;                                       // spectrum type choice array
    private JComboBox jcbsptype;                                     // spectrum type jcb
    private String    nsptype;                                       // Spectrum type
    private String[]  tappf;                                         // apparatus function choice array
    private JComboBox jcbappf;                                       // apparatus function jcb
    private String    nappf;                                         // Apparatus function
    private String[]  tunit;                                         // unit choice array
    private JComboBox jcboutunit;                                    // output unit jcb
    private String    noutunit;                                      // output unit name

    // west panel accessories
    private JButton jbaddlin;                                        // add lines button

    // center panel accessories
    private int       nblfil;                                        // nb of line files
    private int       indlfil;                                       // line files current index
    private int       nblfilmax = 10;                                // max nb of line files (cf.simul.f)
    private JScrollPane jsp;                                         // with lifts
    private JButton[] jblfil;                                        // line file choice button
    private JFileChooser jfclfil;                                    // line file choice
    private JLabel[]  jllfil;                                        // line file labels
    private String[]  lfil;                                          // line file names
    private JFormattedTextField[] jftfcdae;
    private float[]   cdae;                                          // Press Broadening Coeff (cm-1.atm-1)
    private JFormattedTextField[] jftfmass;
    private int[]     mass;                                          // Molar Mass
    private JFormattedTextField[] jftfuk;
    private float[]   uk;                                            // Intensity scaling factor
    private JFormattedTextField[] jftfptt;
    private float[]   ptt;                                           // Total   Pressure Torr
    private JFormattedTextField[] jftfppt;
    private float[]   ppt;                                           // Partial Pressure Torr
    private JFormattedTextField[] jftfcl;
    private float[]   cl;                                            // cuve length (cm)
    private JComboBox[] jcbinunit;                                   // input unit jcb
    private String      ninunit;                                     // input unit name

    // south panel accessories
    private JButton jbreset;                                         // reset
    private JButton jbsave;                                          // save
    private Box     boxsud;                                          // button box

    // Number Format
    private NumberFormat nffloat;                                    // float   format
    private NumberFormat nfint;                                      // integer format

    // CreateJob parameters
    private String   playd;                                          // XTDS installation directory
    private String   workd;                                          // working directory
    private String   npack;                                          // package name

    // variables
    private String   njobf;                                          // job file name
    private PrintWriter out1;                                        // job writing pw
    private String lnsep;                                            // line separator
    private String fisep;                                            // file separator

/////////////////////////////////////////////////////////////////////

    /**
     * Constructs a new CJPSimul.
     */
    public CJPSimul() {

        playd = System.getProperty("xtds.home");                     // XTDS installation directory
        workd = System.getProperty("user.dir");                      // working directory
        lnsep = System.getProperty("line.separator");
        fisep = System.getProperty("file.separator");
        setName("CJPSimul");                                         // for help files
        setLayout(new BorderLayout());

        // south panel
        jbreset = new JButton();                                     // managed by CreateJob
        jbreset.setBackground(Color.WHITE);
        jbsave  = new JButton("Save");                               // job file saving button
        jbsave.setBackground(Color.WHITE);
        jbsave.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent ae) {           // jbsave action
                saveCJP();
            }
        }
        );

        boxsud = Box.createHorizontalBox();                          // add buttons to box
        boxsud.add(jbreset);
        boxsud.add(Box.createHorizontalStrut(15));
        boxsud.add(jbsave);
        psud = new JPanel();
        psud.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        psud.add(boxsud);                                            // add box

        // west panel for jbaddlin button
        nblfil = 0;                                                  // nb of line files
        pouest = new JPanel();
        jbaddlin = new JButton("Add Line File");
        jbaddlin.setBackground(Color.WHITE);
        jbaddlin.addActionListener(this);
        pouest.add(jbaddlin);
        pouest.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),null));

        // center panel to add line files
        pcentre = new JPanel(new BorderLayout());
        jsp = new JScrollPane(pcentre);                              // lift
        pcn = new JPanel(new GridLayout(0,1,5,5));
        pcentre.add(pcn,"North");
        // create panels and arrays for each line file
        pcnx      = new JPanel[nblfilmax];
        pcnxn     = new JPanel[nblfilmax];
        pcnxno    = new JPanel[nblfilmax];
        pcnxnc    = new JPanel[nblfilmax];
        pcnxc     = new JPanel[nblfilmax];
        jblfil    = new JButton[nblfilmax];
        jllfil    = new JLabel[nblfilmax];
        lfil      = new String[nblfilmax];
        jftfcdae  = new JFormattedTextField[nblfilmax];
        cdae      = new float[nblfilmax];
        jftfmass  = new JFormattedTextField[nblfilmax];
        mass      = new int[nblfilmax];
        jftfuk    = new JFormattedTextField[nblfilmax];
        uk        = new float[nblfilmax];
        jftfptt   = new JFormattedTextField[nblfilmax];
        ptt       = new float[nblfilmax];
        jftfppt   = new JFormattedTextField[nblfilmax];
        ppt       = new float[nblfilmax];
        jftfcl    = new JFormattedTextField[nblfilmax];
        cl        = new float[nblfilmax];
        jcbinunit = new JComboBox[nblfilmax];

        // display panels
        add(pouest,  "West");
        add(jsp,     "Center");
        add(psud,    "South");
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Returns the reset button.
     * <br>Called by CreateJob which manages it.
     */
    public JButton getJBreset() {

        return jbreset;
    }

    /**
     * Sets basic parameters and creates north panel.
     * <br>Called by CreateJob to activate this panel.
     *
     * @param cnpack     name of the package
     * @param cmoldirs   molecule directories
     * @param cmxpol     MXPOL
     * @param cnbclab    number of characters of parameter label
     * @param cnbvqn     number of vibrational quantum numbers
     */
    public void setBasicPara(String cnpack, String[] cmoldirs, int cmxpol, int cnbclab, int cnbvqn) {

        npack = cnpack;                                              // package name (lone usefull parameter)
        createNorth();
    }

/////////////////////////////////////////////////////////////////////

    // north panel to display and catch simulation parameters
    private void createNorth() {

        pnord = new JPanel(new BorderLayout());
        pnc   = new JPanel(new GridLayout(5,4,5,5));

        // SIMULATION PARAMETERS
        // catching formats
        nffloat = NumberFormat.getInstance(Locale.US);               // float
        nffloat.setGroupingUsed(false);                              // ie. no thousands seperator
        nffloat.setMaximumFractionDigits(10);                        // ie. no more then 10 digits for fractionnal part
        nfint = NumberFormat.getInstance();                          // integer
        nfint.setGroupingUsed(false);                                // ie. no thousands seperator
        nfint.setParseIntegerOnly(true);                             // ie. only integers
        nfint.setMaximumIntegerDigits(7);                            // ie. no more then 7 digits
        // UMIN choice
        pnc.add(new JLabel("   Lower Frequency Limit (output unit) ",null,JLabel.LEFT));
        jftfumin = new JFormattedTextField(nffloat);
        jftfumin.setValue(new Float(0.0));
        pnc.add(jftfumin);
        // PAF choice
        pnc.add(new JLabel("   Drawing Steps Size (output unit) ",null,JLabel.LEFT));
        jftfpaf = new JFormattedTextField(nffloat);
        jftfpaf.setValue(new Float(0.0));
        pnc.add(jftfpaf);
        // UMAX choice
        pnc.add(new JLabel("   Upper Frequency Limit (output unit) ",null,JLabel.LEFT));
        jftfumax = new JFormattedTextField(nffloat);
        jftfumax.setValue(new Float(0.0));
        pnc.add(jftfumax);
        // THRES choice
        pnc.add(new JLabel("   Intensity Threshold (cm-2/atm) ",null,JLabel.LEFT));
        jftfthres = new JFormattedTextField(nffloat);
        jftfthres.setValue(new Float(0.0));
        pnc.add(jftfthres);
        // TEMP choice
        pnc.add(new JLabel("   Temperature (K) ",null,JLabel.LEFT));
        jftftemp = new JFormattedTextField(nffloat);
        jftftemp.setValue(new Float(0.0));
        pnc.add(jftftemp);
        // TYPE choice
        tsptype = new String[5];
        tsptype[0] = "";
        tsptype[1] = "trans";
        tsptype[2] = "raman";
        tsptype[3] = "absor";
        tsptype[4] = "coabs";
        pnc.add(new JLabel("   Spectrum Type ",null,JLabel.LEFT));
        jcbsptype = new JComboBox(tsptype);
        jcbsptype.setSelectedItem("");                               // default to space
        jcbsptype.setBackground(Color.WHITE);
        jcbsptype.addActionListener (this);
        nsptype = "";
        pnc.add(jcbsptype);
        // RES choice
        pnc.add(new JLabel("   Resolution (output unit) ",null,JLabel.LEFT));
        jftfres = new JFormattedTextField(nffloat);
        jftfres.setValue(new Float(0.0));
        pnc.add(jftfres);
        // APPF choice
        tappf = new String[5];
        tappf[0] = "";
        tappf[1] = "dirac";
        tappf[2] = "sinc";
        tappf[3] = "sinc2";
        tappf[4] = "gauss";
        pnc.add(new JLabel("   Apparatus Function ",null,JLabel.LEFT));
        jcbappf = new JComboBox(tappf);
        jcbappf.setSelectedItem("");                                 // default to space
        jcbappf.setBackground(Color.WHITE);
        nappf = "";
        pnc.add(jcbappf);
        // OUTUNIT choice
        tunit = new String[4];
        tunit[0] = "";
        tunit[1] = "cm-1";
        tunit[2] = "MHz";
        tunit[3] = "GHz";
        pnc.add(new JLabel("   Output Unit ",null,JLabel.LEFT));
        jcboutunit = new JComboBox(tunit);
        jcboutunit.setSelectedItem("");                              // default to space
        jcboutunit.setBackground(Color.WHITE);
        noutunit = "";
        pnc.add(jcboutunit);

        pnord.add(pnc,"Center");

        add(pnord,"North");
        pnord.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),"SIMULATION PARAMETERS"));
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Processes the events.
     */
    public void actionPerformed(ActionEvent evt) {

        // SPECTRUM TYPE
        if (evt.getSource() == jcbsptype ) {
            nsptype = (String)jcbsptype.getSelectedItem();
            if( nsptype == "coabs" ) {
                jcbappf.setSelectedItem("");
                jcbappf.setEnabled(false);
            }
            else {
                jcbappf.setEnabled(true);
            }
            if( nblfil > 0 ) {                                       // if there is line files
                for( indlfil=0; indlfil<nblfil; indlfil++ ) {        // for each one
                    if( nsptype == "raman" ) {
                        jftfppt[indlfil].setValue(new Float(0.0));
                        jftfppt[indlfil].setEnabled(false);
                        jftfcl[indlfil].setValue(new Float(0.0));
                        jftfcl[indlfil].setEnabled(false);
                    }
                    else {
                        jftfppt[indlfil].setEnabled(true);
                        if( nsptype == "coabs" ) {
                            jftfcl[indlfil].setValue(new Float(0.0));
                            jftfcl[indlfil].setEnabled(false);
                        }
                        else {
                            jftfcl[indlfil].setEnabled(true);
                        }
                    }
                }
            }
        }
        // ADD LINE FILES
        if (evt.getSource() == jbaddlin ) {
            addJPlines();
            return;
        }
        if( nblfil > 0 ) {                                           // if there is line files
            for( indlfil=0; indlfil<nblfil; indlfil++ ) {            // for each one
                if( evt.getSource() == jblfil[indlfil] ) {           // if line file choice button
                    jfclfil = new JFileChooser(workd);               // defaut choice directory
                    jfclfil.setSize(400,300);
                    jfclfil.setFileSelectionMode(JFileChooser.FILES_ONLY);  // files only
                    Container parent = jblfil[indlfil].getParent();
                    int choice = jfclfil.showDialog(parent,"Select");  // Dialog, Select
                    if (choice == JFileChooser.APPROVE_OPTION) {     // choice effective
                        lfil[indlfil] = jfclfil.getSelectedFile().getAbsolutePath();  // save it
                        jllfil[indlfil].setText(lfil[indlfil]);      // display it
                    }
                    return;
                }
            }
        }
    }

/////////////////////////////////////////////////////////////////////

    // add a line file panel
    private void addJPlines() {

        if( nblfil == nblfilmax ) {                                  // if maximum reached
            JOptionPane.showMessageDialog(null,"The number of line files is limited to "+nblfilmax);
            return;
        }
        nblfil++;
        indlfil = nblfil-1;
        // instanciate required panels
        pcnx[indlfil] = new JPanel(new BorderLayout());
        pcnxn[indlfil] = new JPanel(new BorderLayout());
        pcnxno[indlfil] = new JPanel();
        pcnxnc[indlfil] = new JPanel();
        pcnxc[indlfil] = new JPanel(new GridLayout(4,4,5,5));
        // line file choice
        jblfil[indlfil] = new JButton("Line File");
        jblfil[indlfil].setBackground(Color.WHITE);
        jblfil[indlfil].addActionListener (this);
        pcnxno[indlfil].add(jblfil[indlfil]);
        jllfil[indlfil] = new JLabel();
        pcnxnc[indlfil].add(jllfil[indlfil]);
        lfil[indlfil] = "";
        pcnxn[indlfil].add(pcnxno[indlfil],"West");
        pcnxn[indlfil].add(pcnxnc[indlfil],"Center");
        // cdae
        pcnxc[indlfil].add(new JLabel("Press Broad Coeff (cm-1.atm-1)"));
        jftfcdae[indlfil] = new JFormattedTextField(nffloat);
        jftfcdae[indlfil].setValue(new Float(0.0));
        pcnxc[indlfil].add(jftfcdae[indlfil]);
        // ptt
        pcnxc[indlfil].add(new JLabel("Total Pressure (Torr)"));
        jftfptt[indlfil] = new JFormattedTextField(nffloat);
        jftfptt[indlfil].setValue(new Float(0.0));
        pcnxc[indlfil].add(jftfptt[indlfil]);
        // mass
        pcnxc[indlfil].add(new JLabel("Molar Mass"));
        jftfmass[indlfil] = new JFormattedTextField(nfint);
        jftfmass[indlfil].setValue(new Integer(0));
        pcnxc[indlfil].add(jftfmass[indlfil]);
        // ppt
        pcnxc[indlfil].add(new JLabel("Partial Pressure (Torr)"));
        jftfppt[indlfil] = new JFormattedTextField(nffloat);
        jftfppt[indlfil].setValue(new Float(0.0));
        if( nsptype == "raman" ) {
            jftfppt[indlfil].setEnabled(false);
        }
        pcnxc[indlfil].add(jftfppt[indlfil]);
        // uk
        pcnxc[indlfil].add(new JLabel("Intensity scaling factor"));
        jftfuk[indlfil] = new JFormattedTextField(nffloat);
        jftfuk[indlfil].setValue(new Float(0.0));
        pcnxc[indlfil].add(jftfuk[indlfil]);
        // cl
        pcnxc[indlfil].add(new JLabel("Path Length (cm)"));
        jftfcl[indlfil] = new JFormattedTextField(nffloat);
        jftfcl[indlfil].setValue(new Float(0.0));
        if( nsptype == "raman" ||
            nsptype == "coabs"    ) {
            jftfcl[indlfil].setEnabled(false);
        }
        pcnxc[indlfil].add(jftfcl[indlfil]);
        // input unit
        pcnxc[indlfil].add(new JLabel("Input Unit"));
        jcbinunit[indlfil] = new JComboBox(tunit);
        jcbinunit[indlfil].setSelectedItem("");
        jcbinunit[indlfil].setBackground(Color.WHITE);
        pcnxc[indlfil].add(jcbinunit[indlfil]);

        pcnx[indlfil].add(pcnxn[indlfil],"North");
        pcnx[indlfil].add(pcnxc[indlfil],"Center");
        pcnx[indlfil].setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),"Line File #"+(indlfil+1)));
        pcn.add(pcnx[indlfil]);
        pcnx[indlfil].revalidate();
        // go to bottom to show the last line file panel
        SwingUtilities.invokeLater(new Runnable() {
             public void run() {
                 final JScrollBar jsbv = jsp.getVerticalScrollBar();
                 jsbv.setValue(jsbv.getMaximum());
             }
        });
        //
    }

/////////////////////////////////////////////////////////////////////

    // save job
    private void saveCJP() {

        if( ! testCJP() ) {                                          // check data
            return;
        }
        // choose job name
        JFileChooser jfcjobf = new JFileChooser(workd);
        jfcjobf.setSize(400,300);
        jfcjobf.setFileSelectionMode(JFileChooser.FILES_ONLY);
        jfcjobf.setDialogTitle("Define the job file to be created");
        jfcjobf.setSelectedFile(new File("job_sim_"));
        int choice = jfcjobf.showSaveDialog(this);
        if (choice == JFileChooser.APPROVE_OPTION) {
            njobf = jfcjobf.getSelectedFile().getAbsolutePath();     // job file name
        }
        else {
            return;                                                  // name NOT chosen
        }
        // create job
        try {
            out1 = new PrintWriter(new BufferedWriter(new FileWriter(njobf)));  // write job

            out1.println("#! /bin/sh");
            out1.println(" set -v");
            out1.println("##");
            out1.println("## Simulation job created by XTDS");
            out1.println("##");
            out1.println("BASD="+playd+fisep+"packages"+fisep+npack);
            out1.println("##");
            out1.println(" SCRD=$BASD"+fisep+"prog"+fisep+"exe");
            out1.println("##");
            out1.println("##  SIMULATION PARAMETERS");
            out1.println("##");
            out1.println(" UMIN="+vumin);
            out1.println(" UMAX="+vumax);
            out1.println(" TEMP="+vtemp);
            out1.println(" RES="+vres);
            out1.println(" PAF="+vpaf);
            out1.println(" THRES="+vthres);
            out1.println("##");
            for( indlfil=0; indlfil<nblfil; indlfil++ ) {            // for each line file
                out1.println(" LFIL"+indlfil+"="+lfil[indlfil]);
                out1.println(" CDAE"+indlfil+"="+cdae[indlfil]);
                out1.println(" MASS"+indlfil+"="+mass[indlfil]);
                out1.println(" UK"+indlfil+"="+uk[indlfil]);
                out1.println(" PTT"+indlfil+"="+ptt[indlfil]);
                if( nsptype != "raman" ) {
                    out1.println(" PPT"+indlfil+"="+ppt[indlfil]);
                    if( nsptype != "coabs" ) {
                        out1.println(" CL"+indlfil+"="+cl[indlfil]);
                    }
                }
                out1.println("##");
            }
            out1.println("##");
            if( nsptype != "coabs" ) {
                out1.println(" APPF="+nappf);
                out1.println("##");
            }
            out1.println("##  SIMULATION");
            out1.println("##");
            out1.println(" $SCRD"+fisep+"passx simul "+nsptype+" $UMIN $UMAX $TEMP $RES $PAF $THRES \\");
            for( indlfil=0; indlfil<nblfil; indlfil++ ) {            // for each line file
                if( indlfil == 0 ) {
                    out1.print  ("                        ");        // 1rst
                }
                else {
                    out1.print  ("                    more");        // others
                }
                out1.print  (" $LFIL"+indlfil+" $CDAE"+indlfil+" $MASS"+indlfil+" $UK"+indlfil+" $PTT"+indlfil);
                if( nsptype != "raman" ) {
                    out1.print  (" $PPT"+indlfil);
                    if( nsptype != "coabs" ) {
                        out1.print  (" $CL"+indlfil);
                    }
                }
                ninunit = (String)jcbinunit[indlfil].getSelectedItem();
                if( ninunit != "cm-1" ) {
                  out1.print  (" "+ninunit);
                }
                out1.println(" \\");
            }
            out1.print  ("                     end");
            if( nsptype != "coabs" ) {
                out1.print  (" $APPF");
            }
            noutunit = (String)jcboutunit.getSelectedItem();
            if( noutunit != "cm-1" ) {
                out1.print  (" "+noutunit);
            }
            out1.println("");
            out1.println("##");
        }
        catch (IOException ioe) {                                    // IO error
            JOptionPane.showMessageDialog(null,"IO error while writing file"+lnsep+
                                               njobf                        +lnsep+
                                               ioe);
            return;
        }
        finally {
            if( out1 != null ) {
                out1.close();
                if( out1.checkError() ) {
                    JOptionPane.showMessageDialog(null,"PrintWriter error while creating file"+lnsep+
                                                       njobf);
                    return;
                }
            }
        }
        JOptionPane.showMessageDialog(null,"The job "+njobf+" has been created");
        try {
            Process monproc = Runtime.getRuntime().exec("chmod u+x "+njobf);  // allow execution
        }
        catch (IOException ioe) {                                    // rights modification error
            JOptionPane.showMessageDialog(null,"IO error while changing rights of file"+lnsep+
                                               njobf                                   +lnsep+
                                               ioe);
            return;
        }
    }

/////////////////////////////////////////////////////////////////////

    // check parameters
    private boolean testCJP() {

        // SIMULATION PARAMETERS
        // UMIN
        vumin = ((Number)jftfumin.getValue()).floatValue();
        if( vumin < 0 ) {
            JOptionPane.showMessageDialog(null,"Lower Frequency >= 0 requested");
            jftfumin.setValue(new Float(0.0));
            return false;
        }
        // UMAX
        vumax = ((Number)jftfumax.getValue()).floatValue();
        if( vumax <= vumin ) {
            JOptionPane.showMessageDialog(null,"Upper Frequency > Lower Frequency requested");
            return false;
        }
        // TEMP
        vtemp = ((Number)jftftemp.getValue()).floatValue();
        if( vtemp < 0 ) {
            JOptionPane.showMessageDialog(null,"Temperature >= 0 requested");
            jftftemp.setValue(new Float(0.0));
            return false;
        }
        // RES
        vres = ((Number)jftfres.getValue()).floatValue();
        if( vres < 0 ) {
            JOptionPane.showMessageDialog(null,"Resolution >= 0 requested");
            jftfres.setValue(new Float(0.0));
            return false;
        }
        // PAF
        vpaf = ((Number)jftfpaf.getValue()).floatValue();
        if( vpaf <= 0 ) {
            JOptionPane.showMessageDialog(null,"Drawing Step Size > 0 requested");
            jftfpaf.setValue(new Float(0.0));
            return false;
        }
        if( vpaf >= vres ) {
            JOptionPane.showMessageDialog(null,"Drawing Step Size < Resolution requested");
            jftfpaf.setValue(new Float(0.0));
            return false;
        }
        // THRES
        vthres = ((Number)jftfthres.getValue()).floatValue();
        if( vthres < 0 ) {
            JOptionPane.showMessageDialog(null,"Intensity Thershold >= 0 requested");
            jftfthres.setValue(new Float(0.0));
            return false;
        }
        // SPECTRUM TYPE
        nsptype = (String)jcbsptype.getSelectedItem();
        if( nsptype == "" ) {
            JOptionPane.showMessageDialog(null,"You have to first define the Spectrum Type");
            return false;
        }
        // APPF
        nappf = (String)jcbappf.getSelectedItem();
        if( nsptype != "coabs" ) {
            if( nappf == "" ) {
                JOptionPane.showMessageDialog(null,"You have to first define the Apparatus Function");
                return false;
            }
        }
        // OUTPUT UNIT
        noutunit = (String)jcboutunit.getSelectedItem();
        if( noutunit == "" ) {
            JOptionPane.showMessageDialog(null,"You have to first define the Output Unit");
            return false;
        }
        // LINE FILES
        if(nblfil == 0) {
            JOptionPane.showMessageDialog(null,"You have to first define at least one Line File");
            return false;
        }
        // simul.f restrictions
        double refl  = 0.;                                           // FWHM referent
        double pdlmh = 0.;                                           // nb of steps of FWHM
        if( nappf == "dirac" ) {
            vres = (float) 0.;                                       // resolution = 0
        }
        // check line files characteristics
        for( indlfil=0; indlfil<nblfil; indlfil++ ) {
            // LINE FILE NAME
            if( lfil[indlfil].length() == 0 ) {
                JOptionPane.showMessageDialog(null,"Line File (#"+(indlfil+1)+") Name requested");
                return false;
            }
            // CDAE
            cdae[indlfil] = ((Number)jftfcdae[indlfil].getValue()).floatValue();
            if( cdae[indlfil] < 0 ) {
                JOptionPane.showMessageDialog(null,"Line File (#"+(indlfil+1)+") Press Broad Coeff >= 0 requested");
                return false;
            }
            // MASS
            mass[indlfil] = ((Number)jftfmass[indlfil].getValue()).intValue();
            if( mass[indlfil] <= 0 ) {
                JOptionPane.showMessageDialog(null,"Line File (#"+(indlfil+1)+") Molar Mass > 0 requested");
                return false;
            }
            // UK
            uk[indlfil] = ((Number)jftfuk[indlfil].getValue()).floatValue();
            if( uk[indlfil] < 0 ) {
                JOptionPane.showMessageDialog(null,"Line File (#"+(indlfil+1)+") Intensity Scaling Factor >= 0 requested");
                return false;
            }
            // PTT
            ptt[indlfil] = ((Number)jftfptt[indlfil].getValue()).floatValue();
            if( ptt[indlfil] < 0 ) {
                JOptionPane.showMessageDialog(null,"Line File (#"+(indlfil+1)+") Total Pressure >= 0 requested");
                return false;
            }
            // PPT
            ppt[indlfil] = ((Number)jftfppt[indlfil].getValue()).floatValue();
            if( nsptype != "raman" ) {
                if( ppt[indlfil] < 0 ) {
                    JOptionPane.showMessageDialog(null,"Line File (#"+(indlfil+1)+") Partial Pressure >= 0 requested");
                    return false;
                }
                if( ppt[indlfil] > ptt[indlfil] ) {
                    JOptionPane.showMessageDialog(null,"Line File (#"+(indlfil+1)+") Partial Pressure <= Total Pressure requested");
                    return false;
                }
            }
            // CL
            cl[indlfil] = ((Number)jftfcl[indlfil].getValue()).floatValue();
            if( nsptype != "raman" &&
                nsptype != "coabs"    ) {
                if( cl[indlfil] <= 0 ) {
                    JOptionPane.showMessageDialog(null,"Line File (#"+(indlfil+1)+") Path Length > 0 requested");
                    return false;
                }
            }
            // INPUT UNIT
            ninunit = (String)jcbinunit[indlfil].getSelectedItem();
            if( ninunit == "" ) {
                JOptionPane.showMessageDialog(null,"Line File (#"+(indlfil+1)+") Input Unit requested");
                return false;
            }
            // simul.f restrictions
            double tef = 0.;                                         // effective temperature
            double sl;
            double sd;
            if( nappf == "gauss" ) {
                sd = (3.581097E-7)*Math.sqrt(vtemp/mass[indlfil])*(vumin+vumax)/2.;
                tef = vtemp*(1.+Math.pow(vres/sd,2.));
            }
            else {
                tef = vtemp;
            }
            sl = cdae[indlfil]*ptt[indlfil]/760.;                    // 760 due to atmospheres
            sd = (3.581097E-7)*Math.sqrt(tef/mass[indlfil])*(vumin+vumax)/2.;
            if( nsptype == "raman" ) {
                sd = Math.sqrt(sd*sd+vres*vres);
            }
            refl = Math.max(refl,sl);
            refl = Math.max(refl,sd);
        }
        // sampling test
        refl = 2.*refl;
        if( vres > refl && ( nappf != "dirac" ) ) {
            refl = vres;
        }
        pdlmh = refl/vpaf;
        if( pdlmh < 3. ) {
            JOptionPane.showMessageDialog(null,"Sampling too sparse"                                 +lnsep+
                                               pdlmh+" drawing steps in a full width at half maximum"+lnsep+
                                               "Has to be >= 3");
            return false;
        }
        if( pdlmh > 20. ) {
            JOptionPane.showMessageDialog(null,"Sampling too dense"                                  +lnsep+
                                               pdlmh+" drawing steps in a full width at half maximum"+lnsep+
                                               "Should be <= 20");
            //return false;                                            // simple warning in simul.f
        }
        // everything is OK
        return true;
    }

}
