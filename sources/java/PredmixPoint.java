/**
 * This class allows to sort Pred_mix file data.
 */
public class PredmixPoint implements Comparable {

    private double x;                                                // Jinf, Jsup, frequency or intensity
    private double diff;                                             // obs-calc
    private String smark;                                            // selection mark
    private String jsyn;                                             // assignment
    private int    comp;                                             // component

    /**
     * Construct a new PredmixPoint.
     *
     * @param cx      Jinf, Jsup, frequency or intensity
     * @param cdiff   obs-calc
     * @param csmark  selection mark
     * @param cjsyn   assignment
     * @param ccomp   component
     */
    public PredmixPoint(double cx, double cdiff, String csmark, String cjsyn, int ccomp) {

        x     = cx;                                                  // Jinf, Jsup, frequency or intensity
        diff  = cdiff;                                               // obs-calc
        smark = csmark;                                              // selection mark
        jsyn  = cjsyn;                                               // assignment
        comp  = ccomp;                                               // component
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Return x.
     */
    public double getX() {

        return x;
    }

    /**
     * Return obs-calc.
     */
    public double getDiff() {

        return diff;
    }

    /**
     * Return selection mark string.
     */
    public String getSmark() {

        return smark;
    }

    /**
     * Return assignment string.
     */
    public String getJsyn() {

        return jsyn;
    }

    /**
     * Return dominating sublevel.
     */
    public int getComp() {

        return comp;
    }

    /**
     * Compare x then obs-calc.
     *
     * @param cppt the PredmixPoint to compare to
     */
    public int compareTo(Object cppt) {

        if( !(cppt instanceof PredmixPoint) ) {                      // not the right object
            throw new ClassCastException();
        }
        double delta = ((PredmixPoint)cppt).getX() - x;             // compare x
        if     ( delta < 0. ) {
            return  1;
        }
        else if( delta > 0. ) {
            return -1;
        }
        else {
            delta = ((PredmixPoint)cppt).getDiff() - diff;           // compare obs-calc
            if     ( delta < 0 ) {
                return  1;
            }
            else if( delta > 0 ) {
                return -1;
            }
        }
        return 0;                                                    // equal
    }

}
