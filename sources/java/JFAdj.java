/*
 * Class to manage extra ParaFile_adj; called by CJPXfit
 */

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import java.text.*;
import java.util.*;

/**
 * This panel collects informations concerning extra fitted parameter files.
 */

public class JFAdj extends    JFrame
                   implements ActionListener {

    private CJPXFit   calling;                                       // calling CJPXFit
    private String[]  ntransp;                                       // transition id

    private JPAdj[]  jpadj;                                          // extra ParaFiles jp
    private String   npack;                                          // package  name
    private String   nmol;                                           // molecule name
    private int      nbadj;                                          // nb of extra
    private int      mxnbadj;                                        // nbadj max

    // panels
    private JPanel pouest;
    private JPanel   pon;
    private JPanel   pos;
    private JPanel pcentre;
    private JPanel   pcn;

    //
    private Box box;
    private JButton     jbadd;
    private JButton     jbremlast;
    private JButton     jbset;
    private String lnsep;                                            // line separator

/////////////////////////////////////////////////////////////////////

    /**
     * Construct a new JFAdj.
     *
     * @param cpt       window X,Y location
     * @param cntransp  possible transition names
     * @param cnpack    package  name
     * @param cnmol     molecule name
     */
    public JFAdj( CJPXFit ccalling, Point cpt, String[] cntransp, String cnpack, String cnmol, int cmxnbadj ) {

        super(" Set extra fitted parameter files");                  // main window

        nbadj   = 0;                                                 // nb of extra ParaFiles
        calling = ccalling;                                          // calling CJPXFit
        ntransp = cntransp;
        npack   = cnpack;
        nmol    = cnmol;
        mxnbadj = cmxnbadj;
        jpadj = new JPAdj[mxnbadj];

        lnsep = System.getProperty("line.separator");
        //setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        getContentPane().setLayout(new BorderLayout());
        setLocation( cpt );

        // west panel for add/remlast/set extra ParaFiles buttons
        box= Box.createVerticalBox();                                // add buttons to box
        jbadd= new JButton("Add");
        jbadd.setBackground(Color.WHITE);
        jbadd.addActionListener(this);
        box.add(jbadd);
        box.add(Box.createVerticalStrut(15));
        jbremlast= new JButton("Remove last one");
        jbremlast.setBackground(Color.WHITE);
        jbremlast.addActionListener(this);
        box.add(jbremlast);
        pon = new JPanel();
        pon.add(box);
        jbset= new JButton("Set");
        jbset.setBackground(Color.WHITE);
        jbset.addActionListener(this);
        pos = new JPanel();
        pos.add(jbset);
        pouest = new JPanel(new BorderLayout());
        pouest.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),null));
        pouest.add(pon,"North");
        pouest.add(pos,"South");

        // center panel for extra ParaFiles panels
        pcentre = new JPanel(new BorderLayout());
        pcn = new JPanel(new GridLayout(0,1,5,5));
        pcentre.add(pcn,"North");
      //jbadd.doClick();

        // display panels
        getContentPane().add(pouest ,"West");
        getContentPane().add(pcentre,"Center");

        setVisible(false);
        pack();
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Process events.
     */
    public void actionPerformed(ActionEvent evt) {

        // add button
        if( evt.getSource() == jbadd ) {
            // test existing extra ParaFiles
            if( ! calling.testAdj() ) {
                return;
            }
            if( nbadj == mxnbadj ) {
                JOptionPane.showMessageDialog(null,"Max number of extra ParaFiles ("+mxnbadj+") reached");
                return;
            }
            jpadj[nbadj] = new JPAdj(this, nbadj+1, ntransp, npack, nmol);
            pcn.add(jpadj[nbadj]);
            pcn.revalidate();
            pack();
            //
            nbadj ++;
            return;
        }

        // remlast button
        if( evt.getSource() == jbremlast ) {
            if( nbadj != 0 ) {
                pcn.remove(jpadj[nbadj-1]);
                pcn.revalidate();
                pack();
                nbadj --;
            }
            return;
        }

        // set button
        if( evt.getSource() == jbset ) {
            // test existing extra ParaFiles
            if( ! calling.testAdj() ) {
                return;
            }
            setVisible(false);
            return;
        }
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Get nbadj.
     */
    public int getNbAdj() {

        return nbadj;
    }

    /**
     * Get JPAdj.
     */
    public JPAdj getJPAdj(int ciadj) {

        if( ciadj <  0      ||
            ciadj >= nbadj    ) {
            JOptionPane.showMessageDialog(null,"Invalid index ("+ciadj+") in JFAdj.getJPAdj");
            return null;
        }
        return jpadj[ciadj];
    }

    /**
     * Test all JPADJ.
     */

    public boolean test() {

        if( nbadj != 0 ) {
            for(int iadj=0; iadj<nbadj; iadj++ ) {
                if( ! jpadj[iadj].test() ) {
                    return false;
                }
                // duplicated ?
                if( iadj >= 1 ) {
                    for(int jadj=0; jadj<iadj; jadj++) {
                        if(  jpadj[jadj].getNtradj().equals(jpadj[iadj].getNtradj())     &&
                            (jpadj[jadj].isPolSelected() == jpadj[iadj].isPolSelected())      ) {
                            JOptionPane.showMessageDialog(null,"Duplicated Extra ParaFile_adj"+lnsep+
                                                               "see #"+(jadj+1)+" and #"+(iadj+1));
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

}
