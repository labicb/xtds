/*
 * Class for STARK spectrum calculation job creation, called by CreateJob
 */

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import java.text.*;
import java.util.*;

/**
 * This panel creates a job to calculate rovibrational transitions.
 */
public class CJPSpectStark extends    JPanel
                           implements CJPint, ActionListener {

    // panels
    private JPanel pnord;
    private JPanel pcentre;
    private JPanel psud;

    // north panel accessories
    private JPanel       pnn;                                        // north-north panel
    private JPanel       pnno;
    private JPanel       pnnc;
    private JPanel       pnc;                                        // north-center panel
    private JPanel       pncn;
    private JPanel       pncno;
    private JPanel       pncnc;
    private JPanel       pncc;
    private JPanel       pncco;
    private JPanel       pnccc;
    private JPanel       pns;                                        // north-south panel
    private JPanel       pnsn;
    private JPanel       pnso;
    private JPanel       pnsc;
    private JPanel       pnscn;
    private JPanel       pnscs;

    private JComboBox    jcbmol;                                     // molecule jcb
    private String[]     trep;                                       // representation choice array
    private JComboBox    jcbrep;                                     // representation jcb
    private String       nrep;                                       // representation name
    private String[]     tjmax;                                      // jmax choice array
    private JComboBox    jcbhjmax;                                   // hjmax jcb
    private String       nhjmax;                                     // hjmax name
    private JComboBox    jcbsmmax;                                   // smmax jcb
    private String       nsmmax;                                     // smmax name

    private JButton      jbhparaf;                                   // hamiltonian    parafile button
    private String       nhparaf;                                    // hamiltonian    parafile name
    private String       nhparar;                                    // hamiltonian    parafile name (reduced)
    private JLabel       lhparaf;                                    // hamiltonian    parafile label
    private JButton      jbpparaf;                                   // polarizability parafile button
    private String       npparaf;                                    // polarizability parafile name
    private String       npparar;                                    // polarizability parafile name (reduced)
    private JLabel       lpparaf;                                    // hamiltonian parafile label
    private JComboBox[]  jcbpsc;                                     // polyad scheme coef. jcb
    private String[]     tpsc;                                       // polyad scheme coef. choice array
    private String       npsc;                                       // current psc name
    private String[]     tdvo;                                       // development order choice array
    private JPanel       trm_jpdvo;                                  // transition moment development order panel
    private JComboBox[]  trm_jcbdvo;                                 // transition moment development order jcb
    private String       trm_ndvo;                                   // transition moment development order name

    private JFormattedTextField jftfelec;                            // Electric Field text field
    private float               velec;                               // Electric Field value
    private JFormattedTextField jftffmin;                            // FMIN text field
    private float               vfmin;                               // FMIN value
    private JFormattedTextField jftffmax;                            // FMAX text field
    private float               vfmax;                               // FMAX value
    private JFormattedTextField jftftvib;                            // TVIB text field
    private float               vtvib;                               // TVIB value
    private JFormattedTextField jftftrot;                            // TROT text field
    private float               vtrot;                               // TROT value
    private JTextField          jtfrinmi;                            // RINMI text field
    private float               vrinmi;                              // RINMI value
    private JRadioButton        jrbfpvib;                            // fpvib option jrb
    private JTextField          jtffpvib;                            // fpvib option jtf
    private float               vfpvib;                              // fpvib value
    private JRadioButton        jrbabund;                            // abund option jrb
    private JTextField          jtfabund;                            // abund option jtf
    private float               vabund;                              // abund value

    // center panel accessories
    private JTextArea   jta;                                         // polyad display text
    private JScrollPane jsp;                                         // with lifts

    // south panel accessories
    private JButton jbreset;                                         // reset
    private JButton jbsave;                                          // save
    private Box     boxsud;                                          // button box

    // CreateJob parameters
    private String   playd;                                          // XTDS installation directory
    private String   workd;                                          // working directory
    private String   npack;                                          // package name
    private String[] moldirs;                                        // molecule directories
    private int      nbvqn;                                          // nb of vibrational quantum numbers
    private int      mxpol;                                          // max nb of polyads
    // parameters catched in this CJPanel
    private String   nmol;                                           // molecule
    private double   hjmax;                                          // hamiltonian    jmax
    private double   jppol;                                          // polarizability jmax
    private double   smmax;                                          // stark          mmax
    private double   jpupp;                                          // jpupp;
    private int[]    vpsc;                                           // polyad scheme coef.
    private int[]    trm_vdvo;                                       // development order

    // polyads
    private Polyad   lowpolyad;                                      // lower polyad
    private int      low_polnb;                                      // lower polyad #
    private JPPolyad jpplow;                                         // lower polyad panel
    private Polyad   upppolyad;                                      // upper polyad
    private int      upp_polnb;                                      // upper polyad #
    private JPPolyad jppupp;                                         // upper polyad panel
    private int      dif_polnb;                                      // upp_polnb-low_polnb

    // variables
    private int     nc;                                              // current polyad #
    private float[] freq;                                            // vibrational state sorting frequencies

    private String         str;                                      // Pa_skel file reading string
    private BufferedReader br;                                       // Pa_skel file reading buffer
    private String         nskelf;                                   // Pa_skel file name
    private String         nskeld;                                   // Pa_skel directory
    private String         njobf;                                    // job file name
    private PrintWriter    out1;                                     // job writing pw
    private boolean        evt_basics;                               // if basics            event
    private boolean        evt_trm;                                  // if transition moment event
    private String         lnsep;                                    // line separator
    private String         fisep;                                    // file separator
    private Font           mono15;                                   // Monospaced 15 font
    private NumberFormat   nffloat;                                  // float format

/////////////////////////////////////////////////////////////////////

    /**
     * Constructs a new CJPSpectStark.
     */
    public CJPSpectStark() {

        playd = System.getProperty("xtds.home");                     // XTDS installation directory
        workd = System.getProperty("user.dir");                      // working directory
        lnsep = System.getProperty("line.separator");
        fisep = System.getProperty("file.separator");
        mono15 = new Font("Monospaced", Font.PLAIN, 15);
        setName("CJPSpectStark");                                    // for help files
        setLayout(new BorderLayout());

        nffloat = NumberFormat.getInstance(Locale.US);
        nffloat.setGroupingUsed(false);                              // ie. no thousands separator
        nffloat.setMaximumFractionDigits(10);                        // ie. NO more then 10 digits for fractionnal part

        // south panel
        jbreset = new JButton();                                     // managed by CreateJob
        jbreset.setBackground(Color.WHITE);
        jbsave  = new JButton("Save");                               // job file saving button
        jbsave.setBackground(Color.WHITE);
        jbsave.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent ae) {           // jbsave action
                saveCJP();
            }
        }
        );

        boxsud = Box.createHorizontalBox();                          // add buttons to box
        boxsud.add(jbreset);
        boxsud.add(Box.createHorizontalStrut(15));
        boxsud.add(jbsave);
        psud = new JPanel();
        psud.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        psud.add(boxsud);                                            // add box

        // center panel polyad display
        pcentre = new JPanel(new BorderLayout());
        jta = new JTextArea();
        jta.setBackground(Color.BLACK);
        jta.setForeground(Color.WHITE);
        jta.setEditable(false);                                      // not editable
        jta.setFont( mono15 );
        jta.setLineWrap(false);                                      // NO line wrap
        jsp = new JScrollPane(jta);                                  // lifts
        pcentre.add(jsp, "Center");

        // display panels
        add(pcentre, "Center");
        add(psud,    "South");
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Returns the reset button.
     * <br>Called by CreateJob which manages it.
     */
    public JButton getJBreset() {

        return jbreset;
    }

    /**
     * Sets basic parameters and creates north panel.
     * <br>Called by CreateJob to activate this panel.
     *
     * @param cnpack     name of the package
     * @param cmoldirs   molecule directories
     * @param cmxpol     MXPOL
     * @param cnbclab    number of characters of parameter label
     * @param cnbvqn     number of vibrational quantum numbers
     */
    public void setBasicPara(String cnpack, String[] cmoldirs, int cmxpol, int cnbclab, int cnbvqn) {

        npack   = cnpack;                                            // package name
        moldirs = cmoldirs;                                          // molecule directories
        mxpol   = cmxpol;                                            // MXPOL
        nbvqn   = cnbvqn;                                            // nb of vibrational quantum numbers

        createNorth();
    }

/////////////////////////////////////////////////////////////////////

    // north panel for polyad characteristics setting and display
    private void createNorth() {

        pnord = new JPanel(new BorderLayout());                      // north panel
        pnn   = new JPanel(new BorderLayout());                      // north-north panel
        pnno  = new JPanel(new GridLayout(5,0,5,5));
        pnnc  = new JPanel(new GridLayout(5,0,5,5));
        pnc   = new JPanel(new BorderLayout());                      // north-center panel
        pncn  = new JPanel(new BorderLayout());
        pncno = new JPanel(new GridLayout(3,0,5,5));
        pncnc = new JPanel(new GridLayout(3,0,5,5));
        pncc  = new JPanel(new BorderLayout());
        pncco = new JPanel(new GridLayout(3,0,5,5));
        pnccc = new JPanel(new GridLayout(3,0,5,5));
        pns   = new JPanel(new BorderLayout());                      // north-south panel
        pnso  = new JPanel(new GridLayout(3,0,5,5));
        pnsc  = new JPanel(new BorderLayout());
        pnscn = new JPanel(new GridLayout(2,0,5,5));
        pnscs = new JPanel(new GridLayout(1,0,5,5));

        // BASICS
        pnno.add(new JLabel(""));
        // molecule choice
        JPanel jpmrjh;
        jpmrjh = new JPanel(new GridLayout(0,6,0,5));
        jpmrjh.add(new JLabel("Molecule ",null,JLabel.RIGHT));
        jcbmol = new JComboBox();
        jcbmol.addItem("");
        for( int i=0; i<moldirs.length; i++ ) {
            jcbmol.addItem(moldirs[i]);
        }
        jcbmol.setSelectedItem("");                                  // default to space
        jcbmol.setBackground(Color.WHITE);
        nmol = "";
        jcbmol.addActionListener(this);
        jpmrjh.add(jcbmol);
        if( npack.equals("D2hTDS") ) {
            // representation choice
            jpmrjh.add(new JLabel("Representation ",null,JLabel.RIGHT));
            trep = new String[4];
            trep[0] = "";                                            // space first
            trep[1] = "Ir";
            trep[2] = "IIr";
            trep[3] = "IIIr";
            jcbrep = new JComboBox(trep);
            jcbrep.setSelectedItem("");                              // default to space
            jcbrep.setBackground(Color.WHITE);
            jpmrjh.add(jcbrep);
        }
        else {
            jpmrjh.add(new JLabel(""));
            jpmrjh.add(new JLabel(""));
        }
        // hamiltonian jmax choice
        jpmrjh.add(new JLabel("Hamiltonian Jmax ",null,JLabel.RIGHT));
        tjmax = new String[201];                                     // 201 due to space and jmax = 0(.5) -> 199(.5)
        tjmax[0] = "";                                               // space first
        for( int i=1; i<tjmax.length; i++) {                         // fill array
            if( npack.equals("C3vsTDS") ) {
                tjmax[i] = String.valueOf(i-1+0.5);
            }
        else {
                tjmax[i] = String.valueOf(i-1);
            }
        }
        jcbhjmax = new JComboBox(tjmax);
        jcbhjmax.setSelectedItem("");                                 // default to space
        jcbhjmax.setBackground(Color.WHITE);
        jpmrjh.add(jcbhjmax);
        pnnc.add(jpmrjh);
        //
        pnno.add(new JLabel(""));
        JPanel jpefsm;
        jpefsm = new JPanel(new GridLayout(0,6,0,5));
        jpefsm.add(new JLabel(""));
        jpefsm.add(new JLabel(""));
        // electric field
        jpefsm.add(new JLabel("Electric Field ",null,JLabel.RIGHT));
        jftfelec = new JFormattedTextField(nffloat);
        jftfelec.setValue(new Float(0.0));
        jpefsm.add(jftfelec);
        // stark Mmax choice
        jpefsm.add(new JLabel("Stark Mmax ",null,JLabel.RIGHT));
        jcbsmmax = new JComboBox(tjmax);
        jcbsmmax.setSelectedItem("");                                // default to space
        jcbsmmax.setBackground(Color.WHITE);
        jpefsm.add(jcbsmmax);
        pnnc.add(jpefsm);
        // hamiltonian parafile choice
        jbhparaf = new JButton("Hamiltonian ParaFile");
        jbhparaf.setBackground(Color.WHITE);
        jbhparaf.addActionListener(this);
        nhparaf = "";
        pnno.add(jbhparaf);
        lhparaf = new JLabel("");                                    // default to space
        lhparaf.setOpaque(true);
        lhparaf.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        pnnc.add(lhparaf);
        // polarizability parafile choice
        jbpparaf = new JButton("Polarizability ParaFile");
        jbpparaf.setBackground(Color.WHITE);
        jbpparaf.addActionListener(this);
        npparaf = "";
        pnno.add(jbpparaf);
        lpparaf = new JLabel("");                                    // default to space
        lpparaf.setOpaque(true);
        lpparaf.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        pnnc.add(lpparaf);
        // polyad scheme coefficients choice
        pnno.add(new JLabel("Polyad Scheme "));
        JPanel jppsc = new JPanel(new GridLayout(0,2*nbvqn+1,0,5));  // nbvqn = nb of vibrational quantum numbers
        jppsc.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        jcbpsc = new JComboBox[nbvqn];
        tpsc = new String[mxpol];                                    // polyad scheme coefficients array
        for( int i=0; i<mxpol; i++) {
            tpsc[i] = String.valueOf(i);                             // 0 -> mxpol
        }
        for ( int i=0; i<nbvqn; i++) {                               // choice Label
            if(i == 0) {
                jppsc.add(new JLabel(" [P]n = "));                   // polyad number
            }
            jcbpsc[i] = new JComboBox();
            jcbpsc[i].addItem("");
            for( int j=0; j<tpsc.length; j++ ) {
                jcbpsc[i].addItem(tpsc[j]);
            }
            jcbpsc[i].setSelectedItem("");                           // default to space
            jcbpsc[i].setBackground(Color.WHITE);
            jcbpsc[i].addActionListener(this);
            vpsc = new int[nbvqn];                                   // polyad scheme coefficient values
            jppsc.add(jcbpsc[i]);
            if(i != nbvqn-1) {                                       // set label
                jppsc.add(new JLabel("*v"+(i+1)+" +"));
            }
            else {
                jppsc.add(new JLabel("*v"+(i+1)));
            }
        }
        pnnc.add(jppsc);
        pnn.add(pnno,"West");
        pnn.add(pnnc,"Center");
        pnn.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),"BASICS"));

        // LOWER POLYAD
        lowpolyad = new Polyad(nbvqn);                               // lower polyad
        lowpolyad.setName("Lower Polyad");                           // its name
        lowpolyad.setPolar(true);                                    // polarizability management requested
        jpplow = new JPPolyad(this, lowpolyad, tpsc, pncn);          // its panel

        // UPPER POLYAD
        upppolyad = new Polyad(nbvqn);                               // upper polyad
        upppolyad.setName("Upper Polyad");                           // its name
        upppolyad.setPolar(true);                                    // polarizability management requested
        jppupp = new JPPolyad(this, upppolyad, tpsc, pncc);          // its panel

        tdvo = new String[10];                                       // order 0 -> 9
        for(int i=0; i<10; i++) {
            tdvo[i] = String.valueOf(i);
        }
        // TRANSITION MOMENT
        // SPECT parameters
        JPanel pnscn1 = new JPanel(new GridLayout(0,10,0,5));
        pnscn1.add(new JLabel("FMIN ",null,JLabel.RIGHT));
        jftffmin = new JFormattedTextField(nffloat);
        jftffmin.setValue(new Float(0.0));
        pnscn1.add(jftffmin);
        pnscn1.add(new JLabel("FMAX ",null,JLabel.RIGHT));
        jftffmax = new JFormattedTextField(nffloat);
        jftffmax.setValue(new Float(0.0));
        pnscn1.add(jftffmax);
        pnscn1.add(new JLabel("TVIB ",null,JLabel.RIGHT));
        jftftvib = new JFormattedTextField(nffloat);
        jftftvib.setValue(new Float(0.0));
        pnscn1.add(jftftvib);
        pnscn1.add(new JLabel("TROT ",null,JLabel.RIGHT));
        jftftrot = new JFormattedTextField(nffloat);
        jftftrot.setValue(new Float(0.0));
        pnscn1.add(jftftrot);
        pnscn1.add(new JLabel("RINMI ",null,JLabel.RIGHT));
        jtfrinmi = new JTextField("");
        pnscn1.add(jtfrinmi);
        pnscn.add(pnscn1);
        JPanel pnscn2 = new JPanel(new GridLayout(0,10,0,5));
        pnscn2.add(new JLabel());
        jrbfpvib = new JRadioButton("fpvib");
        jrbfpvib.setBackground(Color.WHITE);
        jrbfpvib.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        jrbfpvib.addActionListener(this);
        pnscn2.add(jrbfpvib);
        jtffpvib = new JTextField("");
        jtffpvib.setEnabled(false);
        jtffpvib.setBackground(this.getBackground());
        pnscn2.add(jtffpvib);
        pnscn2.add(new JLabel(""));
        jrbabund = new JRadioButton("abund");
        jrbabund.setBackground(Color.WHITE);
        jrbabund.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        jrbabund.addActionListener(this);
        pnscn2.add(jrbabund);
        jtfabund = new JTextField("");
        jtfabund.setEnabled(false);
        jtfabund.setBackground(this.getBackground());
        pnscn2.add(jtfabund);
        pnscn.add(pnscn2);
        // Development Order choice
        pnso.add(new JLabel(""));
        pnso.add(new JLabel(""));
        pnso.add(new JLabel("Dipole Moment Dev. Ord. "));

        pnsc.add(pnscn,"North");
        pnsc.add(pnscs,"South");
        pns.add(pnso,"West");
        pns.add(pnsc,"Center");
        pns.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),"TRANSITION MOMENT"));

        // add panels and sub-panels
        pnc.add(pncn,"North");
        pnc.add(pncc,"Center");

        pnord.add(pnn,"North");
        pnord.add(pnc,"Center");
        pnord.add(pns,"South");
        add(pnord, "North");

    }

/////////////////////////////////////////////////////////////////////

    /**
     * Processes the events.
     */
    public void actionPerformed(ActionEvent evt) {

        // BASICS
        evt_basics = false;
        // molecule
        if (evt.getSource() == jcbmol ) {
            evt_basics = true;
            nmol = (String) jcbmol.getSelectedItem();
            // unselect representation, jmax and parameter file
            if( npack.equals("D2hTDS") ) {
                jcbrep.setSelectedItem("");                          // unselect representation
            }
            jcbhjmax.setSelectedItem("");                            // unselect hamiltonian jmax
            jcbsmmax.setSelectedItem("");                            // unselect stark       mmax
            jftfelec.setValue(new Float(0.0));                       // set to 0 electric field
            nhparaf = "";                                            // unselect parameter file
            lhparaf.setText(nhparaf);
            npparaf = "";                                            // unselect parameter file
            lpparaf.setText(npparaf);
            if( nmol != "" ) {
                nskeld = playd+fisep+"packages"+fisep+npack+fisep+"para"+fisep+nmol;
                nskelf = nskeld+fisep+"Pa_skel";
                if( (! lowpolyad.setInd(npack, nskelf)) ||
                    (! upppolyad.setInd(npack, nskelf))    ) {       // set frequency indexes
                    nmol = "";
                    jcbmol.setSelectedItem("");                      // file error : unselect molecule
                }
            }
        }
        // hamiltonian parafile
        if (evt.getSource() == jbhparaf) {
            evt_basics = true;
            if( nmol == "" ) {
                JOptionPane.showMessageDialog(null,"You have to first define the molecule");
                return;
            }
            JFileChooser jfchparaf = new JFileChooser(playd+fisep+"packages"+fisep+npack+fisep+"para"+fisep+nmol);  // default choice directory
            jfchparaf.setSize(400,300);
            jfchparaf.setFileSelectionMode(JFileChooser.FILES_ONLY);  // files only
            jfchparaf.setDialogTitle("Define the hamiltonian parafile to be used");
            Container parent = jbhparaf.getParent();
            int choice = jfchparaf.showDialog(parent,"Select");       // Dialog, Select
            if (choice == JFileChooser.APPROVE_OPTION) {
                nhparaf= jfchparaf.getSelectedFile().getAbsolutePath();
                nhparar= jfchparaf.getSelectedFile().getName();
                lhparaf.setText(nhparaf);
            }
        }
        // polarizability parafile
        if (evt.getSource() == jbpparaf) {
            evt_basics = true;
            if( nmol == "" ) {
                JOptionPane.showMessageDialog(null,"You have to first define the molecule");
                return;
            }
            JFileChooser jfcpparaf = new JFileChooser(playd+fisep+"packages"+fisep+npack+fisep+"para"+fisep+nmol);  // default choice directory
            jfcpparaf.setSize(400,300);
            jfcpparaf.setFileSelectionMode(JFileChooser.FILES_ONLY);  // files only
            jfcpparaf.setDialogTitle("Define the pmolarizability parafile to be used");
            Container parent = jbpparaf.getParent();
            int choice = jfcpparaf.showDialog(parent,"Select");       // Dialog, Select
            if (choice == JFileChooser.APPROVE_OPTION) {
                npparaf= jfcpparaf.getSelectedFile().getAbsolutePath();
                npparar= jfcpparaf.getSelectedFile().getName();
                lpparaf.setText(npparaf);
            }
        }
        // Polyad Scheme Coefficients
        for (int i=0; i<nbvqn; i++) {
            if(evt.getSource() == jcbpsc[i] ) {                      // whatever coef.
                evt_basics = true;
                npsc = (String) jcbpsc[i].getSelectedItem();
                if(npsc != "") {
                    vpsc[i] =  Integer.parseInt(npsc);               // save coef. value
                }
            }
        }
        // end of basics
        if(evt_basics) {
            jpplow.resetJP();
            jppupp.resetJP();
            resetTrmJP();
            return;
        }

        // OPTION fpvib
        if(evt.getSource() == jrbfpvib) {
            if(jrbfpvib.isSelected()) {
                jtffpvib.setEnabled(true);
                jtffpvib.setBackground(Color.WHITE);
            }
            else {
                jtffpvib.setText("");
                jtffpvib.setEnabled(false);
                jtffpvib.setBackground(this.getBackground());
            }
            return;
        }

        // OPTION abund
        if(evt.getSource() == jrbabund) {
            if(jrbabund.isSelected()) {
                jtfabund.setEnabled(true);
                jtfabund.setBackground(Color.WHITE);
            }
            else {
                jtfabund.setText("");
                jtfabund.setEnabled(false);
                jtfabund.setBackground(this.getBackground());
            }
            return;
        }

    }

/////////////////////////////////////////////////////////////////////

    /**
     * Manages JPPolyad event.
     */
    public void JPPEvent( Polyad curpol, int type, int indice ) {

        if( ! testBasics() ) {                                       // basics checked ?
            jpplow.resetJP();                                        // JPP low reset
            jppupp.resetJP();                                        // JPP upp reset
            resetTrmJP();
        }
        else {
            // if lower polyad
            if( curpol == lowpolyad ) {
                jppupp.resetJP();                                    // JPP upp reset
                resetTrmJP();
                if( lowpolyad.isSetPolnb() ) {
                    low_polnb = lowpolyad.getPolnb();
                    addTrmJP();
                }
            }
            // if upper polyad
            else {
                // lowpolyad fully defined ?
                if( ! lowpolyad.isFullydef() ) {
                    lowpolyad.askPol();                              // no, ask for
                    jppupp.resetJP();                                // JPP upp reset
                    resetTrmJP();
                    if( lowpolyad.isSetPolnb() ) {
                        addTrmJP();
                    }
                }
                else {
                    // validate upp % low
                    switch( type ) {
                        // polyad #
                        case 0 : {
                            if( upppolyad.isSetPolnb() ) {
                                // upp polnb defined
                                upp_polnb = upppolyad.getPolnb();
                                if( upp_polnb < low_polnb ) {
                                    // restriction
                                    JOptionPane.showMessageDialog(null,"The upper polyad number"           +lnsep+
                                                                       "has to be greater than or equal to"+lnsep+
                                                                       "the lower polyad number");
                                    jppupp.resetJP();
                                }
                                else {
                                    // qal restriction
                                    int[] depqal = new int[nbvqn];
                                    for( int i=0; i<nbvqn; i++ ) {
                                        depqal[i] = Math.max(lowpolyad.getVqal(i),upp_polnb);
                                    }
                                    // dvo restriction
                                    int nbfixdvo = low_polnb+1;
                                    int[] fixdvo = new int[nbfixdvo];
                                    int[] fixpdvo = new int[nbfixdvo];
                                    for( int i=0; i<nbfixdvo; i++ ) {
                                        fixdvo[i]  = lowpolyad.getVdvo(i);
                                        fixpdvo[i] = lowpolyad.getVpdvo(i);
                                    }
                                    jppupp.setFix(depqal, fixdvo, fixpdvo);
                                }
                            }
                            break;
                        }
                        // quanta limit
                        case 1 : {
                            int upp_vqal = upppolyad.getVqal(indice);
                            int low_vqal = lowpolyad.getVqal(indice);
                            if( upp_vqal < low_vqal ) {
                                JOptionPane.showMessageDialog(null,"Each upper quanta limit"           +lnsep+
                                                                   "has to be greater than or equal to"+lnsep+
                                                                   "its respective lower quanta limit");
                                jppupp.setJcbqal(Math.max(low_vqal,upp_polnb), indice);
                            }
                            break;
                        }
                    }
                }
            }
        }
        showPol();
    }

/////////////////////////////////////////////////////////////////////

    // display polyads
    private void showPol() {

        jta.setText("");
        if( lowpolyad.isFullydef() ) {                               // lower polyad defined
            lowpolyad.calCvs(vpsc);                                  // cvs calculation
            StringBuffer sb = new StringBuffer();
            lowpolyad.toSb(sb);                                      // display lower polyad
            jta.append(sb.toString());
            if( upppolyad.isFullydef() ) {                           // upper polyad defined
                upppolyad.calCvs(vpsc);                              // cvs calculation
                sb.setLength(0);
                upppolyad.toSb(sb);                                  // display upper polyad
                jta.append(sb.toString());
            }
        }
    }

    // add transition moment JPanel
    private void addTrmJP() {

        trm_jpdvo = new JPanel(new GridLayout(0,2*(low_polnb+1),0,5));
        trm_jpdvo.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        trm_vdvo = new int[low_polnb+1];                             // define value array
        trm_jcbdvo = new JComboBox[low_polnb+1];
        for ( int i=0; i<low_polnb+1; i++) {
            if( i == 0) {
                trm_jpdvo.add(new JLabel(" D ",null,JLabel.RIGHT));
            }
            else {
                trm_jpdvo.add(new JLabel(""));
            }
            trm_jcbdvo[i] = new JComboBox();
            trm_jcbdvo[i].addItem("");
            for( int j=0; j<tdvo.length; j++ ) {
                trm_jcbdvo[i].addItem(tdvo[j]);
            }
            trm_jcbdvo[i].setSelectedItem("");
            trm_jcbdvo[i].setBackground(Color.WHITE);
            trm_jpdvo.add(trm_jcbdvo[i]);
        }
        pnscs.add(trm_jpdvo);                                        // add panel
        trm_jpdvo.revalidate();                                      // re-display it if a suppression occured
    }

    // remove transition moment JPanel
    private void removeTrmJP() {

        if(trm_jpdvo != null) {
            pnscs.remove(trm_jpdvo);
            pnscs.repaint();
        }
    }

    // reset transition moment JPanel
    private void resetTrmJP() {

        removeTrmJP();
    }

/////////////////////////////////////////////////////////////////////

    // save job
    private void saveCJP() {

        // check if everything is defined
        // BASICS
        if( ! testBasics()) {
            return;
        }
        // LOWER POLYAD
        if( ! lowpolyad.isFullydef() ) {
            lowpolyad.askPol();
            return;
        }
        // check if there is at least one vs per sub-polyad
        for(int i=0; i<=low_polnb; i++) {
            if(lowpolyad.getNbvs(i) == 0 ) {
                JOptionPane.showMessageDialog(null,"WARNING : your choice produces NO vibrational state for P"+i+" of "+lowpolyad.getName());
            }
        }
        // UPPER POLYAD
        if( ! upppolyad.isFullydef() ) {
            upppolyad.askPol();
            return;
        }
        // check if there is at least one vs per sub-polyad
        for(int i=0; i<=upp_polnb; i++) {
            if(upppolyad.getNbvs(i) == 0 ) {
                JOptionPane.showMessageDialog(null,"WARNING : your choice produces NO vibrational state for P"+i+" of "+upppolyad.getName());
            }
        }

        dif_polnb = upp_polnb-low_polnb;
        // if lower different from upper
        if( dif_polnb != 0 ) {
            // test if lower and upper are consistent
            for(int i=1; i<low_polnb+1; i++) {
                if( lowpolyad.getNbvs(i) != upppolyad.getNbvs(i) ) {
                    JOptionPane.showMessageDialog(null,"Inconsistent number of vibrational states for P"+i);
                    return;
                }
                if( ! lowpolyad.getCvs(i).equals(upppolyad.getCvs(i)) ) {
                    JOptionPane.showMessageDialog(null,"Inconsistent values of vibrational states for P"+i);
                    return;
                }
            }
        }
        // TRANSITION MOMENT
        if( ! checkTrm()) {
            return;
        }

        // everything is well defined
        // choose job name
        JFileChooser jfcjobf = new JFileChooser(workd);
        jfcjobf.setSize(400,300);
        jfcjobf.setFileSelectionMode(JFileChooser.FILES_ONLY);
        jfcjobf.setDialogTitle("Define the job file to be created");
        jfcjobf.setSelectedFile(new File("job_cal_stark"));
        int choice = jfcjobf.showSaveDialog(this);
        if (choice == JFileChooser.APPROVE_OPTION) {
            njobf = jfcjobf.getSelectedFile().getAbsolutePath();     // job file name
        }
        else {
            return;
        }
        // create job
        try {
            out1 = new PrintWriter(new BufferedWriter(new FileWriter(njobf)));  // write job

            out1.println("#! /bin/sh");
            out1.println(" set -v");
            out1.println("##");
            out1.println("## STARK Spectrum Calculation job created by XTDS");
            out1.print  ("## ");
            if( dif_polnb != 0 ) {
                out1.print  ("P"+upp_polnb+"m");
            }
            out1.println("P"+low_polnb+" of "+nmol+".");
            out1.println("##");
            out1.println("BASD="+playd+fisep+"packages"+fisep+npack);
            out1.println("#");
            out1.println(" SCRD=$BASD"+fisep+"prog"+fisep+"exe");
            out1.println(" PARD=$BASD"+fisep+"para"+fisep+nmol);
            out1.println("##");
            out1.println("## Jmax values.");
            out1.println("## Electric Field.");
            out1.println("##");
            if( npack.equals("C3vsTDS") ) {
                 out1.println(" JPH="+hjmax);
                 out1.println(" JPPol="+jppol);
                 out1.println(" MSt="+smmax);
            }
            else {
                 out1.println(" JPH="+(int) hjmax);
                 out1.println(" JPPol="+(int) jppol);
                 out1.println(" MSt="+(int) smmax);
            }
            out1.println(" Elec="+velec);
            out1.println("###########################################################");
            out1.println("## Parameter files.");
            out1.println("##");
            out1.println(" PARA="+nhparaf);
            out1.println(" PARP="+npparaf);
            out1.println("##");
            out1.println("###########################################################");
            // Lower Polyad
            int cur_low_nbvs = lowpolyad.getNbvs(low_polnb);
            int cur_upp_nbvs = upppolyad.getNbvs(upp_polnb);
            out1.println("##");
            out1.println("## Lower Polyad.");
            out1.println("##");
            out1.println("## Zero-field Hamiltonian matrix elements.");
            out1.println("##");
            out1.print  (" $SCRD"+fisep+"passx hmodel P"+low_polnb);
            lowpolyad.ecrPol(out1, true);
            if( npack.equals("D2hTDS") ) {
                out1.println(" \\"+lnsep+
                             "                    "+nrep);
            }
            else {
                out1.println("");
            }
            out1.print  (" $SCRD"+fisep+"passx parchk P"+low_polnb+"     D");
            for(int j=0; j<low_polnb+1; j++) {
                out1.print  (lowpolyad.getVdvo(j));
            }
            if( low_polnb == 0 ) {
                out1.print  (" ");
            }
            out1.println(" $PARD $PARA");
            out1.print  (" $SCRD"+fisep+"passx rovbas P"+low_polnb+" N"+cur_low_nbvs);
            if(cur_low_nbvs < 10) {
                out1.print  (" ");
            }
            out1.print  (" D");
            for(int j=0; j<low_polnb+1; j++) {
                out1.print  (lowpolyad.getVdvo(j));
            }
            if( low_polnb == 0 ) {
                out1.print  (" ");
            }
            out1.println(" $JPH");
            out1.print  (" $SCRD"+fisep+"passx hmatri P"+low_polnb+" N"+cur_low_nbvs);
            if(cur_low_nbvs < 10) {
                out1.print  (" ");
            }
            out1.print  (" D");
            for(int j=0; j<low_polnb+1; j++) {
                out1.print  (lowpolyad.getVdvo(j));
            }
            if( low_polnb == 0 ) {
                out1.print  (" ");
            }
            out1.println(" $JPH");
            out1.print  (" $SCRD"+fisep+"passx hdi    P"+low_polnb+" N"+cur_low_nbvs);
            if(cur_low_nbvs < 10) {
                out1.print  (" ");
            }
            out1.print  (" D");
            for(int j=0; j<low_polnb+1; j++) {
                out1.print  (lowpolyad.getVdvo(j));
            }
            if( low_polnb == 0 ) {
                out1.print  (" ");
            }
            out1.println(" $JPH $PARA");
            out1.println("##");
            out1.println("## Polarizability matrix elements.");
            out1.println("##");
            out1.print  (" $SCRD"+fisep+"passx polmod P"+low_polnb);
            lowpolyad.ecrPol(out1, false);
            out1.println(" \\");
            out1.print  ("                    P"+low_polnb);
            lowpolyad.ecrPol(out1, false);
            out1.println(" \\");
            out1.print  ("                    D");
            for(int j=0; j<low_polnb+1; j++) {
                out1.print  (lowpolyad.getVpdvo(j));
            }
            if( npack.equals("D2hTDS") ) {
                out1.println(" \\"+lnsep+
                             "                    "+nrep);
            }
            else {
                out1.println("");
            }
            out1.print  (" $SCRD"+fisep+"passx parchk P"+low_polnb+"      D");
            for(int j=0; j<low_polnb+1; j++) {
                out1.print  (lowpolyad.getVdvo(j));
            }
            out1.print  (" P"+low_polnb+"     D");
            for(int j=0; j<low_polnb+1; j++) {
                out1.print  (lowpolyad.getVpdvo(j));
            }
            out1.println(" pol    $PARD  $PARP");
            out1.print  (" $SCRD"+fisep+"passx polmat P"+low_polnb+" N"+cur_upp_nbvs);
            if(cur_upp_nbvs < 10) {
                out1.print  (" ");
            }
            for(int j=0; j<upp_polnb+3; j++) {
                out1.print  (" ");
            }
            out1.print  (" P"+low_polnb+" N"+cur_low_nbvs);
            if(cur_low_nbvs < 10) {
                out1.print  (" ");
            }
            out1.print  (" D");
            for(int j=0; j<low_polnb+1; j++) {
                out1.print  (lowpolyad.getVpdvo(j));
            }
            out1.println(" $JPPol");
            out1.print  (" $SCRD"+fisep+"passx smatri P"+low_polnb+" N"+cur_upp_nbvs);
            if(cur_upp_nbvs < 10) {
                out1.print  (" ");
            }
            for(int j=0; j<upp_polnb+3; j++) {
                out1.print  (" ");
            }
            out1.print  (" P"+low_polnb+" N"+cur_low_nbvs);
            if(cur_low_nbvs < 10) {
                out1.print  (" ");
            }
            out1.print  (" D");
            for(int j=0; j<low_polnb+1; j++) {
                out1.print  (lowpolyad.getVpdvo(j));
            }
            out1.println(" $JPPol $PARP");
            out1.print  (" $SCRD"+fisep+"passx sdi    P"+low_polnb+"   ");
            if(cur_upp_nbvs < 10) {
                out1.print  (" ");
            }
            for(int j=0; j<upp_polnb+3; j++) {
                out1.print  (" ");
            }
            out1.print  ("      ");
            if(cur_low_nbvs < 10) {
                out1.print  (" ");
            }
            out1.print  ("  ");
            for(int j=0; j<low_polnb+1; j++) {
                out1.print  (" ");
            }
            out1.println(" $JPPol $MSt   $Elec");
            out1.println("##");
            // Upper Polyad
            if( dif_polnb != 0 ) {
                out1.println("##");
                out1.println("## Upper Polyad.");
                out1.println("##");
                out1.println("## Zero-field Hamiltonian matrix elements.");
                out1.println("##");
                out1.print  (" $SCRD"+fisep+"passx hmodel P"+upp_polnb);
                upppolyad.ecrPol(out1, true);
                if( npack.equals("D2hTDS") ) {
                    out1.println(" \\"+lnsep+
                                 "                    "+nrep);
                }
                else {
                    out1.println("");
                }
                out1.print  (" $SCRD"+fisep+"passx parchk P"+upp_polnb+"     D");
                for(int j=0; j<upp_polnb+1; j++) {
                    out1.print  (upppolyad.getVdvo(j));
                }
                out1.println(" $PARD $PARA");
                out1.print  (" $SCRD"+fisep+"passx rovbas P"+upp_polnb+" N"+cur_upp_nbvs);
                if(cur_upp_nbvs < 10) {
                    out1.print  (" ");
                }
                out1.print  (" D");
                for(int j=0; j<upp_polnb+1; j++) {
                    out1.print  (upppolyad.getVdvo(j));
                }
                out1.println(" $JPH");
                out1.print  (" $SCRD"+fisep+"passx hmatri P"+upp_polnb+" N"+cur_upp_nbvs);
                if(cur_upp_nbvs < 10) {
                    out1.print  (" ");
                }
                out1.print  (" D");
                for(int j=0; j<upp_polnb+1; j++) {
                    out1.print  (upppolyad.getVdvo(j));
                }
                out1.println(" $JPH");
                out1.print  (" $SCRD"+fisep+"passx hdi    P"+upp_polnb+" N"+cur_upp_nbvs);
                if(cur_upp_nbvs < 10) {
                    out1.print  (" ");
                }
                out1.print  (" D");
                for(int j=0; j<upp_polnb+1; j++) {
                    out1.print  (upppolyad.getVdvo(j));
                }
                out1.println(" $JPH $PARA");
                out1.println("##");
                out1.println("## Polarizability matrix elements.");
                out1.println("##");
                out1.print  (" $SCRD"+fisep+"passx polmod P"+upp_polnb);
                upppolyad.ecrPol(out1, false);
                out1.println(" \\");
                out1.print  ("                    P"+upp_polnb);
                upppolyad.ecrPol(out1, false);
                out1.println(" \\");
                out1.print  ("                    D");
                for(int j=0; j<upp_polnb+1; j++) {
                    out1.print  (upppolyad.getVpdvo(j));
                }
                if( npack.equals("D2hTDS") ) {
                    out1.println(" \\"+lnsep+
                                 "                    "+nrep);
                }
                else {
                    out1.println("");
                }
                out1.print  (" $SCRD"+fisep+"passx parchk P"+upp_polnb+"     D");
                for(int j=0; j<upp_polnb+1; j++) {
                    out1.print  (upppolyad.getVdvo(j));
                }
                out1.print  (" P"+upp_polnb+"     D");
                for(int j=0; j<upp_polnb+1; j++) {
                    out1.print  (upppolyad.getVpdvo(j));
                }
                out1.println(" pol    $PARD  $PARP");
                out1.print  (" $SCRD"+fisep+"passx polmat P"+upp_polnb+" N"+cur_upp_nbvs);
                if(cur_upp_nbvs < 10) {
                    out1.print  (" ");
                }
                for(int j=0; j<upp_polnb+3; j++) {
                    out1.print  (" ");
                }
                out1.print  (" P"+upp_polnb+" N"+cur_upp_nbvs);
                if(cur_upp_nbvs < 10) {
                    out1.print  (" ");
                }
                out1.print  (" D");
                for(int j=0; j<upp_polnb+1; j++) {
                    out1.print  (upppolyad.getVpdvo(j));
                }
                out1.println(" $JPPol");
                out1.print  (" $SCRD"+fisep+"passx smatri P"+upp_polnb+" N"+cur_upp_nbvs);
                if(cur_upp_nbvs < 10) {
                    out1.print  (" ");
                }
                for(int j=0; j<upp_polnb+3; j++) {
                    out1.print  (" ");
                }
                out1.print  (" P"+upp_polnb+" N"+cur_upp_nbvs);
                if(cur_upp_nbvs < 10) {
                    out1.print  (" ");
                }
                out1.print  (" D");
                for(int j=0; j<upp_polnb+1; j++) {
                    out1.print  (upppolyad.getVpdvo(j));
                }
                out1.println(" $JPPol $PARP");
                out1.print  (" $SCRD"+fisep+"passx sdi    P"+upp_polnb+"   ");
                if(cur_upp_nbvs < 10) {
                    out1.print  (" ");
                }
                for(int j=0; j<upp_polnb+3; j++) {
                    out1.print  (" ");
                }
                out1.print  ("      ");
                if(cur_upp_nbvs < 10) {
                    out1.print  (" ");
                }
                out1.print  ("  ");
                for(int j=0; j<upp_polnb+1; j++) {
                    out1.print  (" ");
                }
                out1.println(" $JPPol $MSt   $Elec");
                out1.println("##");
            }
            // Dipole moment
            out1.println("## Upper - lower level transition.");
            out1.println("##");
            out1.print  (" $SCRD"+fisep+"passx dipmod P"+upp_polnb);
            upppolyad.ecrPol(out1, false);
            out1.println(" \\");
            out1.print  ("                    P"+low_polnb);
            lowpolyad.ecrPol(out1, false);
            out1.println(" \\");
            out1.print  ("                    D");
            for(int j=0; j<low_polnb+1; j++) {
                out1.print  (trm_vdvo[j]);
            }
            if( npack.equals("D2hTDS") ) {
                out1.println(" \\"+lnsep+
                             "                    "+nrep);
            }
            else {
                out1.println("");
            }
            out1.print  (" $SCRD"+fisep+"passx parchk P"+upp_polnb+"     D");
            for(int j=0; j<upp_polnb+1; j++) {
                out1.print  (upppolyad.getVdvo(j));
            }
            out1.print  (" P"+low_polnb+"     D");
            for(int j=0; j<low_polnb+1; j++) {
                out1.print  (trm_vdvo[j]);
            }
            out1.println(" dip  $PARD $PARA");
            out1.print  (" $SCRD"+fisep+"passx dipmats P"+upp_polnb+" N"+cur_upp_nbvs);
            if(cur_upp_nbvs < 10) {
                out1.print  (" ");
            }
            for(int j=0; j<upp_polnb+2; j++) {
                out1.print  (" ");
            }
            out1.print  (" P"+low_polnb+" N"+cur_low_nbvs);
            if(cur_low_nbvs < 10) {
                out1.print  (" ");
            }
            out1.print  (" D");
            for(int j=0; j<low_polnb+1; j++) {
                out1.print  (trm_vdvo[j]);
            }
            out1.println(" $MSt $PARA");
            out1.println("##");
            // Spectrum
            out1.println("## Spectrum calculation.");
            out1.println("##");
            out1.print  (" $SCRD"+fisep+"passx spects ");
            out1.print  (" P"+upp_polnb+" N"+cur_upp_nbvs);
            if(cur_upp_nbvs < 10) {
                out1.print  (" ");
            }
            for(int j=0; j<upp_polnb+2; j++) {
                out1.print  (" ");
            }
            out1.print  (" P"+low_polnb+" N"+cur_low_nbvs);
            if(cur_low_nbvs < 10) {
                out1.print  (" ");
            }
            out1.print  (" D");
            for(int j=0; j<low_polnb+1; j++) {
                out1.print  (trm_vdvo[j]);
            }
            out1.println(" $MSt \\");
            out1.print  ("                     "+vfmin+" "+vfmax+" "+vtvib+" "+vtrot+" "+vrinmi);
            if( jrbfpvib.isSelected() ||
                jrbabund.isSelected()    ) {
                out1.print  (" \\"+lnsep+
                             "                    ");
                if( jrbfpvib.isSelected() ) {
                    out1.print  (" fpvib "+vfpvib);
                }
                if( jrbabund.isSelected() ) {
                    out1.print  (" abund "+vabund);
                }
            }
            out1.println("");
            out1.println("##");
            out1.println(" \\rm DIS* EN_* FN_* HA_* MD_* ME_* MH_* MP_* PO_* VP_* VPS_*");
        }
        catch (IOException ioe) {                                    // IO error
            JOptionPane.showMessageDialog(null,"IO error while writing file"+lnsep+
                                               njobf                        +lnsep+
                                               ioe);
            return;
        }
        finally {
            if( out1 != null ) {
                out1.close();
                if( out1.checkError() ) {
                    JOptionPane.showMessageDialog(null,"PrintWriter error while creating file"+lnsep+
                                                       njobf);
                    return;
                }
            }
        }
        JOptionPane.showMessageDialog(null,"The job "+njobf+" has been created");
        try {
            Process monproc = Runtime.getRuntime().exec("chmod u+x "+njobf);  // allow execution
        }
        catch (IOException ioe) {                                    // rights modification error
            JOptionPane.showMessageDialog(null,"IO error while changing rights of file"+lnsep+
                                               njobf                                   +lnsep+
                                               ioe);
            return;
        }
    }

/////////////////////////////////////////////////////////////////////

    // check Basics panel
    private boolean testBasics() {

        boolean askbasics = false;
        //
        if(nmol == "") {
            askbasics = true;                                        // molecule
        }
        //
        if( npack.equals("D2hTDS") ) {
            nrep = (String) jcbrep.getSelectedItem();
            if(nrep == "") {
                askbasics = true;                                    // representation
            }
        }
        //
        nhjmax = (String) jcbhjmax.getSelectedItem();
        if(nhjmax == "") {
            askbasics = true;                                        // jmax
        }
        else {
            hjmax = Double.valueOf(nhjmax);
            jppol = hjmax-2;
            if( jppol < 0 ) {
                JOptionPane.showMessageDialog(null,"Hamiltonian jmax > 2 requested");
                jcbhjmax.setSelectedItem("");
                return false;
            }
        }
        //
        nsmmax = (String) jcbsmmax.getSelectedItem();
        if(nsmmax == "") {
            askbasics = true;                                        // jmax
        }
        else {
            smmax = Double.valueOf(nsmmax);
            if( smmax > jppol ) {
                JOptionPane.showMessageDialog(null,"Polarizability Mmax <= Hamiltonian Jmax-2 requested");
                jcbsmmax.setSelectedItem("");
                return false;
            }
        }
        //
        velec =  ((Number)jftfelec.getValue()).floatValue();         // elec
        if( velec <= 0 ) {
            JOptionPane.showMessageDialog(null,"Electric Field >= 0 requested");
            jftfelec.setValue(new Float(0.0));
            return false;
        }
        //
        if(nhparaf == "") {
            askbasics = true;                                        // hamiltonian    parafile name
        }
        //
        if(npparaf == "") {
            askbasics = true;                                        // polarizability parafile name
        }
        //
        if( ! testPsc()) {
            askbasics = true;                                        // psc
        }
        if( askbasics ) {                                            // ask to fully define Basics
            JOptionPane.showMessageDialog(null,"You have to first fully define BASICS specifications");
            return false;
        }
        return true;
    }

    // check polyad scheme coefficients
    private boolean testPsc() {

        for (int i=0; i<nbvqn; i++) {
            npsc = (String) jcbpsc[i].getSelectedItem();
            if (npsc == "") {
                return false;
            }
        }
        return true;
    }

    // check transition moment
    private boolean checkTrm() {                                     // transition moment

        vfmin =  ((Number)jftffmin.getValue()).floatValue();         // fmin
        if( vfmin < 0 ) {
            JOptionPane.showMessageDialog(null,"FMIN >= 0 requested");
            jftffmin.setValue(new Float(0.0));
            return false;
        }
        vfmax =  ((Number)jftffmax.getValue()).floatValue();         // fmax
        if(vfmax <= vfmin) {
            JOptionPane.showMessageDialog(null,"FMAX > FMIN requested");
            return false;
        }
        vtvib =  ((Number)jftftvib.getValue()).floatValue();         // tvib
        if(vtvib <= 0) {
            JOptionPane.showMessageDialog(null,"TVIB > 0 requested");
            jftftvib.setValue(new Float(0.0));
            return false;
        }
        vtrot =  ((Number)jftftrot.getValue()).floatValue();         // trot
        if(vtrot <= 0) {
            JOptionPane.showMessageDialog(null,"TROT > 0 requested");
            jftftrot.setValue(new Float(0.0));
            return false;
        }
        try {
            vrinmi =  Float.parseFloat(jtfrinmi.getText());          // rinmi
        }
        catch(NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null,"RINMI = "+jtfrinmi.getText()+lnsep+
                                               "is not valid !"             +lnsep+
                                               nfe);
            jtfrinmi.setText("");
            return false;
        }
        if(vrinmi < 0) {
            JOptionPane.showMessageDialog(null,"RINMI >= 0 requested");
            jtfrinmi.setText("");
            return false;
        }
        // fpvib
        if( jrbfpvib.isSelected() ) {
            try {
                vfpvib = Float.parseFloat(jtffpvib.getText());
            }
            catch(NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null,"fpvib = "+jtffpvib.getText()+lnsep+
                                               "is not valid !"             +lnsep+
                                               nfe);
            jtffpvib.setText("");
            return false;
            }
            if( vfpvib <= 0 ) {
                JOptionPane.showMessageDialog(null,"0 < fpvib    requested");
                jtffpvib.setText("");
                return false;
            }
        }
        // abund
        if( jrbabund.isSelected() ) {
            try {
                vabund = Float.parseFloat(jtfabund.getText());
            }
            catch(NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null,"abund = "+jtfabund.getText()+lnsep+
                                               "is not valid !"             +lnsep+
                                               nfe);
            jtfabund.setText("");
            return false;
            }
            if( vabund <= 0 ||
                vabund >  1    ) {
                JOptionPane.showMessageDialog(null,"0 < abund <= 1    requested");
                jtfabund.setText("");
                return false;
            }
        }
        for ( int i=0; i<low_polnb+1; i++) {                         // dvo
            trm_ndvo = (String) trm_jcbdvo[i].getSelectedItem();
            if(trm_ndvo == "" ) {
                JOptionPane.showMessageDialog(null,"You have to first fully define the transition moment developpment order");
                return false;
            }
            trm_vdvo[i] = Integer.parseInt(trm_ndvo);
        }
        return true;
    }

    // ask to fully define transition moment
    private void askTrm() {

        JOptionPane.showMessageDialog(null,"You have to first fully define the transition moment characteristics.");
        return;
    }

}
