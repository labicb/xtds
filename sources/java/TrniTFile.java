/*
 * Class for Ener[_assign]_Pn (trni) file
 */

import java.io.*;
import java.util.*;
import javax.swing.*;

/**
 * This class defines Ener[_assign]_Pn file.
 */
class TrniTFile {

    private String   name;                                           // file name
    private int      nbxy;                                           // nb of data
    private double[] x;                                              // X
    private double[] y;                                              // Y
    private int      nsv;                                            // nb of upper vibrational sublevels
    private int      nbop;                                           // nb of operators
    private int[][]  limcomp;                                        // component limits

    private boolean  headOK;                                         // header read
    private double   beta0;
    private double   gama0;
    private double   pi0;


    private String  str;                                             // to read file
    private File    fich;
    private BufferedReader br;
    private boolean eof;
    private String  lnsep;                                           // line separator

/////////////////////////////////////////////////////////////////////

    /**
     * Constructs a new TrniTFile.
     * <br> (levlst.f format)
     *
     * @param cname  name of the file
     */
    public TrniTFile(String cname) {

        name = cname;                                                // file name

        lnsep  = System.getProperty("line.separator");
        nbxy   = 0;                                                  // nb of points
        headOK = false;                                              // header NOT read
    }

/////////////////////////////////////////////////////////////////////


    // read Ener file header
    // and set nbop, beta0, gama0, pi0, nsv
    public boolean readHeader() {

        int nsvc  = -1;                                              // nb of sub-levels
        try{
            fich = new File(name);
            br   = new BufferedReader (new FileReader(fich));        // open
            int i = 0;                                               // current line #
            // set nbop
            while ((str = br.readLine()) != null) {                  // as long as there is something to read
                i++;
                if(i != 5 ) {
                    continue;                                        // go to the 5th line
                }
                try {
                    str = str+"    ";                                // extend up to at least 4 characters
                    nbop = Integer.parseInt(str.substring(0,4).trim());  // NBOP format I4
                }
                catch (NumberFormatException e) {                    // format error
                    JOptionPane.showMessageDialog(null,"NBOP format error in file"+lnsep+
                                                       fich.getName()             +lnsep+
                                                       str                        +lnsep+
                                                       e);
                    return false;
                }
                break;
            }
            // set beta0, gama0, pi0
            while ((str = br.readLine()) != null) {                  // as long as there is something to read
                i++;
                if(i != nbop+8 ) {
                    continue;                                        // go to the right line
                }
                StringTokenizer st = new StringTokenizer(str);       // separate strings
                if( st.countTokens() < 3 ) {                         // not enough tokens
                    JOptionPane.showMessageDialog(null,"Bad BETA0, GAMA0, PI0 line in file"+lnsep+
                                                       fich.getName()                      +lnsep+
                                                       str);
                    return false;
                }
                try {
                    beta0 = Double.parseDouble(st.nextToken());
                    gama0 = Double.parseDouble(st.nextToken());
                    pi0   = Double.parseDouble(st.nextToken());
                }
                catch (NumberFormatException e) {                    // format error
                    JOptionPane.showMessageDialog(null,"BETA0, GAMA0 or PI0 format error in file"+lnsep+
                                                       fich.getName()                            +lnsep+
                                                       str                                       +lnsep+
                                                       e);
                    return false;
                }
                break;
            }
            // set nsv
            if( (str = br.readLine()) == null ) {                    // read one line
                JOptionPane.showMessageDialog(null,"Unexpected EOF while reading file"+lnsep+
                                                   fich.getName());
                return false;
            }
            try {
                str = str+"    ";                                    // extend up to at least 4 characters
                nsv = Integer.parseInt(str.substring(0,4).trim());  // NSV format I4
            }
            catch (NumberFormatException e) {                        // format error
                JOptionPane.showMessageDialog(null,"NSV format error in file"+lnsep+
                                                   fich.getName()            +lnsep+
                                                   str                       +lnsep+
                                                   e);
                return false;
            }
        }
        catch(IOException ioe) {                                     // IO error
            JOptionPane.showMessageDialog(null,"IO error while reading file"+lnsep+
                                               fich.getName()               +lnsep+
                                               ioe);
            return false;
        }
        finally {                                                    // close
            if (br !=null) {
                try {
                    br.close();
                }
                catch (IOException ignored) {
                }
            }
        }
        return true;
    }

/////////////////////////////////////////////////////////////////////

    public int getNsv() {

        return nsv;
    }

    public int getNbop() {

        return nbop;

    }

    public double getBeta0() {

        return beta0;
    }

    public double getGama0() {

        return gama0;
    }

    public double getPi0() {

        return pi0;
    }

 }
