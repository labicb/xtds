/**
 * This interface allows JPPolyad to call the JPPEvent() method of the various CJPxxx.
 */
public interface CJPint {

    /**
     * Manages event coming from JPPolyad.
     *
     * @param curpol   concerned polyad
     * @param type     modification type ( 0 : polyad number, 1 : quanta limit, 2 : development order )
     * @param indice   index of the concerned element
     */
    void JPPEvent( Polyad curpol, int type, int indice );
}
