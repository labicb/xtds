/*
   Class for transition assignment extraction characteristics panel
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.text.*;
import java.util.*;

/**
 * This panel allows to define all the characteristics of an xasg extraction.
 */
public class JPXasg extends    JPanel
                    implements ActionListener {

    // panels
    private JPanel pouest;
    private JPanel pcentre;
    private JPanel jpopt1;
    private JPanel jpopt2;

    //
    private JFXasg  calling;                                         // calling JFXasg
    private int     iasg;                                            // asg nb from the calling JFXasg
    private String  pmp;                                             // title  of   the calling JFXasg
    private String  npack;                                           // package  name
    private String  nmol;                                            // molecule name
    private boolean showtemp;                                        // show temperature choice

    //
    private JButton             jbasgf;                              // ASG file jb
    private String              nasgf;                               // ASG file name
    private JLabel              lasgf;                               // ASG file jl
    private JTextField          jtfselstr;                           // selection string jtf
    private String              nselstr;                             // selection string in ASG file
    private JRadioButton        jrbunit;                             // unit  jrb
    private JRadioButton        jrbMHz;                              // MHz   jrb
    private JRadioButton        jrbGHz;                              // GHz   jrb
    private JRadioButton        jrbdum;                              // dummy jrb
    private ButtonGroup         bgunit;                              // unit  group
    private JRadioButton        jrbtemp;                             // trot jrb
    private JFormattedTextField jftftemp;                            // trot jtf
    private float               vtemp;                               // trot value
    private JRadioButton        jrbfpvib;                            // partition function jrb
    private JTextField          jtffpvib;                            // partition function jtf
    private float               vfpvib;                              // partition function value
    private JRadioButton        jrbabund;                            // abundance jrb
    private JTextField          jtfabund;                            // abundance jtf
    private float               vabund;                              // abundance value

    private String playd;                                            // XTDS installation directory
    private String fisep;                                            // file separator
    // Number Format
    private NumberFormat nffloat;                                    // float   format

/////////////////////////////////////////////////////////////////////

    /**
     * Constructs a new JPXasg.
     */
    public JPXasg( JFXasg ccalling, int ciasg, String cpmp, String cnpack, String cnmol ) {

        calling  = ccalling;                                         // calling JFXasg
        iasg     = ciasg;                                            // asg nb from the calling JFXasg
        pmp      = cpmp;                                             // title  of   the calling JFXasg
        npack    = cnpack;                                           // package  name
        nmol     = cnmol;                                            // molecule name
        showtemp = false;                                            // show temperature choice

        playd = System.getProperty("xtds.home");                     // XTDS installation directory
        fisep = System.getProperty("file.separator");
        setName("JPXasg");                                           // for help files
        setLayout(new BorderLayout());

        // catching formats
        nffloat = NumberFormat.getInstance(Locale.US);
        nffloat.setGroupingUsed(false);                              // ie. no thousands seperator
        nffloat.setMaximumFractionDigits(10);                        // ie. no more then 10 digits for fractionnal part

        // local panels
        pouest  = new JPanel(new GridLayout(4,0,5,5));
        pcentre = new JPanel(new GridLayout(4,0,5,5));

        // ASG file name
        jbasgf = new JButton("ASG File or Dir.");                    // ASG file button
        jbasgf.setBackground(Color.WHITE);
        jbasgf.addActionListener(this);
        pouest.add(jbasgf);
        nasgf = "";                                                  // ASG file name
        lasgf = new JLabel("");                                      // ASG file jl
        pcentre.add(lasgf);
        // selection string
        pouest.add(new JLabel(" Sel. String "));
        jtfselstr = new JTextField(6);                               // selection string jtf
        jtfselstr.setBackground(Color.WHITE);
        jtfselstr.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        pcentre.add(jtfselstr);
        nselstr = "";
        // options
        jpopt1 = new JPanel(new GridLayout(0,6,5,5));
        jpopt2 = new JPanel(new GridLayout(0,6,5,5));
        // unit
        jrbunit = new JRadioButton("Unit");                          // unit jrb
        jrbunit.setBackground(Color.WHITE);
        jrbunit.addActionListener(this);
        jrbMHz  = new JRadioButton("MHz");                           // MHz  jrb
        jrbMHz.setEnabled(false);
        jrbMHz.addActionListener(this);
        jrbGHz  = new JRadioButton("GHz");                           // GHz  jrb
        jrbGHz.setEnabled(false);
        jrbGHz.addActionListener(this);
        jrbdum  = new JRadioButton();                                // dummy jrb
        bgunit  = new ButtonGroup();                                   // unit group
        bgunit.add(jrbMHz);
        bgunit.add(jrbGHz);
        bgunit.add(jrbdum);
        jpopt1.add(jrbunit);
        jpopt1.add(jrbMHz);
        jpopt1.add(jrbGHz);
        // Temperature
        jrbtemp = new JRadioButton("Temperature");
        jrbtemp.setEnabled(false);
        jrbtemp.setBackground(this.getBackground());
        jrbtemp.addActionListener(this);
        jftftemp  = new JFormattedTextField(nffloat);                // trot jtf
        jftftemp.setValue(new Float(0.0));
        jftftemp.setEnabled(false);
        jftftemp.setBackground(this.getBackground());
        jpopt2.add(jrbtemp);
        jpopt2.add(jftftemp);
        jrbfpvib  = new JRadioButton("fpvib");
        jrbfpvib.setEnabled(false);
        jrbfpvib.setBackground(this.getBackground());
        jrbfpvib.addActionListener(this);
        jtffpvib  = new JTextField("");
        jtffpvib.setEnabled(false);
        jtffpvib.setBackground(this.getBackground());
        jrbabund  = new JRadioButton("abund");
        jrbabund.setEnabled(false);
        jrbabund.setBackground(this.getBackground());
        jrbabund.addActionListener(this);
        jtfabund  = new JTextField("");
        jtfabund.setEnabled(false);
        jtfabund.setBackground(this.getBackground());
        jpopt2.add(jrbfpvib);
        jpopt2.add(jtffpvib);
        jpopt2.add(jrbabund);
        jpopt2.add(jtfabund);

        pcentre.add(jpopt1);
        pcentre.add(jpopt2);

        add(pouest ,"West"  );
        add(pcentre,"Center");
        setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),""+iasg));

    }

/////////////////////////////////////////////////////////////////////

    /**
     * Processes the events.
     */
    public void actionPerformed(ActionEvent evt) {

        // ASG file
        if(evt.getSource() == jbasgf) {
            JFileChooser jfcasgf = new JFileChooser(playd+fisep+"packages"+fisep+npack+fisep+"exp"+fisep+nmol);  // default choice directory
            jfcasgf.setSize(400,300);
            jfcasgf.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);  // files and directories
            jfcasgf.setDialogTitle("Define the assignment file or directory to be used");
            Container parent = jbasgf.getParent();
            int choice = jfcasgf.showDialog(parent,"Select");        // Dialog, Select
            if (choice == JFileChooser.APPROVE_OPTION) {
                nasgf = jfcasgf.getSelectedFile().getAbsolutePath();
                lasgf.setText(nasgf);
                calling.pack();                                      // for long names
            }
            return;
        }

        // unit
        if(evt.getSource() == jrbunit ) {
            if( jrbunit.isSelected() ) {
                jrbMHz.setEnabled(true);
                jrbMHz.setBackground(Color.WHITE);
                jrbGHz.setEnabled(true);
                jrbGHz.setBackground(Color.WHITE);
            }
            else {
                jrbdum.setSelected(true);
                jrbMHz.setEnabled(false);
                jrbMHz.setBackground(this.getBackground());
                jrbGHz.setEnabled(false);
                jrbGHz.setBackground(this.getBackground());
            }
            return;
        }

        // temp
        if(evt.getSource() == jrbtemp ) {
            if( jrbtemp.isSelected() ) {
                jftftemp.setEnabled(true);
                jftftemp.setBackground(Color.WHITE);
                jrbfpvib.setEnabled(true);
                jrbfpvib.setBackground(Color.WHITE);
                jrbabund.setEnabled(true);
                jrbabund.setBackground(Color.WHITE);
            }
            else {
                jftftemp.setValue(new Float(0.0));
                jftftemp.setEnabled(false);
                jftftemp.setBackground(this.getBackground());
                if( jrbfpvib.isSelected() ) {
                    jrbfpvib.doClick();
                }
                jrbfpvib.setEnabled(false);
                jrbfpvib.setBackground(this.getBackground());
                if( jrbabund.isSelected() ) {
                    jrbabund.doClick();
                }
                jrbabund.setEnabled(false);
                jrbabund.setBackground(this.getBackground());
            }
        }

        // fpvib
        if(evt.getSource() == jrbfpvib ) {
            if( jrbfpvib.isSelected() ) {
                jtffpvib.setEnabled(true);
                jtffpvib.setBackground(Color.WHITE);
            }
            else {
                jtffpvib.setText("");
                jtffpvib.setEnabled(false);
                jtffpvib.setBackground(this.getBackground());
            }
        }

        // abund
        if(evt.getSource() == jrbabund ) {
            if( jrbabund.isSelected() ) {
                jtfabund.setEnabled(true);
                jtfabund.setBackground(Color.WHITE);
            }
            else {
                jtfabund.setText("");
                jtfabund.setEnabled(false);
                jtfabund.setBackground(this.getBackground());
            }
        }
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Test criteria
     */
    public boolean test() {

        if( nasgf == "" ) {
            JOptionPane.showMessageDialog(null,"You have to define the ASG File or Dir. of #"+iasg+" of "+pmp+" extraction");
            return false;
        }
        nselstr = jtfselstr.getText();
        if( nselstr.equals("") ) {
            JOptionPane.showMessageDialog(null,"You have to define the Sel. String of #"+iasg+" of "+pmp+" extraction");
            return false;
        }
        if( jrbunit.isSelected()        &&
            (! jrbMHz.isSelected() &&
             ! jrbGHz.isSelected()    )    ) {
            JOptionPane.showMessageDialog(null,"You have to complete Unit option of #"+iasg+" of "+pmp+" extraction");
            return false;
        }
        if( jrbtemp.isSelected() ) {
            vtemp = ((Number)jftftemp.getValue()).floatValue();
            if( vtemp <= 0. ) {
                JOptionPane.showMessageDialog(null,"0. < Temperature    requested for #"+iasg+" of "+pmp+" extraction");
                jftftemp.setValue(new Float(0.0));
                return false;
            }
            // fpvib
            if( jrbfpvib.isSelected() ) {
                try {
                    vfpvib = Float.parseFloat(jtffpvib.getText());
                }
                catch(NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(null,"fpvib = \""+jtffpvib.getText()+"\" is not valid !");
                    jtffpvib.setText("");
                    return false;
                }
                if( vfpvib <= 0. ) {
                    JOptionPane.showMessageDialog(null,"0. < fpvib    requested for #"+iasg+" of "+pmp+" extraction");
                    jtffpvib.setText("");
                    return false;
                }
            }
            // abund
            if( jrbabund.isSelected() ) {
                try {
                    vabund = Float.parseFloat(jtfabund.getText());
                }
                catch(NumberFormatException nfe) {
                JOptionPane.showMessageDialog(null,"abund = \""+jtfabund.getText()+"\" is not valid !");
                jtfabund.setText("");
                return false;
                }
                if( vabund <= 0. ||
                    vabund >  1.    ) {
                    JOptionPane.showMessageDialog(null,"0. < abund <= 1.    requested for #"+iasg+" of "+pmp+" extraction");
                    jtfabund.setText("");
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * get ASG file
     */
    public String getASGfile() {

      return nasgf;
    }

    /**
     * get Sel. String
     */
    public String getSelstr() {

      return nselstr;
    }

    /**
     * is jrbMHz selected?
     */
    public boolean isMHzSelected() {

        return jrbMHz.isSelected();
    }

    /**
     * is jrbGHz selected?
     */
    public boolean isGHzSelected() {

        return jrbGHz.isSelected();
    }

    /**
     * is temp selected?
     */
    public boolean isTempSelected() {

        return jrbtemp.isSelected();
    }

    /**
     * get Temperature
     */
    public float getTemp() {

        if( showtemp ) {
            vtemp = ((Number)jftftemp.getValue()).floatValue();
            return vtemp ;
        }
        else {
            return (float) 0.;
        }
    }

    /**
     * is fpvib selected?
     */
    public boolean isFpvibSelected() {

        return jrbfpvib.isSelected();
    }

    /**
     * get fpvib
     */
    public float getFpvib() {

        if( jrbfpvib.isSelected() ) {
            try {
                vfpvib = Float.parseFloat(jtffpvib.getText());
            }
            catch(NumberFormatException nfe) {
                    return (float) 0.;
            }
            return vfpvib;
        }
        else {
            return (float) 0.;
        }
    }

    /**
     * is abund selected?
     */
    public boolean isAbundSelected() {

        return jrbabund.isSelected();
    }

    /**
     * get abund
     */
    public float getAbund() {

        if( jrbabund.isSelected() ) {
            try {
                vabund = Float.parseFloat(jtfabund.getText());
            }
            catch(NumberFormatException nfe) {
                    return (float) 0.;
            }
            return vabund;
        }
        else {
            return (float) 0.;
        }
    }

    /**
     * show Temperature choice?
     */
    public void setShowTemp( boolean cshow ) {

        showtemp = cshow;
        jrbtemp.setSelected(false);
        jftftemp.setValue(new Float(0.0));
        jftftemp.setEnabled(false);
        jftftemp.setBackground(this.getBackground());
        jrbfpvib.setSelected(false);
        jrbfpvib.setBackground(this.getBackground());
        jtffpvib.setText("");
        jtffpvib.setBackground(this.getBackground());
        jrbabund.setSelected(false);
        jrbabund.setBackground(this.getBackground());
        jtfabund.setText("");
        jtfabund.setBackground(this.getBackground());
        //
        if( showtemp ) {
            jrbtemp.setEnabled(true);
            jrbtemp.setBackground(Color.WHITE);
        }
        else {
            jrbtemp.setEnabled(false);
            jrbtemp.setBackground(this.getBackground());
        }
        revalidate();
        return;
    }

}
